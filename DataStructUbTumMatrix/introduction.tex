%!TEX root = main.tex

\section{Introduction}
Suppose we have a range space $(\rangesp, P)$, where $\rangesp$ is a
family of subsets of the finite set $P$. The range searching problem,
which is of fundamental interest in data structures, asks to maintain
weights $w:P \to \ZZ$ on the elements of $P$, so
that, given a query range $\range \in \rangesp$, we can quickly
compute 
\[
w(\range) = \sum_{p \in \range}{w(p)}.
\]
In the static version of this problem, the weight function $w$ is
specified in advance, and the goal is to preprocess the weights into a
data structure taking at most $s$ words of space, so that each query can
be answered within time complexity $t_q$, for parameters $s$ and $t_q$
that are as small as possible. In the dynamic version, which we study
here, all the weights are initialized to $0$, and the data structure
is required to support an $\textsc{Update}(p,x)$ and a
$\textsc{Query}(\range)$ procedure. The $\textsc{Update}(p,x)$
procedure adds $x \in \ZZ$ to the weight of $p$; the $\textsc{Query}(\range)$
procedure outputs $w(\range)$. The main trade-off in the dynamic case is
between the update time $t_u$, i.e.~the worst-case time complexity of
$\textsc{Update}(p,x)$, and the query time $t_q$, i.e.~the worst-case
time complexity of $\textsc{Query}(\range)$. 

Range searching is extensively studied in computational geometry,
where typically the ground set $P$ is a set of $n$ points in $\RR^d$,
and $\rangesp$ is induced by a family of geometric sets. For example,
some typical choices for $\rangesp$ are $\HH_d(P) = \{H \cap P: H \in
\HH_d\}$ and $\BB_d(P) = \{B \cap P: B \in \BB_d\}$, where $\HH_d$ is
the set of halfspaces in $\RR^d$, and $\BB_d$ is the set of
axis-aligned boxes in $\RR^d$. Then the range searching problem
captures, respectively, counting points inside halfspaces and inside
axis-aligned boxes. Other common choices for geometric range spaces
are those induced by simplices, by Euclidean balls, or more generally
by semi-algebraic sets. See
\cite{Matousek94}~and~\cite{AgarwalErickson96} for excellent surveys
on geometric range searching.


As a general principle, when we are in the geometric setting (i.e.~$P
\subset \RR^d)$, and $\rangesp$ is a range space induced by shapes
with smooth boundary, like balls, or by shapes whose boundaries can
have arbitrary orientation, like simplices or halfspaces, we expect
data structures with $t_u = \softOh(1)$ and $t_q = \softOh(n^{1-1/d})$
(see e.g.~\cite{Welzl88}).\footnote{The $\softOh(\cdot)$ notation
  hides factors polylogarithmic in $|P| = n$ and in $m = |\range|$.}
By contrast, when $\rangesp$ is induced by polyhedral sets whose
facets can only be parallel to a constant number of hyperplanes, as is
the case for axis-aligned boxes, we expect $t_u, t_q = O(\log^d n)$
(see~\cite{Bentley77}). This behavior is curiously reminiscent of
classical results in combinatorial discrepancy theory. The
combinatorial discrepancy of a range space $(\rangesp, P)$ is defined
by
\[
\disc(\rangesp) = \min_{\chi:P \to \{-1, +1\}}
\max_{\range \in \rangesp}
\left|\sum_{p \in \range}\chi(p)\right|.
\]
For any $n$-point set $P$, $\disc(\HH_d(P)) = O(n^{\frac{1}{2} -
  \frac{1}{2d}})$~\cite{Matousek-halfspaces}, and this is in general
tight\cite{alexander1990geometric,chazelle1995elementary}. Moreover,
the same result also holds for simplices, and, up to a logarithmic
factor, for balls in $\RR^d$. On the other hand, $\disc(\BB_d(P)) =
O(\log^{d-\frac12}n)$, and the same is true for any range space
induced by shifts and scalings of a fixed
polytope~\cite{Tusnady-ub}. Once again, this bound is tight up to a
$\sqrt{\log n}$ factor~\cite{MNT}. The analogy goes further than just
the growth rate of the bounds: many of the best upper bounds on the
discrepancy were proved using techniques developed for range searching
data structures. For example, nearly tight discrepancy bounds for
halfspaces, balls, and, in general, any range space with bounded dual
shatter function, follow from Welzl's spanning paths with low crossing
number~\cite{MatWW93}. The best, and also nearly tight, upper bounds
on the discrepancy of axis-aligned boxes are based on Bentley's range
trees, and Matou\v{s}ek used more advanced multi-level partition trees
to also prove results for other families of sets with product
structure~\cite{Matousek-cartesian}. This suggests that there may be a
deeper connection between range searching data structures, on one
hand, and discrepancy theory, on the other.

The strongest available evidence of a general connection between
discrepancy theory and range searching data structures is provided by
Larsen's lower bounds on range searching in the oblvious group
model~\cite{larsen2014range}. To state his result, let us take a short
detour to define the oblivious group model, originally introduced by
Fredman~\cite{Fredman82}. It is common to define the range searching
problem more generally, by allowing the weights to lie in a
commutative group $G$, or even in a commutative semi-group, and define
$w(\range) = \sum_{p \in \range}{w(p)}$ via addition in $G$.  This
allows capturing a number of different problems in the same framework:
different choices of a semigroup give the range emptiness problem, and
the range reporting problem, for example, in addition to the range
counting problem mentioned above. Moreover, many of the common data
structures for these problems do not use anything specific about $G$
and work for any group. Usually, they maintain some fixed set of
linear combination of the point weights, and answer queries by taking
linear combinations of the stored values. The oblivious group model
formalizes and abstracts this type of data structures. In it, a data
structure is specified by two integer matrices $\mm{Q} \in
\ZZ^{\rangesp \times [s]}$ and $\mm{D} \in \ZZ^{[s] \times P}$. It
stores the vector $\vv{y} = \mm{D}\vv{w}$, where $\vv{w}$ is defined
by $\vv{w}_p = w(p)$. The $\textsc{Update}(p,x)$ operation is
performed by adding $x\mm{D} \vv{e}_p$ to $\vv{y}$, where $\vv{e}_p$
is the the standard basis vector corresponding to $p$. The
$\textsc{Query}(\range)$ operation is performed by computing
$(\mm{Q}\vv{y})_{\range}$. For the data structure to be correct, it
must be the case that $\mm{M} = \mm{Q} \mm{D}$, where $\mm{M} \in
\{0,1\}^{\rangesp \times P}$ is the incidence matrix of $\rangesp$,
defined by
\[
\forall \range \in \rangesp, p \in P:\ \ \ 
{M}_{\range, p} = 
\begin{cases}
  1 & p \in \range\\
  0 & p \not \in \range
\end{cases}.
\]
In this model, the update time $t_u$ is equal to the maximum number of
non-zero entries in any column of $\mm{D}$, which we denote
$\nzcol(\mm{D})$, and the query time is equal to the maximum number of
non-zero entries in any row of $\mm{Q}$, which we denote
$\nzrow(\mm{Q})$. We can now state Larsen's lower bound.
\begin{theorem}[\cite{larsen2014range}]\label{thm:larsen}
  For any range space $(\rangesp, P)$, and any oblivious group model
  data structure for $\rangesp$ specified by matrices $\mm{Q}$ and
  $\mm{D}$, we have
  \[
  \sqrt{t_u t_q} = \sqrt{\nzrow(\mm{Q})\nzcol(\mm{D})}
  = 
  \Omega\left(\frac{1}{\Delta \sqrt{\log |\rangesp|}} \disc(\rangesp)\right),
  \]
  where $\Delta$ is the multiplicity of the data structure, equal to
  the largest absolute value of an entry in $\mm{Q}, \mm{D}$. 
\end{theorem}
Most known data structures for geometric range searching problems in
the oblivious group model have constant multiplicity, and for them
Theorem~\ref{thm:larsen} generally provides nearly tight lower
bounds. It also gives a partial explanation for why data structure
techniques have been useful in proving discrepancy upper bounds. It
does not, however, explain why these upper bounds usually turn out to
be tight. In this context, it is natural to ask whether
Theorem~\ref{thm:larsen} has a converse. We will propose such a
converse shortly, after introducing one more definition. We shall work
with a robust notion of discrepancy, called hereditary discrepancy,
and defined as 
\[
\herdisc(\rangesp) = \max_{Q \subseteq P} \disc(\rangesp|_Q),
\]
where $\rangesp|_Q$ is the restriction of $\rangesp$ to $Q$,
i.e.~$\rangesp|_Q = \{\range \cap Q: \range \in \rangesp\}$. Working
with a more robust notion like hereditary discrepancy is necessary, as
discrepancy can be made $0$ by relatively trivial changes to $\rangesp$
(e.g.~making two copies of every point) that otherwise do not change
the complexity of range searching. Moreover, since a data structure
for $\rangesp$ implies a data structure for $\rangesp|_Q$ with the
same parameters, Theorem~\ref{thm:larsen} can be also stated in terms
of hereditary discrepancy. Our proposed converse to
Theorem~\ref{thm:larsen} then is as follows. 
\begin{conjecture}\label{conj:rangesp}
  For every range space $(\rangesp, P)$ there exists a data structure
  in the oblivious group model, specified by matrices $\mm{Q}$ and
  $\mm{D}$, such that
  \[
  \sqrt{t_u t_q} = \sqrt{\nzrow(\mm{Q})\nzcol(\mm{D})}
  = \softOh(\herdisc(\rangesp)).
  \]
\end{conjecture}
It is common to extend the definition of discrepancy and hereditary
discrepancy to matrices: for any  $m \times n$ matrix $\mm{M}$, we define
$\disc(\mm{M}) = \min_{\vv{x} \in \{-1, +1\}^n}\|\mm{M}\vv{x}\|_\infty$,
and we define $\herdisc(\mm{M})$ as the maximum discrepancy of any
submatrix of $\mm{M}$. It is easy to verify that when $\mm{M}$ is the
incidence matrix of a range space $\rangesp$, the two definitions
agree. We also state a slightly more general conjecture for matrices. 
\begin{conjecture}\label{conj:matrices}
  For any $m \times n$ matrix $\mm{M}$ with entries in $\{-1, 0,
  +1\}$, there exist integer matrices $\mm{Q}$ and $\mm{D}$ such that
  $\mm{M} = \mm{Q} \mm{D}$, and 
  \[
  \sqrt{\nzrow(\mm{Q})\nzcol(\mm{D})} = \softOh(\herdisc(\mm{M})).
  \]
\end{conjecture}

The main conceptual contribution of the current paper is proposing
these conjectures, and initiating a systematic investigation of them.
Our main technical result is a proof of Conjecture~\ref{conj:matrices}
in the special case when $\herdisc(\mm{M}) = 1$. By the
Ghouila-Houri theorem~\cite{ghouila1962caracterisation}, the matrices
that satisfy this condition are exactly the totally unimodular (TU)
matrices, i.e.~matrices all of whose subdeterminants lie in $\{-1, 0,
+1\}$. Then, our result is as follows.
\begin{theorem}
  \label{thm:DataStructUBforTumMatrix}
  For any totally unimodular $m \times n$ matrix $\mm{M}$, there exist
  matrices $\mm{Q}$ and $\mm{D}$ with entries in $\{-1, 0, +1\}$, such
  that $\mm{M} = \mm{Q} \mm{D}$, and
  \begin{align*}
    &\nzrow(\mm{Q}) = O(\log(mn)),
    &\nzcol(\mm{D}) = O(\log(mn)).
  \end{align*}
\end{theorem}
Theorem~\ref{thm:DataStructUBforTumMatrix} is, in general, tight,
since the lower-triangular $n\times n$ matrix $\mm{M}$ with entries
equal to $0$ above the main diagonal, and equal to $1$ on or below it
(equivalently, the incidence matrix of prefix intervals on the line)
is totally unimodular, and Fredman has shown that it requires
$\nzrow(\mm{Q}) + \nzcol(\mm{D}) = \Omega(\log
n)$~\cite{Fredman82}. Theorem~\ref{thm:DataStructUBforTumMatrix} can
be seen as evidence for Conjecture~\ref{conj:matrices}. It also gives
a general, and well-behaved class of range spaces/matrices for which
there exist data structures with constant update and query
time. Special cases include interval queries, range spaces
corresponding to paths in a tree, and range spaces that can be derived
from them using operations that preserve total unimodularity.

We prove Theorem~\ref{thm:DataStructUBforTumMatrix} using Seymour's decomposition theorem
for regular matroids~\cite{seymour1980decomposition}, which implies a decomposition theorem for TU
matrices (see also~\cite{truemper1992matroid}). Seymour's theorem implies that any TU
matrix can be built up from network matrices (which correspond,
roughly, to incidence matrices of the ranges space of paths in a
tree), and a couple of special constant size matrices, using $1$-,
$2$-, and $3$-sum operations (defined later), as well as row
permutations and transposition. Our proof first shows that network
matrices support data structures with logarithmic $\nzrow(\mm{Q}),
\nzcol(\mm{D})$. We show that we can build a data structure for
the $k$-sum of two matrices $\mm{M}$ and $\mm{M}'$, $k = 1, 2, 3$,
from data structures for $\mm{M}$ and $\mm{M}'$, with only an additive
constant increase in the parameters of the data structure. Finally, we
show that the decomposition of any $m\times n$ TU matrix can be balanced so that,
when viewed as a binary tree, it has height only $O(\log(mn))$. These
facts, taken together, imply Theorem~\ref{thm:DataStructUBforTumMatrix}. 

Let us state one more, equivalent, version of
Conjecture~\ref{conj:matrices}, as it allows us to point out some
pleasant consequences of it. We recall that the $\gamma_2$ norm of a
matrix $\mm{M}$ is equal to 
\[
\gamma_2(\mm{M}) = \min\{\|\mm{Q}\|_{2\to \infty}\|\mm{D}\|_{1\to 2}: 
\mm{M} = \mm{Q}\mm{D}\},
\]
where $\|\mm{Q}\|_{2\to \infty}$ is the maximum $\ell_2$ norm of a row
of $\mm{Q}$, and $\|\mm{D}\|_{1\to 2}$ is the maximum $\ell_2$ norm of
a column of $\mm{D}$. Observe that it is immediate from the
definitions that, for any data structure for $\mm{M}$ given by
$\mm{Q}$ and $\mm{D}$ with multiplicity $\Delta$, $\gamma_2(\mm{M})
\le \Delta \sqrt{\nzrow(\mm{Q})\nzcol(\mm{D})}$.  It was shown in
\cite{MNT} that, for any $m\times n$ matrix
$\mm{M}$, $\gamma_2(\mm{M}) = O(\log(m)\herdisc(\mm{M}))$, and
$\herdisc(\mm{M}) = O(\sqrt{\log(m)}\gamma_2(\mm{M}))$.\footnote{The
  second inequality was also implicit in~\cite{larsen2014range}.} Therefore,
Conjecture~\ref{conj:matrices} is equivalent to
\begin{conjecture}\label{conj:gamma2}
  For any $m \times n$ matrix $\mm{M}$ with entries in $\{-1, 0,
  +1\}$, there exist integer matrices $\mm{Q}$ and $\mm{D}$ such that
  $\mm{M} = \mm{Q} \mm{D}$, and 
  \[
  \sqrt{\nzrow(\mm{Q})\nzcol(\mm{D})} = \softOh(\gamma_2(\mm{M})).
  \]
\end{conjecture}
The function $\gamma_2(\mm{M})$ is particularly well-behaved. It is,
indeed, a norm on matrices, i.e.~it satisfies the triangle
inequality. It is also multiplicative with respect to the Kronecker
product, and is efficiently computable by solving a semidefinite
program: see \cite{LeeSS08}~and~\cite{MNT} for proofs and
further properties. Conjecture~\ref{conj:gamma2} and the inequality
$\gamma_2(\mm{M})  \le \Delta \sqrt{\nzrow(\mm{Q})\nzcol(\mm{D})}$ then imply
that these properties also hold, approximately, for the geometric mean
of update and query time of an optimal data structure in the oblivious
group model. None of these properties is currently known to hold for
data structures.

Finally, we investigate gaps between update and query time of data
structures, on one hand, and hereditary discrepancy and the $\gamma_2$
norm, on the other. As mentioned already, there exists a range space
$\rangesp$ such that $\herdisc(\rangesp) = 1$, but any data structure
for $\rangesp$ given by $\mm{Q}$ and $\mm{D}$ satisfies
$\sqrt{\nzrow(\mm{Q})\nzcol(\mm{D})} = \Omega(\log n)$. In the other
direction, if we take $\rangesp$ to consist of all subsets of an
$n$-point set $P$, it is easy to show that $\disc(\rangesp) \ge
\frac{n}{2}$, while the trivial data structure given by setting
$\mm{Q}$ to be the incidence matrix of $\rangesp$, and $\mm{D}$ to be
the identity matrix gives $\sqrt{\nzrow(\mm{Q})\nzcol(\mm{D})} =
\sqrt{n}$. Therefore, the hereditary discrepancy can be larger than
$\sqrt{\nzrow(\mm{Q})\nzcol(\mm{D})}$ by a $\Omega(\sqrt{\log
  |\rangesp|})$ factor. As we observed above, for constant
multiplicity data structures, the $\gamma_2$ norm is at most a
constant factor larger than $\sqrt{\nzrow(\mm{Q})\nzcol(\mm{D})}$. As
our next result, we show that the $\gamma_2$ norm could be a
logarithmic factor smaller than $\sqrt{\nzrow(\mm{Q})\nzcol(\mm{D})}$.
\begin{theorem}\label{thm:gap}
  For any positive integer $r$, there exists a $(2^r + r) \times (2^r
  + r)$ matrix $\mm{M}$ with entries in $\{0,1\}$ such that
  $\gamma_2(\mm{M}) \le \sqrt{2r}$, while, any data structure
  for $\mm{M}$ given by $\mm{Q}$ and $\mm{D}$ satisfies 
  \begin{align*}
    &\nzrow(\mm{Q}) = \Omega\left(\frac{r}{\log r}\right),
    &\nzcol(\mm{D}) = \Omega\left(\frac{r}{\log r}\right).
  \end{align*}
\end{theorem}
In Section~??, we also show that if some lower bounds from the
oblivious semigroup model could be proved to hold in the oblivious
group model, they would imply the existence of a counterexample to
Conjecture~\ref{conj:rangesp}. For this reason, we suggest one final
conjecture, which has similarly intersting consequences as the conjectures
we made thus far. We are not aware of any possible counterexamples to
this final conjecture. 
\begin{conjecture}\label{conj:weak}
  For any $m \times n$ matrix $\mm{M}$ with entries in $\{-1, 0,
  +1\}$, there exist integer matrices $\mm{Q}$ and $\mm{D}$ such that
  $\mm{M} = \mm{Q} \mm{D}$, and 
  \begin{align*}
  &\nzrow(\mm{Q}) = \softOh(\gamma_2(\mm{M})),
  &\nzcol(\mm{D}) = \softOh(\gamma_2(\mm{M})).
  \end{align*}
\end{conjecture}
Clearly, Conjecture~\ref{conj:weak} implies the other conjectures up
to a quadratic loss. 