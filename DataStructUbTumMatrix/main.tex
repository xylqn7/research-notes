\documentclass[
11pt, 		% Default font size is 10pt, can alternatively be 11pt or 12pt
a4paper, 	% Alternatively letterpaper for US letter
onecolumn, 	% Alternatively twocolumn
% Alternatively landscape
]{article}
\usepackage{amsfonts, amsmath, amssymb, amsthm}
\usepackage[export]{adjustbox} %% USE: center align equation label next to picture
\usepackage{geometry}
	\geometry{margin=1in}
\usepackage{graphicx}
\usepackage{epstopdf}
	\epstopdfsetup{update}
\usepackage{color}
\usepackage{comment}
\usepackage{hyperref}
\usepackage{mathtools}

\usepackage{fixltx2e}
\usepackage{standalone}
\usepackage{tikz}
	
%% ===> Page Settings <===
\setlength{\parindent}{0em}
\setlength{\parskip}{0.5em}
%% ===> End Page Settings <===

%% ===> Theorem Type Definitions <===
\newtheorem{theorem}{Theorem}
\newtheorem{conjecture}{Conjecture}
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{claim}[theorem]{Claim}
\newtheorem{corollary}[theorem]{Corollary}
\newtheorem{definition}[theorem]{Definition}
%% ===> END Theorem Type Definitions <===

%% ===> Deliminaters <===
\DeclarePairedDelimiter\ceil{\lceil}{\rceil}
\DeclarePairedDelimiter\floor{\lfloor}{\rfloor}
\DeclarePairedDelimiter\anglebrac{\langle}{\rangle}
\DeclarePairedDelimiter\norm{\lVert}{\rVert}
%% ===> END Deliminaters <===

%% ===> Macros <===
\newcommand{\softOh}{\widetilde{O}}
\newcommand\nzcol{\kappa}
\newcommand\nzrow{\rho}
%% ---> Rings and Feilds <---
\newcommand\NN{\mathbb{N}}
\newcommand\ZZ{\mathbb{Z}}
\newcommand\QQ{\mathbb{Q}}
\newcommand\RR{\mathbb{R}}
\newcommand\FF{\mathcal{F}}
%% ---> Linear Algebra <---
\newcommand\rank{\mathrm{rank}}
\newcommand\mm[1]{\mathbf{#1}}	% Bolded capitals are MATRICES
\newcommand\vv[1]{\mathbf{#1}}	% Bolded lowercases are VECTORS
%% ---> Discrepancy <---
\newcommand\disc{\mathrm{disc}}
\newcommand\herdisc{\mathrm{herdisc}}
% ---> Range Spaces <---
\newcommand{\rangesp}{\mathcal{R}}
\newcommand{\range}{R}
\newcommand{\HH}{\mathcal{H}} % halfspaces
\newcommand{\BB}{\mathcal{B}} % axis-aligned boxes
%% ===> End Math Operators <===

% ===> Comments <===
\newcommand{\sn}[1]{\footnote{Sasho: {#1}}}
\newcommand{\xy}[1]{\footnote{Lily: {#1}}}
% ===> End Comments <===

% ===> Commands <===
% ---> usage: \fig{NAME}{SCALE}{CAPTION}
\newcommand\fig[3]{
	\begin{figure}[ht]
		\centering
		\includegraphics[scale=#2]{Figures/#1}
		\caption{#3}
		\label{fig:#1}
	\end{figure}
}

% ---> usage: \teq{NAME}{SCALE}
\newcommand\teq[2]{
	\begin{equation}
		\begin{aligned}
			\centering
			\includestandalone[scale=#2]{Figures/#1}
			\label{eq:tikz:#1}
		\end{aligned}
	\end{equation}
}

% ---> usage: \feq{NAME}{SCALE}
\newcommand\feq[2]{
	\includegraphics[valign=c,scale=#2]{Figures/#1}
}
% ===> End Commands <===

\title{On the Tightness of Discrepancy Lower Bounds for Range Searching}
\date{\today}
\author{XYL, AN}

\begin{document}
	%---------------------------
	%	PRINT ARTICLE INFO
	%---------------------------
	
	\maketitle

	\input{introduction}
	\input{preliminaries}

	\subsection{Regrouping $k$-Sums}
	Let $\mm{B}$ and $\mm{B}'$ be two matrices. $\mm{B}$ and $\mm{B}'$ are \emph{equivalent up to permutation}, denoted $\mm{B} \cong \mm{B}'$ if $\mm{B}'$ is identical to some row and column permutation of $\mm{B}$.   
	\begin{lemma}{(Regrouping $k$-Sums.)}
		\label{lem:SumOpAssociativity}	
		Let $\mm{B}_1$, $\mm{B}_2$, and $\mm{B}_3$ be three TUM matrices, $\vv{r}_1$, $\vv{r}_2$, and $\vv{r}_3$ the row and column indices of each of these. Then either
		\[\left((\mm{B}_1', \vv{r}_1) \oplus_i (\mm{B}_2, \vv{r}_2), \vv{r}\right) \oplus_j (\mm{B}_3, \vv{r}_3) \cong \left((\mm{B}_1, \vv{r}_1) \oplus_{i'} (\mm{B}_3, \vv{r}_3), \vv{r}'\right) \oplus_{j'} (\mm{B}_2, \vv{r}_2)\]
		or 
		\[\left((\mm{B}_1', \vv{r}_1) \oplus_i (\mm{B}_2, \vv{r}_2), \vv{r}\right) \oplus_j (\mm{B}_3, \vv{r}_3) \cong (\mm{B}_1, \vv{r}_1) \oplus_{i'} \left((\mm{B}_2, \vv{r}_2) \oplus_{j'} (\mm{B}_3, \vv{r}_3), \vv{r}'\right)\]
		where $\vv{r}$ and $\vv{r}'$ are row and column indices of their respective matrices and $\mm{B}_1'$ and $\mm{B}_2'$ are identical to $\mm{B}_1$ and $\mm{B}_2$ expect with the addition or removal of one row or column.
	\end{lemma} 
	\begin{proof}
		There are four choices for $\oplus_i$, namely $i = 1,2,3,3'$, and similarly four for $\oplus_j$. If either $\oplus_i$ or $\oplus_j$ is a $1$-sum, then it is easy to see that such regrouping is possible. When $i = 1$, $\vv{r}$ will correspond to the indices of one submatrix only, so it suffices to take the $j$-sum of this matrix with $\mm{B}_3$. If $j = 1$ then take the $1$-sum of either $\mm{B}_1$ or $\mm{B}_2$ with $\mm{B}_3$, then take the $i$-sum with the remaining matrix. Among the nine other combinations of $(i,j)$, we show $(2,2)$, $(2,3)$, $(3,3)$ since these are representative of the others.
		
		In the following we assume that none of the chosen rows or columns are identically zero. For a $k$-sum with $k = 2,3$, this situation is equivalent to using a $(k-1)$-sum.
		
		For $i = j = 2$, let the input matrices be as follows.
		\begin{equation}
			\label{eq:2SumAssocSetup}
			\feq{2SumAssocSetup}{0.6}
		\end{equation}
		We would like to regroup the operation 
		\[\left((\mm{B}_1, i_1) \oplus_{2} (\mm{B}_2, i_2), r\right) \oplus_{2} (\mm{B}_3, i_3)\] 
		where $r$ is some column of the matrix $(\mm{B}_1, i_1) \oplus_{2} (\mm{B}_2, i_2)$. In particular, a $2$-sum involving $(\mm{B}_3, i_3)$ should occur first.  The results differ depending on the index of column $r$; either it is among the columns of $Y_1$ in $\mm{B}_1$ or $Y_2$ of $\mm{B}_2$. The former case is shown in Equation \eqref{eq:2S-base1}. 
		\begin{align}
			\feq{2S-base1}{0.5} \label{eq:2S-base1}\\
			\feq{2S-result1}{0.5} \label{eq:2S-result1}
		\end{align}
		Equation $\eqref{eq:2S-result1}$ shows a different order of the $2$-sum operations that produces a matrix \emph{equivalent up to permutation} with $\left((\mm{B}_1, i_1) \oplus_{2} (\mm{B}_2, i_2), r_1\right) \oplus_{2} (\mm{B}_3, i_3)$. Thus
		\[\left((\mm{B}_1, i_1) \oplus_{2} (\mm{B}_2, i_2), r_1\right) \oplus_2 (\mm{B}_3, i_3 ) \cong \left((\mm{B}_1,r_1) \oplus_2 (\mm{B}_3, i_3), i_1\right) \oplus_2(\mm{B}_2, i_2).\]
		The latter case is shown in Equation \eqref{eq:2S-base2}. 
		\begin{align}
			\feq{2S-base2}{0.5} \label{eq:2S-base2}\\
			\feq{2S-result2}{0.5} \label{eq:2S-result2}
		\end{align}
		Equation \eqref{eq:2S-result2} shows a matrix which is \emph{equivalent up to permutation}. Thus
		\[\left((\mm{B}_1, i_1) \oplus_{2} (\mm{B}_2, i_2), r_2\right) \oplus_2 (\mm{B}_3, i_3 ) \cong (\mm{B}_1, i_1) \oplus_2 \left((\mm{B}_2,r_2) \oplus_2 (\mm{B}_3, i_3), i_2\right).\]
		
		Next $i = 2$, $j = 3$. In Equation \eqref{eq:23SumSetup}, the input matrices. 
		\begin{equation}
			\label{eq:23SumSetup}
			\feq{23SumSetup}{0.5}
		\end{equation}
		Reorder the $k$-sums in 
		\[\left((\mm{B}_1,i_1) \oplus_{2} (\mm{B}_2, i_2),r,s,t\right) \oplus_{3} (\mm{B}_3, i_3, j_3, k_3)\] 
		so that an operation which involves $\mm{B}_3$ occurs first. There are four cases to consider depending on the column indices $r$, $s$ and the row index $t$.  See Equation \eqref{eq:23S-base}.
		\begin{equation}
			\label{eq:23S-base}
			\feq{23S-base}{0.5}
		\end{equation}
		\begin{enumerate}
			\item $r = r_1$, $s = s_1$, $t = t_1$ ($r, s \in Y_1$ and $t \in X_1$): The result of the $2$-sum followed by the $3$-sum is shown in Equation \eqref{eq:23S-base1}.
			\begin{equation}
				\label{eq:23S-base1}
				\feq{23S-base1}{0.5}
			\end{equation} 
			Since $t_1$ was the chosen row for $(\mm{B}_1, i_1) \oplus_2 (\mm{B}_2,i_2)$, we know that $\vv{u}_{t_1}^{\top} = \vv{0}$, $\alpha_{r_1} = \alpha_{s_1} = 1$, $\alpha_{i_1}\alpha_{r_2} = \alpha_{i_1}\alpha_{s_2} = 0$, and $\alpha_{i_1}\vv{u}_{i_2}^{\top} = \vv{0}$. Since we assumed that none of the chosen rows or columns are identically zero, we must have $\alpha_{i_1} = 0$. Thus $t_1$ can be the chosen row for a $3$-sum on $\mm{B}_1$. The matrix in Equation \eqref{eq:23S-base1} is \emph{equivalent up to permutation} to
			\[\left((\mm{B}_1,r_1,s_1,t_1) \oplus_{3} (\mm{B}_3,i_3,j_3,k_3), i_1\right) \oplus_2 (\mm{B}_2, i_2).\]
			\item $r = r_2$, $s = s_2$, $t = t_1$ ($r, s \in Y_2$ and $t \in X_1$): This sequence of operations yields the matrix shown in Equation \eqref{eq:23S-base2}.
			\begin{equation}
				\label{eq:23S-base2}
				\feq{23S-base2}{0.5}
			\end{equation} 
			Since $t_1$ was the chosen row of $(\mm{B}_1, i_1) \oplus_2 (\mm{B}_2, i_2)$, we must have $\vv{u}_{t_1}^{\top} = \alpha_{i_1}\vv{u}_{i_2}^{\top} = \vv{0}$, $\alpha_{r_1} = \alpha_{s_1} = 0$, $\alpha_{i_1}\alpha_{r_2} = \alpha_{i_1}\alpha_{s_2} = 1$. This implies that $\alpha_{i_1} = \alpha_{r_2} = \alpha_{s_2}$, and $\vv{u}_{i_2}^{\top} = 1$. Let $\mm{B}_1'$ be $\mm{B}_1$ with row $t_1$ deleted. Since this row is identical to the row of the identity matrix, $\mm{B}_1'$ is TUM. Let $\mm{B}_2'$ be $\mm{B}_2$ with row duplicated. Call this new row $i_2'$. By negating row $i_2'$ appropriately, we can choose this row for $3$-sum. These sequence of operations is TUM preserving so $\mm{B}_2'$ is TUM. The matrix of Equation \eqref{eq:23S-base2} is equivalent to the result of
			\[(\mm{B}_1',i_1) \oplus_{2} \left((\mm{B}_2', r_2,s_2,i_2') \oplus_{3} (\mm{B}_3,i_3,j_3,k_3), i_2\right).\]
			\item $r = r_2$, $s = s_2$, $t = t_2$ ($r, s \in Y_2$ and $t \in X_2$): This sequence of operations yields the matrix shown in Equation \eqref{eq:23S-base3}.
			\begin{equation}
				\label{eq:23S-base3}
				\feq{23S-base3}{0.5}
			\end{equation}
			It is apparent that output of the sum operations above is equivalent to the output of
			\[(\mm{B}_1,i_1) \oplus_{2} \left((\mm{B}_2, r_2,s_2,t_2) \oplus_{3} (\mm{B}_3,i_3,j_3,k_3), i_2\right).\]			
			\item $r = r_1$, $s = r_2$, $t = t_1$ ($r \in Y_1$, $s \in Y_2$, and $t \in X_1$): This sequence of operations yields the matrix shown in Equation \eqref{eq:23S-base4}. The $3$-sum requires that $\vv{u}_{t_1}^{\top} = \vv{0}$, $\alpha_{r_1} = 1$, $\alpha_{s_1} = 0$, $\alpha_{i_1}\alpha_{r_2} = 1$, $\alpha_{i_1}\alpha_{s_2} = 0$, and $\alpha_{i_1}\vv{u}_{i_2}^{\top} = \vv{0}$. We deduce that $\alpha_{i_1} \neq 0$, $\alpha_{r_2} \neq 0$, $\alpha_{s_2} = 0$, and $\vv{u}_{i_2}^{\top} = \vv{0}$. W.l.o.g assume that $\alpha_{r_2} = 1$. Equation \eqref{eq:23-base4-simple} shows these simplifications. 
			\begin{align}
				&\feq{23S-base4}{0.5} \label{eq:23S-base4}\\
				&\feq{23S-base4-simple}{0.5} \label{eq:23-base4-simple}
			\end{align} 
			Define $\mm{B}_2'$ and $\mm{B}_3'$ in Equation \eqref{eq:23S-result4-setup}. $\mm{B}_2'$ is TUM since adding a column of the identity matrix is a TUM property preserving operation. $\mm{B}_3'$ is TUM by considering its submatrices. If If the submatrix does not contain column $k_3'$, then its determinant is $\{-1,0,1\}$. The same is true if it contains $k_3'$ and one of the rows $i_3'$ and $j_3'$. Otherwise consider the cofactor expansion of the submatrix along column $k_3'$ to see that it has determinant zero.
			\begin{equation}
				\label{eq:23S-result4-setup}
				\feq{23S-result4-setup}{0.5} 
			\end{equation}
			Then by Equation \eqref{eq:23S-result4}, the sequence of operations 
			\[(\mathbf{B}_1,r_1,i_1,t_1) \oplus_{3} \left((\mathbf{B}_2',r_2',r_2,i_2) \oplus_{3} (\mathbf{B}_3',i_3',j_3',k_3'),i_3,j_3,k_3\right)\]
			produces a matrix which is equivalent to $\left((\mathbf{B}_1,i_1,j_1,k_1) \oplus_{2} (\mathbf{B}_2,i_2,j_2,k_2), r_1, r_2, t_1\right) \oplus_{3} (\mathbf{B}_3,i_3,j_3,k_3)$ up to permutation. 
			\begin{equation}
				\label{eq:23S-result4}
				\feq{23S-result4}{0.5} 
			\end{equation}
		\end{enumerate} 
				
		Next $i = j = 3$. In Equation \eqref{eq:3S-setup}, the inputs.
		\begin{equation}
			\label{eq:3S-setup}
			\feq{3S-setup}{0.5}
		\end{equation} 
		Reorder the following sequence of $3$-sums.
		\[\left((\mm{B}_1, i_1, j_1, k_1) \oplus_{3} (\mm{B}_2, i_2, j_2, k_2), r,s,t\right) \oplus_{3} \left(\mm{B}_3, i_3, j_3, k_3\right).\]
		The output of $(\mm{B}_1, i_1, j_1, k_1) \oplus_{3} (\mm{B}_2, i_2, j_2, k_2)$ is shown in Equation \eqref{eq:3S-base}. 
		\begin{equation}
			\label{eq:3S-base}
			\feq{3S-base}{0.5}
		\end{equation}
		Where $\vv{b}_{r_2} = \alpha_{i_2}\vv{a}_{i_1} + \alpha_{j_2}\vv{a}_{j_1}$, $\vv{b}_{s_2} = \beta_{i_2}\vv{a}_{i_1} + \beta_{j_2}\vv{a}_{j_1}$, $\gamma_{r_2} = \alpha_{i_1}\alpha_{i_2} + \alpha_{j_1}\alpha_{j_2}$, $\gamma_{s_2} = \alpha_{i_1}\beta_{i_2} + \alpha_{j_1}\beta_{j_2}$. The specific matrices to consider depends on the location of the chosen columns $r, s$ and column $t$; whether the former are in $Y_1$ or $Y_2$ and whether the latter are in $X_1$ or $X_2$.
		\begin{enumerate}
			\item $r = r_1$, $s = s_1$, $t = t_1$: Equation \eqref{eq:3S-base1} shows the output matrix.
			\begin{equation}
				\label{eq:3S-base1}
				\feq{3S-base1}{0.5}
			\end{equation}
			Since row $t_1$ of $(\mm{B}_1, i_1, j_1, k_1) \oplus_3 (\mm{B}_2, i_2, j_2, k_2)$ was chosen for the $3$-sum, we must have $\vv{u}_{t_1}^{\top} = \alpha_{i_1}\vv{u}_{i_2}^{\top} + \alpha_{j_1}\vv{u}_{j_2}^{\top} = \vv{0}$, $\alpha_{r_1} = \alpha_{s_1} = 1$, and $\gamma_{r_2} = \gamma_{s_2} = 0$. From this we can deduce that $\alpha_{i_1} = \alpha_{j_1} = 0$ since otherwise $\mm{B}_2$ would have have a submatrix with determinant two and $\mm{B}_2$ would not be TUM. Thus we have that the matrix of Equation \eqref{eq:3S-base1} is \emph{equivalent up to permutation} with the output of 
			\[\left((\mm{B}_1, r_1, s_1, t_1) \oplus_3 (\mm{B}_3, i_3, j_3, k_3)\right)\oplus_3 (\mm{B}_2, i_2, j_2, k_2).\]
			\item $r = r_2$, $s = s_2$, $t = t_1$: Equation \eqref{eq:3S-base2} shows the output matrix.
			\begin{equation}
				\label{eq:3S-base2}
				\feq{3S-base2}{0.5}
			\end{equation}
			Since row $t_1$ of $(\mm{B}_1, i_1, j_1, k_1) \oplus_3 (\mm{B}_2, i_2, j_2, k_2)$ was chosen for the $3$-sum, we must have $\vv{u}_{t_1}^{\top} = \alpha_{i_1}\vv{u}_{i_2}^{\top} + \alpha_{j_1}\vv{u}_{j_2}^{\top} = \vv{0}$, $\alpha_{r_1} = \alpha_{s_1} = 0$, and $\gamma_{r_2} = \gamma_{s_2} = 1$. From this and the fact that all sub-determinants of $\mm{B}_1$ and $\mm{B}_2$ have magnitude at most one, derive $\alpha_{i_1} = \alpha_{j_1} \neq 0$, one of $\alpha_{i_2}$ and $\alpha_{j_2}$ is zero and the other is one, similarly with $\beta_{i_2}$ and $\beta_{j_2}$, and $\vv{u}_{i_2}^{\top} = \vv{u}_{j_2}^{\top} = \vv{0}$. Let $\mm{B}_1'$ be $\mm{B}_1$ with row $t_1$ removed. Let $\mm{B}_2'$ is $\mm{B}_2$ with row $t_2'$ added. Shown in Equation \eqref{eq:3S-result2-setup}. $\mm{B}_1'$ is a submatrix of a TUM matrix and all sub-determinants of $\mm{B}_2'$ are $\{-1,0,1\}$ so both matrices are TUM\footnote{I could explain this a bit more but it should be easy to check.}.
			\begin{equation}
			\label{eq:3S-result2-setup}
			\feq{3S-result2-setup}{0.5}
			\end{equation}
			Thus Equation \eqref{eq:3S-base2} is \emph{equivalent up to permutation} with the output of 
			\[(\mm{B}_1', i_1, j_1, k_1) \oplus_3 \left((\mm{B}_2', r_2, s_2, t_2') \oplus_3 (\mm{B}_3, i_3, j_3, k_3), i_2, j_2, k_2\right).\]
			\item $r = r_2$, $s = s_2$, $t = t_2$: Equation \eqref{eq:3S-base3} shows the output matrix.
			\begin{equation}
			\label{eq:3S-base3}
			\feq{3S-base3}{0.5}
			\end{equation}
			Observe that this equivalent up to permutation with the output of 
			\[(\mm{B}_1, i_1, j_1, k_1) \oplus_3 \left((\mm{B}_2, r_2, s_2, t_2) \oplus_3 (\mm{B}_3, i_3, j_3, k_3), i_2, j_2, k_2\right).\] 
			\item $r = r_1$, $s = r_2$, $t = t_1$: Equation \eqref{eq:3S-base4} shows the output matrix.
			\begin{equation}
				\label{eq:3S-base4}
				\feq{3S-base4}{0.5}
			\end{equation}
		\end{enumerate}
	
		Transpositions of the above $k$-sum operations can also be reordered by similar reasoning.
	\end{proof}
	If an associated tree has internal nodes labeled only by sum operations, then it can be re-balanced via rotations. This corresponds to changing the order of sum operations using Lemma \ref{lem:SumOpAssociativity}. It remains the consider how standard matrix operations interact with this change of order. 
	
	\subsection{Matrix Operations and Pivots with respect to Rotations}
	Let $T(\mm{B})$ be an associated tree. $u$ is an internal node labeled with $\oplus_{i}$ which outputs $\mm{B}_1 \oplus_{i} F\left(\mm{B}_2 \oplus_{j} \mm{B}_3\right)$ for a set of unary operations $F$. See Figure \ref{fig:tumUb-OperationsNRotations}. If we wish to change the order of $\oplus_{i}$ and $\oplus_{j}$, we must performing a left rotation on $u$. To this end, find sets of unary operations $F_1, F_2, F_3, F_4, F_5$ such that 
	\[\mm{B}_1 \oplus_{i} F\left(\mm{B}_2 \oplus_{j} \mm{B}_3\right) = F_5\left(F_4\left(F_1(\mm{B}_1) \oplus_{i} F_2(\mm{B}_2)\right) \oplus_{j} F_3(\mm{B}_3)\right).\] 
	In the following we will consider each unary operation in turn an show that they do not obstruct rotations. The proof of the Lemma is found in the Appendix. 
	\fig{tumUb-OperationsNRotations}{1}{The order of operations before and after a rotation on $\oplus_{i}$. $F$ and $F_i$ are a sequence of standard matrix operations and pivots.}
	
	\begin{lemma}{(Transposition, Negation, and Row Addition.)}
		\label{lem:TransposeNegationRowAddition}
		Let $\mm{B}$ be a TUM matrix and $T(\mm{B})$ be its associated tree. Then any transposition, negation, and row (column) addition operation can be moved below any sum operation. 
	\end{lemma}

	\subsection{Balancing the Associated Tree}
	Suppose $T(\mm{B})$ is the tree associated with TUM matrix $\mm{B} \in \{-1,0,1\}^{m \times n}$ with root $v_0$. The dimensions of a node $v_i$ in $T(\mm{B})$, denoted $d(v_i)$, is the product of the dimensions of the TUM matrix output at this node i.e. if $v_i$ outputs matrix $\mm{B}_i \in \{-1,0,1\}^{m_i \times n_i}$ then $d(v_i) = m_in_i$. Note that $d(v_0) = mn$ for root $v_0$ of $T(\mm{B})$ since $v_0$ outputs $\mm{B}$.
	
	\begin{definition}
		\label{def:BalancedTree}
		Let $\mm{B} \in \{-1,0,1\}^{m \times n}$ be a TUM matrix with associated tree $T(\mm{B})$ and root node $v_0$. $T(\mm{B})$ is \textbf{balanced}, if when $T(\mm{B})$ has a leaf $\ell$ of dimension $d(\ell) > \frac{mn}{2}$, the root-to-$\ell$ path in $T(\mm{B})$ contains at most two sum operations. Otherwise all leaves have dimension bounded above by $\frac{mn}{2}$. Let $v_1$ be sum operation closest to the root. In this case $T(\mm{B})$ is \textbf{balanced} if the left and right children of $v_1$ output $\mm{B}_1 \in \{-1,0,1\}^{m_1 \times n_1}$ and $\mm{B}_2 \in \{-1,0,1\}^{m_2 \times n_2}$ respectively such that $\max\{m_1n_1, m_2n_2\} \leq \frac{3mn}{4}$.
	\end{definition}
	
	\begin{lemma}{(Equivalent Balanced Tree.)}
		\label{lem:EquivalentBalancedTree}
		Let $\mm{B}$ be TUM. Then $T(\mm{B})$ is equivalent to a \emph{balanced} tree $T'$.
	\end{lemma}
	\begin{proof}
		There are two cases to consider: (1) $T(\mm{B})$ contains a leaf $\ell$ with $d(\ell) > \frac{mn}{2}$ or (2) it does not. 
		
		In the first case, locate leaf $\ell$. Let $v_0, v_1, ..., v_s, \ell$ be the vertices on the root-to-$\ell$ path. Let $v_{k_1}, ..., v_{k_t}$ be the subset of these vertices labeled by a sum operation. We will perform a sequence of rotations on $v_{k_i}$ to ensure that there are at most two sum operations between $\ell$ and the root. Suppose this is not already the case. Consider nodes $v_{k_1}, v_{k_2}, v_{k_3}$. W.l.o.g assume that $v_{k_2}$ is the right descendant of $v_{k_1}$. If $v_{k_3}$ is also the right descendant of $v_{k_2}$, then perform a left rotation on $v_{k_1}$. Use Lemma \ref{lem:TransposeNegationRowAddition} to handle unary operations labeling $v_{k_1 + 1}, ..., v_{k_2 -1}$. If $v_{k_3}$ is a left descendant of $v_{k_2}$, perform a right rotation at $v_{k_2}$ followed by a left rotation at $v_{k_1}$ taking care of the unary operations $v_{k_1 + 1}, ..., v_{k_2 - 1}$ and $v_{k_2 + 1}, ..., v_{k_3-1}$ as before. These are standard operations needed to maintain a balanced binary trees. As a result, the number of nodes labeled with sum operations between $\ell$ and the root decreases by one. Repeat the procedure until this number is at most a constant, namely two. 
		
		In the second case, all leaves of $T(\mm{B})$ have dimension at most $\frac{mn}{2}$. Suppose that $T(\mm{B})$ is not already balanced. Then let $v_0$ be a node labeled with a sum operation which is closest to the root and $v_1$, $v_2$ its left and right children. We must have $d(v_1) > \frac{3mn}{4}$ or $d(v_2) > \frac{3mn}{4}$. W.l.o.g assume the former. Since no leaf has dimension greater than $\frac{mn}{2}$, $v_1$ must have some descendant which is labeled by a sum operation. Let the closest such node be $v_1'$ with left and right children $v_3$ and $v_4$. Perform left rotations at $v_1'$ until the dimension of left child is at least the dimension of the right child or there are no sum operations in the right sub-tree of $v_1'$. See Figure \ref{fig:tumUb-LeftRotation}. Perform a right rotation at $v_0$. See Figure \ref{fig:tumUb-RightRotation}. Use Lemma \ref{lem:TransposeNegationRowAddition} to relocate intermediate unary operations. Observe that either $d(v_4) \leq \frac{3mn}{8}$ or $d(v_4) > \frac{3mn}{8}$ and no descendant of $v_4$ is a sum operation. Since all leaves have dimension at most $\frac{mn}{2}$ and standard matrix operations do not increase the number of non-zero entries in any row or column of the decomposition, by Lemma \ref{lem:MatrixOpDecomp}, $d(v_4) \leq \frac{mn}{2}$. It follow that $d(v_0) = d(v_2) + d(v_4) \leq \frac{mn}{2} + \frac{mn}{4} \leq \frac{3mn}{4}$. We are done if $d(v_0) \geq \frac{mn}{4}$. Otherwise repeat the same procedure with $v_1'$ in place of $v_0$.
		
		\fig{tumUb-LeftRotation}{1}{Result after left rotating on the left child of $v_0$. Either (left) the right grandchild of the left child has the smaller dimension or (right) the right grandchild has the larger dimension but is a leaf.}
		\fig{tumUb-RightRotation}{1}{Result after performing a right rotation at $v_0$.}
	\end{proof}

	The proof of Theorem \ref{thm:DataStructUBforTumMatrix} proceeds by induction on the dimension of $\mm{B}$. We will find a constant $c_1$ such that there exists a decomposition $\mm{B} = \mm{Q}\mm{D}$ with $\nzrow(\mm{Q}),\nzcol(\mm{D}) \leq c_1\log\left(mn\right)$. Let $T(\mm{B})$ be the associated tree of $\mm{B}$. By Lemma \ref{lem:EquivalentBalancedTree}, there exists a balanced tree $T'$ which is equivalent to $T(\mm{B})$. This means that if $T'$ contains a leaf $\ell$ of dimension $d(\ell) > \frac{mn}{2}$, then the root-to-$\ell$ path contains at most two nodes labeled by sum operations. Otherwise, all leaves have dimension bounded above by $\frac{mn}{2}$. Further, if $v_1$ is a node labeled by a sum operation which is closest to the root, then the two children of $v_1$ have dimension at most $\frac{3mn}{4}$. Consider each case in turn. 
	
	Suppose $T'$ has such a leaf $\ell$ with $d(\ell) > \frac{mn}{2}$. Let $\mm{B}_{\ell}$ be the output of $\ell$. $\mm{B}_\ell$ is a network matrix, so by Lemma \ref{lem:NetworkMatricesWellDecomposed}, there exists a constant $c_2$ such that $\mm{B}_{\ell}$ can be decomposed as $\mm{Q}_{\ell}\mm{D}_{\ell}$ where $\nzrow(\mm{Q}_{\ell}), \nzcol(\mm{D}_{\ell}) \leq c_2\log mn$. Since $T'$ is balanced, there must be at most two sum operations on the root-to-$\ell$ path. Let $u_0, ..., u_s, \ell$ be the vertices on this path. If none of $u_0, ..., u_s$ are labeled by a sum operation, then $u_0, ..., u_s$ must all be standard matrix operations. By Lemma \ref{lem:MatrixOpDecomp}, we can take $c_1 = c_2$, since standard matrix operations do not increase the number of non-zero entries in any row or column of the decomposition. Next suppose there is one sum operation $v_1$ among $u_0, ..., u_s$. Let $v_2$ and $v_3$ be its children such that $v_2$ is also among $u_0, ..., u_s$. The outputs of $v_2$ and $v_3$ are $\mm{B}_2$ and $\mm{B}_3$ respectively. As above, $\mm{B}_2$ has a decomposition $\mm{Q}_2\mm{D}_2$ such that $\nzrow(\mm{Q}_2) \leq \nzrow(\mm{Q}_\ell)$ and $\nzcol(\mm{D}_2) \leq \nzcol(\mm{D}_{\ell})$. By the inductive hypothesis, $\mm{B}_3$ has a decomposition $\mm{Q}_3\mm{D}_3$ such that $\nzrow(\mm{Q}_3), \nzcol(\mm{D}_3) \leq c_1\log mn - c_1$. By Lemma \ref{lem:SumOp}, the output $\mm{B}_1$ of $v_1$ can be decompose as $\mm{B}_1\mm{Q}_1$ such that 
	\begin{align*}
	\nzrow(\mm{Q}_1) &\leq \max\left(\nzrow(\mm{Q}_2), \nzrow(\mm{Q}_3)\right) + 2\\
	&\leq \max\left(c_2\log mn, c_1\log mn - c_1\right) + 2\\
	&= c_1\log mn - c_1 + 2\\
	&\leq c_1\log mn - 3
	\end{align*}
	when $c_1 \geq \max\{c_2 + 1, 5\}$ for sufficiently large $mn$. Thus $\mm{B}$ has a decomposition $\mm{Q}\mm{D}$ with $\nzrow(\mm{Q}), \nzrow(\mm{D}) \leq c_1 \log mn$. Finally we consider when there are two sum operation among $u_0, ..., u_s$. Similar to the case with one sum operation among $u_0, ..., u_s$, $\mm{B}$ has a decomposition $\mm{Q}\mm{D}$ such that $\nzrow(\mm{Q}), \nzcol(\mm{D}) \leq c_1\log mn$ when $c_1 \geq \max\{c_2 + 1, 10\}$.
	
	If the dimension of all leaves is at most $\frac{mn}{2}$, then let $v_1$ be the node in $T'$ closest to the root which is labeled by a sum operation. Suppose $v_1$ outputs matrix $\mm{B}_1$. Further let $v_2$ and $v_3$ be the left and right children of $T'$ which output $\mm{B}_2 \in \{-1,0,1\}^{m_2 \times n_2}$ and $\mm{B}_3 \in \{-1, 0, 1\}^{m_3 \times n_2}$ respectively. Since $T'$ is balanced, $\max\{m_2n_2, m_3n_3\} \leq \frac{3mn}{4}$. Apply the induction hypothesis to $\mm{B}_2$ and $\mm{B}_3$ to obtain decompositions $\mm{B}_2 = \mm{Q}_2\mm{D}_3$ and $\mm{B}_3 = \mm{Q}_3\mm{D}_3$ such that $\nzrow(\mm{Q}_2), \nzcol(\mm{D}_2) \leq c_1\log m_2n_2$ and $\nzrow(\mm{Q}_3), \nzcol(\mm{D}_3) \leq c_1\log m_3n_3$. By Lemma \ref{lem:SumOp}, $\mm{B}_1 = \mm{B}_2 \oplus_{i} \mm{B}_3$ has a decomposition $\mm{Q}_1\mm{D}_1$ such that 
	\[\nzrow(\mm{Q}_1) \leq \max(\nzrow(\mm{Q}_2), \nzrow(\mm{Q}_3)) + 2 \mbox{ and } \nzcol(\mm{D}_1) \leq \max(\nzcol(\mm{D}_2), \nzcol(\mm{D}_3)) + 2.\]
	Bound $\nzrow(\mm{Q}_1)$ as follows, 
	\begin{align*}
	\nzrow(\mm{Q}_1) &\leq \max\left(\nzrow(\mm{Q}_2), \nzrow(\mm{Q}_3)\right) + 2\\
	&\leq c_1\max\left(\log m_1 n_1, \log m_2 n_2\right) + 2\\
	&= c_1\log(mn) - c_1\log\frac{4}{3} + 2\\
	&\leq c_1\log(mn) - 3
	\end{align*}
	when $c_1\log\frac{4}{3} \geq 5$. A similar bound can be obtained for $\nzcol(\mm{D}_1)$. By Lemma \ref{lem:MatrixOpDecomp} the sequence of unary operations $u_1, ..., u_{k}$ on the root-to-$v_1$ path increase the number of non-zero entries in the rows and columns of the decomposition by at most a constant amount. Thus the original matrix $\mm{B}$ has a decomposition $\mm{Q}\mm{D}$ where $\nzrow(\mm{Q}), \nzcol(\mm{D}) \leq c_1\log mn$. 
	
	\section{Appendix}
	Proof of Lemma \ref{lem:TransposeNegationRowAddition}.
		\begin{proof}
		In the following we only consider column operations; row operations behave similarly. First define the standard matrix operations. Let $\mm{B}$ be a matrix. Then $f(\mm{B}) = \mm{B}^{\top}$, $g(\mm{B}, i)$ negates the $i$\textsuperscript{th} column of $\mm{B}_i$, $h_0(\mm{B})$ adds a row of all zeros, $h_{e}(\mm{B}, i)$ adds row $i$ of the identity matrix, $h_{d}(\mm{B}, i)$ duplicates the $i$\textsuperscript{th} column of $\mm{B}$. It suffices to show that negation, row addition, and transposition can be moved below a sum operations appearing below it in the tree.   
		
		First negation. Since the $k$-sums all interact similarly with a negation, consider the $3$-sum only. Let the input matrices be
		\begin{equation}
			\label{eq:StdMatrixOps-setup}
			\feq{StdMatrixOps-setup}{0.6}
		\end{equation}
		As we have seen before, the result of the taking the $2$-sum of $\mm{B}_1$ and $\mm{B}_2$ is the matrix shown in Equation \eqref{eq:StdMatrixOps-base}.
		\begin{equation}
			\label{eq:StdMatrixOps-base}
			\feq{StdMatrixOps-setup}{0.6}
		\end{equation}
		Then 
		\begin{align*}
			g\left((\mm{B}_1, i_1, j_1, k_1) \oplus_3 (\mm{B}_2, i_2, j_2, k_2), r_1\right) &\cong \left(g(\mm{B}_1, r_1), i_1, j_1, k_1\right) \oplus_3 (\mm{B}_2, i_2, j_2, k_2)\\ 
			&\mbox{and}\\ 
			g\left((\mm{B}_1, i_1, j_1, k_1) \oplus_3 (\mm{B}_2, i_2, j_2, k_2), r_2\right) &\cong \left(\mm{B}_1, i_1, j_1, k_1\right) \oplus_3 \left(g(\mm{B}_2, r_2), i_2, j_2, k_2\right).
		\end{align*}
		
		Column additions are quite similar. We will only consider the duplication operation $h_{d}$ since we can assume that the matrix contains a column of zeros and all columns of the identity matrix. Again, we will consider only the $3$-sum operation. Let $\mm{B}_1$ and $\mm{B}_2$ be as above. Then
		\begin{align*}
			h_d\left((\mm{B}_1, i_1, j_1, k_1) \oplus_3 (\mm{B}_2, i_2, j_2, k_2), r_1\right) &\cong \left(h_d(\mm{B}_1, r_1), i_1, j_1, k_1\right) \oplus_3 (\mm{B}_2, i_2, j_2, k_2)\\ 
			&\mbox{and}\\ 
			h_d\left((\mm{B}_1, i_1, j_1, k_1) \oplus_3 (\mm{B}_2, i_2, j_2, k_2), r_2\right) &\cong \left(\mm{B}_1, i_1, j_1, k_1\right) \oplus_3 \left(h_d(\mm{B}_2, r_2), i_2, j_2, k_2\right).
		\end{align*}
		
		Finally transpositions. For a $1$-sum, $f(\mm{B}_1 \oplus_{1} \mm{B}_2) \cong f(\mm{B}_1) \oplus_{1} f(\mm{B}_2)$. For $2$ or $3$-sums,
		\begin{align*}
			f\left((\mm{B}_1, i_1) \oplus_2 (\mm{B}_2, i_2)\right) &\cong \left(f(\mm{B}_2), i_2\right) \oplus_2 \left(f(\mm{B}_1), i_1)\right)\\
			&\mbox{and}\\
			f\left((\mm{B}_1, i_1, j_1, k_1) \oplus_3 (\mm{B}_2, i_2, j_2, k_2)\right) &\cong \left(f(\mm{B}_2), i_2, j_2, k_2\right) \oplus_3 \left(f(\mm{B}_1), i_1, j_1, k_1)\right)\\
		\end{align*} 
		For a $3'$-sum, let $\mm{B}_1$ and $\mm{B}_2$ be the input matrices and $\mm{B}_1'$ and $\mm{B}_2'$ be defined as in Equation \eqref{eq:StdMatrixOps-transpose}.
		\begin{equation}
			\label{eq:StdMatrixOps-transpose}
			\feq{StdMatrixOps-transpose}{0.6}
		\end{equation}
		We show that $\mm{B}_1'$ is TUM by considering all sub-determinants. If the submatrix only contains row of $i_1'$ and $j_1'$ then its determinant is one of $\{-1,0,1\}$ since it is a submatrix of $\mm{B}_1$. Otherwise the submatrix contains row $i_1'$ and $j_1'$. Then, by co-factor expansion along $k_1'$, we see that the determinant is equal to the determinant of a submatrix of $\mm{B}_1$. $\mm{B}_2'$ satisfies the TUM property by a similar argument\footnote{I could clarify this a bit more if necessary, but it should be a simple linear algebra exercise.}. Thus
		\[f\left((\mm{B}_1, i_1, j_1, k_1) \oplus_{3'} (\mm{B}_2, i_2, j_2, k_2)\right) \cong \left(f(\mm{B}_2'), i_2, j_2, k_2\right) \oplus_3 \left(f(\mm{B}_1'), i_1, j_1, k_1\right).\]
	\end{proof}
	
	%---------------------------
	%	BIBLIOGRAPHY
	%---------------------------		
	\bibliographystyle{acm}
	\bibliography{../bibliography}
\end{document}