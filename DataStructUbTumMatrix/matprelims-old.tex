We will cover the necessary technical background here. Readers comfortable with matroid decompositions and their matrix decomposition counterparts should feel free to skip this section. All relevant definitions are summarized at the end.

\begin{definition}{(Matroids.)}
	\label{def:Matroid} A \emph{matroid} $M$ is a tuple $(E, \mathcal{I})$ where $E$ is the underlying set and $\mathcal{I}$ is the family of independent subsets of $E$ i.e. $\mathcal{I}$ satisfies the following properties:
	\begin{enumerate}
		\item $\emptyset \in \mathcal{I}$.
		\item If $I \in \mathcal{I}$ and $I_1 \subseteq I$, then $I_1 \in \mathcal{I}$. 
		\item If $I_1, I_2 \in \mathcal{I}$ and $|I_1| > |I_2|$, then there exists an element $x \in I_1 - I_2$ such that $I_2 \cup x \in \mathcal{I}$.
	\end{enumerate}
	
	A \emph{basis} of $M$ is a maximal element of
        $\mathcal{I}$. Observe that all basis have the same
        cardinality. The size of any basis is then the \emph{rank} of
        $M$, denoted $\rank(M)$. We will slightly abuse this notation
        for $\rank(E')$ where $E' \subseteq E$ by defining $\rank(E')
        \coloneqq \rank(M')$ where $M' = (E', \mathcal{I}')$ and $I' =
        \{I \subseteq E': I \in \mathcal{I}\}$.
	
	\emph{Duality}, \emph{deletion}, and \emph{contraction} form the set of core matroid operations. For matroid $M = (E, \mathcal{I})$ with basis $\mathcal{B}$, the dual of $M$, denoted $M^*$, is the matroid $(E, \mathcal{I}^*)$ where $\mathcal{I}^*$ has maximal elements $\mathcal{B}^* = \{E - B: B \in \mathcal{B}\}$. Let $U$ and $V$ be subsets of $E$. The deletion of $U$ from $M$, denoted $M\backslash U$, is the matroid $(E\backslash U,\mathcal{I}')$ where $\mathcal{I}' = \{I \in \mathcal{I}: I \subseteq E\backslash U\}$. The contraction of $V$ from $M$, denoted $M/V$, is the matroid $(E\backslash V, \mathcal{I}'')$ where $\mathcal{I}'' = \{I - V: I \cup B_V \in \mathcal{I}\}$ for some maximal independent set $B_V \subseteq V$. Deletions and contractions can occur in any order, so $M\backslash U/V$ is well defined. Any matroid isomorphic to $M\backslash U/V$ is a \emph{minor} of $M$.
\end{definition}
Given a graph $G = (V,E)$, let the \emph{graphic matroid} $M(G)$ of
$G$ be $(E, \mathcal{I})$ where $\mathcal{I}$ is the family of
forests, i.e.~acyclic subgraphs, of $G$. Observe that each basis of
$M(G)$ corresponds to a spanning tree. The dual of $M(G)$, denoted
$M^*(G)$, is the \emph{cographic matroid} of $G$.

Given a matrix $\mm{B}$ with elements in a field $\FF$, let the vector
(a.k.a.  linear) matroid $M[\mm{B}]$ of $\mm{B}$ be $(E, \mathcal{I})$
where $E$ is the set of columns of $\mm{B}$ and $\mathcal{I}$ is the
family of subsets of $E$ which are linearly independent over
$\FF$. Denote by $\rank_{\FF}(\mm{B})$ the rank of $\mm{B}$ over
$\FF$. For our purposes we require a special case of the
submodularity of rank. Let $\mm{B}$ be a matrix over field $\FF$ which
can be divided into quadrants
\[\mm{B} = \begin{bmatrix}
\mm{A}_{11} & \mm{A}_{12}\\
\mm{A}_{21} & \mm{A}_{22}
\end{bmatrix} \mbox{, with } \mm{X} = 
\begin{bmatrix}
\mm{A}_{21} & \mm{A}_{22}
\end{bmatrix} \mbox{ and } \mm{Y} = 
\begin{bmatrix}
\mm{A}_{12} \\
\mm{A}_{22}
\end{bmatrix}.\]
Then the rank function satisfies the following inequality
\begin{equation}
\label{eq:Submodularity}
\rank_{\FF}(\mm{B}) + \rank_{\FF}(\mm{A}_{22}) \geq \rank_{\FF}(\mm{X}) + \rank_{\FF}(\mm{Y}).
\end{equation}

If a matroid $M$ is isomorphic to some $M[\mm{B}]$, with $\mm{B}$ as
defined above, then we say $M$ is \emph{representable} over
$\FF$. Matroids representable over $GF(2)$ are binary. Matroids
representable over every field are called \emph{regular}. A well-known
characterization of regular matroids (due to Tutte) states that they
are exactly the matroids as those which can be represented by totally
unimodular matrices.

\begin{definition}{(Total Unimodular Matrices.)}
	\label{def:Tum} 
	A \emph{Totally Unimodular Matrix} (TU matrix) is a $\{-1,0,1\}$
        matrix such that every sub-matrix has determinant $\{-1,0,1\}$
        (over the reals).
\end{definition}
The following set of unary operations preserve the TU property:
\begin{itemize}
	\item Transposition.
	\item Permutations of rows or columns.
	\item Negating rows or columns. 
	\item Adding an auxiliary row or column. Auxiliary rows are defined as: a row of all zeros, a row of the identity matrix $\mm{I}$, or an existing row. Auxiliary columns are defined similarly.
	\item Deleting an auxiliary row or column where auxiliary rows and columns for deletions are similar to those for addition. 
\end{itemize} 
We define these to be the \emph{standard matrix operations} we can
perform on any TU matrix. Since adding and removing rows and columns
of $\mm{I}$ does not change the TU property, we assume that all TU
matrices $\mm{B}$ come equipped with $\mm{I}$ i.e.~we assume that any
TU matrix $\mm{B}$ has the form $\mm{B} = [\mm{I}\ \mm{B}']$. Moreover, any
vector matroid $M[\mm{A}]$ is isomorphic to a matroid $M[\mm{B}]$
represented by a matrix $\mm{B} = [\mm{I}\ \mm{B}']$.
% \begin{tikzpicture}[scale=0.5]
% \draw node at (0,0) {$\mm{B} = $};
% \draw (1,-0.5) rectangle node{$\mm{I}$} +(1,1);
% \draw (2,-0.5) rectangle node{$\mm{A}$} +(1,1);
% \end{tikzpicture} 
For simplicity, we call
% \begin{tikzpicture}[scale=0.5]
% \draw node at (0,0) {$\mm{B}' = $};
% \draw (1,-0.5) rectangle node{$\mm{A}$} +(1,1);
% \end{tikzpicture} 
$\mm{B}'$ the \emph{standard representation matrix} of $M = M[\mm{B}]$. We
index the rows of the standard representation $\mm{B}'$ by the
elements of $M$ that correspond to columns of $\mm{I}$;
i.e.~the $i$-th row of $\mm{B}'$ is indexed by the element of $M$
corresponding to the $i$\textsuperscript{th} standard basis vector
$\mm{e}_i$. 

Suppose the matroid $M[\mm{B}]$ is represented by the $m\times n$
matrix $\mm{B} = [\mm{I}\ \mm{B}']$. Then the first $m$ columns of
$\mm{B}$ form a distinguished basis of $M$. At times during our matrix
decomposition it may be necessary to change the basis of $M$ by
exchanging one of its columns with a of $\mm{B}'$. We do this by
\emph{pivoting} on a non-zero entry $B_{i,j}'$. This operation
exchanges the $i$\textsuperscript{th} row and $j$\textsuperscript{th}
column of $\mm{B}'$, which corresponds to exchanging columns $i$ and
$m + j$ in $\mm{B}$, and transforming the new $i$\textsuperscript{th}
column into $\mm{e}_i$ via elementary row operations.

We use Seymour's celebrated regular matroid decomposition theorem,
stated next.
\begin{theorem}{(Regular Matroid Decomposition, \cite{seymour1980decomposition}.)}
	\label{thm:SeymourMatroidDecomposition}
	Every regular matroid $M$ may be constructed by means of $1-, 2-, 3-$sums, starting with matroids each isomorphic to a minor of $M$ and each either graphic, cographic, or isomorphic to $R_{10}$. 
\end{theorem}
In the following, we describe the translation from this theorem about matroid into a TU matrix decomposition. A more thorough exposition can be found in the works of Truemper \cite{truemper1992matroid}, and Oxley \cite{oxley2003matroid, oxley2006matroid}.

The \emph{atomic components} of Seymour's decomposition are graphic
and cographic matroids as well as matroids isomorphic to $R_{10}$, so
let us first find matrices corresponding to each of these. Let $G$ be
an undirected graph, and let us pick an arbitrary orientation on the
edges. Let $\mm{A}$ be the directed incidence matrix of $G$ whose rows
and columns correspond to the vertices and edges of $G$,
respectively. An entry $A_{ij}$ of $\mm{A}$ is equal to $+1$ if there is an
edge $(i,j)$ in $G$ going from $i$ to $j$, $-1$ if there is an edge
$(i,j)$ in $G$ going from $j$ to $i$, and $0$ otherwise. $\mm{A}$ is a
totally unimodular representation for the graphic matroid $M(G)$ over
the reals. A \emph{network matrix} is any matrix derived from a
directed incidence matrix of a graph $G$ after a series of pivots
(over the reals), deletions, and contractions. It turns out that every
graphic matroid has a network matrix as a standard representation. Let
$M^*(G)$ be the cographic matroid of $G$. If $\mm{B}$ is a standard
representation of $M(G)$, then $\mm{B}^{\top}$ is a standard
representation of $M^*(G)$. Thus a matroid is graphic or cographic if
and only if it is representable by a network matrix.

$\mm{R}_{10}$ is a regular matroid with two unique standard
$GF(2)$-representations. For each $GF(2)$-representation $\mm{B}$,
there is a way to assign signs to the entries of $\mm{B}$ to produce
TU matrix $\mm{B}'$, and this assignment is unique up to scaling rows
and columns by $\pm 1$.. Thus there are essentially only two unique TU
representations of $\mm{R}_{10}$ up to modification by standard matrix
operations. These are denoted $\mm{B}_{10}^{(1)}$ and
$\mm{B}_{10}^{(2)}$ and are shown in Equation
\eqref{eq:TumButNotNetwork}. This procedure of turning a
$GF(2)$-matrix whose vector matroid is regular into a TU matrix is
call \emph{signing} and will be used later when explaining the sum
operations.

\begin{equation}
\label{eq:TumButNotNetwork}
\mm{B}_{10}^{(1)} = \begin{bmatrix}
1 	& -1 	& 0 	& 0 	& -1\\
-1 	& 1 	& -1 	& 0 	& 0\\
0 	& -1 	& 1 	& -1 	& 0\\
0 	& 0 	& -1 	& 1 	& -1\\
-1 	& 0 	& 0 	& -1 	& 1
\end{bmatrix}\mbox{, } \mm{B}_{10}^{(2)} = 
\begin{bmatrix}
1 & 1 & 1 & 1 & 1\\
1 & 1 & 1 & 0 & 0\\
1 & 0 & 1 & 1 & 0\\
1 & 0 & 0 & 1 & 1\\
1 & 1 & 0 & 0 & 1
\end{bmatrix}
\end{equation} 

Next we find TU matrix sums corresponding to the matroid sums. Since these are instances of a more general matroid $k$-sums, it helps to understand those first. Given matroid $M = (E, \mathcal{I})$, an exact $k$-separation of $M$ is a partition of $E$ into $(E_1,E_2)$ such that $|E_1|, |E_2| \geq k$ and $\rank(E_1) + \rank(E_2) = \rank(E) + k-1$. Let $X_2$ be a maximal independent set of $E_2$. Extend $X_2$ by $X_1$ so that $X_1 \cup X_2$ is a basis of $M$. Let $Y_i = E_i - X_i$ for $i \in \{1,2\}$. With $X_1 \cup X_2$ as the basis, $M$ can be represented over $GF(2)$ by the standard representation matrix  
\begin{equation}
\label{eq:KSumMatrix}
\feq{Prelim-GF2Rep}{0.5}
\end{equation}
where $|X_1 \cup Y_1|, |X_2 \cup Y_2| \geq k$ and $\rank(\mm{D}) = k-1$. 

We would like to decompose $\mm{B}$ into two matrices $\mm{B}_1$ and $\mm{B}_2$ such that $\mm{C}_1$ is a submatrix of $\mm{B}_1$, $\mm{C}_2$ is a submatrix of $\mm{B}_2$, and $\mm{B}$ can be reconstructed using $\mm{B}_1$ and $\mm{B}_2$. There are many valid decomposition. We will use the method of Truemper \cite{truemper1992matroid}. First divide $\mm{B}$ into
\begin{equation}
\feq{Prelim-GF2RepExpanded}{0.5}
\end{equation} 
with quadrants specified shortly. Define $\mm{B}_1$ and $\mm{B}_2$ as the components of $\mm{B}$, shown below. 
\begin{equation}
\feq{Prelim-GF2Decomp}{0.5}
\end{equation}
Then $M[\mm{B}_1] \oplus_k M[\mm{B}_2]$ is a $k$-sum decomposition of $M[\mm{B}]$. 

All blocks of $\mm{B}$ except $\mm{D}_{12}$ appear as submatrices of $\mm{B}_1$ or $\mm{B}_2$, so it remains to reconstruct $\mm{D}_{12}$ in order to reconstruct $\mm{B}$. Let $\mm{D}'$ be a $(k-1) \times (k-1)$ submatrix of $\mm{D}$. Observe that $\mm{D}'$ can be made non-singular as follows. Since $\rank(\mm{D}) = k-1$, there exists a subset of $k-1$ rows and $k-1$ columns of $\mm{D}$ with full rank. Let\begin{tikzpicture}[scale=0.5]
\draw node at (0,0) {$\mm{X} = $};
\draw (1,-0.5) rectangle node{$\mm{D}_1$} +(1.5,1);
\draw (2.5,-0.5) rectangle node{$\mm{D}'$} +(1.5,1);
\end{tikzpicture}
be this subset of $k-1$ rows and\begin{tikzpicture}[scale=0.5]
\draw node at (0,0) {$\mm{Y} = $};
\draw (1,0) rectangle node{$\mm{D}'$} +(1.5,1);
\draw (1,-1) rectangle node{$\mm{D}_2$} +(1.5,1);
\end{tikzpicture} 
be this subset of $k-1$ columns. Then let $\mm{D}'$ be the intersection of $\mm{X}$ and $\mm{Y}$. By the submodularity Equation \eqref{eq:Submodularity}, 
\[\rank(\mm{D}) + \rank(\mm{D}') = k-1 + \rank(\mm{D}') \geq \rank(\mm{X}) + \rank(\mm{Y}) = 2(k-1).\]
Thus $\rank(\mm{D}) \geq k-1$. Since $\rank(\mm{D}') \leq k-1$ as it is a submatrix of $\mm{X}$ and $\mm{Y}$, $\rank(\mm{D}') = k-1$ and $\mm{D}'$ is indeed a non-singular submatrix of $\mm{D}$. Observe that $\rank(\mm{D}') = \rank(\mm{D})$ so the rows of
\begin{tikzpicture}[scale=0.5]
\draw (0,-0.5) rectangle node{$\mm{D}_{12}$} +(1.5,1);
\draw (1.5,-0.5) rectangle node{$\mm{D}_{2}$} +(1.5,1);
\end{tikzpicture} are a linear combination of the rows of $\mm{X}$. Thus there exists a matrix $\mm{L}$ such that\begin{tikzpicture}[scale=0.5]
\draw node at (0,0) {$\mm{L}\mm{X} = $};
\draw (1.2,-0.5) rectangle node{$\mm{D}_{12}$} +(1.5,1);
\draw (2.7,-0.5) rectangle node{$\mm{D}_{2}$} +(1.5,1);
\end{tikzpicture} By solving the system of equations $\mm{D}_{12} = \mm{L}\mm{D}_1$ and $\mm{D}_2 = \mm{L}\mm{D}'$ for $\mm{D}_{12}$, we find that $\mm{D}_{12} = \mm{D}_2 \left(\mm{D}'\right)^{-1} \mm{D}_1$.  

In the following, as above, all matrices will be over $GF(2)$. However, just as for $R_{10}$, we can sign the representative binary matrices to produce corresponding TU matrix $k$-sum since their vector matroids are regular. Each matroid $k$-sum operation corresponds to a submatrix $\mm{D}'$ of rank $k-1$. When $k = 1$, $\mm{D}'$, $\mm{u}^{\top}$, and $\mm{a}$ are all empty. Thus
\begin{equation}
\feq{1SumDef}{0.5}
\end{equation}
When $k = 2$, $\mm{D}' = [1]$, $[\mm{D}_1; \mm{D}']$ is a row of $\mm{B}_1$, and $[\mm{D}'/\mm{D}_2]$ is a column of $\mm{B}_2$. Let us define a family of $2$-sum operations $\oplus_2$ where $i$ and $j$ index a row of $\mm{B}_1$ and a column of $\mm{B}_2$ respectively. Thus 
\begin{equation}
\feq{2SumDef}{0.5}
\end{equation}

When $k = 3$, $\mm{D}'$ is a $2 \times 2$ non-singular matrix in $GF(2)$. For simplicity let $\mm{D}'$ be the identity matrix. If $\mm{B}$ as shown in \eqref{eq:KSumMatrix}, then the corresponding component matrices are 
\begin{equation}
\feq{3SumDef}{0.5}
\end{equation}
where $\vv{a}_{i_1}$, $\vv{a}_{j_1}$ are linearly independent as are $\vv{u}_{i_2}^{\top}$ and $\vv{u}_{j_2}^{\top}$. As in the $\oplus_2$ case, the indices are used to indicate the rows and columns of each matrix which participate in the sum operation. $i_1$, $j_1$ index the independent rows of $\mm{B}_1$ and $k_1$ the column containing the zero vector. $i_2$, $j_2$ index the independent columns of $\mm{B}_2$ and $k_2$ the row containing the zero vector.

However, by a pivot operation, the matroid $3$-sum can also manifest as
\begin{equation}
\label{eq:3SumAlt}
\feq{Prelim-GF2RepAlt}{0.5}
\end{equation}
where $\rank(\mm{R}) = \rank(\mm{S}) = 1$. In this case, the desired decomposition is
\begin{equation}
\feq{3SumAltDef}{0.5}
\end{equation}
where $\vv{a}_{i_1} = \vv{a}_{j_1}$ and $\vv{a}_{i_2} = \vv{a}_{j_2}$. 
