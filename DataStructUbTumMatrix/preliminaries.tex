%!TEX root = main.tex


% 	Let $\mm{B} \in \{-1,0,1\}^{m \times n}$. Let $\nzrow(\mm{B})$ and $\nzcol(\mm{B})$ to be the maximum number of non-zero elements in any row and column of $\mm{B}$ respectively. We conjecture that there exist matrices $\mm{Q} \in \{-1,0,1\}^{m \times s}$ and $\mm{D} \in \{-1,0,1\}^{s \times n}$ such that $\mm{B} = \mm{Q}\mm{D}$ and 
% \[\sqrt{\nzrow(\mm{Q}) \cdot \nzcol(\mm{D})} \in \tilde{O}\left(\herdisc(\mm{B})\right).\] 
% Since $O(\log mn)\cdot\herdisc(\mm{B}) = \gamma_2(\mm{B})$ [\cite{matousek2014factorization}], this implies that \[\sqrt{\nzrow(\mm{Q}) \cdot \nzcol(\mm{D})} \leq \gamma_2(\mm{B}).\]

% Our main results is as follows. 
% \begin{theorem}{(Main Result.)}
% 	\label{thm:DataStructUBforTumMatrix}
% 	For every TUM matrix $\mm{B}$, there exist matrices $\mm{Q}$ and $\mm{D}$ with entries in $\{-1,0,1\}$ such that $\mm{B} = \mm{Q}\mm{D}$ and $\sqrt{\nzrow(\mm{Q}) \cdot \nzcol(\mm{D})} \leq \tilde{O}\left(\herdisc(\mm{B})\right)$. 
% \end{theorem}
% Since $\herdisc(\mm{B}) = 1$ \cite{doerr2000linear}, this is equivalent to finding matrices $\mm{Q} \in \{-1,0,1\}^{m \times s}$ and $\mm{D} \in \{-1,0,1\}^{s \times n}$ such that $\mm{B} = \mm{Q}\mm{D}$ and $\nzrow(\mm{Q}) \cdot \nzcol(\mm{D}) \leq O(\log m n)^2$. This complements discrepancy lower bounds for data structures of \cite{larsen2014range}. 

\paragraph{Notation.} In the following, let Greek letters be scalars ($\alpha, \beta, ...$), bold-face lower-case letter at the start of the alphabet be column vectors ($\vv{a}, \vv{b}, ...$), the transpose of bold-face lower-case letters at the end of the alphabet be row vectors ($\vv{u}^{\top}, \vv{v}^{\top}, ...$), and bold-face upper-case letters be matrices ($\mm{A}, \mm{B}, \mm{Q}, \mm{D}, ...$).

\section{Preliminaries on TU Matrices}

Recall from the Introduction that a matrix is \emph{totally
  unimodular} if all of its square submatrices have determinant in
$\{-1, 0, +1\}$.
The following set of unary operations preserve the TU property:
\begin{itemize}
	\item Transposition.
	\item Permutations of rows or columns.
	\item Negating rows or columns. 
	\item Adding an auxiliary row or column. Auxiliary rows are defined as: a row of all zeros, a row of the identity matrix $\mm{I}$, or an existing row. Auxiliary columns are defined similarly.
	\item Deleting an auxiliary row or column where auxiliary rows and columns for deletions are similar to those for addition. 
\end{itemize} 
We define these to be the \emph{standard matrix operations} we can
perform on any TU matrix. Since adding and removing rows and columns
of $\mm{I}$ does not change the TU property, we assume that all TU
matrices $\mm{B}$ come equipped with $\mm{I}$ i.e.~we assume that any
TU matrix we deal with $\mm{B}$ has the form $\mm{B} = [\mm{I}\
\mm{B}']$.

The TU property is also preserved by a class
of binary matrix operations, called $k$-sums, where $k$ is
$1$, $2$, or $3$. Let $\mm{B}_1$ and $\mm{B}_2$ be TU matrices. The
structure of matrix $k$-sums are as follows.
\begin{align}
&\feq{1SumDef}{0.5} \label{eq:1SumDef}\\
&\feq{2SumDef}{0.5} \label{eq:2SumDef}\\
&\feq{3SumDef}{0.5} \label{eq:3SumDef}\\
&\feq{3SumAltDef}{0.5} \label{eq:3SumAltDef}
\end{align}
Above, the indices are used to indicate the rows and columns of each
matrix which participate in the sum operation. In the case of the
$\oplus_3$ operation, $\vv{a}_{i_1}$, $\vv{a}_{j_1}$ are linearly
independent as are $\vv{u}_{i_2}^{\top}$ and $\vv{u}_{j_2}^{\top}$. In
the case of the $\oplus_{3'}$ operation, $\vv{a}_{i_1} = \vv{a}_{j_1}$
and $\vv{a}_{i_2} = \vv{a}_{j_2}$.

A basic class of TU matrices are the network matrices, defined next. 
\begin{definition}
  \label{def:NetworkMatrix}
  A \textbf{network matrix} $\mm{N} \in \{-1, 0, 1\}^{m \times n}$
  corresponds to a directed tree $T$ on $n+1$ vertices $v_0, v_1, ...,
  v_{n}$. Each column of $\mm{N}$ is indexed by an ordered pair
  $(i,j)$ for $i, j \in \{0, 1, ..., n\}$, and each row is indexed by
  one of the $n$ directed edges $e_1, .., e_n$ of $T$. Let $p_{i,j}$
  be the unique path in $T$ from $v_i \rightsquigarrow v_j$. Then
  each entry in $\mm{N}$ is given by 
  \[N_{k,(i,j)} = \begin{cases}
    1 &\mbox{if } e_k \in p_{i,j}\\
    -1 &\mbox{if } -e_k \in p_{i,j}\\
    0 &\mbox{if } e_k, -e_k \notin p_{i,j}
  \end{cases},\]
  where $-e_k$ denotes $e_k$ with the orientation reversed. 
\end{definition}

We also need a couple of special small matrices, which happen to be,
in a certain sense, the minimal TU matrices which are not network
matrices.
\begin{equation}
\label{eq:TumButNotNetwork}
\mm{B}_{10}^{(1)} = \begin{bmatrix}
1 	& -1 	& 0 	& 0 	& -1\\
-1 	& 1 	& -1 	& 0 	& 0\\
0 	& -1 	& 1 	& -1 	& 0\\
0 	& 0 	& -1 	& 1 	& -1\\
-1 	& 0 	& 0 	& -1 	& 1
\end{bmatrix}\mbox{, } \mm{B}_{10}^{(2)} = 
\begin{bmatrix}
1 & 1 & 1 & 1 & 1\\
1 & 1 & 1 & 0 & 0\\
1 & 0 & 1 & 1 & 0\\
1 & 0 & 0 & 1 & 1\\
1 & 1 & 0 & 0 & 1
\end{bmatrix}
\end{equation} 

Recall that the \emph{linear matroid} $M[\mm{B}]$ represented by a
matrix $\mm{B}$ is the pair $(E, \mathcal{I})$, where $E$ indexes
columns of $\mm{B}$, and $\mathcal{I}$ is the family of subsets of $E$
which index sets of columns that are linearly independent over $\FF$;
we then say that $M = M[\mm{B}]$ is representable over $\FF$. A
matroid which is representable over any field is called
\emph{regular}. It turns out that the regular matroids are exactly
those that are representable over the reals by a totally unimodular
matrix. One class of regular matroid are the graphic and co-graphic
matroids, which are representable, respectively, by matrices of the
form $[\mm{I}\ \mm{B}]$ and $[\mm{I}\ \mm{B}^\top]$, where $\mm{B}$ is
a network matrix.\footnote{A better known definition is that a graphic
  matroid is a pair $M(G) = (E, \mathcal{I})$, where $E$ is the set of
  edges of the graph $G$, and $\mathcal{I}$ is the set of all acyclic
  subgraphs of $G$; a co-graphic matroid is a pair $M^\ast(G) = (E,
  \mathcal{I}^\ast)$, where $E$ are again the edges of $G$, and
  $\mathcal{I}^\ast$ is the set of all subgraphs of $G$ whose
  complement is connected. The definition in terms of network matrices
  is equivalent.} In a celebrated
result~\cite{seymour1980decomposition}, Seymour showed that every
regular matroid can be decomposed into graphic and co-graphic
matroids, and a special matroid $R_{10}$, using the matroid variants
of $1$-, $2$-, and $3$-sums. This result can be translated into a
decomposition theorem for TU matrices, which we state next, following
the formulation in~\cite{truemper1992matroid}.

\begin{theorem}{(TU Matrix Decomposition, \cite{seymour1980decomposition,truemper1992matroid}.)}
  \label{thm:TumMatrixDecomposition}
  Any TU matrix is, up to permutations of row and column indices, and
  scaling of the rows and columns by $\{\pm1\}$, a network
  matrix, or is the transpose of such a matrix, or is the matrix
  $\mm{B}_{10}^{(1)}$ or $\mm{B}_{10}^{(2)}$ of Equation
  \eqref{eq:TumButNotNetwork}, or may be constructed recursively by
  matrix $1$-, $2$-, or $3$-sums.
\end{theorem}

We call network matrices and the pair of five-by-five matrices the \emph{atomic components}. Typically we denote matrix operations such as scaling, transposition, or addition of rows and columns by lowercase letters (e.g. $f$, $g$, ...). A sequence of such operations will be denoted by uppercase letters (e.g. $F$, $G$, ...). Instead of row and column permutations, we select the necessary rows and columns during a $k$-sum. %We pivot implicitly by considering the two possible results of a matrix $3$-sum --- Equations \eqref{eq:3SumDef} and \eqref{eq:3SumAltDef} --- separately.

\section{Proof Outline}
\begin{definition}
	\label{def:AssociatedTree}
	Let $\mm{B}$ be a TU matrix. A \textbf{tree associated with $\mm{B}$}, denoted $T(\mm{B})$, is one whose leaves are the atomic matrices, whose internal nodes are $k$-sums or standard matrix operations, and whose root outputs $\mm{B}$. All trees which output $\mm{B}$ are \textbf{equivalent}. 
\end{definition}

By Theorem \ref{thm:TumMatrixDecomposition}, every TU matrix $\mm{B}$
has an associated tree. Suppose that $\mm{B}_1 = \mm{Q}_1\mm{D}_{1}$
and $\mm{B}_2 = \mm{Q}_2\mm{D}_2$ are two TU matrices. It suffices to
show the following.
\begin{enumerate}
	\item An atom $\mm{A} \in \{-1,0,1\}^{m_A \times n_A}$ can be decomposed into $\mm{Q}_A\mm{D}_A$ such that $\nzrow(\mm{Q}_A), \nzcol(\mm{D}_A) \in O(\log m_An_A)$.
	\item For any standard matrix operation $f$, $f(\mm{B}_1)$ can be decomposed into $\mm{Q}'\mm{D}'$ such that $\nzrow(\mm{Q}') \leq \nzrow(\mm{Q}_1)$ and $\nzcol(\mm{D}') \leq \nzcol(\mm{D}_1)$.  
	\item The $k$-sum of $\mm{B}_1$ and $\mm{B}_2$ with $k \in \{1,2,3\}$ can be decomposed into $\mm{Q}\mm{D}$ such that
	\[\nzrow(\mm{Q}) \leq \max\left(\nzrow(\mm{Q}_1), \nzrow(\mm{Q}_2)\right) + \alpha_1 \mbox{ and } \nzcol(\mm{D}) \leq \max\left(\nzcol(\mm{D}_1), \nzcol(\mm{D}_2)\right) + \alpha_1\] 
	for constant $\alpha_1$.
	\item It is possible to reorder a sequence of $k$-sum operations.
	\item Standard matrix operations do not ``interfere'' when reordering the $k$-sums in the previous step.
\end{enumerate}

The high level intuition is as follows.  Apply induction to the
dimension of the matrix constructed at each node in the tree. The base
case consists of atomic matrices. For internal nodes labeled by sum
operations, apply the induction hypothesis to its two children. If the
dimensions of a child is more than a constant fraction of the total
dimension, then we apply a left or right rotation at that node, in
order to rebalance the tree. Observe that rotations, in the absence of
matrix operations, is equivalent to changing the order of operations
in a sequence of $k$-sums. This does not change the output at the
root, so $\mm{B}$ can be decomposed into $\mm{Q}\mm{D}$ such that
$\sqrt{\nzrow(\mm{Q})\nzcol(\mm{D})} \leq O(\log mn)$. To show that this
holds in full generality, we show that changing the order of
operations is valid in the presence of standard matrix operation, too.

\subsection{Decomposing Network Matrices}
To show that the first property above is satisfied, it suffices to
check network matrices since matrices of constant size have trivial
decompositions. We first prove an easy lemma.

\begin{lemma}\label{lm:row-sum}
  Let $\mm{M}$ be a matrix with factorization $\mm{M} = \mm{Q}\mm{D}$,
  and let $\mm{M}'$ be another matrix each of whose rows
  (resp.~columns) is the sum or difference of at most $2$ rows
  (resp.~columns) from $\mm{M}$. Then $\mm{M}'$ has a factorization
  $\mm{M}' = \mm{Q}'\mm{D}$ with $\nzrow(\mm{Q}') \le 2
  \nzrow(\mm{Q})$ (resp.~factorization $\mm{M}' = \mm{Q}\mm{D}'$ with
  $\nzcol(\mm{D}') \le 2 \nzcol(\mm{D})$).
\end{lemma}
\begin{proof}
  In the row case, suppose that row $i$ of $\mm{M}$ is the sum
  (resp.~difference) of rows $j$ and $k$ of $\mm{M}'$. Then we form
  row $i$ of $\mm{Q}'$ by adding (resp.~taking the difference of) rows $j$ and $k$
  of $\mm{Q}$. As the $\nzrow$ function is subadditive, the lemma
  follows. The column case is analogous.
\end{proof}

\begin{lemma}{(Network Matrices Decomposition.)}
	\label{lem:NetworkMatricesWellDecomposed}
	Let $\mm{N} \in \{-1,0,1\}^{m \times n}$ be a network matrix. Then there exist matrices $\mm{Q}$ and $\mm{D}$ such that $\mm{N} = \mm{Q}\mm{D}$ and $\nzrow(\mm{Q}), \nzcol(\mm{D}) \in O(\log mn)$.
\end{lemma}
\begin{proof}
  First, we make some simplifying observations. Let $v_0$ be the root
  vertex. W.l.o.g direct the edges in $T$ from the root to leaf by
  negating the rows of $\mm{N}$ corresponding to edges orientated from
  child to parent. This clearly does not change the parameters of an
  optimal factorization. Further, the column vector $\vv{n}_{(i,j)}$
  of $\mm{N}$ corresponding to each path $p_{i,j}$ (i.e.~the path from
  $v_i$ to $v_j$) is equal to $\vv{n}_{(0,j)}
  -\vv{n}_{(0,i)}$. Therefore, by Lemma~\ref{lm:row-sum}, it is
  enough to find a factorization of the the submatrix of $\mm{N}$ with
  columns indexed by pairs $(0,i)$. For the rest of the proof, we will
  replace $\mm{N}$ with this submatrix. 

  Let us now form a graph $T'$ on the same vertex set as $T$, so that
  for each edge $e$ in $T$, $T'$ contains both $e$ and $-e$ (i.e.~$e$
  with the direction reversed). We denote by $E$ the edges of $T$, and
  by $-E$ the reversed edges, so that the edges of $T'$ are $E \cup
  -E$. We perform an Eulerian walk on $T'$ starting from $v_0$, and
  list the edges $E \cup -E$ in the order they are encountered during
  the walk. Let $t_i$ be the edges encountered in the Eulerian walk
  until the first time vertex $v_i$ is visited.  Let $\mm{A}$ be a $2n
  \times n$ matrix with rows indexed by $E \cup -E$, columns indexed
  by $1, \ldots n$, and the column indexed by $i$ equal to the
  indicator vector of $t_i$. $\mm{A}$ is a submatrix of the $2n \times
  2n$ upper triangular matrix with $1$'s on and above the main
  diagonal. It is well-known (see e.g.~\cite{Fredman82}) that matrices $\mm{A}$
  of this form have a factorization $\mm{A} = \mm{Q}_A \mm{D}_A$ such
  that $\nzrow(\mm{Q}_A) = O(\log n)$ and $\nzcol(\mm{D}_A) = O(\log
  n)$.

  Notice that the path $p_{0,i}$ is exactly the subset of edges $e$ in
  $t_i$ for which $-e \not \in t_i$. Therefore, if we take $\mm{A}_+$ to
  be the submatrix of $\mm{A}$ with rows indexed by $E$ and $\mm{A}_-$
  be the submatrix with rows indexed by $-E$, we have that, after
  apppropriately identifying rows corresponding to $E$ and $-E$,
  $\mm{N} = \mm{A}_+ + \mm{A}_-$. The lemma now follows from the
  factorization of $\mm{A}$ above, and Lemma~\ref{lm:row-sum}.
%	Suppose we had a decomposition $\mm{N} = \mm{Q} \cdot \mm{D}$ and weights $\vv{w} \in \RR^{n+1}$.  Then each entry of $\mm{D}\vv{w}$ computes the sum of various objects. Updating some entry $w_i$ of $\vv{w}$, is equivalent to recomputing $\vv{d}_iw_i$ where $\vv{d}_i$ is column $i$ of $\mm{D}$. Thus $\nzcol(\mm{D})$ is an upper bound on the number of group operations required for an update. Each entry of $\mm{Q}\cdot(\mm{D}\mm{v})$ computes the sum of several objects. Querying the region corresponding to $\vv{q}_j^{\top}$, row $j$ of $\mm{Q}$, is equivalent to computing $\vv{q}_j^{\top}\cdot(\mm{D}\mm{v})$. Thus $\nzrow(\mm{Q})$ is an upper bound on the number of group operations required for a query. Thus decomposing $\mm{N}$ is equivalent to constructing a data structure for the range-search problem --- ranges are root-to-node paths --- with query and update time on the order of $O(\log mn)$. 
%	Recall such a data structure when $T$ is a path. If vertices are labeled $v_0, ..., v_{n}$ from root-to-leaf, then construct an object for every set of vertices whose index prefix --- written in binary --- are identical. Since the indices are of length $\log n$ and there are $2^c$ possible prefixes of length $c$, there are $O(n\log n)$ objects in total with each vertex appearing in $O(\log n)$ objects. Querying any $p_{0,i}$ path requires summing $O(\log n)$ object. 
%	For a directed tree $T$ with root $v_0$, construct a list of vertices by traversing $T$ in post-fix order. Every vertex will appear twice. Let the first and second appearances correspond to coefficients $1$ and $-1$ in the object sums respectively. Apply the path construction to the list of vertices. Since the list is of length $2(n+1)$, there will be $O(n \log n)$ objects with each vertex appearing in $O(\log n)$. Observe that every root-to-leaf path in $T$ can be constructed by taking a prefix of the list. Thus querying any path $p_{0,i}$ also requires summing $O(\log n)$ objects. 
\end{proof}

\subsection{Decomposing Outputs of Various Operations}
To show that every step from leaf to root in the decomposition tree does not add too many non-zero entries to the rows (columns) of the decomposition, let us consider standard matrix operations, sum operations, and pivoting in turn.

\subsubsection{Standard Matrix Operations}
\begin{lemma}{(Standard Matrix Operations Decomposition.)}
	\label{lem:MatrixOpDecomp}
	Let $f$ be any standard matrix operation. Then for TU matrix
        $\mm{A} = \mm{Q}_A\mm{D}_A$, the result of these operation on
        $\mm{A}$, denoted $f(\mm{A})$, can be decomposed as
        $\mm{Q}\mm{D}$ such that $\max\{\nzrow(\mm{Q}), \nzcol(\mm{D})\}
        \le \max\{\nzrow(\mm{Q}_A),\nzcol(\mm{D}_A)\}$. 
\end{lemma}
\begin{proof}
  If $f$ is the transposition operation, then $f(\mm{A}) =
  \mm{D}^{\top}_{A}\mm{Q}^{\top}_{A}$. Thus $\nzrow(\mm{D}^{\top}_A) =
  \nzcol(\mm{D}_A)$ and $\nzcol(\mm{Q}^{\top}_A) =
  \nzrow(\mm{Q}_A)$. If $f$ is the row (column) negation operation
  then $f(\mm{A}) = \mm{I}'\mm{Q}_A\mm{D}_A$ where $\mm{I}'$ is the
  identity matrix with the corresponding entry negated. Thus
  $\nzrow(\mm{I}'\mm{Q}_A) = \nzrow(\mm{Q}_A)$. Finally, consider the
  case where $f$ adds an auxiliary row (column) to $\mm{A}$. Recall
  that all matrices are assumed to have the identity matrix as a
  subset of its columns. We can further assume that all atomic
  matrices already have a row (column) of all zeroes and all
  ones. Thus it suffices to only consider adding row $\vv{u}_i^{\top}$
  to $\mm{A}$ where $\vv{u}_i^{\top}$ is equivalent to row $i$ of
  $\mm{A}$. Let us find a decomposition for $\mm{A}' = [\mm{A}^{\top};
  \mm{u}_i]^{\top}$. Let $\vv{a}_{i}$ be the $i$\textsuperscript{th}
  column of $\mm{D}_A$. Then $\mm{A}' = \mm{Q}_A\mm{D}'$ where
  $\mm{D}' = [\mm{D}_A; \vv{a}_i]$. Thus $\nzcol(\mm{D}') =
  \nzcol(\mm{D}_A)$.
\end{proof}

\subsubsection{Sum Operations}
We do the same for the output of various sum operations with respect to their inputs.
\begin{lemma}{(Sum Operations Decomposition.)}
	\label{lem:SumOp}
	Let $\mm{B}_1 = \mm{Q}_1\mm{D}_1$ and $\mm{B}_2 =
        \mm{Q}_2\mm{D}_2$ be TU matrices. Then the $k$-sum of $\mm{B}_1$ and $\mm{B}_2$ for $k \in \{1,2,3\}$ can be decomposed into $\mm{Q}\mm{D}$ such that 
	\[\nzrow(\mm{Q}) \leq \max\left(\nzrow(\mm{Q}_1), \nzrow(\mm{Q}_2)\right) + 2 \mbox{ and } \nzcol(\mm{D}) \leq \max\left(\nzcol(\mm{D}_1), \nzcol(\mm{D}_2)\right) + 2.\] 
\end{lemma}
\begin{proof}
	Let $\mm{A}_1$ and $\mm{A}_2$ be submatrices of $\mm{B}_1$ and $\mm{B}_2$ under consideration. Since we have decompositions for $\mm{B}_1$ and $\mm{B}_2$, we can decompose $\mm{A}_1 = \mm{Q}_1'\mm{D}_1'$ and $\mm{A}_2 = \mm{Q}_2'\mm{D}_2'$. Note that $\nzrow(\mm{Q}_1') \leq \nzrow(\mm{Q}_1)$, $\nzrow(\mm{Q}_2') \leq \nzrow(\mm{Q}_2)$, $\nzcol(\mm{D}_1') \leq \nzcol(\mm{D}_1)$, $\nzcol(\mm{D}_2') \leq \nzcol(\mm{D}_2)$ so it suffices to show the bounds in-terms of $\mm{Q}_1'$, $\mm{Q}_2'$, $\mm{D}_1'$, and $\mm{D}_2'$.
	
	The decomposition for the $1$-sum of $\mm{B}_1$ and $\mm{B}_2$ is shown in Equation \eqref{eq:tikz:1SumDecomp}. Note that $\nzrow(\mm{Q}) = \max\{\nzrow(\mm{Q}_1'), \nzrow(\mm{Q}_2')\}$ and $\nzcol(\mm{D}) = \max\{\nzcol(\mm{D}_1'), \nzcol(\mm{D}_2')\}$.
	\teq{1SumDecomp}{1}
	
	Next, the decomposition for the $2$-sum of $\mm{B}_1$ and $\mm{B}_2$ is shown in Equation \eqref{eq:tikz:2SumDecomp}. Note that $\nzrow(\mm{Q}) = \max\{\nzrow(\mm{Q}_1') + 1, \nzrow(\mm{Q}_2')\}$ and $\nzcol(\mm{D}) = \max\{\nzcol(\mm{D}_1'), \nzcol(\mm{D}_2') + 1\}$.
	\teq{2SumDecomp}{1.1}
	
	Finally, the decomposition for the $3$-sum of $\mm{B}_1$ and $\mm{B}_2$ is shown in Equation \eqref{eq:tikz:3SumDecomp} and the decomposition for the alternate variant is shown in Equation \eqref{eq:tikz:3SumAltDecomp}. In the former cases, $\nzrow(\mm{Q}) = \max\{\nzrow(\mm{Q}_1')+2, \nzrow(\mm{Q}_2')\}$ and $\nzcol(\mm{D}) = \max\{\nzcol(\mm{D}_1')+2, \nzcol(\mm{D}_2')\}$. And in the latter case, $\nzrow(\mm{Q}) = \max\{\nzrow(\mm{Q}_1')+1, \nzrow(\mm{Q}_2')+1\}$ and $\nzcol(\mm{D}) = \max\{\nzcol(\mm{D}_1')+1, \nzcol(\mm{D}_2')+1\}$.
	\teq{3SumDecomp}{1}
	\teq{3SumAltDecomp}{1}
	
	The transpose of these $k$-sum operations can be similarly decomposed.
\end{proof}

