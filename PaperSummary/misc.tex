% !TEX root = 00.tex

\section{Probability}
\subsection{$m$ Balls in Every Bin (Erd\"{o}s \cite{erdHos1961classical}, 1961)}
An old result of Erd\"{o}s which analyzes a particular balls and bins model \cite{erdHos1961classical}. 

\section{Computational Geometry}
\subsection{Voronoi Cells of Lattices wrt Arbitrary Norms (Bl\"{o}er and Kohn \cite{blomer2018voronoi}, 2018)}
Given a lattice $\Lambda$ and a norm $\norm{\cdot}$, the authors consider the Voronoi cell about the origin:
\[C_{0} = \mathcal{V}(\Lambda, \norm{\cdot}) = \{\vv{x} \in \RR^{n}| \forall \vv{v} \in \Lambda: \norm{\vv{x}} \leq \norm{\vv{x} - \vv{v}}\}.\]
It is known that $C_0$ is centrally symmetric and star-shaped. For the Euclidean norm, $C_{0}$ has at most $2(2^{n}-1)$ facets. ``In particular, in the definition of the Voronoi cell of a lattice in $\RR^{n}$ only $2(2^{n}-1)$ lattice vectors $\vv{v}$ need to be considered in the definition of $C_0$. These are the \emph{Voronoi-relevant} vectors. Unfortunately it is $\#\class{P}$ hard to determine the exact number of facets of $C_0$. 

It was shown that every lattice $\Lambda \subset \RR^{n}$ and every strictly convex\footnote{A norm $\norm{\cdot}: \RR^{n} \rightarrow \RR_{\geq 0}$ is \emph{strictly convex} if for all $\vv{x}, \vv{y} \in \RR^{n}$ with $\norm{\vv{x}} = \norm{\vv{y}}$ and all $t \in (0,1)$: $\norm{t\vv{x} + (1-t)\vv{y}} < \norm{\vv{x}}$.} has $C_0$ equivalent to 
\[\tilde{\mathcal{V}}(\Lambda, \norm{\cdot}) = \left\{\vv{x} \in \RR^{n}| \forall \vv{v} \in \Lambda \mbox{ Voronoi-relevant with respect to } \norm{\cdot}\right\}.\]
Further, there is a bijection between Voronoi-relevant vectors and facets of the Voronoi cell $C_0$. For the Euclidean norm, this give us the required bound on the complexity of $C_0$. However For other norms, such as $\metric{3}$, there exists a lattice $\Lambda_k$ in $\RR^{3}$ such that there are at least $k$ Voronoi-relevant vectors. Generally, for strictly convex and smooth norms $\norm{\cdot}$, the number of Voronoi-relevant vectors of $\Lambda$ with respect to $\norm{\cdot}$ cannot be bounded by a function depending on $n$ and $\norm{\cdot}$ alone. 



\section{Number Theory}	
\subsection{Equal Pairwise Sums Partitioning of \texorpdfstring{$\NN$}{N} (Lambek and Moser \cite{lambek1959some}, 1959)}
This is a pretty light-hearted result of Lambek and Moser (1959) which partitions the natural number into two sequences $A = \{a_i\}$ and $B = \{b_j\}$ such that $0 = a_1 < a_2 < a_3 < \cdots$ and $b_1 < b_2 < b_3 < \cdots$ such that the sums of pairs of numbers from $A$ are the same (with respect to multiplicity) as the sums of pairs of number from $B$.

The proof is a standard generating function argument. Let $F(x) = \sum_{i = 1}^{\infty} x^{a_i}$ and $G(x) = \sum_{i = 1}^{\infty} x^{b_i}$. Then $F(x) + G(x) = (1 - x)^{-1}$. Define $H(x) = F(x) - G(x)$. By the sum condition we also have 
\[F(x)^2 - F(x^2) = G(x)^2 - G(x^2)\]
since $[x^k] F(x)^2$ is the number of ways to make $k$ using two terms of $A$\footnote{When $k$ is even, it can be made from the sum of two identical terms, but this gets counted twice. Thus we need to subtract $F(x^2)$.}. The same is true of $G(x)^2$ and $G(x^2)$. Rearranging, we have
\[F(x)^2 - G(x)^2 = (F(x) + G(x))\cdot(F(x) - G(x)) = (1 - x)^{-1}H(x) = H(x^2).\] 
This give us the recurrence relation $(1-x) = H(x)/H(x^2)$. Multiplying together $n$ such ratios and taking the limit as $n \rightarrow \infty$, 
\[\lim_{n\rightarrow \infty} \frac{H(x)}{H(x^{2})}\cdots\frac{H(x^{2^{n-1}})}{H(x^{2^{n}})} = (1 - x)(1-x^2)(1-x^4)(1-x^8)\cdots = \sum_{n = 0}^{\infty}(-1)^{\epsilon(n)}x^{n}\]
where $\epsilon(n)$ is the number of ones in the binary representation of $n$. 

Finally we write $F(x)$ in-terms of $H(x)$ only. 
\[2F(x) = H(x) + (1 - x)^{-1} = \sum_{n = 0}^{\infty}(-1)^{\epsilon(n)}x^{n} + \sum_{n = 0}^{\infty}x^{n}.\]
Note that the only non-zero terms of $F(x)$ are those entries where $\epsilon(n)$ is even. Thus $A = \{0, 3, 5, 6, ...\}$ and $B = \{1, 2, 4, 7, ...\}$.

We can ask the same question of a finite set $\{0,1,2,...,n\}$. The argument is much the same as above, except that we would be using the closed form of the partial sum instead of $(1-x)^{-1}$. Further the question can be asked of the products of pairs of positive integers i.e. sequences $A$ and $B$ as above with $1 = a_1 < a_2 < a_3 < \cdots$ and $b_1 < \cdots$ such that the products of pairs of elements of $A$ are the same as product of pairs of elements of $B$. Here we use the Dirichlet series $F(x) = \sum_{i = 1}^{\infty} a_i^{-s}$ and $G(x) = \sum_{i = 1}^{\infty}b_i^{-s}$ with the observation that 
\[F(x) + G(x) = \sum_{n = 1}^{\infty} n^{-s} = \prod_{p}(1- p^{-s})^{-1}\]
for primes $p$ by one of Euler's identities. As before, if we define $H(x) = F(x) - G(x)$, the $H(x)/H(2x) = \prod_{p}(1-p^{-s})^{-1}$. Taking a product over all such ratios, 
\[H(x) = \prod_p\prod_{k=0}(1 - p^{-2^{k}})^{-1} = \prod_p(1 + \alpha(p)p^{-s} + \alpha(p^2)p^{-2s} + \alpha(p^{3})p^{-3s} + \cdots) = \sum_{n \geq 1} \alpha(n)n^{-s}\]
where $\alpha(n)$ is the number of ones in the binary representations of the exponents of $n$ i.e. if $n = p_{1}^{e_1}p_{2}^{e_2}\cdots p_{r}^{e_r}$ the $\alpha(n) = \sum_{i = 1}^{r} \epsilon(e_i)$. Rearranging as before, we have that $A$ contains all numbers with $\alpha(n)$ even and $B$ all $\alpha(n)$ odd\footnote{Note that it is not possible to partition the finite list $[n]$ into two such that the product of pairs is the same. Consider the last four entries entries. Either $n$ and $n-1$ are in the same or different parts. If they are in the same parts then no sum of pairs of the other part can ever be as large as $n(n-1)$. If they are in different parts then $n-1$ and $n-2$ must be in the same part for similar reasons. However $n(n-3) < (n-1)(n-2)$.}.
