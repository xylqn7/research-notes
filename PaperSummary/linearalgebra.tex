% !TEX root = 00.tex

\section{Linear Programs}
\subsection{Small-Dimensional LP in \texorpdfstring{$O(n)$}{O(n)} wrt Constraints (Seidel \cite{seidel1991small}, 1991)}
Seidel (1991) gives a randomized algorithm which solves linear programming on matrices with $m$-constraints and $d$ variables in $O(d!m)$ time. Using this LP solver, Seidel also constructs the convex hull of $n$ points in $\RR^{d}$ in expected $O(n^{\floor{d/2}})$ time. The first moment analysis for the LP solver is super simple (but the variance analysis is quite a bit worse).
\subsubsection*{Linear Program Solver}
Assume that the points are in general position. Take as input a matrix $\mm{A} \in \RR^{m \times d}$ and a vector $\vv{c} \in \RR^{d}$. We find an $\vv{x} \in \RR^{d}$ which maximizes $\vv{c}^{\top}\vv{x}$ in times $O(d!m)$. Assume, for now, that the constraint polytope of $\mm{A}$ is bounded, non-empty, and that the unique optimum solution $\vv{x}^{*}$ of $\mm{A}$ has exactly $d$ tight constraints. We consider the other cases at the end. Let $\mathcal{H}$ be the set of $m$ half-spaces which correspond to the $m$ constraints of $\mm{A}$. Let $P_{\mathcal{H}}$ be the constraint polytope which is the intersection of the half-spaces of $\mathcal{H}$. The recursive algorithm proceeds as follows:
\begin{enumerate}
	\item Select a constraint $H$ uniformly at random and let $\mathcal{H}' = \mathcal{H} - H$. 
	\item Recursively solve the LP with constraint polytope $P_{\mathcal{H}'}$ and objective function $\vv{c}$. Let $\vv{x}$ be output.
	\item Let $h$ be the hyperplane associated with $H$. Determine if $\vv{x} \in h$. If so, then $\vv{x} = \vv{x}^*$. Otherwise we will compute $\vv{x}^{*}$ from $\vv{x}$ as follows.
	\item In $\vv{x} \notin h$, then the optimum solution must be on $h$. Thus we reduce this to a simpler version of the LP by restrict our attention to $h$. The half-spaces in question are $\overline{\mathcal{H}'} = \{G \cap h: G \in \mathcal{H}'\}$, and the objective function is the projection of $\vv{c}$ onto $h$. This $LP$ has $d-1$ variables and $m-1$ constraints.   
\end{enumerate} 
Let $T(m, d)$ be the running time of this algorithm on a real matrix of dimension $m \times d$. Then we have the following recurrence relation for $T(m,d)$:
\begin{align*}
	T(m,d) = \begin{cases}
		O(m) &\mbox{if $d = 1$}\\
		O(d) &\mbox{if $m = 1$}\\
		T(m-1, d) + O(d) + \frac{d}{m}\cdot\left(O(md) + T(m-1, d-1)\right) &\mbox{otherwise}
	\end{cases}
\end{align*}
The first two cases reflect the running time of the algorithm when the recurrence bottoms out. The third case is split into two parts, the portion which is not multiplied by $\frac{d}{m}$ and the part which is. The first part is the cost incurred at each step regardless of the choice of $H$: these are steps two and three of the algorithm. We need to remove $H$ from $\mathcal{H}$ and recursively solve the LP with $m-1$ constraints on $d$ variables. The remaining cost depends on our choice of $H$. Only when $H$ is a tight constraint for $\vv{x}^*$ do we have $\vv{x} \neq \vv{x}^*$. Since $\vv{x}^*$ is tight on exactly $d$ constraints, with probability $\frac{d}{m}$ do we have to execute step four. Thus the stuff inside the brackets multiplying $\frac{d}{m}$ --- the probability that step four is executed --- is exactly the cost of executing step four. 

Solving the recurrence relation, we see that $T(m,d) = O(md!)$ as required. The crucial step is to relate $T(m,d)$ with $T(m-1, d)$. If you squint, the recurrence relation amounts to $T(m,d) = T(m-1, d) + O(d) + O(d^2) + O(d!)$ by induction so for every constraint we have to do, in expectation about $O(d!)$ more work. 

It remains to consider cases where the LP is infeasible or unbounded. We can detect the former case when the recurrence processes the base case $T(m, 1)$. We can approximate the latter case by adding additional constraints so that the polytope fits within some bounding box $B_{\alpha}$. If the optimum solution was unbounded, then it will now be a vertex of $B_{\alpha}$. 

\subsubsection*{Constructing the Convex Hull of Points}
Suppose that $V$ is the set of vertices\footnote{A word regarding terminology. For polytopes in $\RR^{d}$, the $(d-1)$-dimensional faces are \emph{facets}, the $(d-2)$-dimensional faces are \emph{ridges}, the $1$-dimensional faces are \emph{edges}, and the $0$-dimensional faces are vertices of the polytope.} of the $d$-polytope (i.e. each facet of the polytope has exactly $d$ vertices on its boundary) $P \in \RR^d$ that we wish to construct and let $V = n$. Let $\mathcal{F}(P)$ be the facet graph of $P$ i.e. every facet is a vertex of $\mathcal{F}(P)$ corresponding to $d-1$ points of $V$ and edges correspond to ridges incident to a pair of facets. \emph{We want to construct $\mathcal{F}(P)$ for $V$ starting from the empty graph.} 

Let us first define a notion of \emph{visibility} for each facets of $P$ with respect to some $\vv{x} \in \RR^{d}$. A facet $F$ is visible if the bounding hyperplane of $F$ separates $\vv{x}$ from $P$ (intuitively, you can see $F$ from $\vv{x}$). Otherwise $F$ is obscure. For lower dimensional faces, a face $G$ is \emph{visible} if all its incident facets are visible, \emph{obscure} if all its incident facets are obscure, and \emph{horizontal} otherwise. 

The algorithm iteratively adds the vertices of $V$ to an initially empty graph in a random order. The initial $d + 1$ vertices form a simplex. For every additional vertex $\vv{v}$, we first determine if $\vv{v}$ is in the partially constructed polytope. If not, then we find a facet $F$ visible from $\vv{v}$. Having found $F$, we perform a depth-first-search in the facet graph until we have found all visible facets and ridges and all horizontal ridges. Remove all visible facets and ridges. Form a facet with every horizontal ridge and the vertex $\vv{v}$\footnote{It is actually a pretty interesting data-structure problem of doing this efficiently.}. Add edges to the facet graph by walking along the ridges of the newly created facets. 

Observe that finding a visible facet $F$ or determining that $\vv{v}$ is inside the polytope is essentially solving an LP where each facet corresponds to a constraint. For fixed $d$, solving this LP takes expected $O(d)$ time. You should reread the paper and summarize the total running time of this algorithm.