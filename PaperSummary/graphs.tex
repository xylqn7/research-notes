% !TEX root = 00.tex

\section{Hardness Results}
\subsection{$3$-Coloring of Hyper-graphs is Hard (Lovasz)}
Lov\'{a}sz showed that graph 3-colorability is as hard as graph $k$-colorability by the following sequence of reductions
\[\class{COL} \leq_{p} \class{H-2COL} \leq_{p} \class{3COL}.\] 
Lov\'{a}sz also states --- but mostly does not prove --- several sufficient conditions and necessary conditions for hyper-graphs to be 2-chromatic\cite{lovasz1975ratio}. Concluding the report he alludes to some results that he and Erd\H{o}s are working on regarding matching and vertex cover on hypergraphs \footnote{These are publish in \cite{erdHos1973problems} which may be a worthwhile read.}. 

\problem{Graph Colorability}{COL}{
	Let $G = (V,E)$ be a graph and $k \in \NN$.
}{Is $G$ $k$-colourable?
}{The problem can be reduced to Hypergraph 2-Colorability then to $\class{3COL}$ as did Lov\'{a}sz in this paper or directly to $\class{3COL}$ as described by Chv\'{a}tal in his notes (see \emph{Remark} below).}

\problem{Hypergraph 2-Colorability}{H-2COL}{
	Let $H = (V,E)$ be a hypergraph where $V$ are the vertices and $E$ are the edges where for $e \in E$, $e \subset V$.
}{Does there exists a two coloring of the vertices such that no edge is monochromatic?
}{Lov\'{a}sz reduced this problem $\class{3COL}$.}

\subsubsection*{Details}
The main result consists of two reductions. There are issues with both these reductions as written so we describe Lov\'{a}sz intended reductions.
\begin{enumerate}
	\item $\class{COL} \leq_{p} \class{H-2COL}$: Let $G = (V,E)$ and $k \in \NN$ be given. We will construct hypergraph $H$ as follows. Create $k$ isomorphic copy of $G$ denoted $G_i$ for every $i \in [k]$. Let $V = \{v_1, ..., v_n\}$ be the vertex set of $G$. Then $V_i = \{v_{i,1}, ..., v_{i,n}\}$ denotes the vertex set of $G_i$. Introduce another vertex $g$. Then the vertex set of $H$ is
	\[V_H = \left(\bigcup_{i = 1}^{k} V_i\right) \cup \{g\}.\]
	Let $Eg$ denote the hyperedges $\{\{u,v,g\}: \mbox{ for edges } uv \in E\}$. And let $f_j$ denote the hyperedge $\{v_{1,j}, ..., v_{k,j}\}$ for $j \in [n]$. Then the edge set of $H$ is 
	\[E_H = \left(\bigcup_{i=1}^{k}E_ig\right) \cup \left\{f_1, ..., f_n\right\}.\]
	Here is why the reduction works:
	\begin{enumerate}
		\item \textbf{Sound}: Consider a $k$-colorable graph $G$. We will find a red-green coloring of $H$. Let $\chi: V \rightarrow [k]$ be a valid $k$-coloring of $G$. For every $v_{i,j} \in V_H$ color $v_{i,j}$ green if $\chi(v_j) = i$ and $v_{i,j}$ red otherwise. Further color vertex $g$ green. Observe that for every edge $f_j$, exactly one entry of $f_j$ will be green since $v_j$ is given some color by $\chi$. Thus none of the edges $f_j$ for $j \in [n]$ is monochromatic. Consider next a hyperedge $e_i = \{u_i,v_i,g\}$ where $u_iv_i \in E_i$. Since $\chi$ is a valid coloring, $u_i$ and $v_{i}$ will not both be colored green. Thus $e_i$ will not be monochromatic since it contains at least one red and one green entry.
		\item \textbf{Complete}: Suppose $H$ has a 2-coloring $\chi_H: V_H \rightarrow \{red, green\}$. We want to define a valid $k$-coloring $\chi: V \rightarrow [k]$. W.l.o.g suppose that $\chi_H(g) = green$. Since $\chi_H$ is a proper 2-coloring of $H$, each $f_j$ has some entry $v_{i,j}$ such that $\chi_H(v_{i,j}) = green$. Color $\chi(v_j) = i$. Observe that every vertex of $G$ gets a color. Further no edge $uv \in E$ can have the same color $i$. If they did, hyperedge $\{u_i,v_i,g\}$ would be monochromatic in $E_H$.
	\end{enumerate}
	Consider the example graph $G$ shown in Figure \ref{graph:k3}. To see if $G$ is $2$-colorable, we can reduce it to in hypergraph $H$ shown in Table \ref{table:redux2colk3}.
	
	\begin{figure}[ht]
		\centering
		\begin{tikzpicture}
		\tikzstyle{every state}=[draw,circle,fill=black,minimum size=4pt, inner sep=0pt]
		\node[state] (v_1) [label=above:$v_1$] {};
		\node[state] (v_2) [below left=of v_1, label=left:$v_2$] {};
		\node[state] (v_3) [below right=of v_1, label=right: $v_3$] {};
		\path[-]
		(v_1) 	edge[swap] 	node {$e_1$} (v_2)
		edge		node {$e_3$} (v_3)
		(v_2)	edge[swap] 	node {$e_2$} (v_3);
		\end{tikzpicture}
		\caption{Input graph $G$ to $\class{COL}$ which we want to $2$-color.}
		\label{graph:k3}
	\end{figure}
	
	\begin{table}[ht]
		\centering
		\begin{tabular}{|c|c|c|c|c|c|c|c|}
			\hline
			& $v_{1,1}$ & $v_{1,2}$ & $v_{1,3}$ & $v_{2,1}$ & $v_{2,2}$ & $v_{2,3}$ & $g$\\ \hline
			$e_{1,1}$ & $1$ & $1$ & & & & & $1$\\ \hline
			$e_{1,2}$ & & $1$ & $1$ & & & & $1$\\ \hline
			$e_{1,3}$ & $1$ & & $1$ & & & & $1$\\ \hline
			$e_{2,1}$ & & & & $1$ & $1$ & & $1$\\ \hline
			$e_{2,2}$ & & & & & $1$ & $1$ & $1$\\ \hline
			$e_{2,3}$ & & & & $1$ & & $1$ & $1$\\ \hline
			$f_{1}$ & $1$ & & & $1$ & & &\\ \hline
			$f_{2}$ & & $1$ & & & $1$ & &\\ \hline
			$f_{3}$ & & & $1$ & & & $1$ &\\ \hline
		\end{tabular}
		\caption{Reduction $\class{COL} \leq_{p} \class{H-2COL}$ for graph shown in Figure \ref{graph:k3}.}
		\label{table:redux2colk3}
	\end{table}
	
	\item $\class{H-2COL} \leq_{p} \class{3COL}$: Let $H = (V_H, E_H)$ be any hypergraph. We will construct a graph $G_3$ which is 3-colorable if and only if $H$ is 2-colorable. For hyperedge $e \in E_H$, let $|e|$ be the number of non-zero entries of $e$. Construct for each edge $e$ an odd cycle $C_e$ such that the length of $C_e$ is greater than or equal to $|e|$. Let $b$ be another vertex. The vertex set of $G_3$ is 
	\[V_3 = V_H \cup \left(\bigcup_{e \in E_H}V(C_e)\right) \cup \{b\}\]
	where $V(C_e)$ denotes the vertices of $C_e$. Let $B$ be the set of edges $\{bv: v \in V_H\}$ between $b$ and the vertices of $H$. Let $T_e$ be a set of surjective edges between $V(C_e)$ and the entries of $e$. Then the edge set of $G_3$ is 
	\[E_3 = B \cup \left(\bigcup_{e\in E_H}T_e\right).\]
	Here is why the reduction works:
	\begin{enumerate}
		\item \textbf{Sound}: Suppose that there is a valid 2-coloring of $H$. Then $V_H$ can be colored using red and green such that every cycle is adjacent to both red and green vertices. Thus, using all three colors (red, green, blue) it is possible to color each odd cycle $C_e$. Finally color $b$ blue since it is only adjacent to green and red vertices. 
		\item \textbf{Complete}: Let $\chi_3: V_3 \rightarrow \{red,green,blue\}$. W.l.o.g suppose that $\chi_3(b) = blue$. Then $V_H$ can be colored using only red and green. If any cycle is adjacent to vertices of a single color, w.l.o.g. green, then the odd cycle must be colored using only red and blue which is impossible. 
	\end{enumerate}
	Consider the hypergraph shown in Table \ref{table:h2colinstance}. The graph generated by the reduction can be seen in Figure \ref{graph:redux}.
	
	\begin{table}[ht]
		\centering
		\begin{tabular}{|c|c|c|c|}
			\hline
			& $v_{1}$ & $v_{2}$ & $v_{3}$\\ \hline
			$e_{1}$ & $1$ & $1$ &\\ \hline
			$e_{2}$ & & $1$ & $1$\\ \hline
			$e_{3}$ & $1$ & & $1$\\ \hline
			$e_{4}$ & & & $1$\\ \hline
			$e_{5}$ & $1$ & &\\ \hline
		\end{tabular}
		\caption{Input to an $\class{H-2COL}$ instance.}
		\label{table:h2colinstance}
	\end{table}
\end{enumerate}

\begin{figure}[ht]
	\centering
	\begin{tikzpicture}
	\tikzstyle{every node}=[draw,circle,fill=black,minimum size=4pt, inner sep=0pt]
	\draw (0,5) node (b) [label=above:$b$] {}
	--(0,3) node (e_3) [label=above left:$e_3$] {};
	\draw (0,5) -- (-2,3) node (e_2) [label=above left: $e_2$] {};
	\draw (0,5) -- (-4,3) node (e_1) [label=above left: $e_1$] {};
	\draw (0,5) -- (2,3)  node (e_4) [label=above right: $e_4$] {};
	\draw (0,5) -- (4,3)  node (e_5) [label=above right: $e_5$] {};
	\draw (-5,1) node (C_11) [label=left: $c_{1,1}$] {}
	--(-4,1) node (C_12) [label=below: $c_{1,2}$] {};
	\draw (-4,1) -- (-3,1) node (C_13) [label=below right: $c_{1,3}$] {};
	\draw (-1,1) node (C_21) [label=below left: $c_{2,1}$] {}
	--(0,1) node (C_22) [label=below: $c_{2,2}$] {};
	\draw (0,1) -- (1,1) node (C_23) [label=below right: $c_{2,3}$] {};
	\draw (3,1) node (C_31) [label=below left: $c_{3,1}$] {}
	--(4,1) node (C_32) [label=below: $c_{3,2}$] {};
	\draw (4,1) -- (5,1) node (C_33) [label=right: $c_{3,3}$] {};
	\draw (-5,1) to [out=-90, in=-90] (-3,1);
	\draw (-1,1) to [out=-90, in=-90] (1,1);
	\draw (3,1) to [out=-90, in=-90] (5,1);
	\draw (-5,1) -- (-4,3);
	\draw (-4,1) -- (0,3);
	\draw (-3,1) -- (4,3);
	\draw (-1,1) -- (-4,3);
	\draw (0,1) -- (-2,3);
	\draw (1,1) -- (-2,3);
	\draw (3,1) -- (-2,3);
	\draw (4,1) -- (0,3);
	\draw (5,1) -- (2,3);
	\end{tikzpicture}
	\caption{Input graph $G$ to $\class{COL}$ which we want to $2$-color.}
	\label{graph:redux}
\end{figure}

\subsubsection*{Observations}
This reduction, though short, was quite difficult for me to comprehend. It certainly did not help that Lov\'{a}sz was very terse in his writing, offering little explanation for the soundness and completeness of his reductions and that both reduction presented in the paper were \emph{wrong}. For $\class{COL} \leq_{p} \class{H-2COL}$ the edge sets were defined incorrectly. Instead of hyper-edges $f_i = \{x_{1,v}, ..., x_{k,v}, g\}$ as written, it should be $f_i = \{x_{1,v}, ..., x_{k,v}\}$ and instead of edges $\{u, v\} \in E(G_i)$ for all edges in the $k$ isomorphic copies of $G$, we should instead have $\{u, v, g\}$. For $\class{H-2COL} \leq_{p} \class{3COL}$, Lov\'{a}sz forgot to add the extra vertex $b$ adjacent to all the vertices in $V_H$. Without it, you could three color the vertices in $V_H$.

To be honest, I had to work backwards from \href{https://users.encs.concordia.ca/~chvatal/notes/color.html}{Chv\'{a}tal's explanation}. Instead of reducing $\class{COL}$ to $\class{H-2COL}$, then reducing $\class{H-2Col}$ to $\class{3COL}$ as did Lov\'{a}sz, Chv\'{a}tal reduced $\class{COL}$ to $\class{3COL}$ directly. This exposition made much more sense to me and I was was able to recover Lov\'{a}sz' intended reduction from there. Thank you Chv\'{a}tal!

His reduction is as follows. Take a graph $G = (V,E)$ and a $k \in \NN$. We will form a graph $F = (V_F, E_F)$ such that $G$ is $k$-colorable if and only if $F$ is $3$-colorable (using colors $red, blue, green$). W.l.o.g suppose $k$ is odd (if not, simply add an extra vertex $v'$ to $G$ and add edges from every vertex to $v'$, now ask if $G$ is $(k+1)$-colorable). $V_F$ will consist of the following types of vertices
\begin{itemize}
	\item A $k \times n$ grid of vertices labeled $v_{i,j}$ for $i \in [k]$ and $j \in [n]$.
	\item For each column $j$ add vertices $c_{1,j}, ..., c_{k,j}$ (eventually we will form a cycle from these).
	\item For every edge $rs \in E$ and all $i \in [k]$ add vertices $\{x_{i,s,t}, y_{i,s,t}, z_{i,s,t}\}$ (these will also be connected together in a cycle).  
	\item A vertex $g$, which will eventually be colored $green$.
	\item A vertex $b$, which will eventually be colored $blue$. 
\end{itemize}
And $E_F$ will consist of the following type of edges:
\begin{itemize}
	\item Add an edge $v_{i,j}b$ for every $i \in [k]$ and $j \in [n]$. Now every vertex in the grid is connected to $b$.
	\item Form a $k$-cycle from $c_{1,j}, ..., c_{k,j}$ for $j \in [n]$.
	\item Add all edges edges $v_{i,j}c_{i,j}$. This connects the cycles to the grid of vertices.
	\item Form a $3$-cycle from $\{x_{i,s,t}, y_{i,s,t}, z_{i,s,t}\}$. Further, add edges $v_{i,s}x_{i,s,t}$ and $v_{i,t}y_{i,s,t}$ for all $i \in [k]$.
	\item Finally add edge $bg$.
\end{itemize}

\fig{lovasz1973coveringsandcolorings}{0.75}{Chv\'{a}tal's direct rediction from $\class{COL}$ to $\class{3COL}$.}

See Figure \ref{fig:lovasz1973coveringsandcolorings} for a visual of the reduction. There are a couple key properties to keep in mind: there are four different types of vertices: those in the grid, those in the cycle attached to each grid, those in the triangle edge enforcer gadgets, and those with a fixed coloring, namely $b$ and $g$. W.l.o.g $b$ is colored blue. Since all vertices in the grid are adjacent to $b$, they can only be colored using red and green. Further, w.l.o.g $g$ is colored green. From the edge enforcer gadgets we see for every edge $st \in E$, at least one of $v_{i,s}$ and $v_{i,t}$ is \emph{not} colored green. Finally since the cycles are odd, they cannot all be adjacent to the same color. Read off a valid $k$ coloring of the graph by coloring each $v_j$ color $i$ if $v_{i,j}$ is green.  

\section{Graph Coloring}
\subsection{Lb on List Coloring Number (Alon \cite{DBLP:journals/rsa/Alon00}, 2000)}
\label{sec:lb-list-coloring-number}
For a graph $G$, let $L(v)$ be a list of valid colors for vertex $v$. $G$ is list colorable if there exists a proper coloring $\chi$ of $G$ such that $\chi(v) \in L(v)$. We say $G$ is $k$-choosable if for every choice of lists $L(v)$ for $v$ of size at least $k$, $G$ is colorable\footnote{I just recently thought about this, but how come there is no mention of the underlying set of available colours? Does it not matter if we are choosing three colours from among four or four hundred?}. The list coloring number $\chi_{\ell}(G)$ is the smallest constant $k$ for which $G$ is $k$-choosable.

For graphs with minimum degree at least $d$, Alon show that $\chi_{\ell}(G) \geq (1/2 - o(1))\log d$ \cite{DBLP:journals/rsa/Alon00}.

\subsubsection*{Details: Proof} 

\subsection*{Finding Fixed Length Even Cycles}
In an undirected graph $G = (V,E)$, determining if $G$ contains a cycle of length $2k$ --- and finding one if it exists --- takes time $O((2k)!V^2)$ \cite{yuster1997finding}.

\subsubsection*{Background}
Previous results include a $O(VE)$ algorithm which finds paths of length $k-1$ for fixed integer $k\geq 2$, which can be easily extended to a cycle-$k$-finding algorithm, and a $O(V^{\omega_{0,1}}\log V)$ algorithm where $\omega_{0,1}$ is complexity of Boolean matrix multiplication. Both these algorithms work for both directed and undirected graphs

Compare this with the question of determining if a graph contains a cycle of even length which is not known to be in $\class{P}$ or $\class{NP}$-complete. Further, if $k$ is a part of the input, then the problem is known to be $\class{NP}$-Hard. 

\subsubsection*{Details: Proof}

\subsection{List Coloring Bipartite Graphs}
The following are a collection of potentially relevant results related to our goal of finding the group- coloring of the hypercube graph $Q_n$ (note that $Q_n$ is $n$ regular and has $2^{n}$ vertices). We study related results in list coloring ($\chi_{\ell}(Q_n)$ is unknown to the best of our knowledge). Recall that list coloring a graph $G = (V, E)$ is the following problem: for every family of lists $\{L_v\}_{v \in V}$, there exists a function $f: V \rightarrow \cup L_v$ such that $f(u) \neq f(v)$ if $uv \in E$ and $f(u) \in L_u$ and $f(v) \in L_v$. Some basic results to keep in-mind: $\chi_{\ell}(Q_n) = \Omega(\log n)$ by Alon using the probabilistic method (still need to figure out his final inequality). Further a result of Molloy and someone else gives $\chi_{\ell}(Q_n) \in O(n/\log n)$.  
\begin{itemize}
	\item \emph{On a List Coloring Conjecture of Reed} (Bohman and Holzman, 2001) Define the vertex-color degree of a vertex $v$ wrt to a color $c$ as 
	\[d_c(v) = |\{u \in V: \{u,v\} \in \mbox{ and } c \in L_u\}.\]
	It is \emph{not} true that if there exists a positive integer constant $d$ such that $d_c(v) \leq d$ for all $v \in V$ and $c \in L_v$, and $|L_v| \geq d + 1$ for every $v \in V$. Then there exists a proper coloring of $G$ using $\{L_v\}_{v \in V}$. This is inspired by the idea that two adjacent vertices are easy to color if their list do not overlap very much --- in the extreme case when the lists do not overlap at all it is as if the edge simply does not exist. 
	\item \emph{A Haj\'{o}s-like Theorem for List Coloring} (Gravier, 1996) The following is a classic result of Haj\'{o}s. Let $\mathcal{G}_q$ be the class of all graphs that are not $q$-colorable. Then $\mathcal{G}_q$ is closed under (1) addition of vertices and edges, (2) the following change of incidence relationships (let $G_1$ and $G_2$ be two vertex-disjoint graphs and $a_1b_1$ and $a_2b_2$ be edges in $G_1$ and $G_2$ respectively; the graph $G_1 \cup G_2$ obtained by deleting edges $a_ib_i$, identifying $a_1a_2$, and adding edge $b_1b_2$ --- there is a picture in the actual paper which is helpful for understanding), and (3) identification of two non-adjacent vertices. Haj\'{o}s showed that every $G \in \mathcal{G}_q$ can be obtained from $K_{q+1}$ using these three operations. 
	
	This paper shows that $\mathcal{G}'_q$, defined to be the set of all graphs which are not $q$-choosable (aka. $q$-list colorable) can be similarly characterize, though the third operation needs to be slightly modified to (3') identifying two non-adjacent vertices with the same lists. In particular, it was shown that every graph in $\mathcal{G}'_q$ can be obtained by operations (1), (2), and (3)' from a complete bipartite graph $K_{\alpha, \beta}$ with fixed $\alpha, \beta$ for each $q$. 
	
	According to Wikipedia, Haj\'{o}s theorem is generally used for constructions (e.g. counter examples to certain conjectures, and facets in certain polytopes). \emph{Haj\'{o}s' Theorem for List Coloring} (Kr\'{a}l, 2004) is an extension of this paper which replaces Gravier's construction rules with a slightly shorter list of rules. 
	\item \emph{Some Problems in List Coloring Bipartite Graphs} (Hull, 1997) This is Thomas Hull's dissertation. The list-coloring problem that he considers and that we find interesting is the effects of edge removal to the list coloring number of bipartite graphs. It turns out that for $K_{m,n}$ when $n \geq m^{m}$, $\chi_{\ell}(K_{m,n}) \geq m + 1$ with an essentially unique list assignment. Thus removing an edge in this situation will reduce the list-coloring number.
	\item \emph{The Choice Number of Random Bipartite Graphs} (Alon and Krivelevich, 1998) A random bipartite graph $G(n, n, p)$ where each edge is added with probability $p$ almost surely has $\chi_{\ell}(G) = (1 + o(1))\log(np)$. \emph{More will be added to this section as I plan to read this paper in full.}
	\item \emph{Graphs Whose Choice Number is Equal to Their Chromatic Number} (Gravier and Maffray, 1997) Let $\chi(G)$ be the chromatic number of $G$, then graphs which satisfy $\chi(G) = \chi_{\ell}(G)$ are $\chi$-choosable (aka. chromatic choosable). Famously all line-graphs are conjectured to be $\chi$-choosable\footnote{\emph{The list chromatic index of a bipartite multigraph} (Galvin, 1995) shows that the line-graph of all bipartite graphs are $\chi$-choosable. Gravier and Maffray showed a bunch of other graphs are $\chi$-chromatic, including claw-free graphs.}. This paper shows some other $\chi$-choosable graphs. Of interest: determining if a graph is $k$-choosable for $k \geq 3$ is $\pi^{p}_{2}$-complete\footnote{This is shown in Gutner's Master Thesis from 1992. It makes me wonder if there is a simpler reduction from Linear Discrepancy or the Lattice problem to $k$-choosability.}. The actual examples are a bit annoying and not likely to be useful (they consider claw-free graphs since these are a forbidden configuration in a line graph; determining whether or not a graph is a line graph can be done in poly-time in the size of the graph).
	\item The follow is a set of papers is going to be on Ohba's conjecture. \emph{On Chromatic-Choosable Graphs} (Ohba, 2002) shows that $\chi(G + K_n) = \chi_{\ell}(G + K_n)$ for sufficiently large $n$. It follows that if $n \leq \chi(G) + \sqrt{2\chi(G)}$, then $\chi(G) = \chi_{\ell}(G)$. Ohba further conjectures that $n \leq 2\chi(G) + 1$ should be the actual upper bound. The conjecture was proved by Noel, Reed, Wu in 2014.
\end{itemize}

\section{Cycles Short and Long}
\subsection{Finding Short Even Cycles can be done in \texorpdfstring{$O(n^2)$}{O(n2)} (Yuster \cite{yuster1997finding}, 1997)}
This paper has a bunch of nice results, but the proofs might not be worth getting into since they just apply the standard tactic of \emph{case analysis} to variants of the BFS algorithm \cite{yuster1997finding}. We include here a list of prior works and results in the paper since some of them (e.g. finding shortest cycle, finding all vertices at a fixed distance from a fixed vertex, etc.) seem quite useful.

The following are some know results. There is a distinction between directed and undirected graphs results. Note that the problem: \emph{given a directed graph $G$, does $G$ contain a directed cycle of an even length?} is not known to be in $\class{P}$ or $\class{NP}$ (\emph{Even cycles in directed graphs}, Thomassen).
\begin{itemize}
	\item Finding all pairs of vertices connected by a length $k-1$ path (Monien); $O(mn)$. This easily extends to an $O(mn)$ algorithm for finding $k$ cycles (fixed $k$). 
	\item Finding the shortest cycle in a directed or undirected graph via BFS from all vertices; $O(mn)$.
	\item If there are enough edges ($\geq 100\ell n^{1 + 1/\ell}$ for $\ell \geq 2$), then there exists large even cycles ($C_{2k}$ for every $k \in [\ell, \ell\cdot n^{1/\ell}]$) which can be found in polynomial time, namely $O(kn^2)$ (Bondy, Simonovits). Think about the special case where $\ell = 2$. This says that if $m \geq 200n^{3/2}$, then $G$ contains a cycle $C_{2\sqrt{n}}$ which can be found in time $O(n^{2 + 1/2})$.
	\item Given vertex $s$ in a \emph{directed or undirected} graph $G$, we can find all $v \in V$ such that there exists a paths $p_{v}: s \rightsquigarrow v$ of length $k$. These vertices $v$, and their corresponding path $p_{v}$, can be found in time $O(k!m)$ (Monien). Notice from this result it is also quite easy to find cycles of length $k$ in time $O((k+1)!m)$ and determine if there exists a length $k$ path connecting a vertex in $A$ to a vertex in $B$ for disjoint subsets $A$ and $B$ of $V$\footnote{Just add a dummy vertex connecting to all the vertices of $A$!}. 
\end{itemize}

Next are the results in the paper.
\begin{itemize}
	\item For every $k \geq 2$, there is an $O((2k)!n^2)$ time algorithm that decides whether an \emph{undirected} graph $G$ contains a $C_{2k}$ and finds one if it does. 
	\item Find the shortest even cycle in an \emph{undirected} graph in time $O(n^2)$. Makes use of the previous result
	\item Find the shortest odd cycle in an \emph{undirected} graph in time $O(M(n)\log n)$ where $M(n)$ is the time to matrix multiply two $n \times n$ matrices. Note if a graph had an odd cycle of length $k$ then $\mm{A}^{k}_{i,i} = 1$ where $\mm{A}$ is the adjacency matrix of the graph\footnote{Note that $\mm{A}$ is actually the adjacency matrix in $GF_2$ since multiplicity does not matter.}. Thus to find the shortest odd cycle it suffices to find the smallest $k$ for which some entry $i$ satisfies the above. This can be done in $O(\log V)$ matrix multiplication steps using binary search. 
	\item Find the shortest odd cycle in an \emph{directed} graph in time $O(mn)$. This is comparable to some of the work done before.
\end{itemize}

\subsection{Lb length of Long Cycles in Bipartite Graph (Jackson~\cite{jackson1985long}, 1981)}
This 1981 paper of Jackson gives a lower bound on the length of a cycle in a bipartite graph given certain degree conditions, namely for any integer $\ell$ and bipartite graph $G = (A, B)$ where $B$ is the smaller part, then when there are enough edges\footnote{This means that $m > |B| + (|A|-1)(\ell - 1)$ if $|B| 
	\leq 2\ell - 2$ and $m > (|B| + |A| - 2\ell + 3)(\ell - 1)$ otherwise.}, then $G$ contains a cycle of length at least $2\ell$. To give a concrete example suppose that $|A| = |B| = n$. Then to be guaranteed to get a cycle of length $n/2$, $G$ must have at least $(n+3)n/2$ edges, or more than half the edges.

Note that this corresponds to a related result of Voss and Zuluaga that says that every 2-connected bipartite graph with bipartition $(A, B)$ and minimum degree $\ell$ contains a cycle of length at least $2\min(|A|, |B|, 2\ell - 2)$. 