% !TEX root = 00.tex
\section{Weekly Readings}
\begin{enumerate}
	\item (1982, Karmarkar-Karp~\cite{karmarkar1982differencing}) \href{https://www2.eecs.berkeley.edu/Pubs/TechRpts/1983/CSD-83-113.pdf}{Approximate set partition}. 
	
	\textbf{Problem.} Partition is $\mathsf{NP}$-hard, so we should not expect a fast algorithm to solve it exactly, but what about solving it approximately? Given: $n$ numbers $a_1, ..., a_n \in [0,1]$. Require: A partition of $a_1, ..., a_n$ into $m$ sets $A_1, ..., A_m$ such that we minimize 
	\[D(A_1, ..., A_m) = \left(\max_{i}\sum_{a_j \in A_i} a_j\right) - \left(\min_{i}\sum_{a_j \in A_i} a_j\right).\]
	When $m = 2$, the problem reduces to minimizing the discrepancy of one set. 
	
	\textbf{Contribution.} For $m = 2$, there is a polynomial time algorithm which finds a partition of size $O(n^{-c\log n})$ for some constant $c$ i.e. $D(A_1, A_2) = O(n^{-c\log n})$.
	
	\textbf{Algorithm.} The combination of two quite natural ideas. \emph{Compaction} takes a set of numbers $b_1, ..., b_k$ and reduces them to one number by removing the two largest numbers and adding back the magnitude of their difference. Note that if $d$ is the final number, then we can undo the differencing procedure in-order to obtain a partition with $D(A_1, A_2) = d$. \emph{Random pairing} divides the interval $[0,1]$ into many smaller intervals of size $1/N$ and randomly picks two entries in each interval and takes their difference. Since the two numbers are in the same interval, their difference is naturally bounded by $1/N$. Put these two pieces together as follows. Let $S_t$ and $a_t$ be the elements and size of the interval containing the elements in the $t$\textsuperscript{th} iteration. $K$ is to be determined.
	\begin{enumerate}
		\item \textbf{Partition.} Let $n_t = |S_t|$, $N_t = \frac{n_t}{K}$, $a_{t+1} = \frac{a_t}{N_t}$. 
		\item \textbf{Resample.} This is step restricts our attention to a subset of the entries in each interval. Apparently we are doing this to make the entries in each interval appear independent.
		\item \textbf{Random Paring.} Randomly pair up the elements in each interval as previously described. Let the set of magnitudes of differences be $B_t$. If there is an odd number of elements in this interval to begin with, there should be one element remaining at the end. 
		\item \textbf{Compaction.} Compact all the solitary elements to get some final element $d$. If $d$ is too large, i.e. $d > a_{t+1}$, difference $d$ with the elements of $B_t$ until it is sufficiently small. 
	\end{enumerate}

	\item (2020, Guo-Mousa) Local-to-Global contraction in simplicial complexes. High-dimensional expanders, mixing times of Markov chains. Potentially an interesting direction or a useful tool. 
	\item (2020, Jukna-Seiwert~\cite{Jukna_2020}) \href{https://arxiv.org/pdf/2012.12838.pdf}{Sorting can exponentially speed up pure DP}.
	
	\textbf{Problem.} How many operations does it take to solve the MST problem given a DP algorithm using $\min$, $\max$, and $+$? 
	
	\textbf{Contribution.} Only $O(n^3)$ suffice. Most of these are $\min$ and $\max$ operations with $+$ used at the end to compute the value of the MST (hence the name as $\min$ and $\max$ together allows for sorting). This is quite surprising since computing the MST with a DP algorithm using only $\min$ and $+$ (these are called \emph{pure DPs}) requires $2^{O(\sqrt{n})}$ operations. 
	
	\emph{Note: the actual result here is just a reduction. We take this opportunity to review the Floyd-Warshall algorithm for finding the minimum path between all pairs of vertices in a directed graph. There is also a notable result of Jerrum and Snir who showed that it takes $2^{\Omega(n)}$ $\min$ and $+$ DP operations to solve the minimum arborescence problem\footnote{The minimum arborescence problem is essentially directed MST.}. That result is more technically interesting.}
	
	\textbf{Reduction.} Write the MST problem using $\min$, $\max$, and $+$. To this end, for any distribution of weights $w: E \rightarrow \RR_+$, let 
	\[\dist_w(u,v) = \min_{p: u\rightsquigarrow v}\max_{e\in p}w(e)\]
	i.e. the minimum weight maximum edge on any $u,v$ path. Further for edge $e = (u,v)$ define $\dist_{w}(e) = \dist_{w}(u,v)$. Then we claim that for any graph $G$, edge weight distribution $w$, and edges $e_1, ..., e_{n-1}$ in the MST,
	\[\mathrm{mst}(w) = \dist_{w_1}(e_1) + \cdots + \dist_{w_{n-1}}(e_{n-1})\]
	where the weight distribution $w_i$ sets the weight of edges $e_1, ..., e_{i-1}$ to zero. Since each $\dist_{w_i}$ is a min-max computation, we can find $\mathrm{mst}(w)$ using a $(\min, \max, +)$ DP in polynomial time. 
	
	Thus it suffices to prove the claim. Since we can incrementally modify the weight distributions one edge at a time, it suffices to fix the weight distribution $w$ and for any edge $e$ show that 
	\[\mathrm{mst}(w) = \mathrm{mst}(w') + \dist_{w}(e)\]
	where $w'$ is identical to $w$ except $w'(e) = 0$. Let $e = uv$. Show the equality holds by showing inequalities in both directions. To show that $\mathrm{mst}(w) \ge \cdots$, let $T$ be an MST of $G$ wrt to $w$ and consider two cases: (1) $e \in T$ and (2) $e \notin T$. In the former note that 
	\[\mathrm{mst}(w') = \mathrm{mst}(w) - w(e) \leq \mathrm{mst}(w) - \dist_{w}(e).\] 
	In the latter, consider the $uv$ path $p$ in $T$. To construct $\mathrm{mst}(w')$ we remove the largest weight edge $e'$ in $p$ and add (the now weight zero) edge $e$. Further we have $\dist_w(e) \ge w(e')$ since every path must have a weight \emph{at most} $\disc_w(e)$ edge.
	
	To show that $\mathrm{mst}(w) \le \cdots$ we first fix a spanning tree $T'$ which uses edge $e$ wrt $w'$. Then we show that there exists an edge $f$ such that $w(f) \leq \dist_w(e)$ and $T' - e + f$ is a spanning tree wrt $w$. There exists a $uv$ path $p$ whose maximum edge weight is $\dist_{w}(e)$. Notice that some edge of $p$ does not exist in $T'$ (otherwise $T'$ would have a cycle) and crosses the cut induced by $e$. Thus we can simply add this edge and remove $e$ to form a spanning tree.     
	
	We should become more familiar with old, standard algorithms. These are the building blocks of our field so having them more easily accessible can't hurt. 
	
	\textbf{Floyd-Warshall.} All pairs shortest paths in a directed graph (generalize undirected graphs). 
	
	\textbf{Problem.} Given a directed graph $G$ (with no directed negative weight cycles, but negative edges are allowed), find the shortest directed path between any pair of nodes. 
	
	\textbf{Algorithm.} This is a DP algorithm and amounts to filling out an $n\times n$ table. There are $n$ iterations. At iteration $k$, for any pair of vertices $(i,j)$ we find the shortest path between $i$ and $j$ using only the vertices $1, 2, ..., k$. Entry $(i,j)$ at iteration $k$ is then $\mathsf{ShortestPath}(i,j,k)$ which is recursively $\mathsf{ShortestPath}(i,j,k-1)$ if the shortest path does not use $k$ or $\mathsf{ShortestPath}(i,k,k-1) + \mathsf{ShortestPath}(k,j,k-1)$ if $k$ is in the shortest path. 
	
	\textbf{Analysis.} The running time of this algorithm is $O(n^3)$. Roy in 1959 and Warshall in 1962 came up with something similar.  
	\item (2020, Armaselu) \href{https://arxiv.org/pdf/2012.12846.pdf}{Improved algorithm for computing the maximum-volume bichormatic separating box}.
	
	\textbf{Problem.} There are $n$ red points and $m$ blue points. Find the axis-aligned rectangle with the largest volume which contains all the red points and the fewest number of blue points possible. 
	
	\textbf{Contribution.} An algorithm which runs in time $O(m^2 + n)$ which finds said rectangle. 
	
	This paper is poorly written (typos, terrible graphs, an agonizingly long list of cases) and, I am pretty sure, the result it sub-optimal. For one thing, I am pretty sure we can make the simplifying assumption that there are no blue points inside the minimum volume box enclosing the red points. This is because the volume of the minimum box which contains all the red points is \emph{fixed} (in any axis, the points with the largest and smallest coordinate dictate the distance between two parallel faces of the box).
	
	Then it is just a matter of figuring out which of the blue points are on the boundary of the maximum volume box containing all the red points and none of the other blue points. I am pretty sure if you just sort the blue points in each coordinate, figuring out which blue points are critical is easy.
	
	\item (2020, Georgakopoulous, Haslegrav, Sauerwald, Sylvester) \href{https://drops.dagstuhl.de/opus/volltexte/2020/11761/pdf/LIPIcs-ITCS-2020-76.pdf}{Choice and Bias in Random Walks}
	
	Let the \emph{choice random walk} (CRW) be a random walk where at vertex $v$, the choice is \emph{not} uniformly among all neighbors of $v$, but instead the \emph{better} of two uniformly chosen random neighbors of $v$ depending on the criterion to optimize. The standard non-graph basis example will be the toilet roll problem\footnote{In the language of balls-and-bins: There are some $n$ bins, at every iteration pick two bins at random and put a ball in the one with a smaller load. What is the expected load after adding $m$ balls? Uniformly choosing the balls will lead to an expected load of $O\left(\frac{\log n}{\log \log n}\right)$, but using the \emph{power-of-two-choices} as was just described, the expected load drops to $O\left(\log \log n\right)$.}. In particular, they show that there exists a definition of better such that the expected time to visit every vertex is $O(|E|)$.
	
	Generally, this power-of-two-choices falls under something known as the Achlioptas process\footnote{Yeah, I think it is same Achlioptas from the improved JL.} The idea is quite clever and worth keeping in mind (though with many clever ideas the analysis might be quite complicated).
	\item (2020, Goldenberg, Karthik) \href{https://drops.dagstuhl.de/opus/volltexte/2020/11686/pdf/LIPIcs-ITCS-2020-1.pdf}{Hardness amplification of optimization problems}.
	
	\textbf{Problem.} Suppose there is a distribution $\mathcal{D}$ over instances $\Pi$ of a size $n$ optimization problem such that the probability of failure is $\frac{1}{\alpha(n)}$ for every randomized algorithm of running time $t(n)$ with distribution $\mathcal{D}$. Then there exists another distribution $\mathcal{D'}$ over instances of $\Pi$ of size $O(n\cdot \alpha(n))$ such that every randomized algorithm with running time $\frac{t(n)}{\poly(\alpha(n))}$ fails to solve $\Pi$ with probability $1 - \frac{1}{100}$. More concretely, consider the Max-Clique problem on graphs $G$ with $n$ vertices. Suppose for every randomized algorithm $\mathcal{A}$ with running time $\poly(n)$, 
	\[\Pr_{G\sim\mathcal{D}}\left[\mathcal{A}\mbox{ finds a max clique in $G$ w.p.\ } \geq 2/3\right]\leq 1 - \frac{1}{n}.\]
	Then we show the exists of a distribution $\mathcal{D}'$ over graphs $G'$ with $n' = \poly(n)$ vertices such that every randomized algorithm $\mathcal{A}'$ with running time $\poly(n')$ satisfies 
	\[\Pr_{G' \sim \mathcal{D}'}\left[\mathcal{A}'\mbox{ finds a max clique in $G'$ w.p.\ } \geq 2/3\right] \leq \frac{1}{100}.\]
	Further, if $\mathcal{D}$ is $\poly(n)$-time samplable, then so is $\mathcal{D}'$.
	
	\textbf{Amplification procedure.}
	We describe the amplification procedure for Max-Clique in particular. Given the distribution $\mathcal{D}$ on $G$, we describe a distribution $\mathcal{D}'$ on a product graph $H$. 
	\begin{enumerate}
		\item For $k = \poly(n)$, sample $G_1, ..., G_k$ from $\mathcal{D}$.
		\item Form the vertex set $V(H) = V(G_1)\sqcup\cdots\sqcup V(G_k)$.
		\item For all $i \in [k]$, add $E(G_i)$ to $E(H)$. Further for all $i,j \in [k]$ with $i \neq j$, add all possible edges between $V(G_i)$ and $V(G_j)$ to $E(H)$. 
	\end{enumerate}
	Note that $\mathcal{D}'$ is $\poly(n)$-time samplable if $\mathcal{D}$ is. 
	
	Suppose that with probability $> 1/100$, for a graph $H$ distributed like $\mathcal{D}'$, a randomized algorithm $\mathcal{A}'$ finds a max-clique in $H$ w.p.\ at least $2/3$. We show that with probability $> 1 - 1/n$, for a graph $G$ distributed like $\mathcal{D}$, a randomized algorithm $\mathcal{A}$ finds a max-clique in $G$ w.p.\ at least $2/3$. 
	
	The algorithm is as follows: construct $O(n)$ graphs $H$ and then run $\mathcal{A}'$ to find the max-clique on each. In particular for each iteration $i$, we pick $i \in [k]$ uniformly at random and sample $k-1$ graphs $G_1,...,G_{i-1}, G_{i+1}, ..., G_k$ from $\mathcal{D}$. Then form $H$ by taking the product of these $k-1$ graphs together with $G$ in the manner described in the amplification procedure. Find the max-clique in $H$ restricted to $G$. The largest clique among all samples $H$ will be the max-clique for $G$ with high probability. The proof requires a result of Feige and Kilian \emph{(2000, Two-Prover Protocols - Low Error at Affordable Rates)}. For universe $X$, distribution $\mathcal{T}$ over $X$, and $f: X^k \rightarrow \{0,1\}^k$, define
	\begin{align*}
		\mu &= \expected_{x^k\sim \mathcal{T}^k}\left[f\left(x^k\right)\right]\mbox{ and }\\
		\mu_x &= \expected_{x_1,..., x_{i-1}, x_{i+1}, ..., x_k \sim \mathcal{T}} \left[f(x_1, ..., x_{i-1}, x, x_{i+1}, ..., x_k)\right]\mbox{. Then}\\
		&\Pr_{x \sim \mathcal{T}}\left[|\mu_x - \mu| \geq k^{-1/6}\right] \leq k^{-1/6}.
	\end{align*}
	
	\item (1993, Kahn-Kalai) \href{https://www.ams.org/journals/bull/1993-29-01/S0273-0979-1993-00398-7/S0273-0979-1993-00398-7.pdf}{A counter-example to Borsuk's Conjecture}.
	
	The problem of Borsuk is to show that every diameter one set in $\RR^d$ can be partitioned into $d+1$ closed sets of diameter $< 1$. Let $f(d)$ be the smallest number so that every set in $\RR^d$ of diameter 1 can be partitioned into $f(d)$ sets of diameter $< 1$. The conjecture then becomes $f(d) = d + 1$ (apparently the simplex in $\RR^d$ shows that $f(d) \geq d + 1$). The conjecture is widely assumed to be true as it has been proved in dimensions $2$ and $3$ and in all dimensions for centrally symmetric convex bodies and smooth convex bodies.
	
	As it turns out, this conjecture is actually false. The authors proved that for sufficiently large $d$, $f(d) \geq (1.2)^{\sqrt{d}}$. The key is to reduce Borsuk's conjecture to the following claim about sets: Let $K$ be a family of $k$-subsets of $\{1, 2, ..., n\}$ such that every two members of $K$ have $t$ elements in common. Then $K$ can be partitioned into $n$ parts so that in each part every two members have $(t+1)$ elements in common. 
	\item (2021, Manurangsi) Linear Discrepancy is $\Pi_2$-Hard to Approximate. \emph{This is an unpublished note that the author e-mailed to Sasho solving my linear discrepancy hardness question.}
	
	\textbf{Problem.} In our half complete linear discrepancy paper we conjectured that linear discrepancy is $\Pi_2$-Hard. The author does one better and shows that it is $\Pi_2$-Hard to $\frac{9}{8}-\epsilon$ approximate. 
	
	\textbf{Reduction.} Unsurprisingly the key is to get a better gap between the yes and no instances in the reduction. The author does this in a clever way by triplicating the incidence matrix and tacking on the matrix 
	\[\mm{G} = \begin{bmatrix}
		1 & 1 & -1\\
		1 & -1 & 1\\
		-1 & 1 & 1
	\end{bmatrix}.\] 
	There are several important properties about $\norm{\cdot}_{\infty}$ of $\mm{G}$. In particular, for any $\mm{u} \in [0,1]^{3}$ and sign $b \in \{-1,1\}$, there exists a $\mm{z} \in \{0,1\}^3$ s.t.\
	\begin{enumerate}
		\item Low discrepancy\footnote{Note that the lower bound is also $4/3$ as can be seen when taking $\mm{u} = (1/3- \epsilon, 1/3, 1/3)$.}: $\norm{\mm{G(u-z)}}_{\infty} \leq \frac{4}{3}$. 
		\item Sign agreement\footnote{In the original proof the author claimed that it sufficed to consider $b = 1$ wlog.\, but I don't think that is the case. Instead I wrote out what each of the $\mm{z}$'s should be when $b = -1$ for different conditions of $u_1 + u_2 + u_3$ in an email to Sasho (2021, January 11).}: $b\ip{\mm{1}}{(\mm{u-z})} \geq 0$.
		\item Small gap: $|\ip{\mm{1}}{(\mm{u-z})}| \leq 2$.		
	\end{enumerate}
	Let $(V, \phi)$ be a $NAE3SAT$ instance. Let $\mm{B}$ be the incidence matrix. Define $\mm{A}$ as 
	\[\mm{A} = \begin{bmatrix}
		\frac{1}{3}\mm{B} & \frac{1}{3}\mm{B} & \frac{1}{3}\mm{B}\\
		\mm{I} & \mm{I} & -\mm{I}\\
		\mm{I} & -\mm{I} & \mm{I}\\
		-\mm{I} & \mm{I} & \mm{I}\\
	\end{bmatrix}.\]
	We show that YES-instances convert to YES-instances in both directions. To show soundness ($\lindisc(\mm{A}) \leq \frac{3}{2}$ implies the existence of a good assignment $\psi$), take a matrix $\mm{A}$. Each vector $\mm{x} \in \{0,1\}^{3n}$ can be decomposed into three vectors $\mm{x}^1$, $\mm{x}^2$, $\mm{x}^3 \in \{0,1\}^n$. Then in the portion of $\mm{A}$ with only the identity matrices, with $\mm{x} \in \{0,1\}^3$, we have exactly 
	\[\norm{\mm{G}(0.5\cdot\mm{1} - \mm{x})}_{\infty} \leq \frac{3}{2}.\]
	It can be checked that $\mm{x} = \mm{0}$ or $\mm{x} = \mm{1}$. As such, the discrepancy of the rows of $\mm{A}$ containing the $\mm{B}$s are exactly $\norm{\mm{B}(0.5\cdot\mm{1} - \mm{x}_1)} \leq 3/2$ and we can take $\mm{x}_1$ to be our truth assignment. 
	
	To show completeness ($\psi: V \rightarrow \{0,1\}$ such that every row has at most two variables with the same assignment implies $\lindisc(\mm{A}) \leq 4/3$). By the properties of $\mm{G}$, the rows of $\mm{A}$ which only consist of identity matrices satisfies this bound. It suffices to show that $\norm{\mm{B}(\mm{w}^{\mathrm{sum}} - \mm{x}^{\mathrm{sum}})}_{\infty}$ where $\mm{w}^{\mathrm{sum}} = \mm{w}^1 + \mm{w}^2 + \mm{w}^3$ and $\mm{x}^{\mathrm{sum}} = \mm{x}^1 + \mm{x}^2 + \mm{x}^3$ for decompositions of $\mm{w} \in [0,1]^{3n}$ and $\mm{x} \in \{0,1\}^{3n}$ as above. Define the sign $b_i$ of each variable $v_i$ to be $1 - 2\psi(v_i)$. Recall that there are at most three non-zero entries in every row of $\mm{B}$. By the properties of \emph{sign agreement} and \emph{small gap}, the values of $\ip{\mm{1}}{\mm{w}^{\mathrm{sum}}_i - \mm{x}^{\mathrm{sum}}_i}$ agrees in sign with $1-2\psi(v_i)$ and $|\ip{\mm{1}}{\mm{w}^{\mathrm{sum}}_i - \mm{x}^{\mathrm{sum}}_i}| \leq 2$. Thus the absolute values of the sum is at most 4 as required. 
	
	\textbf{$\Pi_2$-Hardness.} Use essentially the same problem $NAE\forall\exists 3SAT$. Modify the matrix $\mm{A}$ above to a matrix $\mm{A}'$ to handle the universal variables. Note that gap is still the same $\lindisc(\mm{A}') \leq 4/3$ for YES-instances and $\lindisc(\mm{A}') \geq 3/2$ for NO-instances. 
	\item (2021, Sandeep) \href{https://arxiv.org/pdf/2101.02854.pdf}{Almost Optimal Inapproximability of Multidimensional Packing Problems}.
	
	Focuses on showing the hardness of three problems: Vector Bin Packing, Vector Scheduling, and Vector Bin Covering. We only describe the result for Vector Bin Packing. We are given a collection of $n$ vectors $\{v\}$ in $[0,1]^d$. Partition $\{v\}$ into the smallest number of sets $V_1, ..., V_k$ so that $\norm{\sum_{v \in V_i} v}_{\infty} \leq 1$ for every $i \in [k]$. There is no poly-time $\Omega(\log d)$ factor asymptotic approximation algorithm (for large constant $d$). This matches existing $\ln d + O(1)$ approx.\ algorithms.

	\item (2021, De, Mossel, Neeman) \href{https://eccc.weizmann.ac.il/report/2021/005/}{Robust testing of low-dimensional functions}.
	
	Let $f: \RR^{n} \rightarrow \{-1,1\}$ be a classifier. We call $f$ a \emph{linear $k$-junta} if it is completely determined by some $k$-dimensional subspace of the input space. Recently the authors showed that linear $k$-juntas are testable i.e.\ there exists an algorithm which distinguishes between:
	\begin{enumerate}
		\item $f: \RR^{n} \rightarrow \{-1, 1\}$ is a linear $k$-junta with surface area $s$,
		\item $f$ is $\epsilon$-far from any linear $k$-junta with surface area $(1+\epsilon)s$.
	\end{enumerate}
	The query complexity independent of the ambient dimension $n$.
	
	Now the authors are back with a variant of the result which is noise tolerant i.e.\ for any $c > 0$ and $\epsilon > 0$ there exists an algorithm which distinguishes between:
	\begin{enumerate}
		\item $f: \RR^{n} \rightarrow \{-1, 1\}$ has correlation at least $c$ with some linear $k$-junta with surface area $s$,
		\item $f$ has correlation at most $c-\epsilon$ with any linear $k$-junta with surface area $s$.
	\end{enumerate}
	The query complexity of the tester is $k^{\poly(\log k/\epsilon)}$. This results in a fully noise tolerant tester with query complexity $k^{O(\poly(\log k/\epsilon))}$ for the class of intersections of $k$-halfspaces (constant k) over the Gaussian space. 
	\item (2021, Dudek, Gawrychowski, Pokorski) Strictly in-place algorithms for permuting and inverting permutation. 
	
	Ok from what I understand, the problem is this. You are given an array $\mm{A}$ of length $n$ and a permutation $\pi$ via an array $\mm{P}$ of length $n$. At each index $i$ of $\mm{P}$ is $\pi(i)$. Your goal is to use constant space and rearrange $\mm{A}$ so that $a_i$ is in position $\pi(i)$. 
	
	The way to do this is to permute the elements in the same cycle together all at one and to only do this once per cycle. Typically you would either choose the largest or smallest element and only perform the permutation if you encounter this element. The running time is $O(n^2)$. You can get a better running time if you allow $O(\log n)$ space.   
	
	\item (2021, Alman, Williams) \href{https://drops.dagstuhl.de/opus/volltexte/2020/11768/pdf/LIPIcs-ITCS-2020-83.pdf}{OV graphs are probably hard instances}.
	
	This is more of a reduction than a result. A graph $G$ on $n$ nodes is an Orthogonal Vectors (OV) graph of dimension $d$ if there are vectors $v_1, ..., v_n \in \{0,1\}^d$ such that the nodes $i$ and $j$ are adjacent in $G$ iff $\anglebrac{v_i,v_j} = 0$ over $\ZZ$. The authors solve several standard graph theory problems on OV graphs and find that if there exists algorithms which solve the problem faster on OV graphs of dimension $d = O(\log n)$, then the following conjecture about the time required to solve sparse Max-$k$-SAT instances will be false. 
	
	In particular, we consider the problems: determining if there exists a directed $k$-cycle in $G$ ($k \geq 3$), (2) computing the square of the adjacency matrix of $G$ over $\ZZ$ or $\FF_2$.
	
	\begin{conjecture}{\textup{(Sparse Max-2-SAT).}}
		For every $\epsilon > 0$, there exists a $c > 0$ so that $n$ variable Max-2-SAT on $cn$ clauses cannot be solved in time $O\left(2^{n(\omega/3-\epsilon)}\right)$.
	\end{conjecture}
	
	There are also some additional results about OV graphs: (1) all problems which are NP-hard on constant-degree graphs are also NP-hard on OV graphs of dimension $O(\log n)$, and (2) max-clique and online matrix-vector multiplication can be solved faster on OV graphs. 
	
	The reason OV graphs are interesting is because of the OV conjecture\footnote{For every $\epsilon > 0$, there is a $c > 0$ such that $OV$ in dimension $d = c\log n$ cannot be solved in $O(n^{2-\epsilon}$ randomized time.}. Most fine-grain complexity follows from Williams' original SETH implies OVC result. 
	\item (2020, Csik\'{o}s, Mustafa) \href{https://arxiv.org/pdf/2008.08970.pdf}{A simple proof of optimal approximations}.
	
	\textbf{Problem.} Given a finite set system $(X, \mathcal{F})$, construct a small set $A \subseteq X$ such that every set of $\mathcal{F}$ is ``well-approximated'' by $A$. In particular, if 
\end{enumerate}