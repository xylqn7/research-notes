#import cplex
import csv
import matplotlib.pyplot as plt
import networkx as nx
import numpy as np
import pandas as pd
from pulp import *
from tqdm import tqdm

from enum import Enum
import itertools
import math
import random
import time
import sys
import concurrent.futures

NUM_CORES=1

def solve_EF2(tup):
    F,n,k,c = tup
    N=range(n)
    K=range(k)
    prob = LpProblem("CheckMMS")
    prob += 0  # No objective

    # Assignment variables
    X = pulp.LpVariable.dicts("assignment",
                                ((i,j) for i in N for j in K),
    							lowBound=0,
    							upBound=1,
    							cat='Binary')

    #Each agent is assigned to exactly one group
    for i in N:
        prob += pulp.lpSum([X[i,j] for j in K]) == 1

    # Each group has size at most \ceil{n/2} and at least \floor{n/2}
    for j in K:
        prob += pulp.lpSum([X[i,j] for i in N]) <= math.ceil(n/k)+1
        prob += pulp.lpSum([X[i,j] for i in N]) >= math.floor(n/k)


    # EF2
    for i in N:
        for j in K:
            for j1 in K:
                prob += pulp.lpSum( [F[i,v] * X[v,j] for v in N])>=pulp.lpSum( [F[i,v] * X[v,j1] for v in N])-10000*(1-X[i,j])-c

    prob.solve()
    #print(LpStatus[prob.status])
    solution = np.zeros(n)
    if LpStatus[prob.status]!="Optimal":
        print(LpStatus[prob.status])
        optimal=0
        print(F,n,k)
        sys.exit()
    else:
        optimal=1
    return optimal, F

## ===> Find coalition given cut (Y, Ybar)
## [IN] G (np.matrix: nxn): adjacency matrix
##      N (list[int]: length n): index of vertices
##      Y (list[int]: length n//2): current cut
## [OUT] S (list[int]: length n//2): a weak core for Y
def find_coalition(G, N, Y):
    Ybar = [i for i in N if i not in Y]
    prob = LpProblem("Coalition")
    prob += 0

    X = pulp.LpVariable.dicts("inSet", N, lowBound = 0, upBound = 1, cat = 'Binary')
    prob += pulp.lpSum(X) == len(N)//2
    for u in N:
        if u in Y:
            current_utility = pulp.lpSum([G[u,v] for v in Y])
        else:
            current_utility = pulp.lpSum([G[u,v] for v in Ybar])
        prob += pulp.lpSum([G[u,v]*X[v] for v in N]) >= current_utility - 1000*(1-X[u]) + 1
    status = prob.solve(PULP_CBC_CMD(msg=0))
    if LpStatus[prob.status] != "Optimal":
        return None
    return [i for i in N if X[i].value() == 1]

def show_relationship(dominant_coalition):
    # Shows the dominant relationships between parts Y
    visited = {key:False for key in dominant_coalition}
    for key in visited:
        if not visited[key]:
            print(f"{key} <= ", end="")
            visited[key] = True
            temp_key = dominant_coalition[key]
            while temp_key != None:
                print(f"{temp_key} <= ", end="")
                visited[temp_key] = True
                temp_key = dominant_coalition[temp_key]
            print("None")

def weak_core(G, n):
    # Generate all partitions
    #print("Testing graph:")
    #print(G)
    N = range(n)
    all_partitions = itertools.combinations(N, n//2)
    dominant_coalition = {}

    # For each partition find a coalitition
    for Y in all_partitions:
        if Y not in dominant_coalition:
            X = find_coalition(G, N, Y)
            if X != None:
                X.sort()
                X = tuple(X)
            dominant_coalition[Y] = X
            Ybar = tuple([i for i in N if i not in Y])
            dominant_coalition[Ybar] = X

    if None not in dominant_coalition.values():
        print("Graph DOES NOT have opt partition!")
        print(G, n)
        sys.exit()

def weak_core_opt(G, n):
    N = range(n)
    Y = range(n//2+1)
    while Y:
        X = find_coalition(G, N, Y)
        if X != None:
            X.sort()
            X = tuple(X)
        dominant_coalition[Y] = X
        Ybar = tuple([i for i in N if i not in Y])
        dominant_coalition[Ybar] = X


# ===> Generate Erdos-Renyi random graph
# [IN]  n (INT: positive) - number of nodes
#       p (FLOAT: [0,1]) - probability edge occurs
# [OUT] G (np.matrix: nxn) - adjacency matrix
def G_np(n, p):
    G = np.zeros((n,n))
    for i in range(n-1):
        for j in range(i+1, n):
            t = np.random.binomial(1, p)
            G[i,j] = t
            G[j,i] = t
    return G

def main():
    nMin = 8 ## Number of nodes in the graph
    nMax = 13
    k = 3 ## Number of partitions
    c = 0 ## Number of envyable nodes in another part
    p = 0.7 ## Probability an edge occurs
    nTrials = 1
    stop = False

    for n in tqdm(range(nMin,nMax,2)):
        parallel_inputs=[]
        for it in range(nTrials):
            parallel_inputs.append(G_np(n,p))
        executor = concurrent.futures.ProcessPoolExecutor(NUM_CORES)
        futures = [executor.submit(weak_core, item, n) for item in parallel_inputs]
        concurrent.futures.wait(futures)
        for future in futures:
            assert(future.done())
            G = future.result()

if __name__ == '__main__':
    main()
