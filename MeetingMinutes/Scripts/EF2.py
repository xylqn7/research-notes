#import cplex
import csv
import matplotlib.pyplot as plt
import networkx as nx
import numpy as np
import pandas as pd
from pulp import *
import qpsolvers
import tqdm

from enum import Enum
import itertools
import math
import random
import time
import sys

def solve_EF(F, n, k, c):
    N = range(n) ## [0, ..., n-1]
    K = range(k) ## [0, ..., k-1]
    prob = LpProblem("CheckMMS")
    prob += 0  # No objective

    pairs = [(i,j) for i in N for j in K]
    # Assignment variables X[i,j]=1 when agent i is assigned to group j
    X = pulp.LpVariable.dicts("assignment", pairs,
            lowBound = 0,
            upBound = 1,
            cat = 'Binary')

    #Each agent is assigned to exactly one group
    for i in N:
        prob += pulp.lpSum([X[i,j] for j in K]) == 1

    # Each group has size at most \ceil{n/2} and at least \floor{n/2}
    for j in K:
    #    prob += pulp.lpSum([X[i,j] for i in N]) <= math.ceil(n/k)
        prob += pulp.lpSum([X[i,j] for i in N]) >= math.floor(n/k)


    # EF2
    for vertex in N:
        for part in K:
            ## TODO: fix this if statement. note that X[vertex,part] has no value
            if X[vertex,part] == 1:
                for other in K:
                    prob += pulp.lpSum([F[vertex,v] * X[v,part] for v in N]) >= (pulp.lpSum([F[vertex,v] * X[v,other] for v in N])-c)

    status = prob.solve()
    print(LpStatus[status])
    ##for pair in pairs:
    ##    if X[pair].value() == 1:
    ##        print(pair)
    ##print(F)

    if LpStatus[prob.status]!="Optimal":
        print(F,n,k)
        sys.exit()

def one_sided_EF(G, n, c):
    N = range(n)
    prob = LpProblem("CheckOneSided")
    prob += 0

    X = pulp.LpVariable.dicts("inSet", N, lowBound = 0, upBound = 1, cat = 'Binary')
    prob += pulp.lpSum(X) == n//2
    for vertex in N:
        prob += pulp.lpSum([G[vertex, v]*X[v] for v in N]) >= pulp.lpSum([G[vertex,v]*(1-X[v]) - 1000*(1-X[vertex]) for v in N]) - c
    status = prob.solve(PULP_CBC_CMD(msg=0))
    if LpStatus[prob.status] != "Optimal":
        print(G, n)
        sys.exit()

## ===> Find coalition given cut (Y, Ybar)
## [IN] G (np.matrix: nxn): adjacency matrix
##      N (list[int]: length n): index of vertices
##      Y (list[int]: length n//2): current cut
## [OUT] S (list[int]: length n//2): a weak core for Y
def find_coalition(G, N, Y):
    Ybar = [i for i in N if i not in Y]
    prob = LpProblem("Coalition")
    prob += 0

    X = pulp.LpVariable.dicts("inSet", N, lowBound = 0, upBound = 1, cat = 'Binary')
    prob += pulp.lpSum(X) == len(N)//2
    for u in N:
        if u in Y:
            current_utility = pulp.lpSum([G[u,v] for v in Y])
        else:
            current_utility = pulp.lpSum([G[u,v] for v in Ybar])
        prob += pulp.lpSum([G[u,v]*X[v] for v in N]) >= current_utility - 1000*(1-X[u]) + 1
    prob.solve(PULP_CBC_CMD(msg=0))
    if LpStatus[prob.status] != "Optimal":
        return None
    return [i for i in N if X[i].value() == 1]

def weak_core(G, n):
    # Generate all partitions
    print("Testing graph:")
    print(G)
    counter_example = True
    N = range(n)
    all_partitions = itertools.combinations(N, n//2)
    dominant_coalition = {}

    # For each partition find a coalitition
    for Y in all_partitions:
        if Y not in dominant_coalition:
            X = find_coalition(G, N, Y)
            if X != None:
                X.sort()
                X = tuple(X)
            dominant_coalition[Y] = X
            Ybar = tuple([i for i in N if i not in Y])
            dominant_coalition[Ybar] = X
    for key in dominant_coalition:
        if dominant_coalition[key] == None and counter_example:
            print(f"Maximal partition: {key}")
            counter_example = False
            show_graph(G, n)
    if counter_example:
        print(f"Graph DOES NOT have opt partition!")
        show_graph(G, n)
        sys.exit()

def weak_core_opt(G, n):
    N = range(n)
    Y = [i for i in range(n//2)]
    record = []
    while Y:
        record.append(tuple(Y))
        print(f"{Y} <= ", end="")
        X = find_coalition(G, N, Y)
        if X == None:
            break
        X.sort()
        Y = X
        if tuple(Y) in record:
            print("We caught one!")
            print(record)
            show_graph(G,n)
            sys.exit()
    print()
    show_graph(G,n)

# ===> Generate Erdos-Renyi random graph
# [IN]  n (INT: positive) - number of nodes
#       p (FLOAT: [0,1]) - probability edge occurs
# [OUT] G (np.matrix: nxn) - adjacency matrix
def G_np(n, p):
    G = np.zeros((n,n))
    for i in range(n-1):
        for j in range(i+1, n):
            t = np.random.binomial(1, p)
            G[i,j] = t
            G[j,i] = t
    # Add a self-loop to nodes which are adjacent to all other nodes
    #for i in range(n):
    #    if sum([G[i,j] for j in range(n)]) == n-1:
    #        G[i,i] = 1
    return G

def show_graph(G, n):
    graph_G = nx.Graph()
    nodes_G = range(n)
    edges_G = [(i,j) for i in range(n) for j in range(i,n) if G[i,j]]
    graph_G.add_nodes_from(nodes_G)
    graph_G.add_edges_from(edges_G)
    plt.plot()
    nx.draw(graph_G, with_labels=True)
    plt.show()

class Op(Enum):
    EF = 1
    ONE_SIDED_EF = 2
    WEAK_CORE = 3
    WEAK_CORE_OPT = 4

def test(test_type):
    n = 30 ## Number of nodes in the graph
    k = 3 ## Number of partitions
    c = 0 ## Number of envyable nodes in another part
    p = 0.5 ## Probability an edge occurs
    nTrials = 1
    pTrials = 1

    for i in range(pTrials):
        #p = random.random()
        print(f"Trail {i+1}: {p}")
        for trial in tqdm.tqdm(range(nTrials)):
            G = G_np(n, p)
            if test_type == Op.EF:
                solve_EF(G, n, k, c)
            if test_type == Op.ONE_SIDED_EF:
                one_sided_EF(G, n, c)
            if test_type == Op.WEAK_CORE:
                weak_core(G, n)
            if test_type == Op.WEAK_CORE_OPT:
                weak_core_opt(G,n)

def main():
    ##test(Op.WEAK_CORE_OPT)
    solve_ilp()

if __name__ == '__main__':
    main()

# ===> Cycle graph: 0 -> 4 -> 1 -> 5 -> 2 -> 3 -> 0
# G = np.zeros((n,n))
# G[0,3] = 1
# G[3,0] = 1
# G[0,4] = 1
# G[4,0] = 1
# G[1,4] = 1
# G[4,1] = 1
# G[1,5] = 1
# G[5,1] = 1
# G[2,3] = 1
# G[3,2] = 1
# G[2,5] = 1
# G[5,2] = 1
