from pulp import *
#import cplex
import time
from itertools import *
import numpy as np
import csv
import pandas as pd
import random
from sys import argv
import math
import random
import itertools
from itertools import combinations
import concurrent.futures
from tqdm import tqdm
import sys

c=2
NUM_CORES=15

def solve_EF2(tup):
    F,n,k=tup
    N=range(n)
    K=range(k)
    prob = LpProblem("CheckMMS")
    prob += 0  # No objective

    # Assignment variables
    X = pulp.LpVariable.dicts("assignment",
                                ((i,j) for i in N for j in K),
    							lowBound=0,
    							upBound=1,
    							cat='Binary')

    #Each agent is assigned to exactly one group
    for i in N:
        prob += pulp.lpSum([X[i,j] for j in K]) == 1

    # Each group has size at most \ceil{n/2} and at least \floor{n/2}
    for j in K:
        prob += pulp.lpSum([X[i,j] for i in N]) <= math.ceil(n/k)+1
        prob += pulp.lpSum([X[i,j] for i in N]) >= math.floor(n/k)


    # EF2
    for i in N:
        for j in K:
            for j1 in K:
                prob += pulp.lpSum( [F[i,v] * X[v,j] for v in N])>=pulp.lpSum( [F[i,v] * X[v,j1] for v in N])-10000*(1-X[i,j])-c

    prob.solve()
    #print(LpStatus[prob.status])
    solution = np.zeros(n)
    if LpStatus[prob.status]!="Optimal":
        print(LpStatus[prob.status])
        optimal=0
        print(F,n,k)
        sys.exit()
    else:
        optimal=1
    return optimal, F




def main():
    stop=0

    for n in  tqdm(range(2,8)):
        if (stop==0):
            for k in range(1,n-2):

                parallel_inputs=[]
                for it in range(1000):
                    F=np.zeros((n,n))
                    for i in range(n-1):
                        for j in range(i+1,n):
                            t=np.random.binomial(1, 0.5)
                            F[i,j]=t
                            F[j,i]=t
                    tup = (F,n,k)
                    parallel_inputs.append(tup)
                executor = concurrent.futures.ProcessPoolExecutor(NUM_CORES)
                futures = [executor.submit(solve_EF2, item) for item in parallel_inputs]
                concurrent.futures.wait(futures)
                for future in futures:
                    assert(future.done())
                    out_tup = future.result()
                    optimal, F= out_tup
                    if optimal==0:
                    #    print(F,n,k)
                        stop=1
        else:
            break






if __name__ == '__main__':

    main()
