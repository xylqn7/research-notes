import random as rn
from typing import Dict, List
import numpy as np

def max_prefix_sum(color: List[int], perm: List[int]) -> int:
    prefix_sum = 0
    max_sum = float("-inf")
    for term in perm:
        prefix_sum += color[term]
        if abs(prefix_sum) > max_sum:
            max_sum = abs(prefix_sum)
    return max_sum

def heuristic(n: int, p0: List[int], p1: List[int], p2: List[int]) -> List[int]:
    pairs = []
    colors = [0 for i in range(n)]
    adj = {i:[] for i in range(n)}
    for i in range(0,n,2):
        a0, b0 = (p0[i], p0[i+1])
        a1, b1 = (p1[i], p1[i+1])
        a2, b2 = (p2[i], p2[i+1])
        adj[a0].append(b0)
        adj[b0].append(a0)
        adj[a1].append(b1)
        adj[b1].append(a1)
        adj[a2].append(b2)
        adj[b2].append(a2)
        pairs.append((a0,b0))

    while pairs:
        a, b = pairs.pop()
        if colors[a] == 0 or colors[b] == 0:
            if colors[a]:
                colors[b] = -colors[a]
            elif colors[b]:
                colors[a] = -colors[b]
            else:
                get_color_a = 0
                get_color_b = 0
                for c in adj[a]:
                    if colors[c] != 0:
                        get_color_a = -colors[c]
                for c in adj[b]:
                    if colors[c] != 0:
                        get_color_b = -colors[c]
                if get_color_a:
                    colors[a] = get_color_a
                    colors[b] = -get_color_a
                elif get_color_b:
                    colors[b] = get_color_b
                    colors[a] = -get_color_b
                else:
                    colors[a] = 1
                    colors[b] = -1
                pairs += [(a, c) for c in adj[a]]
                pairs += [(b, c) for c in adj[b]]
    return colors

##===> Brute force all possible colorings
def test_exact(n: int, ntrials: int) -> None:
    disc_data = []
    min_disc = float("inf")
    best_color = None
    colors = {}
    process_colors(n, colors)
    #print(colors)

    perm = [i for i in range(n)]
    identity = [max_prefix_sum(colors[i], perm) for i in range(2**n)]
    #print(identity)

    for t in range(ntrials):
        perm1 = [i for i in range(n)]
        perm2 = [i for i in range(n)]
        rn.shuffle(perm1)
        rn.shuffle(perm2)
        #print(f"P1: {perm1}")
        #print(f"P2: {perm2}")

        for i in range(2**n):
            disc1 = max_prefix_sum(colors[i], perm1)
            disc2 = max_prefix_sum(colors[i], perm2)
            disc = max(disc1, disc2, identity[i])
            #print(f"disc1: {disc1}, disc2: {disc2}, id: {identity[i]}")

            if disc < min_disc:
                min_disc = disc
                best_color = colors[i]

        #print(f"disc: {min_disc}, opt_coloring: {best_color}")
        disc_data.append(min_disc)

##===> Random colorings with equal number of +/- ones
def test_rand(n: int, ntrials: int) -> None:
    ncolors = n*n
    all_disc = []

    for t in range(ntrials):
        print(f"Trial {t}")
        min_disc = float("inf")
        best_color = None
        p0 = [i for i in range(n)]
        p1 = [i for i in range(n)]
        p2 = [i for i in range(n)]
        rn.shuffle(p1)
        rn.shuffle(p2)

        for i in range(ncolors):
            rand_half = [j for j in range(n)]
            rn.shuffle(rand_half)
            rand_half = rand_half[:n//2]
            color = [(-1)**(j in rand_half) for j in range(n)]

            disc0 = max_prefix_sum(color, p0)
            disc1 = max_prefix_sum(color, p1)
            disc2 = max_prefix_sum(color, p2)
            disc = max(disc0, disc1, disc2)

            if disc < min_disc:
                best_color = color
                min_disc = disc

        all_disc.append(min_disc)
    print(f"max disc: {max(all_disc)} min disc: {min(all_disc)}")


if __name__ == "__main__":
    n = 160
    ntrials = 24
    max_disc = 0
    min_disc = float("inf")
    for i in range(1):
        #print(f"===> TEST {i+1}: n is {n}")
        disc = test_rand(n, ntrials)
