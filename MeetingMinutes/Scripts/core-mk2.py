from scipy.optimize import linprog
import numpy as np
from pulp import *
import mosek

import itertools
from enum import Enum
import random
import tqdm
import sys

EPSILON = 0.00001
INF = 1e10

class P_Type(Enum):
    LP = 0
    ILP = 1

def get_edges(n):
    e = {}
    m = 0
    for i in range(n):
        for j in range(i+1,n):
            e[(i,j)] = m
            e[(j,i)] = m
            m += 1
    return m, e

def get_partitions(n):
    all_parts = list(itertools.combinations(range(n), n//2))
    partitions = []
    for p in all_parts:
        pcomp = [i for i in range(n) if i not in p]
        pcomp = tuple(pcomp)
        all_parts.remove(pcomp)
        partitions.append((p, pcomp))
    return partitions

##---> New constraints for core/partition dominance pair
def add_constraints(m, e, core, partition):
    constraints = np.zeros((0, m))
    core0 = set(core).intersection(partition[0])
    core1 = set(core).intersection(partition[1])
    rest0 = set(partition[0]).difference(core0)
    rest1 = set(partition[1]).difference(core1)

    for i in core:
        row = [0 for i in range(m)]
        if i in core0:
            for j in rest0:
                row[e[i,j]] = 1
            for j in core1:
                row[e[i,j]] = -1
        elif i in core1:
            for j in rest1:
                row[e[i,j]] = 1
            for j in core0:
                row[e[i,j]] = -1
        constraints = np.vstack([constraints, row])
    return constraints

def mod_constraints(alpha, constraints):
    m, n = constraints.shape
    new_constraints = np.zeros((m,n))
    for i in range(m):
        for j in range(n):
            if constraints[i,j] > 0:
                new_constraints[i,j] = alpha*constraints[i,j]
            if constraints[i,j] < 0:
                new_constraints[i,j] = constraints[i,j]
    return new_constraints

def max_lp(alpha_max, alpha_min, constraints):
    if alpha_max - alpha_min < EPSILON:
        new_constraints = mod_constraints(alpha_min, constraints)
        feasible, xx = lp(new_constraints, weight=1)
        return alpha_min, xx

    alpha_mid = (alpha_max + alpha_min)/2
    new_constraints = mod_constraints(alpha_max, constraints)
    if lp(new_constraints):
        return max_lp(alpha_max, alpha_mid, constraints)
    return max_lp(alpha_mid, alpha_min, constraints)

##---> Solve LP given constraints
def lp(constraints, verbose=0, weight=0):
    nConstraints, nVariables = constraints.shape
    c = [0 for i in range(nVariables)]
    b = [-EPSILON for i in range(nConstraints)]
    res = linprog(c, A_ub=constraints, b_ub=b, bounds=[(0,1)])
    if verbose:
        print(res)
    if weight:
        return res.status == 0, res.x
    return res.status == 0

def weight_in(edge, weights, vertex, part):
    weight = 0
    for u in part:
        if u != vertex:
            weight += weights[edge[(vertex, u)]]
    return weight

def approx_ratio(e, weights, dominance, core_part):
    ratios = []
    for p in list(dominance.keys()):
        core = core_part[dominance[p]]
        min_ratio = 2
        for v in core:
            v_part = p[0] if v in p[0] else p[1]
            weight_part = weight_in(e, weights, v, v_part)
            weight_core = weight_in(e, weights, v, core)
            ratio = weight_core/weight_part if weight_part > 0 else INF
            min_ratio = min(ratio, min_ratio)
        ratios.append(min_ratio)
    return min(ratios)

def test(e, constraints, partitions, dominance, core_part):
    nConstraints, nVariables = constraints.shape
    A = np.copy(constraints)
    if lp(A):
        if len(partitions) == 0:
            return True, constraints
        p = partitions.pop(0)
        dom_list = list(dominance.keys())
        random.shuffle(dom_list)
        for dom in dom_list:
            core = dom[0]
            if dom in core_part:
                core = core_part[dom]
            new_const = add_constraints(nVariables, e, core, p)
            new_A = np.vstack([A, new_const])
            if not lp(new_A) and dom not in core_part:
                core = dom[1]
                new_const = add_constraints(nVariables, e, core, p)
                new_A = np.vstack([A, new_const])

            dominance[p] = dom
            core_part[dom] = core
            res, new_A = test(e, new_A, partitions, dominance, core_part)

            if res:
                return True, new_A
            dominance.pop(p)
        partitions.append(p)
        #print(p)
    #print("Infeasible")
    return False, constraints

def core(n=6):
    ##---> Initialization
    dominance = {} # [key] partition stores the partition containing the core dominating it
    core_part = {}
    # initialize edges
    m, e = get_edges(n)
    # initialize list of all partitions
    partitions = get_partitions(n)

    A = np.zeros((0, m))

    ##---> Seed core/partition relationships
    dominance[((0,1,2),(3,4,5))] = ((0,1,5),(2,3,4))
    dominance[((0,1,5),(2,3,4))] = ((0,1,3),(2,4,5))
    dominance[((0,1,3),(2,4,5))] = ((0,1,2),(3,4,5))
    core_part[((0,1,2),(3,4,5))] = (0,1,2)
    core_part[((0,1,5),(2,3,4))] = (2,3,4)
    core_part[((0,1,3),(2,4,5))] = (0,1,3)
    A = np.vstack([A, add_constraints(m, e, (2,3,4), ((0,1,2),(3,4,5)))])
    A = np.vstack([A, add_constraints(m, e, (0,1,3), ((0,1,5),(2,3,4)))])
    A = np.vstack([A, add_constraints(m, e, (0,1,2), ((0,1,3),(2,4,5)))])

    ##---> Try to extend the given relations
    random.shuffle(partitions)
    res, A = test(e, A, partitions, dominance, core_part)

    ##---> Output results
    if res:
        feasible, w = max_lp(2, 1, A)
        alpha = approx_ratio(e, w, dominance, core_part)
        return alpha, w, dominance, core_part
    return 0, [], {}, {}

def repeat(nTrials):
    max_alpha, max_w, max_dominance, max_core_part = 0, [], {}, {}
    for trial in tqdm.tqdm(range(nTrials)):
        alpha, w, dominance, core_part = core()
        if alpha > max_alpha:
            max_alpha = alpha
            max_w, max_dominance, max_core_part = w, dominance, core_part
    print(max_dominance)
    print(max_core_part)
    print(max_w)
    print(max_alpha)

def main():
    nTrials = 4000
    repeat(nTrials)
    #alpha, w, dominance, core_part = core()
    #print(dominance)
    #print(e)
    #print(w)
    #print(f"alpha = {alpha}")

if __name__ == "__main__":
    main()

## ===> The following is the same optimization but done using the
## mosek software. TBH I am not sure it works, but mostly I
## should have just used binary search.

## Define a stream printer to grab output from MOSEK
#def streamprinter(text):
#    sys.stdout.write(text)
#    sys.stdout.flush()
#
## Maximize the alpha value using SDP in MOSEK
#def mosek_sdp(constraints, verbose=0, weight=0):
#    m, n = constraints.shape
#    BARVARDIM = n+2 # n variables, alpha (to maximize), beta (dummy set to one)
#    with mosek.Env() as env:
#        with env.Task(0, 0) as task:
#            #task.set_Stream(mosek.streamtype.log, streamprinter)
#
#            # Turn constraint matrix into vectors representing
#            # the lower triangular portion of every coeff matrix
#            barai, baraj, baraval = [], [], []
#            for i in range(m):
#                row = constraints[i]
#                ai, aj, val = [], [], []
#                # populate barai, baraj, baraval
#                for j in range(n):
#                    entry = constraints[i,j]
#                    if entry != 0:
#                       ai.append(j+2)
#                       aj.append(0 if entry > 0 else 1)
#                       val.append(1 if entry > 0 else -1)
#                barai.append(ai)
#                baraj.append(aj)
#                baraval.append(val)
#
#            # Number of constraints and variables
#            task.appendcons(m+1) # to fix the value of beta
#            task.appendbarvars([BARVARDIM])
#
#            # Set constraint bounds
#            for i in range(m):
#                task.putconbound(i, mosek.boundkey.up, -INF, 0)
#            task.putconbound(m, mosek.boundkey.fx, 1, 1)
#
#            # Add objective SDP as linear combinations of appended matrices
#            symc = task.appendsparsesymmat(BARVARDIM, [0], [0], [1]) #alpha at (0,0)
#            task.putbarcj(0, [symc], [1])
#
#            # Add constraint SDP as linear combinations of appended matrices
#            syma = []
#            for i in range(m):
#                a = task.appendsparsesymmat(BARVARDIM, barai[i], baraj[i], baraval[i])
#                syma.append(a)
#                task.putbaraij(i, 0, [a], [1])
#            a = task.appendsparsesymmat(BARVARDIM, [1], [1], [1])
#            syma.append(a)
#            task.putbaraij(m, 0, [a], [1])
#
#            task.putobjsense(mosek.objsense.maximize)
#            task.optimize()
#
#            prosta = task.getprosta(mosek.soltype.itr)
#            solsta = task.getsolsta(mosek.soltype.itr)
#            feasible = solsta == mosek.solsta.optimal
#
#            xx = [0 for j in range(BARVARDIM)]
#            if feasible:
#                task.getxx(mosek.soltype.itr, xx)
#                #task.getbarxj(mosek.soltype.itr.0, barx)
#                print("Feasible")
#            return feasible, xx
#
## Solve the LP using MOSEK
#def mosek_lp(constraints, verbose=0, weight=0):
#    m, n = constraints.shape
#    with mosek.Env() as env:
#        with env.Task(0, 0) as task:
#            #task.set_Stream(mosek.streamtype.log, streamprinter)
#
#            # Number of constraints and variables
#            task.appendcons(m)
#            task.appendvars(n)
#
#            # Bounds (bound keys) for constraints
#            bkc = [mosek.boundkey.up for i in range(m)]
#            blc = [-INF for i in range(m)]
#            buc = [0 for i in range(m)]
#
#            # Coefficient of objective function
#            c = [0 for i in range(n)]
#
#            # Bounds (bound keys) for variables
#            bkx = [mosek.boundkey.ra for j in range(n)]
#            blx = [0 for j in range(n)]
#            bux = [1 for j in range(n)]
#
#            for j in range(n):
#                task.putcj(j, c[j])
#
#                task.putvarbound(j, bkx[j], blx[j], bux[j])
#                for i in range(m):
#                    task.putaij(i,j, constraints[i,j])
#
#            for i in range(m):
#                task.putconbound(i, bkc[i], blc[i], buc[i])
#
#            task.putobjsense(mosek.objsense.maximize)
#            task.optimize()
#            #task.solutionsummary(mosek.streamtype.msg)
#            status = task.getsolsta(mosek.soltype.bas)
#            feasible = status == mosek.solsta.optimal
#
#            xx = [0 for j in range(n)]
#            if feasible:
#                task.getxx(mosek.soltype.bas, xx)
#            if weight:
#                return feasible, xx
#            return feasible
