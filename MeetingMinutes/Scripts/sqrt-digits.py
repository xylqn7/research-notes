import math

# INPUT:
#   x (+ve int): sqrt target
#   n (nat): number of digits after the decimal point of the sqrt
# OUTPUT:
#   digits (List[int]): list containing sqrt. First entry is the value before
#       the sqrt. All subsequent entries are in {0,...,9} and represent
#       the ith digit after the sqrt
def sqrt_digits(x, n):
    mantissa = math.floor(math.sqrt(x))
    rem = x - mantissa*mantissa
    digits = [mantissa]
    count = 0
    while not rem == 0 and count <= n:
        rem = math.floor(rem*100)
        mantissa *= 10
        mid = mantissa*2
        digit = 1
        while (mid+digit)*digit <= rem:
            digit += 1
        digit -= 1
        rem -= (mid+digit)*digit
        mantissa += digit
        digits.append(digit)
        count += 1
    return digits

def test1():
    x, n = 4, 10
    print(f"{n} digits of sqrt({x}) is\n {sqrt_digits(x,n)}")
    x, n = 1234, 10
    print(f"{n} digits of sqrt({x}) is\n {sqrt_digits(x,n)}")
    x, n = 5, 10
    print(f"{n} digits of sqrt({x}) is\n {sqrt_digits(x,n)}")

    n = 4
    print(f"Sum of {n//2} odd digits of sqrt({x}):")
    digits = sqrt_digits(x,n)
    sum_odd = sum([digits[i] for i in range(1,n,2)])
    print(f"{sum_odd % 10}")

def test2():
    total = 30
    for i in range(1, total+1):
        for j in range(1,i):
            target = i/j
            for k in range(1, total+1):
                num = k
                while num/k > target:
                    num -= 1


def main():
    test2()

if __name__ == "__main__":
    main()
