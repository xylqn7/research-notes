from scipy.optimize import linprog
import numpy as np
from pulp import *

import itertools
from enum import Enum
import random
import tqdm

EPSILON = 0.00001

class P_Type(Enum):
    LP = 0
    ILP = 1

def get_edges(n):
    e = {}
    m = 0
    for i in range(n):
        for j in range(i+1,n):
            e[(i,j)] = m
            e[(j,i)] = m
            m += 1
    return m, e

def get_partitions(n):
    all_parts = list(itertools.combinations(range(n), n//2))
    partitions = []
    for p in all_parts:
        pcomp = [i for i in range(n) if i not in p]
        pcomp = tuple(pcomp)
        all_parts.remove(pcomp)
        partitions.append((p, pcomp))
    return partitions

##---> New constraints for core/partition dominance pair
def add_constraints(m, e, core, partition):
    constraints = np.zeros((0, m))
    core0 = set(core).intersection(partition[0])
    core1 = set(core).intersection(partition[1])
    rest0 = set(partition[0]).difference(core0)
    rest1 = set(partition[1]).difference(core1)

    for i in core:
        row = [0 for i in range(m)]
        if i in core0:
            for j in rest0:
                row[e[i,j]] = 1
            for j in core1:
                row[e[i,j]] = -1
        elif i in core1:
            for j in rest1:
                row[e[i,j]] = 1
            for j in core0:
                row[e[i,j]] = -1
        constraints = np.vstack([constraints, row])
    return constraints

##---> Solve LP given constraints
def lp(constraints, verbose=0, weight=0):
    nConstraints, nVariables = constraints.shape
    c = [0 for i in range(nVariables)]
    b = [-EPSILON for i in range(nConstraints)]
    res = linprog(c, A_ub=constraints, b_ub=b, bounds=[(0,1)])
    if verbose:
        print(res)
    if weight:
        return res.status == 0, res.x
    return res.status == 0

##---> Solve ILP given constraints
def ilp(constraints, verbose=0, weight=0):
    nConstraints, nVariables = constraints.shape
    A = np.copy(constraints)
    np.rint(A, out=A)
    E = [i for i in range(nVariables)]

    prob = LpProblem("EdgeConstraints")
    X = pulp.LpVariable.dicts("edge", E, lowBound = 0, upBound = 1, cat = 'Binary')
    # turn every row of A into a constraint
    for i in range(nConstraints):
        prob += pulp.lpSum([X[j]*A[i,j] for j in range(nVariables)]) <= -1

    prob.solve(PULP_CBC_CMD(msg=verbose))
    feasible = LpStatus[prob.status] == "Optimal"
    if weight:
        return feasible, [X[edge].value() for edge in E]
    return

def weight_in(edge, weights, vertex, part):
    weight = 0
    for u in part:
        if u != vertex:
            weight += weights[edge[(vertex, u)]]
    return weight

def approx_ratio(e, weights, dominance, core_part):
    ratios = []
    for p in list(dominance.keys()):
        core = core_part[dominance[p]]
        min_ratio = 2
        for v in core:
            v_part = p[0] if v in p[0] else p[1]
            ratio = weight_in(e, weights, v, core)/ weight_in(e, weights, v, v_part)
            min_ratio = min(ratio, min_ratio)
        ratios.append(min_ratio)
    return min(ratios)

# p_type (P_Type): 0 for LP, 1 for ILP
def test(p_type, e, constraints, partitions, dominance, core_part):
    nConstraints, nVariables = constraints.shape
    A = np.copy(constraints)
    if (p_type == P_Type.LP and lp(A)) or (p_type == P_Type.ILP and ilp(A)):
        if len(partitions) == 0:
            return True, constraints
        p = partitions.pop(0)
        dom_list = list(dominance.keys())
        random.shuffle(dom_list)
        for dom in dom_list:
            core = dom[0]
            if dom in core_part:
                core = core_part[dom]
            new_const = add_constraints(nVariables, e, core, p)
            new_A = np.vstack([A, new_const])
            if not lp(new_A) and dom not in core_part:
                core = dom[1]
                new_const = add_constraints(nVariables, e, core, p)
                new_A = np.vstack([A, new_const])

            dominance[p] = dom
            core_part[dom] = core
            res, new_A = test(p_type, e, new_A, partitions, dominance, core_part)

            if res:
                return True, new_A
            dominance.pop(p)
        partitions.append(p)
        #print(p)
        #print("Infeasible")
    return False, constraints

def core(p_type, n):
    ##---> Initialization
    dominance = {} # [key] partition stores the partition containing the core dominating it
    core_part = {}
    # initialize edges
    m, e = get_edges(n)
    # initialize list of all partitions
    partitions = get_partitions(n)

    A = np.zeros((0, m))

    ##---> Seed core/partition relationships
    if n == 6:
        dominance[((0,1,2),(3,4,5))] = ((0,1,5),(2,3,4))
        dominance[((0,1,5),(2,3,4))] = ((0,1,3),(2,4,5))
        dominance[((0,1,3),(2,4,5))] = ((0,1,2),(3,4,5))
        core_part[((0,1,2),(3,4,5))] = (0,1,2)
        core_part[((0,1,5),(2,3,4))] = (2,3,4)
        core_part[((0,1,3),(2,4,5))] = (0,1,3)
        A = np.vstack([A, add_constraints(m, e, (2,3,4), ((0,1,2),(3,4,5)))])
        A = np.vstack([A, add_constraints(m, e, (0,1,3), ((0,1,5),(2,3,4)))])
        A = np.vstack([A, add_constraints(m, e, (0,1,2), ((0,1,3),(2,4,5)))])
    elif n == 10:
        dominance[((0,1,3,4,5),(2,6,7,8,9))] = ((0,1,2,3,9),(4,5,6,7,8))
        #dominance[((0,1,2,3,9),(4,5,6,7,8))] = ((0,1,2,7,8),(3,4,5,6,9))
        #dominance[((0,1,2,7,8),(3,4,5,6,9))] = ((0,1,3,4,5),(2,6,7,8,9))
        core_part[((0,1,3,4,5),(2,6,7,8,9))] = (0,1,3,4,5)
        #core_part[((0,1,2,3,9),(4,5,6,7,8))] = (4,5,6,7,8)
        #core_part[((0,1,2,7,8),(3,4,5,6,9))] = (0,1,2,7,8)
        A = np.vstack([A, add_constraints(m, e, (0,1,3,4,5), ((0,1,2,7,8),(3,4,5,6,9)))])
        #A = np.vstack([A, add_constraints(m, e, (4,5,6,7,8), ((0,1,3,4,5),(2,6,7,8,9)))])
        #A = np.vstack([A, add_constraints(m, e, (0,1,2,7,8), ((0,1,2,3,9),(4,5,6,7,8)))])

    ##---> Try to extend the given relations
    random.shuffle(partitions)
    res, A = test(p_type, e, A, partitions, dominance, core_part)

    ##---> Output results
    if res:
        feasible, w = lp(A, weight=1) if p_type == P_Type.LP else ilp(A, weight=1)
        alpha = approx_ratio(e, w, dominance, core_part)
        #print(dominance)
        #print(e)
        #print(w)
        #print(alpha)
        return alpha, w, dominance, core_part
    return 0, [], {}, {}

def repeat(nTrials):
    max_alpha, max_w, max_dominance, max_core_part = 0, [], {}, {}
    for trial in tqdm.tqdm(range(nTrials)):
        alpha, w, dominance, core_part = core(P_Type.LP, 6)
        if alpha > max_alpha:
            max_alpha = alpha
            max_w, max_dominance, max_core_part = w, dominance, core_part
    print(max_dominance)
    print(max_core_part)
    print(max_w)
    print(max_alpha)

def main():
    nTrials = 1000
    repeat(nTrials)
    #core(P_Type.LP, 6)

if __name__ == "__main__":
    main()
