import itertools
import random
INF = 10000000

def umbrella_sum(n, A, B):
    return sum([A[i]*B[i]*A[(i+1) % n] for i in range(n)])

def opt_perm(n, A, B):
    max_sum = 0
    max_perm = []
    for p in list(itertools.permutations(range(n))):
        permA = [A[i] for i in p]
        temp_sum = umbrella_sum(n, permA, B)
        #print(temp_sum, permA)
        if temp_sum > max_sum:
            max_sum = temp_sum
            max_perm = p
    return max_sum, max_perm

def dp_perm(n, A, B):
    opt = []
    memo_len = 0
    for i in range(n):
        memo = {}
        S = [x for x in range(n) if x != i]
        opt.append(top_down(n, A, B, i, i, S, n-2, memo))
        print(f"i: {i}", memo)
        memo_len = len(memo)
    return max(opt), memo_len

def top_down(n, A, B, init, last, S, k, memo):
    if k == 0:
        index = S[0]
        return A[init]*B[k]*A[index]
    else:
        opt = []
        for s in S:
            modS = [j for j in S if j != s]
            modS.sort()
            key = tuple(modS)
            if key not in memo:
                value = top_down(n, A, B, init, s, modS, k-1, memo)
                value += A[last]*B[k]*A[s]
                if k == n-2:
                    value += A[init]*B[n-1]*A[s]
                memo[key] = value
            opt.append(memo[key])
        return max(opt)

def dp_perm_bottom_up(n, A, B):
    memo_table = []
    value_table = []
    for i in range(n):
        old = [0 for i in range(n)]
        new = [0 for i in range(n)]
        ## Base Case:
        for col in range(n):
            old[col] = [[i,col], A[i]*B[0]*A[col]]
        ## Inductive Cases:
        for row in range(1, n):
            for col in [x for x in range(n) if x != i]:
                used, opt = old[k]
                last = used[-1]
                max_new = 0
                max_ind = 0
                for k in [x for x in range(n) if x not in used]:
                    add = A[last]*B[row]*A[k]
                    if add > max_new:
                        max_new = add
                        max_ind = k
                used.append(max_ind)
                new[col] = [used, max_new]
            old = new
        value_table.append(max([memo[i][0] for i in range(n)]))
        value_index = value_table.index()
        memo_table.append(memo[value_index][0])
    max_value = max(memo_table, key=itemgetter)
    max_index = memo_table.index(max_value)
    return max_sum, max_perm

#A = [4,4,6,3,1,3]
#B = [9,7,3,8,2,3]
def run_trial(n, max_val, A = None, B = None):
    if A == None:
        A = [random.randrange(1, max_val) for i in range(n)]
    if B == None:
        B = [random.randrange(1, max_val) for i in range(n)]
    opt_sum, opt_p = opt_perm(n, A, B)
    dp_sum, dp_space = dp_perm(n, A, B)
    if dp_sum < opt_sum:
        print(A, B)
        print(f"Optimum Sum: {opt_sum}")
        print([A[i] for i in opt_p])
        print("DP is suboptimal!")
        print(f"DP Sum: {dp_sum}")
        print(f"DP memo entries: {dp_space}")
        return False

def main():
    n = 4
    MAX = 10
    nTrials = 1000
    for trial in range(nTrials):
        if not run_trial(n, MAX, A = [2,7,4,7], B = [8,9,3,3]):
            break


if __name__ == "__main__":
    main()
