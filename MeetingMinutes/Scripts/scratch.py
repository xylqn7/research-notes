import random as rand

def odd_heads_one_tail():
    c = 1
    count_head = False
    toss = rand.randint(0, 2)
    while not count_head or toss == 0:
        c += 1
        if toss == 0:
            count_head = not count_head
        elif toss == 1:
            count_head = False
        toss = rand.randint(0,2)
    return c

def test(n, func):
    results = []
    for i in range(n):
        r = func()
        results.append(r)
    print(results)
    return sum(results)//len(results)

def main():
    test(10, odd_heads_one_tail)

if __name__ == "__main__":
    main()
