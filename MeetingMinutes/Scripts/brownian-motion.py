import math

from scipy.stats import norm, multivariate_normal
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation

### ===> Helper Functions <===

def get_cholesky_factor(n, factor=np.zeros((0,0))):
    """ Factors Cov := I_n - J_n/n (J_n: nxn all-ones matrix).

    The built-in numpy cholesky factorization algorithm is numerically
    unstable so is unable to factor Cov when n >= 8. Luckily
    the factor A of Cov has a particular pattern. It is nx(n-1) dim
    and it has column i with i zeros, a positive num alpha, then the
    remainder are negative nums, beta. Note that the sum of the
    alpha and betas is zero. For col i, beta = -1/(sqrt{n-i}sqrt{n-i-1})

    Arguments:
    n -- dimension of Cov to factor
    factor -- factors Cov_n if Cov_n' is given for n' >= n
    """
    if len(factor) > 0:
        if n == 0:
            return np.zeros((0,0))
        return factor[-n:, -(n-1):]
    factor = np.zeros((n, n-1))
    for i in range(n, 1, -1):
        negative_val = -1/(math.sqrt(i)*math.sqrt(i-1))
        positive_val = -(negative_val*(i-1))
        factor[n-i,n-i] = positive_val
        factor[n-i+1:,n-i] = negative_val
    return factor


def is_pos_def(A):
    """ Checks if matix A is positive definite. """
    return np.all(np.linalg.eigvals(A) > 0)


def is_float(value):
    try:
        float(value)
        return True
    except ValueError:
        return False


def is_int(value):
    try:
        int(value)
        return True
    except ValueError:
        return False


### ===> Brownian Motion Tutorials <===

def bm_n_dim(x0, n, dt, delta, out=None):
    """ Generates an n dimensional brownian motion.

    Example from:
    https://scipy-cookbook.readthedocs.io/items/BrownianMotion.html

    Arguments:
    x0 -- inital position of motion
    n -- number of dimensions
    dt -- step size
    delta -- standard deviation
    out -- matrix with columns as the step positions

    """
    x0 = np.asarray(x0)
    r = norm.rvs(size=x0.shape + (n,), scale=math.sqrt(dt))
    if out is None:
        out = np.empty(r.shape)
    np.cumsum(r, axis=-1, out=out)
    out += np.expand_dims(x0, axis=-1)
    return out


def plot_bm(m):
    """ Plots m 1D brownian motions

    Arguments:
    m -- number of realizations to generate

    """
    delta = 0.1 # Wiener process parameter
    T = 1 # Total time
    N = 500 # Number of steps
    dt = T/N # Time step size
    x = np.empty((m,N+1)) # Storing result of realizations
    x[:, 0] = 50 # Initial values

    brownian(x[:,0], N, dt, delta, out=x[:,1:])

    t = np.linspace(0.0, N*dt, N+1)
    for k in range(m):
        plt.plot(t, x[k])
    plt.xlabel('t', fontsize = 12)
    plt.ylabel('x', fontsize = 12)
    plt.grid(True)
    plt.show()


### ===> Bounded Brownian Motion <===

def plot_bm_3d(dt):
    """ Plots the bounded brownian motion in 3D

    Arguments:
    dt -- step size

    """
    A = get_cholesky_factor(3)
    nSteps, steps = bm_hyperplane(A, 3, dt, get_steps=True)

    # "steps" is n x d matrix. The rows are the coordinates of each step.
    steps = np.reshape(steps, (nSteps, 3))
    #print(steps)

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.set_xlim3d([0,1])
    ax.set_xlabel('x')
    ax.set_ylim3d([0,1])
    ax.set_ylabel('y')
    ax.set_zlim3d([0,1])
    ax.set_zlabel('z')
    points, = ax.plot([], [], [], '.-', alpha=0.05)
    def animate(i):
        points.set_data(steps[0:i,0], steps[0:i,1])
        points.set_3d_properties(steps[0:i,2])
        return points,
    an = animation.FuncAnimation(
            fig, animate,
            frames=(nSteps+1), interval=40, blit=True)
    plt.show()


def test(dt, dMin, dMax, dInt, nTrials):
    """ Compute the average number of steps for the bounded
    brownian motion to terminate in different dimension.

    Arguments:
    dt -- step size
    dMin -- minimum dimension to test (>= 1)
    dMax -- maximum dimension to test (<= 100)
    dInt -- interval of testing dimension
    nTrials -- number of trials per dimension

    """
    data_pts = []
    print(f"Number of trials per dimension: {nTrials}")
    for d in range(dMin, dMax, dInt):
        average = 0
        A = get_cholesky_factor(d)
        for i in range(nTrials):
            nSteps, steps = bm_hyperplane(A, d, dt)
            for step in steps:
                print(step)
            average += nSteps
        average /= nTrials
        print(f"{d}: {average}")
        data_pts.append(average)
    return data_pts


def bm_hyperplane(A, n, dt, get_steps=False):
    """ Main controller for computing the number of steps it takes
    for the brownian motion in the unit hypercube intersected with
    the hyperplane x_1 + ... + x_n = 1 to reach a vertex.

    Arguments:
    A -- cholesky factor of the covariance matrix
    n -- dimension of the walk
    dt -- step size
    get_steps -- if True, returns the sequence of positions obtained
    during the walk. Otherwise, returns []
    """
    x0 = np.ones((n,1)) * (1/n) # Initial vector
    nSteps, steps = bm_interior(A, x0, dt, get_steps)
    return nSteps, steps


def bm_interior(A, x0, dt, get_steps):
    """ Recursive iterations for bm_hyperplane.

    While there are indices of x0 more than ERR away from 0 or 1,
    bm_interior will run a brownian motion on the yet unfrozen
    indices until they are ERR away from 0 and 1.

    Arguments:
    A -- cholesky factor of the covariance matrix
    x0 -- initial position of current walk
    dt -- step size
    get_steps -- if True, returns the sequence of positions obtained
    during the walk. Othrwise, returns []

    """
    n, m = A.shape
    nSteps = 0
    steps = []

    if n == 0 or n == 1:
        return 0, []

    cov = np.eye(n) - 1/n*np.ones((n,n))
    while all([ERR < x0[i] < 1-ERR for i in range(len(x0))]):
        # Use the give cholesky factorization
        #step =  A.dot(norm.rvs(size=(m,1), scale=dt))

        # Use the built-in scipy multivariate Gaussian generator
        step = dt*multivariate_normal.rvs(cov=cov)
        step = step.reshape((n,1))

        x0 += step
        nSteps += 1
        if get_steps:
            # Each step should be represented by a list of floats
            flat_list = [item for sublist in x0.tolist() for item in sublist]
            steps.append(flat_list)

    if n == 2:
        # The walk ends here with one index ~1 and the other ~0
        return nSteps, steps

    freeze_list = [ERR > x0[i] or x0[i] > 1-ERR for i in range(n)]
    frozen = [i for i, x in enumerate(freeze_list) if x]

    n1 = n - len(frozen)
    A = get_cholesky_factor(n1, factor=A)
    x1 = np.reshape(np.delete(x0, frozen), (n1,1))
    nStepsRecurse, stepsRecurse = bm_interior(A, x1, dt, get_steps)

    if get_steps:
        last = steps[-1]
        # Add the coordinated to the recursive steps
        for s in stepsRecurse:
            step = []
            entry = 0
            for i in range(n):
                if i in frozen:
                    step.append(last[i])
                else:
                    step.append(s[entry])
                    entry += 1
            steps.append(step)
    return nSteps + nStepsRecurse, steps


## ===> MAIN <===

ERR = 10e-6 # Global error parameter for floating point comparisons
if __name__ == "__main__":

    option = input("3D graphic (G) or test random walk in bounded hypercube (T)? ")
    while option != 'G' and option != 'T':
        option = input("Invalid input: Enter (G) for graphic or (T) for test: ")

    if option == 'G':
        plot_bm_3d(0.05)
    else:
        dt, dMin, dMax, dInt, nTrials = (0.05, 4, 10, 1, 250)
        print("Input step size (<1), min dim, max dim, dim interval, and # iterations separated by spaces e.g. '=> 0.05 4 10 1 200' or press ENTER for standard inputs.")
        option_str = input("=> ")
        while True:
            if len(option_str) == 0:
                break
            option = option_str.strip().split()
            valid = [is_float(option[0])] + [is_int(s) for s in option[1:]]
            if len(option) == 5 and all(valid):
                dt = float(option[0])
                dMin, dMax, dInt, nTrials = [int(i) for i in option[1:]]
                break
            else:
                print("Invalid input. Please try again or press ENTER to use standard inputs.")
                option_str = input("=> ")

        data_pts = test(dt, dMin, dMax, dInt, nTrials)

        # Plotting number of steps vs number of iterations
        plt.plot([i for i in range(dMin, dMax, dInt)], data_pts)
        plt.xlabel('Dimensions')
        plt.ylabel(f'Average # steps over {nTrials} trials with step size {dt}')
        plt.show()

