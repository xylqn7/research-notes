\usepackage[english]{babel}
\usepackage{amsfonts, amsmath, amssymb, amsthm}
\usepackage{algorithm}
\usepackage[noend]{algpseudocode}
\usepackage[toc,page]{appendix}
\usepackage{bbold}
\usepackage[utf8]{inputenc}
\usepackage{geometry}
	\geometry{margin=1in}
\usepackage[acronym, toc]{glossaries}
\usepackage{graphicx}
\usepackage{epstopdf}
	\epstopdfsetup{update}
\usepackage{caption}
\usepackage{color}
\usepackage{comment}
\usepackage{hyperref}
\usepackage{mathtools}
\usepackage{multirow}
\usepackage{multicol}
\usepackage{thmtools}
\usepackage{tikz}
	\usetikzlibrary{arrows, automata, fit, positioning}
	\tikzset{
		>=stealth,
		node distance = 6em,
		on grid,
		auto
	}
\usepackage[color=cyan]{todonotes}

%% ===> Page Settings <===
\setlength{\parindent}{0em}
\setlength{\parskip}{0.5em}
%% ===> End Page Settings <===

%% ===> Theorem Type Definitions <===
\newtheorem{theorem}{Theorem}
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{proposition}[theorem]{Proposition}
\newtheorem{claim}[theorem]{Claim}
\newtheorem{corollary}[theorem]{Corollary}
\newtheorem{definition}[theorem]{Definition}
\newtheorem{example}[theorem]{Example}
\newtheorem{conjecture}[theorem]{Conjecture}
%% ===> END Theorem Type Definitions <===

%% ===> Deliminaters <===
\DeclarePairedDelimiter\ceil{\lceil}{\rceil}
\DeclarePairedDelimiter\floor{\lfloor}{\rfloor}
\DeclarePairedDelimiter\anglebrac{\langle}{\rangle}
\DeclarePairedDelimiter\norm{\lVert}{\rVert}
%% ===> END Deliminaters <===

%% ===> Math Operators <===
%% ---> General <---
\DeclareMathOperator*{\round}{rd}
\DeclareMathOperator*{\YES}{\mathsf{YES}}
\DeclareMathOperator*{\NO}{\mathsf{NO}}
\DeclareMathOperator*{\argmax}{\arg\!\max}
\DeclareMathOperator*{\argmin}{\arg\!\min}
\DeclareMathOperator*{\range}{range}
\DeclareMathOperator*{\poly}{poly}
\newcommand\metric[1]{L_{#1}}
\DeclareMathOperator*{\sign}{sign}
\newcommand\family[1]{\mathcal{#1}}	% Cursive captials are subsets of the power set
\newcommand\op[1]{\textsc{#1}}		% Operations
\newcommand{\ind}[1]{\mathbb{#1}}
%% ---> Probability <---
\DeclareMathOperator*{\prob}{\mathbb{P}}
\DeclareMathOperator*{\expected}{\mathbb{E}}
\DeclareMathOperator*{\var}{Var}
\DeclareMathOperator*{\pseudoexpected}{\tilde{\mathbb{E}}}
\DeclareMathOperator*{\normal}{\mathcal{N}}
\DeclareMathOperator*{\uniform}{\mathcal{U}}
\DeclareMathOperator*{\geo}{Geo}
\newcommand{\distrib}[1]{\mathcal{#1}}
%% ---> Geometry <---
\DeclareMathOperator*{\area}{area}
\DeclareMathOperator*{\dist}{dist}
\DeclareMathOperator*{\vol}{vol}
\newcommand\polytope[1]{\mathcal{#1}}
%% ---> Linear Algebra <---
\DeclareMathOperator*{\rank}{rank}
\DeclareMathOperator*{\sspan}{span}
\DeclareMathOperator*{\trace}{trace}
\DeclareMathOperator*{\supp}{support}
\DeclareMathOperator*{\diag}{diag}
\newcommand\mm[1]{\mathbf{#1}}	% Bolded capitals are MATRICES
\newcommand\vv[1]{\mathbf{#1}}	% Bolded lowercases are VECTORS
%% ---> Discrepancy <---
\newcommand\disc{\mathrm{disc}}
\newcommand\vecdisc{\mathrm{vecdisc}}
\newcommand\herdisc{\mathrm{herdisc}}
\newcommand\hervecdisc{\mathrm{hervecdisc}}
\newcommand\lindisc{\mathrm{lindisc}}
\newcommand\lattice{\Lambda}
%% ---> Graph Theory <---
\DeclareMathOperator*{\girth}{girth}
\DeclareMathOperator*{\diam}{diam}
%% ---> Complexty <---
\newcommand\Class[1]{\mathsf{#1}}
%% ===> End Math Operators <===

%% ===> Style Operators <===
\newcommand*{\vertbar}{\rule[-1ex]{0.5pt}{2.5ex}}
\newcommand*{\horzbar}{\rule[0.5ex]{2ex}{0.5pt}}
%% ===> End Style Operators <===

% ===> Comments <===
\newcommand{\sn}[1]{\todo[linecolor=red!40, backgroundcolor=white,bordercolor=red, size=\scriptsize]{Sasho: {#1}}}
\newcommand{\xn}[1]{\todo[linecolor=blue!40, backgroundcolor=white,bordercolor=blue, size=\scriptsize]{Lily: {#1}}}
% ===> Comments <===

%% ===> Macros <===
\newcommand\NN{\mathbb{N}}
\newcommand\ZZ{\mathbb{Z}}
\newcommand\QQ{\mathbb{Q}}
\newcommand\RR{\mathbb{R}}
\newcommand\CC{\mathbb{C}}
\newcommand\FF{\mathbb{F}}
\newcommand\Th{\text{th}}

\newcommand\Basis{\mathcal{B}}
\newcommand\Ball{\mathcal{B}}
%% ===> END Macros <===

% ===> Command Definitions <===
\newcommand\mynotes[1]{\textcolor{red}{#1}}
% ---> usage: \fig{NAME}{SCALE}{CAPTION}
\newcommand\fig[3]{
	\begin{figure}[ht]
		\centering
		\includegraphics[scale=#2]{figures/#1}
		\caption{#3}
		\label{fig:#1}
	\end{figure}
}	

% ---> usage: \problem{NAME}{ABBREV}{INST}{QUESTION}{COMMENT}
\newcommand\problem[5]{
	\def\temp{#5}\ifx\temp\empty
		% Do not include comments
		\fbox{
			\parbox{\textwidth}{
				[$\mathsf{#2}$] \textbf{#1}\\
				#3\\
				\textbf{Question:} #4
				\label{prob:#2}
			}	
		}
	\else
		\fbox{
			\parbox{\textwidth}{
				[$\mathsf{#2}$] \textbf{#1}\\
				#3\\
				\textbf{Question:} #4\\
				\textbf{Comments:} #5
				\label{prob:#2}
			}	
		}
	\fi
}

\newcommand\underconstruction{
	\begin{figure}[ht]
		\centering
		\includegraphics[scale=0.1]{../figures/construction}
		\caption*{\textcolor{red}{This section is under construction!}}
	\end{figure}
}
%% ===> END Command Definitions <===