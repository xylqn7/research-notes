\documentclass[a4paper,USenglish,cleveref, autoref, thm-restate]{lipics-v2019}
%This is a template for producing LIPIcs articles. 
%See lipics-manual.pdf for further information.
%for A4 paper format use option "a4paper", for US-letter use option "letterpaper"
%for british hyphenation rules use option "UKenglish", for american hyphenation rules use option "USenglish"
%for section-numbered lemmas etc., use "numberwithinsect"
%for enabling cleveref support, use "cleveref"
%for enabling autoref support, use "autoref"
%for anonymousing the authors (e.g. for double-blind review), add "anonymous"
%for enabling thm-restate support, use "thm-restate"

%\graphicspath{{./graphics/}}%helpful if your graphic files are in another directory
\usepackage{structure} % Input the document layout and structure

\makeglossaries
\input{glossary.tex} % Input the glossary

\title{On the Computational Complexity of Linear Discrepancy}

\author{Lily Li}{Department of Computer Science, University of Toronto, Canada}{xinyuan@cs.toronto.edu}{}{}

\author{Aleksandar Nikolov}{Department of Computer Science, University of Toronto, Canada}{anikolov@cs.toronto.edu}{}{}

\authorrunning{L. Li and A. Nikolov} %TODO mandatory. First: Use abbreviated first/middle names. Second (only in severe cases): Use first author plus 'et al.'

\Copyright{Lily Li and Aleksandar Nikolov} %TODO mandatory, please use full first names. LIPIcs license is "CC-BY";  http://creativecommons.org/licenses/by/3.0/

\ccsdesc[500]{Theory of computation~Design and analysis of algorithms}

\keywords{discrepancy theory, linear discrepancy, rounding,
  NP-hardness}


\funding{This research was supported by an NSERC Discovery Grant (application number RGPIN-2016-06333).}

\nolinenumbers %uncomment to disable line numbering

%\hideLIPIcs  %uncomment to remove references to LIPIcs series (logo, DOI, ...), e.g. when preparing a pre-final version to be uploaded to arXiv or another public repository

%Editor-only macros:: begin (do not touch as author)%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\EventEditors{Fabrizio Grandoni, Peter Sanders, and Grzegorz Herman}
\EventNoEds{3}
\EventLongTitle{28th Annual European Symposium on Algorithms (ESA 2020)}
\EventShortTitle{ESA 2020}
\EventAcronym{ESA}
\EventYear{2020}
\EventDate{September 7--9, 2020}
\EventLocation{Pisa, Italy (Virtual Conference)}
\EventLogo{}
\SeriesVolume{173}
\ArticleNo{62}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}

\maketitle

\begin{abstract}
  Many problems in computer science and applied mathematics require rounding a vector $\vv{w}$ with entries in $[0,1]$ to a binary vector $\vv{x}$ with entries in $\{0,1\}$ so that, for a given matrix $\mm{A}$, $\mm{A}\vv{x}$ is as close to $\mm{A}\vv{w}$ as possible. For example, this problem arises in LP rounding algorithms used to approximate $\class{NP}$-hard optimization problems and in the design of uniformly distributed point sets for numerical integration. For a given matrix $\mm{A}$, the worst-case error over all choices of $\vv{w}$ incurred by the best possible rounding is measured by the linear discrepancy of $\mm{A}$, a notion first introduced by Lovasz, Spencer, and Vesztergombi (EJC, 1986).

  We initiate the study of the computational complexity of linear discrepancy. Our investigation proceeds in two directions: (1) proving hardness results and (2) finding both exact and approximate algorithms to evaluate the linear discrepancy of certain matrices. For (1), we show that linear discrepancy is $\class{NP}$-hard so we do not expect to find an efficient exact algorithm in the general case. Thus for (2), we restrict our attention to matrices with a constant number of rows. We present a poly-time exact algorithm for matrices consisting of a single row and matrices with a constant number of rows and entries of bounded magnitude. We also present an exponential-time approximation algorithm for general matrices, and an algorithm that approximates linear discrepancy to within an exponential factor. 
\end{abstract}

\input{sec1-introduction-v3}
\input{sec2-hardness-v3}
\input{sec3-exactalgorithm-v3}
\input{sec4-openproblems}
		
\printglossary
\printglossary[type=\acronymtype]

%%
%% Bibliography
%%

%% Please use bibtex, 
\bibliography{bibliography}
\bibliographystyle{plainurl}% the mandatory bibstyle

\newpage
\appendix

\input{appendix-v2}

\end{document}


\section{Notation}

Below bold-face capital letters such as
$\mm{A}$ denote matrices; $A_{i,j}$ is the entry in the
$i$\textsuperscript{th} row and $j$\textsuperscript{th}
column of the matrix $\mm{A}$. Bold-face lowercase letters such as $\vv{x}$ denote vectors;
$x_i$ is the $i$\textsuperscript{th} entry of the vector $\vv{x}$. We denote the all ones
vector by $\ind{1}$. Whenever we write $i \in [n]$, we mean $i \in
\{1,2, ..., n\}$. For simplicity of exposition, assume that $\mm{A}
\in \QQ^{m \times n}$ in the following sections. We will explicitly
indicate other uses of $\mm{A}$.