% !TEX root = MAIN-lindisc.tex

\section{Introduction}


A number of questions in mathematics and computer science can be
reduced to the following basic rounding question: given a
vector $\vv{w}\in [0,1]^n$, and an $m\times n$ matrix $\mm{A}$, find
an integer vector $\vv{x} \in \{0,1\}^n$ such that $\mm{A}\vv{x}$ is as
close as possible to $\mm{A}\vv{w}$. For example, many
$\class{NP}$-hard optimization problems can be modeled as an integer
program
\begin{align*}
	\min\quad &\vv{c}^\top \vv{x}\\
	\text{s.t.}\quad &\mm{A}\vv{x} \ge \vv{b}\\
	&\vv{x} \in \{0,1\}^n
\end{align*}
This integer program can be relaxed to a linear program by replacing
the integer variables $\vv{x}\in \{0,1\}^n$ with real-valued variables
$\vv{w} \in [0,1]^n$. A powerful method in approximation algorithms is
to solve this linear programming relaxation to get an optimal
$\vv{w}$, and then round $\vv{w}$ to an integer solution $\vv{x}$
which is feasible (i.e., $\mm{A}\vv{x} \ge \vv{b}$), and has objective
value not much bigger than $\vv{c}^\top \vv{w}$. Often, a useful
intermediate step is to guarantee that $\vv{x}$ is approximately
optimal, i.e., that the coordinates of $\vv{b} - \mm{A}\vv{x}$ are
bounded from above. This approximately feasible solution can then,
hopefully, be turned into a truly feasible one with a small loss in
the objective value. This method was used, for example, by
Rothvoss~\cite{rothvoss2013approximating}, and Rothvoss and
Hoberg~\cite{hoberg2017logarithmic} to give the best known
approximation algorithm for the bin packing problem.

Another example is provided by the problem of constructing uniformly
distributed points, or, more generally, points that are
well-distributed with respect to some measure. Variants of this
problem date back to work by Weyl, van der Corput, van
Aardenne-Ehrenfest, and Roth, and have important applications to such
fields as numerical integration; see the book of
Matou\v{s}ek~\cite{matouvsek1999geometric} for references and an
introduction to the area. In the classical setting, the problem is
to find, for any positive integer $n$, a set of $n$ points $P$ in
$[0,1]^d$, so as to minimize the quantity
\[
  \sup_{R \in \mathcal{R}_d} ||R \cap P| - n \lambda_d(R)|,
\]
where $\mathcal{R}_d$ is the set of all axis-aligned boxes contained
in $[0,1]^d$, and $\lambda_d$ is the Lebesgue measure on $\RR^d$. The quantity
above is known as the (unnormalized) discrepancy of $P$. Note that if
we sample a random point uniformly from $P$, then it would land in $R$
with probability $\frac{|R \cap P|}{n}$; on the other hand, if we
sample a random point uniformly from $[0,1]^d$, then it would land in
$R$ with probability $\lambda_d(R)$. The problem of minimizing the
discrepancy of $P$ is then equivalent to finding a distribution that
is uniform over $n$ points that ``looks the same'' as the continuous
uniform distribution to all boxes $R$.

The discrepancy minimization
problem can be modeled by the rounding problem with which we started
our discussion. To that end, we can discretize the domain $[0,1]^d$ to
a finite set $X$ of size $N$, and let $\mm{A}$ be the incidence matrix
of sets induced by axis-aligned boxes, i.e., each row of $\mm{A}$ is
associated with a box $R$, and equals the indicator vector of
$R\cap X$. If we also let $\vv{w} = \frac{n}{N}\ind{1}$, where
$\ind{1}$ is the all-ones vector, then, for a sufficiently fine
discretization, each coordinate of $\mm{A}\vv{w}$ is a close
approximation of $n\lambda_d(R)$. The problem of finding an $n$-point
set $P$ of minimum discrepancy then becomes essentially equivalent to
minimizing $\|\mm{A}\vv{x} - \mm{A}\vv{w}\|_\infty$ over
$\vv{x} \in \{0,1\}^N$, where $\|\cdot\|_\infty$ is the standard
$\ell_\infty$ norm. In particular, given $\vv{x}$, we can take $P$ to consist of the points in $X$ for which the corresponding coordinate in $\vv{x}$ is
set to $1$. Then, since $[0,1]^d \in \mathcal{R}_d$, we have
$\left||P| - n\right| \le \|\mm{A}\vv{x} - \mm{A}\vv{w}\|_\infty$ and
we can remove or add at most $\|\mm{A}\vv{x} - \mm{A}\vv{w}\|_\infty$ points
to $P$ to make it exactly of size $n$. The discrepancy of $P$ is then
bounded by $2 \|\mm{A}\vv{x} - \mm{A}\vv{w}\|_\infty$ plus the
additional error incurred by the discretization of $[0,1]^d$. 

These two examples motivate the definition of \emph{linear
  discrepancy}, initially introduced by Lov\'asz, Spencer, and
Vesztergombi~\cite{lovasz1986discrepancy}. The smallest possible error for rounding $\vv{w}$ with
respect to $\mm{A}$ is
\[
	\lindisc(\mm{A}, \vv{w}) = \min_{\vv{x} \in \{0,1\}^{n}} \norm{\mm{A}\left(\vv{w} - \vv{x}\right)}_{\infty}.
\]
This is the linear discrepancy of $\mm{A}$ with respect to
$\vv{w}$. The linear discrepancy of $\mm{A}$ is defined as
the worst case over all $\vv{w}\in [0,1]^n$ i.e.
\begin{equation}
  \label{eq:lindisc}
  \lindisc(\mm{A}) = \max_{\vv{w} \in [0,1]^n} \lindisc(\mm{A}, \vv{w}). %= \max_{\vv{w} \in [0,1]^n}\min_{\vv{x} \in \{0,1\}^{n}} \norm{\mm{A}\left(\vv{w} - \vv{x}\right)}_{\infty}
\end{equation}
It will be useful to consider a maximizer of equation
\eqref{eq:lindisc} i.e. $\mm{w}^* \in [0,1]^n$ such that
$\lindisc(\mm{A}, \mm{w}^*) = \lindisc(\mm{A})$. We call $\vv{w^*}$ a
\emph{deep-hole} of $\mm{A}$. Every $\mm{A}$ has at least one
deep-hole, since linear discrepancy is a continuous function over the
compact set $[0,1]^n$.
%Further, if $\vv{w}^*$ is a deep-hole then so is $\ind{1} -
%\vv{w}^*$, simply take the colouring to be $\ind{1}-\vv{x}$.

The special case of $\lindisc(\mm{A}, \vv{w})$ when $\vv{w} = \frac12 \ind{1}$ is especially
well studied. When $\mm{A}$ is the indicator matrix of a collection
$\family{S}$ of $m$ subsets of a universe $X$, $\lindisc(\mm{A}, \frac12 \ind{1})$ measures
to what extent it is possible to choose a subset $S$ of $X$ that
contains approximately half the elements of each set. This is a
rescaling of the well-known combinatorial discrepancy of $\family{S}$,
defined as
\[\disc(\family{S}) = \min_{\chi:X \to \{-1, +1\}}
  \max_{S \in \family{S}}\left|\sum_{s \in S} \chi(s)\right|
\]
It is straightforward to check that, by a change of variables,
$\disc(\family{S}) = 2\cdot\lindisc(\mm{A}, \frac12 \ind{1})$ where,
again, $\mm{A}$ is the incidence matrix of $\family{S}$. This
definition can be extended to arbitrary matrices $\mm{A}$ as
$\disc(\mm{A}) = 2\cdot\lindisc(\mm{A}, \frac12 \ind{1})$. Combinatorial
discrepancy has been widely studied in combinatorics and computer
science, see~\cite{beck1996discrepancy, chazelle2001discrepancy, matouvsek1999geometric}.

Sometimes $\disc(\mm{A})$ can be small ``by accident", thus it is
useful to define a more robust discrepancy variant.\footnote{Consider
  any matrix $\mm{B} \in \RR^{m \times n}$ and let
  $\mm{A} \in \RR^{m \times 2n}$ be the concatenation of two copies of
  $\mm{B}$ side by side. Regardless of the discrepancy of $\mm{B}$,
  $\disc(\mm{A}) = 0$ since there exists $\vv{x} \in \{-1, 1\}^n$ such
  that $\norm{\mm{A}\vv{x}}_{\infty} = 0$, namely
  \[\vv{x}^{\intercal} = [\underbrace{-1, ..., -1}_{n}, \underbrace{1, ..., 1}_{n}].\]}		
The hereditary discrepancy of $\mm{A}$ is the maximum discrepancy over all sub-matrices, i.e.,
\begin{equation}
  \label{eq:hereditarydiscrepancy}
  \herdisc(\mm{A}) = \max_{\mm{B}} \disc(\mm{B}),
\end{equation}
where $\mm{B}$ ranges over submatrices of $\mm{A}$.


A fundamental theorem by Lov\'asz, Spencer, and Vesztergombi shows that linear
discrepancy can be bounded above by twice the hereditary discrepancy.
\begin{theorem}
  \label{eq:lindiscubstandardherdisc}
  (Lov\'{a}sz et al. 1986, \cite{lovasz1986discrepancy}) $\lindisc(\mm{A}) \leq 2\cdot\herdisc(\mm{A})$.
\end{theorem}
A number of the applications of combinatorial discrepancy use this
basic theorem. In particular, it is common to give an upper bound on
the hereditary discrepancy, and from that deduce an upper bound on the
linear discrepancy. For example, this strategy was used to give
approximation algorithms for bin
packing~\cite{rothvoss2013approximating,hoberg2017logarithmic}, and broadcast scheduling~\cite{BansalKN14},
and to design point sets well distributed with respect to arbitrary
Borel measures~\cite{nikolov17tusnady,transference}. However, linear discrepancy can be
much smaller (by a factor of at least $2^n$) than hereditary
discrepancy,\footnote{Whether this remains true if the matrix $\mm{A}$
  has bounded entries is a tantalizing open question.} so hereditary
discrepancy lower bounds do not translate to linear discrepancy, and,
in general, linear discrepancy lower bounds appear to be
challenging. 
Arguably, a better understanding of linear discrepancy
itself would allow proving more and tighter results, in comparison with going
through hereditary discrepancy. For example, it is likely that new
analytic tools to estimate linear discrepancy would allow progress on
questions in geometric discrepancy theory, as well as questions about the
integrality gaps of linear programming relaxations of important
optimization problems, such as the bin packing problem. 

A sequence of recent works has shed light on the computational
complexity of combinatorial and hereditary discrepancy. It is now
known that combinatorial discrepancy does not allow efficient
approximation algorithms, even in a weak sense (assuming
$\class{P} \neq \class{NP}$)\cite{charikar2011tight}, while hereditary
discrepancy is $\class{NP}$-hard to approximate better than a factor
of two~\cite{austrin20172+varepsilon}, and can be approximated within
poly-logarithmic factors~\cite{matousek18factorization}. Despite being the tool most
directly relevant to many applications of discrepancy, however,
essentially nothing is known about the computational complexity of
linear discrepancy itself. In this paper, we initiate the study of
linear discrepancy from a computational viewpoint, and give both
the first hardness results, as well as the first exact and approximate
algorithms for it.

Before stating our results, it is worth mentioning that linear
discrepancy can also be seen as an analogue of the covering radius in
lattice theory. Let $\lattice \subset \RR^n$ be a lattice,
i.e.\ integer combination of $n$ independent vectors of $\RR^n$, and let us choose
$\vv{b}_1, \ldots, \vv{b}_n$ to be a basis of $\lattice$. Let $\mm{B}$
be a matrix with the $\vv{b_i}$ as its columns. The
covering radius of $\lattice$ in the $\metric{p}$-norm is
defined as
\begin{equation}
  \label{eq:coveringradius}
  \rho(\lattice)
  = \max_{\vv{y} \in \RR^n}\min_{\vv{z} \in     \lattice}\norm{\vv{y} - \vv{z}}_p
  = \max_{\vv{w} \in \RR^n}\min_{\vv{x} \in \ZZ^{n}}\norm{\mm{B} \cdot (\vv{w}-\vv{x})}_p
  = \max_{\vv{w} \in [0,1]^n}\min_{\vv{x} \in \ZZ^{n}}\norm{\mm{B} \cdot (\vv{w}-\vv{x})}_p,
\end{equation}
and is independent of the basis. This definition is equivalent to the
the definition of $\lindisc(\mm{A})$, except that the minimum is over
$\ZZ^n$ rather than $\{0,1\}^n$. Haviv and Regev showed that the
covering radius problem ($\class{CRP}$) in the $\metric{p}$-norm is
$\Pi_2$-hard to approximate within some fixed
constant for all large
enough $p$~\cite{haviv2006hardness}, and Guruswami, Micciancio, and Regev
showed it
can be approximated within a factor of $2^{O(n\log n / \log \log n)}$
for the case of $p =2$~\cite{guruswami2005complexity}.

\subsection{Our Results} 

Let us start with the simple observation that, when $\mm{A}$ is a
single row matrix, deciding $\lindisc(\mm{A}, t\ind{1}) = 0$
is the $\class{NP}$-hard Subset Sum problem with target sum
$t \sum_{j = 1}^n{A_{1,j}}$, and is, therefore,
$\class{NP}$-hard. This does not show, however, that computing
$\lindisc(\mm{A})$ is $\class{NP}$-hard. In this work we show the
following hardness result for linear discrepancy.
\begin{theorem}
  \label{thm:hardness}
  The Linear Discrepancy problem of deciding, given an $m\times n$
  matrix $\mm{A}$ with rational entries, and a rational number $t$,
  whether $\lindisc(\mm{A}) \le t$, is $\class{NP}$-hard and is
  contained in the class $\Pi_2$.
\end{theorem}
	
% Suppose $\mm{A} \in \RR^{d \times n}$ for constant $d$ with fundamental parallelepiped $\polytope{P}_{A}$. By the geometric interpretation above, $\lindisc(\mm{A})$ is the smallest scaling of $\polytope{P}_{A} + \vv{x}$ for $\vv{x} \in \{0,1\}^{n}$ such their union covers the unit hypercube $\{0,1\}^{n}$. Apply $\mm{A}$ to $[0,1]^{n}$ to obtained the zonotope $\polytope{Z}_A \in \RR^{d}$. Apply $\mm{A}$ to each fundamental parallelepiped $\polytope{P}_A$ to obtain the $\metric{\infty}$ unit-ball centered at $\mm{A}\vv{x}$ for each $\vv{x} \in \{0,1\}^n$. Then $\lindisc(\mm{A})$ can be interpreted as the radius of the largest empty square with center in $\polytope{Z}_A$.
	
% 	If we had to find the largest such circle, instead of square, then this is identical to the largest empty circle (LEC) problem in computational geometry \cite{toussaint1983computing}. In the plane, it is possible to compute the largest empty circle contained in the convex hull of $n$ points in time $O(n\log n)$ by constructing the Voronoi diagram associated with these points. This corresponds to finding the solution to a variant of linear discrepancy 
% 	\[\lindisc_{2}(\mm{A}) = \max_{\vv{w} \in [0,1]^{n}}\min_{\vv{x} \in \{0,1\}^n} \norm{\mm{A}(\vv{w} - \vv{x})}_{2}\]
% 	in time $O(n2^{n})$ where $\mm{A} \in \RR^{2 \times n}$. Generally there exists algorithms, both randomized and deterministic, which compute the Voronoi diagram in $\metric{2}$ of $n$ points in $\RR^d$ with $O(n^{\ceil{d/2}})$ expected \cite{clarkson1989applications, seidel1991small} and $O(n\log n + n^{\ceil{d/2}})$ deterministic \cite{chazelle1993optimal} time\footnote{It is well known that computing the Voronoi diagram of $n$ points in $\RR^{d}$ for $\metric{2}$-norm can be interpreted as computing the convex hull of $n$ points in $\RR^{d+1}$ \cite{brown1979voronoi}. Thus all references here compute the convex hull of $n$ points in $\RR^{d}$.}. This can be transformed into algorithms which solve $\lindisc_{2}(\mm{A})$ for $\mm{A} \in \RR^{d \times n}$ in $O(2^{\ceil{nd/2}})$ expected and $O(n2^{n} + 2^{\ceil{nd/2}})$ deterministic time.
	
% 	To find the largest empty square, we can construct the Voronoi diagram in the $\metric{\infty}$-norm. For $n$ points in $\RR^{d}$, such randomized algorithms have expected running time $O(n^{\ceil{d/2}}\log^{d-1}n)$ \cite{DBLP:journals/dcg/BoissonnatSTY98}.\footnote{This is a side note: I actually do not feel comfortable saying that if there exists an algorithm for constructing a Voronoi diagram then there is also a polytime algorithm to solve the largest empty square problem, but that is what many Computational Geometers seem comfortable with (see \emph{Computing the Largest Empty Rectangle} of Chazelle, Drysdale, and Lee --- who cite an incorrect proof of Shamos ---- as well as the paper of Toussaint referenced above).}
	
We present algorithms for computing linear discrepancy exactly when
the matrix $\mm{A}$ has a constant number of rows. We start with a
result for a single row matrix.
\begin{theorem}
  \label{thm:exact-one-row}
  For any matrix $\mm{A} \in \QQ^{1 \times n}$, $\lindisc(\mm{A})$ can be computed in time $O(n\log n)$. 
\end{theorem}
Note that this stands in contrast to the observation above that
computing $\lindisc(\mm{A}, \vv{w})$ is hard even for a single-row
matrix $\mm{A}$. 
In addition to the theorem above, we also give a corresponding
rounding result, showing that any $\vv{w}\in \QQ^n$ can be efficiently
rounded to within error bounded by the linear discrepancy in the case
of single row matrices. 
\begin{theorem}
  \label{thm:approx-one-row}
  For any matrix $\mm{A} \in \QQ^{1 \times n}$ and any $\vv{w} \in ([0,1]
  \cap \QQ)^n$, we can find an $\vv{x} \in \{0,1\}^{n}$ such that $\norm{\mm{A}(\vv{w} - \vv{x})}_{\infty} \leq \lindisc(\mm{A})$ in time $O(n\log n)$.
\end{theorem}
This result stands in contrast with the hardness of the subset sum
problem, which easily implies that it is $\class{NP}$-hard to round
$\vv{w}$ to within error $\lindisc(\mm{A}, \vv{w})$ even when $\mm{A}$
is a single row matrix.

We can extend Theorem~\ref{thm:exact-one-row} to the case of matrices
with a bounded number of rows, with the additional assumption that the
entries of $\mm{A}$ are bounded. Removing this additional assumption
is a fascinating open question. 
\begin{theorem}
  \label{thm:exact-const-row}
  For any matrix $\mm{A} \in \ZZ^{d \times n}$ where $d$ is some fixed constant and $\max_{i,j}|A_{i,j}| \leq \delta$, $\lindisc(\mm{A})$ can be computed in time $O\left(d(n\delta)^{d^2+d}\right)$.  
\end{theorem}
        
We further present an approximation algorithm for linear discrepancy.
\begin{theorem}
  \label{thm:approx}
  For any matrix $\mm{A} \in \QQ^{m \times n}$, $\lindisc(\mm{A})$ can be
  approximated in polynomial time within a factor of $2^{n+1}$. 
%  \[\norm{\mm{A}}_{\infty \rightarrow \infty} \leq O(2^{n})\lindisc(\mm{A}) \leq O(2^{n}) \norm{\mm{A}}_{\infty \rightarrow \infty}\]
\end{theorem}
%where $\norm{\mm{A}}_{p \rightarrow q}$ is the operator norms defined as
%\[\norm{\mm{A}}_{p \rightarrow q} = \max_{\vv{x}\in \RR^n} \frac{\norm{\mm{A}\vv{x}}_{q}}{\norm{\vv{x}}_{p}}.\]


% \subsection{Geometric Interpretation}


% 	Each discrepancy variant mentioned above has a corresponding geometric interpretation. Let $\polytope{P}_{A} = \{\vv{x}: \norm{\mm{A}\vv{x}}_{\infty} \leq 1\}$ be the \emph{fundamental parallelepiped} of $\mm{A}$. Observe that $\polytope{P}_{A}$ is convex and symmetric. Imagine placing a scaled copy $t \cdot \polytope{P}_{A}$ at every corner of the unit hyper-cube $\{0,1\}^n$. The smallest $t$ for which some polytope contains $\frac{1}{2}\ind{1}$ is exactly $\disc(\mm{A})$. Similarly, the smallest $t$ for which the union of all polytopes covers the unit-hypercube $\{0,1\}^n$ is $\lindisc(\mm{A})$. 
 
% 	\fig{geometricview}{1}{The geometric interpretation of discrepancy (left) and linear discrepancy (right). The dark gray polygons on the left represent $\polytope{P}_{A}$ placed at each corner of the unit cube. The scaling required to cover the center is the discrepancy. The scaling required to cover the entire cube is the linear discrepancy.}
	
% 	Formally, we can define these as
% 	\begin{align*}
% 		\disc(\mm{A}) &= \inf\left\{t \geq 0: \frac{1}{2}\ind{1} \in \bigcup_{\vv{x} \in \{0,1\}^n} \vv{x} + t\polytope{P}_{A}\right\} \mbox{ and }\\
% 		\lindisc(\mm{A}) &= \inf\left\{t \geq 0: [0,1]^n \subset \bigcup_{\vv{x} \in \{0,1\}^n} \vv{x} + t\polytope{P}_{A}\right\}.
% 	\end{align*}
% 	See Figure \ref{fig:geometricview}. Hereditary discrepancy can be defined analogously\footnote{It might first appear as though the hereditary discrepancy is the scaling which covers all points of the form $\{0, 1/2, 1\}^n$ --- these correspond to the centers of all low dimensional faces of the unit hyper-cube --- but it is not so. Consider the row matrix $\mm{A} = [1,2]$. The maximum discrepancy is achieved on the subset $\mm{A}' = [2]$ so $\herdisc(\mm{A}) = 1$. However, for any $\vv{w} \in \{0,1/2,1\}^2$, there exists some $\vv{x} \in \{0,1\}^2$ such that $(w_1 - x_1) + 2(w_2 - x_2) \leq \frac{1}{2}$. To accurately represent $\herdisc(\mm{A})$ we must take care when dealing with the deleted columns. If column $i$ is removed, then $w_i \in \{0,1\}$ and the chosen colouring $\vv{x}$ must satisfy $x_i = w_i$. Thus the hereditary discrepancy is the scaling which covers the center of all low dimensional faces \emph{using polytopes placed at a vertex on the boundary of that face}.} 
	
% 	Various resources give more thorough treatments of these topics \cite{beck1996discrepancy, chazelle2001discrepancy, matouvsek1999geometric}.



%	\subsection{Paper Structure}
%	In section two we prove the hardness result for linear discrepancy by first reproving hardness results for discrepancy and hereditary discrepancy. In section three  we presents algorithms for Theorems \ref{thm:exact-const-row}, \ref{thm:approx-one-row}, \ref{thm:exact-const-row}, and \ref{thm:approx} in this order. The techniques used to prove Theorem \ref{thm:exact-one-row} and \ref{thm:approx-one-row} are independent of the techniques used to prove Theorem \ref{thm:exact-const-row} which are independent of the techniques used to prove Theorem \ref{thm:approx} so the subsection can be read in any order. In section four we cover many possible directions in which to continue this research.

        