% !TEX root = MAIN-lindisc.tex

\section{Hardness Result}	
	In this section, we show that linear discrepancy ($\class{LDS}$) is
	$\class{NP}$-Hard by reducing from monotone not-all-equal
	3-$\class{SAT}$ ($\class{MNAE3SAT}$)~\cite{gold1978complexity} to
	each. The decision problem version of linear discrepancy we consider
	is defined below.

	\problem{Monotone Not-All-Equal 3-$\class{SAT}$}{MNAE3SAT}{Let $U$ be a collection of variables $\{u_1, ..., u_n\}$ and $\mathcal{C}$ be a 3-$\mathsf{CNF}$ with clauses $\{C_1, ..., C_m\}$ such that $C_i = t_{i,1} \lor t_{i,2} \lor t_{i,3}$ for positive literals $t_{i,j}$.}{Does there exist a truth assignment $\tau: U \rightarrow \{\mathsf{T}, \mathsf{F}\}$ such that $\mathcal{C}$ is satisfied and each clause has at least one true and one false literal?}{}
	
	\problem{Linear Discrepancy}{LDS}{Let $\mm{A} \in \QQ^{m \times n}$ be a matrix and $t \geq 0$ a rational value.}{Is $\lindisc(\mm{A}) \leq t$?}{}
		
	% \subsection{Discrepancy and Hereditary Discrepancy}
	
	% 	To see that this reduction is sound, suppose instead that $\mathcal{C}$ is a $\YES$-instance of $\class{MNAE3SAT}$. Let $\tau^*$ be a satisfying truth assignment of $\mathcal{C}$. Let $\vv{x}^*$ be the indicator vector of the true variables in $\tau^*$. Then 
	% 	\[\disc(\mm{A}) \leq \left\|\mm{A}\left(\frac{1}{2} \cdot \ind{1} - \vv{x}^{*}\right)\right\|_{\infty} = \frac{1}{2}\] 
	% 	since every clause has exactly two elements with the same truth value. Thus $\mm{A}$ is a $\YES$-instance of $\class{DS}$. 

	% 	Hereditary discrepancy can be shown hard using the same reduction with $t = 1$. A $\NO$-instance of $\class{MNAE3SAT}$ translates into a $\NO$-instance of $\class{HDS}$ since $\herdisc(\mm{A}) \geq  \disc(\mm{A}) = \frac{3}{2}$. A $\YES$-instance of $\class{MNAE3SAT}$ translates into a $\YES$-instance of $\class{HDS}$ as it did for $\class{DS}$. 
		
	% 	Let $\tau^*$ be a satisfying assignment of $\mathcal{C}$ and $\vv{x}^*$ be the indicator vector of the true variables. Consider any subset of the variables $U' \subseteq U$. We will show that $\mm{A}_{U'}$, the matrix $\mm{A}$ restricted to the variables in $U'$, satisfies $\disc(\mm{A}_{U'}) \leq 1$. Consider a row of $\mm{A}_{U'}$. Either the row has fewer than three or exactly three non-zero entries. In the former case any assignment to the variables achieves discrepancy $\leq 1$ for that row. In the latter case all variables in the associated clause are intact. The rows of $A_{U'}$ which have three non-zero entries correspond to a subset of the clauses in $\mathcal{C}$. Since $\tau^*$ is a satisfying assignment of $\mathcal{C}$, it must also be a satisfying assignment of this subset of clauses. Let $\vv{x}^*_{U'}$ be the vector $\vv{x}^*$ restricted to the variables in $U'$ and $\frac{1}{2}\cdot\ind{1}_{U'}$ be the vector $\frac{1}{2}\cdot\ind{1}$ restricted to the variables in $U'$. Then 
	% 	\[\herdisc(\mm{A}) \leq \max_{U' \subset U}\left\|\mm{A}_{U'}\left(\frac{1}{2}\cdot\ind{1}_{U'} - \vv{x}_{U'}\right)\right\|_{\infty} \leq 1\] 
	% 	since $U'$ was an arbitrary subset of $U$.
        
	\subsection{Linear Discrepancy}
	Before we show that linear discrepancy is hard, we will show that the value of $\lindisc(\mm{A})$ can be expressed using a polynomial number of bits in the bit complexity of a matrix for rational matrices. Due to space constraints, the proof can be found in Section~\ref{app:lemma-certificateinpi2}.
	\begin{lemma}
		\label{lem:certificateinpi2}
		For any matrix $\mm{A} \in \QQ^{m \times n}$, there exists a
		deep hole for $\mm{A}$ with bit complexity polynomial in $n$
		and the bit complexity of $\mm{A}$, and, therefore, $\lindisc(\mm{A})$
		can be written in number of bits polynimial in $n$ and the bit
		complexity of $\mm{A}$.
	\end{lemma}
	
	\begin{proof}[Proof of Theorem~\ref{thm:hardness}]
		Note first that the fact that $\class{LDS}$ is contained in $\Pi_2$
		is a straightforward consequence of
		Lemma~\ref{lem:certificateinpi2}: the ``for-all'' quantifier is over
		potential deep holes $\vv{w}\in [0,1]^n$ of the appropriate
		polynomially bounded bit complexity, and the ``exists'' quantifier is
		over $\vv{x} \in \{0,1\}^n$. 
		
		Next we prove hardness.  Let 3-$\mathsf{CNF}$ $\mathcal{C}$ be a
		$\class{MNAE3SAT}$ instance as described above. The corresponding
		$\class{LDS}$ instance will be the incidence matrix
		$\mm{A} \in \{0,1\}^{m \times n}$ of $\mathcal{C}$: column
		$\vv{a}_j$ of $\mm{A}$ corresponds to variable $u_j$ and row
		$\vv{r}_i$ of $\mm{A}$ corresponds to clause $C_i$, and $A_{i,j} =
		1$ if and only if variable $u_j$ appears in clause $C_i$. Let the
		target $t$ in the $\class{LDS}$ problem be $\frac{3}{2} - \epsilon$
		for $\epsilon > 0$ to be determined later.
		
		
		Consider first that case that $\mathcal{C}$ is a $\NO$-instance of
		$\class{MNAE3SAT}$ i.e. for every truth assignments $\tau$, there
		exists a clause $C_i$ whose literals all get the same truth
		assignment. Each $\vv{x} \in \{0, 1\}^n$ corresponds to a truth
		assignment. If $x_i = 1$ (resp. $x_i = 0$) then $u_i$ is true
		(resp. $u_i$ is false). Let $C_j$ be the clause whose literals have
		the same truth value. Then
		\[
		\lindisc(\mm{A}) \ge \lindisc(\mm{A}, (1/2)\cdot \ind{1}) \geq
		\left|\vv{r}_j \left(\frac{1}{2} \cdot \ind{1} -
		\vv{x}\right)\right| = \frac{3}{2} > \frac32 - \epsilon,\]
		so $\mm{A}$ is a $\NO$-instance of $\class{LDS}$. 
		
		Consider next the case that $\mathcal{C}$ is a $\YES$-instance of
		$\class{MNAE3SAT}$, and let $\tau$ be a satisfying
		assignment. Suppose $\vv{w}^* \in [0,1]^n$ is a deep-hole of
		$\mm{A}$. If $w_i^* = \frac{1}{2}$ for all $i \in [n]$ then
		\[
		\lindisc(\mm{A}) = \lindisc(\mm{A}, (1/2)\cdot \ind{1})
		=\disc(\mm{A}) \leq \left\|\mm{A}\left(\frac{1}{2} \cdot \ind{1} -
		\vv{x}^{*}\right)\right\|_{\infty} = \frac{1}{2}
		\] 
		since every clause has exactly two elements with the same truth
		value. Thus $\mm{A}$ is a $\YES$-instance of $\class{LDS}$ as long
		as we choose $\epsilon \le 1$. Suppose then that $\vv{w}^* \neq
		\frac12 \ind{1}$, and let $\epsilon$
		be a lower bound on the smallest non-zero gap between $w_i^*$ and $1/2$ i.e. for all
		$w_i^* \neq \frac{1}{2}$,
		\[\left|w_i^* - \frac{1}{2}\right| \geq
		\epsilon.\]
		By Lemma~\ref{lem:certificateinpi2}, which implies that $\vv{w}^*$
		has polynomial bit complexity, we know that we can choose such an
		$\epsilon$ of polynomial bit complexity. 
		We will show that $\lindisc(\mm{A}, \vv{w}^*) \leq \frac{3}{2} - \epsilon$ by constructing a colouring $\vv{x}^*$. Let 
		\[x_i^* = 
		\begin{cases}
		\round(w_i^*) &\mbox{if } w_i^* \neq \frac{1}{2}\\
		\tau(u_i) &\mbox{otherwise}
		\end{cases}\]
		where $\round(w_i^*)$ is $w_i^*$ rounded to the closest integer and $u_i$ is the variable corresponding to column $i$. Let $\vv{r}$ be a row of matrix $\mm{A}$ with non-zero entries in columns $i$, $j$, and $k$. We bound the discrepancy of row $\vv{r}$ based on the number of rounded variables $R_v$ among $\{x_i, x_j, x_k\}$.  
		\begin{enumerate}
			\item[$R_v = 0$:] Since none of the variables are rounded, $w_i^* = w_j^* = w_k^* = \frac{1}{2}$ and 
			\[\left|\vv{r}\left(\vv{x}^*-\vv{w}^*\right)\right| = \left|\left(x_i^* - \frac{1}{2}\right) + \left(x_j^* - \frac{1}{2}\right) + \left(x_k^* - \frac{1}{2}\right)\right| = \frac{1}{2}\]
			since $\tau$ is a satisfying assignment.
			\item[$R_v = 1$:] W.l.o.g assume that that $x_i^*$ is the rounded value and $w_j^* = w_k^* = \frac{1}{2}$. Then  
			\[\left|\vv{r}\left(\vv{x}^* - \vv{w}^*\right)\right| = \left|\left(x_i^* - w_i^*\right) + \left(x_j^* - \frac{1}{2}\right) + \left(x_k^* - \frac{1}{2}\right)\right| \leq \left(\frac{1}{2} - \epsilon\right) + 1 = \frac{3}{2} - \epsilon.\]
			\item[$R_v = 2$:] W.l.o.g assume that $x_i^*$ and $x_j^*$ are the rounded values and $w_k^* = \frac{1}{2}$. Then  
			\[\left|\vv{r}\left(\vv{x}^* - \vv{w}^*\right)\right| = \left|\left(x_i^* - w_i^*\right) + \left(x_j^* - w_j^*\right) + \left(x_k^* - \frac{1}{2}\right)\right| \leq 2 \cdot \left(\frac{1}{2} - \epsilon\right) + \frac{1}{2} = \frac{3}{2} - 2\epsilon.\]
			\item[$R_v = 3$:] All three values are rounded so  
			\[\left|\vv{r}\left(\vv{x}^* - \vv{w}^*\right)\right| = \left|\left(x_i^* - w_i^*\right) + \left(x_j^* - w_j^*\right) + \left(x_k^* - w_k^*\right)\right| \leq 3\cdot\left(\frac{1}{2} - \epsilon\right) = \frac{3}{2} - 3\epsilon.\]
		\end{enumerate}
		Since $\vv{r}$ was an arbitrary row of
	                $\mm{A}$, $\lindisc(\mm{A}) = \lindisc(\mm{A},
	                \vv{w}^*) \leq \frac{3}{2} - \epsilon$ as
	                required. This completes the reduction. 
	\end{proof}
		
