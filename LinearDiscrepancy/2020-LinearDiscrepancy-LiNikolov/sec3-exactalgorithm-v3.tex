% !TEX root = MAIN-lindisc.tex

\section{Algorithms for Linear Discrepancy}

In the following we consider restrictions and variants of linear
discrepancy for which we are able to give poly-time algorithms. The first subsection considers matrices with a single
row. The second subsection considers matrices
$\mm{A} \in \ZZ^{d \times n}$ with constant $d$ and entry of largest
magnitude $\delta$. In that case, we compute $\lindisc(\mm{A})$ in
time $O\left(d(2n\delta)^{d^2}\right)$. The third subsection presents
a poly-time $2^n$ approximation to $\lindisc(\mm{A})$ for
$\mm{A} \in \QQ^{m \times n}$.

\subsection{Linear Discrepancy of a Row Matrix} 	
\label{sssection:onerow}
We begin by developing some intuition for the linear discrepancy of a
one-row matrix, $\mm{A} = [a_1, ..., a_n]$. For now, let us make the
simplifying assumption that the entries of $\mm{A}$ are non-negative
and sorted in decreasing order. Define the \emph{subset sums} of
$\mm{A}$ to be the multi-set
$\family{S}(\mm{A}) = \{s_1, ..., s_{2^n}\}$ where each
$s_i = \mm{A}\vv{x}$ for exactly one $\vv{x} \in \{0,1\}^n$. Enumerate
the element of $\family{S}(\mm{A})$ in non-decreasing order, i.e.
$s_i \leq s_{i+1}$. If $\ell_{A} = 2\cdot\lindisc(\mm{A})$, then
$\ell_A$ is the width of the largest gap between consecutive entries
in $\family{S}(\mm{A})$.
	
Suppose $\mm{A}_i = [a_1, ..., a_i]$. Let us consider how
$\family{S}(\mm{A}_i)$ and $\lindisc(A_i)$ change for the first couple
of values of $i$. Clearly, $\family{S}(\mm{A}_1) = [0, a_1]$ and
$\lindisc(\mm{A}_1) = \frac{a_1}{2}$. $\family{S}(\mm{A}_2)$ is the
disjoint union of $\family{S}(\mm{A}_1)$ and
$\family{S}(\mm{A}_1)$ shifted to the right by $a_2$. Since
$a_1 \geq a_2$, $\family{S}(\mm{A}_2) = [0, a_2, a_1, a_1 + a_2]$
where the largest gap is of size $\max(a_2, a_1 - a_2)$. See Figure
\ref{fig:onerowalgexp}. In general, the entries of
$\family{S}(\mm{A}_i)$ consists of two copies of
$\family{S}(\mm{A}_{i-1})$ with one shifted to the right by $a_i$. The
gaps in $\family{S}(\mm{A}_i)$ are gaps previously in
$\family{S}(\mm{A}_{i-1})$ or between an element of
$\family{S}(\mm{A}_{i-1})$ and one in
$\{a_i + s: s \in \family{S}(\mm{A}_{i-1})\}$.
	
	\fig{onerowalgexp}{1}{Obtaining $\family{S}(\mm{A}_2)$ from $\family{S}(\mm{A}_1)$ when $a_1 \geq a_2$.}
	
	A similar structure occurs for general matrices with real valued entries with two caveats: (1) the previous interval is shifted left or right depending on the sign of the current entry (negative and positive respectively) and (2) the smallest entry of $\family{S}(\mm{A})$ is not zero but the sum of the negative entries in $\mm{A}$.
	
	\begin{lemma}
          \label{lem:onerowsortinghelps}
          Suppose $\mm{A}_{k-1} = [a_1, ..., a_{k-1}]$ with entries in $\RR$ and $|a_i| \geq |a_{i+1}|$. Let the largest gap in $\family{S}(\mm{A}_{k-1})$ be of size $\ell_{k-1}$. Then, for $\mm{A}_{k} = [a_1, ..., a_{k-1}, a_{k}]$ where $|a_{k}| \leq |a_{i}|$ for all $i \in [k-1]$, the largest gap in $\family{S}(\mm{A}_k)$ is of size $\max(|a_k|, \ell_{k-1} - |a_k|)$.
        \end{lemma}
	\begin{proof}
          Again, it is important to remember that the entries of $\family{S}(\mm{A}_{k})$ are exactly those in $\family{S}(\mm{A}_{k-1})$ along with those in $\{a_k + s: s \in \family{S}(\mm{A}_{k-1})\}$. Let $\ell = \max(|a_k|, \ell_{k-1} - |a_k|)$.
		
          We first show that $2\cdot\lindisc(\mm{A}_{k}) \leq \ell$ by
          showing that gaps between consecutive entries in
          $\family{S}(\mm{A}_{k})$ have size at most $\ell$. If
          $(s_{j}, s_{j+1})$ is a consecutive pair in
          $\family{S}(\mm{A}_{k-1})$ such that $s_{j+1} - s_j > \ell$,
          then $s_j$ and $s_{j+1}$ are no longer consecutive in
          $\family{S}(\mm{A}_{k})$, since
          $s_{j} \leq s_{j} + a_k \leq s_{j+1}$ if $a_k > 0$ and
          $s_{j} \leq s_{j+1} + a_k \leq s_{j+1}$ if $a_k <
          0$. See Figure \ref{fig:polytimealgrowmatrixlemma1}.
          Then, the gap given by any such pair gets split into gaps of
          size at most $\max\{|a_k|, s_{j+1} - s_j - |a_k|\} \le \ell$,
          where the inequality holds because $s_{j+1} - s_j \le \ell_{k-1}$.
          It follows that the size of each gap in
          $\family{S}(\mm{A}_k)$ is at most $\ell$.

          \fig{polytimealgrowmatrixlemma1}{1}{All consecutive pairs in $\family{S}(\mm{A}_{k-1})$ of size greater than $|a_k|$ will be divided into two or more consecutive pairs in $\family{S}(\mm{A}_k)$. The red interval indicates what happens when $a_k > 0$. The blue interval indicates what happens when $a_k < 0$.} 
		
          Next we will show that
          $2\cdot\lindisc(\mm{A}_{k}) \geq \ell$ by producing a pair
          of consecutive entries in $\family{S}(\mm{A}_{k})$ which
          achieves gap $\ell$. Suppose $\ell = |a_{k}|$. Recall that
          $s_0$ is the smallest subset sum of all entries in
          $\mm{A}_{k}$, which equals the sum of all negative entries
          in $\mm{A_k}$. Then it is easy to check that $s_1$ equals
          $s_0 + |a_k|$, where we recall that $a_k$ is the entry in
          $\mm{A}_k$ with minimum absolute value. Therefore, 
          $(s_0, s_0 + |a_{k}|)$ is a consecutive pair in
          $\family{S}(\mm{A}_k)$. This means that if $\ell = |a_k|$,
          then we are done, as we have produced a pair with gap $\ell$.
		
          When $\ell = \ell_{k-1} - |a_k| > |a_k|$, we split our analysis into two cases: (1) $a_k > 0$ and (2) $a_k < 0$. 
		
		In the former case, let $(s_{j^*}, s_{j^*+1})$ be a consecutive pair in $\family{S}(\mm{A}_{k-1})$ that achieves gap $\ell_{k-1}$ and suppose, towards a contradiction, that $s_{j^*} + a_k$ and $s_{j^*+1}$ do not appear consecutively in $\family{S}(\mm{A}_k)$. Then there must be some $s \in \family{S}(\mm{A}_k)$ such that $s_{j^*} + a_k < s < s_{j^*+1}$. Note that $s$ cannot be an element of $\family{S}(\mm{A}_{k-1})$ since $s_{j^*}$ and $s_{j^*+1}$ are consecutive in $\family{S}(\mm{A}_{k-1})$, so $s - a_k$ must be an element of $\family{S}(\mm{A}_{k-1})$. However, since $s > s_{j^*} + a_k$, we have $s - a_k > s_{j^*}$. This is a contradiction since $s_{j^*}$ and $s_{j^*+1}$ are consecutive entries in $\family{S}(\mm{A}_{k-1})$. See Figure \ref{fig:polytimealgrowmatrix}. Thus $(s_{j^*} + a_k, s_{j^*+1})$ must be a consecutive pair in $\family{S}(\mm{A}_k)$.
		
		\fig{polytimealgrowmatrix}{0.6}{Suppose $a_k < \ell_{k-1} - a_k$ and there exists $s \in \family{S}(\mm{A}_{k})$ such that $s_{j^*} + a_k < s < s_{j^*+1}$.}
		
		The latter case, when $a_k < 0$, is similar. Again there exists a pair of consecutive entries $(s_{j^*}, s_{j^*+1})$ in $\family{S}(\mm{A}_{k-1})$ which achieves gap $\ell_{k-1}$. Suppose, towards contradiction, that $s_{j^*}$ and $s_{j^*+1} - |a_k|$ do not appear consecutively in $\family{S}(\mm{A}_k)$. Then there must be some $s \in \family{S}(\mm{A}_k)$ such that $s_{j^*} < s < s_{j^*+1} - |a_k|$. Again, $s$ cannot be an element of $\family{S}(\mm{A}_{k-1})$ since $s_{j^*}$ and $s_{j^*+1}$ are consecutive in $\family{S}(\mm{A}_{k-1})$, so $s + |a_k|$ must be an element of $\family{S}(\mm{A}_{k-1})$. However since $s < s_{j^*+1} - |a_k|$, we have $s + |a_k| < s_{j^* + 1}$. This is a contradiction since $s_{j^*}$ and $s_{j^*+1}$ are consecutive entries in $\family{S}(\mm{A}_{k-1})$.
	\end{proof}

        Lemma~\ref{lem:onerowsortinghelps} has the following curious corollary.
	\begin{corollary}
		\label{cor:onerowtakemagnitudes}
		Let $\mm{A} = [a_1, ..., a_n]$ and $\mm{A}' = [|a_1|, ..., |a_n|]$. Then $\lindisc(\mm{A}) = \lindisc(\mm{A}')$.
	\end{corollary}
	
	Lemma \ref{lem:onerowsortinghelps} and Corollary \ref{cor:onerowtakemagnitudes} suggest an algorithm: replace the entries of $\mm{A}$ by their magnitudes. Sort $\mm{A}$. Consider each entry in turn and update the largest gap accordingly. See Algorithm \ref{alg:lindisc}.
	
	\begin{proof}[Proof of Theorem \ref{thm:exact-one-row}]
		By Corollary $\ref{cor:onerowtakemagnitudes}$ it is sufficient to consider row matrices with non-negative entries. Suppose that $\mm{A} = [a_1, ..., a_n]$ is such a matrix with entries sorted in decreasing order. Algorithm \ref{alg:lindisc} correctly outputs the linear discrepancy for matrices with a single entry. Let $\mm{A}_i = [a_1, ..., a_i]$. Lemma \ref{lem:onerowsortinghelps} gives us a recursive method for computing the largest gap in $\family{S}(\mm{A}_{i+1})$ from the largest gap in $\family{S}(\mm{A}_i)$. Since $\lindisc(\mm{A})$ is half the size of the largest gap in $\family{S}(\mm{A})$, Algorithm \ref{alg:lindisc} computes $\lindisc(\mm{A})$ as required.
	\end{proof} 
	
	\begin{algorithm}
		\DontPrintSemicolon % Some LaTeX compilers require you to use \dontprintsemicolon instead 
		\KwIn{Matrix $\mm{A} \in \QQ^{1\times n}$.}
		\KwOut{$\lindisc(\mm{A})$.}
		\For{$i$ from $1$ to $n$}{
			$\mm{A}[i] \leftarrow |a_i|$\;
		}
		sort $\mm{A}$ in decreasing order\;
		$\ell \leftarrow a_1$\;
		\For{$i$ from $2$ to $n$}{
			$\ell \leftarrow \max(a_i, \ell - a_i)$\;
		}
		\Return{$\frac{\ell}{2}$}\;
		\caption{Linear discrepancy of row matrix.}
		\label{alg:lindisc}
	\end{algorithm}
	
	Thus, for any row matrix $\mm{A}$ with $n$ elements, we can find $\lindisc(\mm{A})$ in time $O(n \log n)$.  
	
	% \begin{corollary}
	% 	If $\mm{A}$ is an arithmetic progression of the form $[a_0, a_0 + k, \cdots, a_{0} + (n-1)k]$, then 
	% 	\[\lindisc(A) = \frac{\max(a_0, k)}{2}.\]
		
	% 	Similarly, if $\mm{A}$ is a geometric progression of the form $[a_0r^{0}, \cdots, a_{0}r^{n-1}]$, then 
	% 	\[\lindisc(A) = \begin{cases}
	% 	\frac{a_0}{2} &\mbox{if } r \leq 1,\\
	% 	a_0\left(r^{n-1} - \frac{\left(r^{(n-1)}-1\right)}{(r-1)}\right) &\mbox{otherwise}.
	% 	\end{cases}\]
	% \end{corollary}
	
	\subsubsection{One Row Linear Discrepancy Rounding}	
	Let $\lindisc(\mm{A}) = \ell$. By the definition of linear discrepancy, for every $\vv{w} \in [0,1]^{n}$ there exists an $\vv{x} \in \{0,1\}^{n}$ such that $\norm{\mm{A}(\vv{w} - \vv{x})}_{\infty} \leq \ell$. In-fact, if $\vv{w}$ is not a deep-hole, there exists an $\vv{x}$ which satisfies $\norm{\mm{A}(\vv{w} - \vv{x})}_{\infty} < \ell$. However it is not obvious that finding such an $\vv{x}$ can be done efficiently i.e. in polynomial time with respect to the bit complexity of $\mm{A}$ and $n$. By reducing from the subset-sum problem, we observe that it is difficult to compute $\lindisc(\mm{A}, \vv{w})$ let alone find an $\mm{x}$ which minimizes $\norm{\mm{A}(\vv{w} - \vv{x})}_{\infty} \leq \ell$.
	
	\begin{proof}[Proof of Theorem \ref{thm:approx-one-row}]
		To begin, let $\mm{A} = [a_1, ..., a_n]$ for positive $a_i$ in non-increasing order. We will consider $\mm{A}$ with arbitrary entries at the end. Let $w = \mm{A}\vv{w}$. As before, let $\family{S}(\mm{A}) = [s_{0}, ..., s_{2^{n}-1}]$ be the subset-sums of $\mm{A}$ where each $s_i = \mm{A}\vv{x}$ for an $\vv{x} \in \{0,1\}^n$ and $s_i \leq s_{i+1}$ for all $i$. Recall that $2\cdot\lindisc(\mm{A})$ is the largest gap between any two consecutive entries in $\family{S}(\mm{A})$. Our algorithm will find a pair of subset sums containing $w$. If we can show that the size of the interval between these two subset sums is no more than the gap between some two consecutive entries in $\family{S}(\mm{A})$, then the closest subset sum to $w$ among these two will be within $\lindisc(\mm{A})$ of $w$.
		
		Just as in Algorithm \ref{alg:lindisc}, we refine the interval between two subset sums containing $w$ by incrementally adding the entries of $\mm{A}$ in decreasing order. Initially our interval is $g_0 = [0, \sum_{i = 1}^{n} a_i]$. We maintain the invariants: (1) $w \in g_i$ for all $i$, and (2) the end-points of $g_i$ are subset sums.
		
		Suppose $w \in g_i = [u, v]$ and we are considering $a_i$. If $u + a_{i} > w$ then set $v \leftarrow \min(v, u + a_i)$. Otherwise let $u \leftarrow u + a_i$. Algorithm \ref{alg:lindiscvariant} computes this interval and the associated vectors $\vv{u}$ and $\vv{v}$ representing its endpoints.
		
		Consider the values of $u$ and $v$ at the end of the algorithm. We claim that the final interval $[u,v]$ is at most the width of some gap between two consecutive terms in $\family{S}(\mm{A})$, the array of all subset sums of $\mm{A}$. Notice $u = a_1u_1 + \cdots + a_nu_n$ where $\vv{u} = [u_1, ..., u_n]$ is an endpoint of the interval once Algorithm \ref{alg:lindiscvariant} completes.  
		
		We partition $\vv{u}$ into maximal blocks where all entries in the same block have the same value i.e. $[u_1, u_2, ..., u_{\ell_1}], ..., [u_{\ell_r + 1}, u_{\ell_r+2}, ..., u_{n}]$ such that $u_{\ell_i + 1} = u_{\ell_i + 2} = \cdots = u_{\ell_{i+1}}$ for $i = 0, 1, ..., r-1$ where $\ell_0 = 0$.
		
		We claim that Algorithm \ref{alg:lindiscvariant} outputs an interval containing $w$ whose width is at most the distance between some two consecutive entries in $\family{S}(\mm{A})$. The proof is by induction on $r$, the number of blocks. In the base case, $r = 1$ and there is only one block. Thus $u = 0$ or $u = \sum a_i$. In the case where $u = 0$, we must have $a_i > w$ for all $i \in [n]$, and $v = a_n$. Thus $w \in [0, a_n]$ with consecutive elements $0$ and $a_n$ of $\family{S}(\mm{A})$. In the latter case when $u = \sum a_i$, we can output $\vv{w}$ since it is already a subset sum.
		
		Suppose next that the claim holds for all matrices where the algorithm outputs a vector $\vv{u}$ with $k$ blocks, and we will show that it still holds for a matrix $\mm{A}$ whose output $\vv{u}$ has $k+1$ blocks. Let $\vv{u}' = [u_1, ..., u_{\ell_{k+1}}]$ and $\vv{v}' = [v_1, ..., v_{\ell_{k+1}}]$ be the final vectors after running the algorithm on $\mm{A}' = [a_1, ..., a_{\ell_{k+1}}]$. Further let $u' = \sum_{i = 1}^{\ell_{k+1}} a_iu_i$ and $v' = \sum_{i = 1}^{\ell_{k+1}} a_iv_i$. By the induction hypothesis, the width of $[u', v']$ is at most the distance between some two consecutive elements in the list of subset sums of $\mm{A}'$. The last block of $\vv{u}$ is $[u_{\ell_{k+1} + 1}, ..., u_{n}]$. The entries of this block are either all zeros or all ones. Consider each case in-turn. 
		
		First suppose $u_{\ell_{k+1}+1} = \cdots = u_{n} = 0$. Since none of the $a_i$ for $i = \ell_{k+1} + 1, ..., n$ were added to $u$, it must be the case that $u' + a_i > w$ for all such $i$. Thus the interval $[u, v] = [u', \min\left(v', u' + a_n\right)]$ has width at most $a_n$. Since $0$ and $a_n$ are consecutive in $\family{S}(\mm{A})$, as $|a_n|$ is the entry with the smallest magnitude in $\mm{A}$, the output interval satisfies our requirements.  
		
		Next suppose $u_{\ell_{k+1}+1} = \cdots = u_{n} = 1$. It must be the case that $u = u' + a_{\ell_{k+1} + 1} + \cdots + a_n \leq w$. Observe that $a_{\ell_{k+1}}$ is in the $k$\textsuperscript{th} block and so $u_{\ell_{k+1}} = 0$. Let $[u'', v'']$ be our interval after processing the $k-1$\textsuperscript{st} block i.e. $u'' = \sum_{i = 1}^{\ell_k} a_iu_i$ and $v'' = \sum_{i = 1}^{\ell_k} a_iv_i$. Notice that since none of the entries in the $k$\textsuperscript{th} block were added to $u''$, we must have $u'' + a_{i} > w$ for all $i = \ell_{k} + 1, ..., \ell_{k+1}$. In such cases, we always update $v'' \leftarrow \min(v'', u'' + a_{i})$ after each such $i$, thus the interval $[u', v']$ has width at most $a_{\ell_{k+1}}$. Thus it suffices to show that $a_{\ell_{k+1} + 1} + \cdots + a_n$ and $a_{\ell_{k+1}}$ are consecutive in $\family{S}(\mm{A})$. First note that $a_{\ell_{k+1} + 1} + \cdots + a_n \le a_{\ell_{k+1}}$ since $u' + a_{\ell_{k+1} + 1} + \cdots + a_n \le w \le v' \le u' + a_{\ell_{k+1}}$. The two subset sums then are also consecutive, since $a_i > a_{\ell_{k+1}}$ for all $i < \ell_{k+1}$. 
		
		Now consider the case where $\mm{A}$ can have both positive and negative entries. Without loss of generality we can assume that none of the entries are zero. Let ${A}_{-} = \{a_i \in \mm{A}: a_i < 0\}$ and ${A}_{+} = \{a_i \in \mm{A}: a_i > 0\}$. It suffices to set $u_0 = \sum_{a \in {A}_{-}}a$ and $v_0= \sum_{a \in {A}_{+}} a$ and let $\vv{u}$ and $\vv{v}$ be the indicator vectors of $\mm{A}_{-}$ and $\mm{A}_{+}$ respectively. The remainder of the algorithm is identical except that the matrix should be sorted in decreasing order of \emph{magnitude} and every time an element $a_i \in \mm{A}_{-}$ is added to $u$, its entry in $\vv{u}$ should be set to zero. 
	\end{proof} 
	
	\begin{algorithm}
		\DontPrintSemicolon % Some LaTeX compilers require you to use \dontprintsemicolon instead 
		\KwIn{A vector $\vv{w} \in [0,1]^n$ and a row matrix $\mm{A} = [a_1, ..., a_n]$ of positive integers sorted in increasing order.}
		\KwOut{A vector $\vv{x} \in \{0,1\}^{n}$ such that $\norm{\mm{A}(\vv{w} - \vv{x})}_{\infty} \leq \lindisc(\mm{A})$.}
		$\mm{A} \leftarrow \op{sort-decreasing}(\mm{A})$\;
		$\vv{u} \leftarrow \op{zeros}(n)$\;
		$\vv{v} \leftarrow \op{ones}(n)$\;
		$w \leftarrow \mm{A}\vv{w}$, $u \leftarrow \mm{A}\vv{u}$, $v \leftarrow \mm{A}\vv{v}$\;
		\Return $\vv{v}$ if $w == v$\;
		\For{$k = 1..n$}{
			\If{$u + a_k > w$}{
				$v \leftarrow \min\left(v, u + a_k\right)$\;
				\If{$v == u + a_k$}{
					$\vv{v} \leftarrow \op{copy}(\vv{u})$\;
					$\vv{v}[k] \leftarrow 1$\;
				}
			} \Else{
				$u \leftarrow u + a_{k}$\;
				$\vv{u}[k] \leftarrow 1$\;
			}
		}
		\Return{$\vv{u}$ if $u$ is closer to $w$ else $\vv{v}$}\;
		\caption{Finding a close subset sum to $\mm{A}\vv{w}$.}
		\label{alg:lindiscvariant}
	\end{algorithm}

	\subsection{Constant Rows with Bounded Matrix Entries}
	Let $\mm{A} \in \ZZ^{d \times n}$ with $\max_{i,j} |A_{i,j}| \leq \delta$. Let $Z = \mm{A}[0,1]^{d}$ be the zonotope of $\mm{A}$ and let $T = [-n\delta, n\delta]^{d} \cap \ZZ^{d}$ be the set of all integer lattice points of $Z$. The following algorithm computes $\lindisc(\mm{A})$ in polynomial time with respect to $n$ for fixed $d$ and $\delta$. The algorithm makes use of Lemma~\ref{lem:lec-in-higher-dimensions}, which is proved in the Appendix.

	\begin{proof}[Proof of Theorem \ref{thm:exact-const-row}]
		
	For every one of the $(2n\delta + 1)^{d}$ integral points $\vv{b} \in T$, compute whether $\mm{A}\vv{x} = \vv{b}$ for some $\vv{x}\in \{0,1\}^n$ using dynamic programming. This procedure generalizes dynamic programming algorithms for knapsack and subset sum and will be outlined in the following. Let $\vv{a}_1, ..., \vv{a}_n$ be the columns of $\mm{A}$. Construct a matrix $\mm{M}$ with dimensions $[-n\delta, n\delta]^{d} \times n$. Cell $(\vv{v}, i)$ of $\mm{M}$ contains the indicator $[\mm{M}(\vv{v}-\vv{a}_i, i-1) \lor \mm{M}(\vv{v},i-1)]$; this corresponds to a linear combination of the first $i-1$ columns of $\mm{A}$ which adds up to $\vv{v}-\vv{a}_i$ or a linear combination of the first $i-1$ columns which adds up to $\vv{v}$. The first column of $\mm{M}$ is the indicator vector for $\{\vv{a}_1\}$. Computing the entries of $\mm{M}$ takes time $O(2n\delta)^{d+1}$. $\mm{M}(\vv{b},n)$ indicates the feasibility of $\mm{A}\vv{x} = \vv{b}$. Computing this for all $\vv{b}$ takes time $O(2n\delta)^{d+1}$. Let $S \subseteq T$ be the set of points $\vv{b}$ in $Z$ such that $\mm{A}\vv{x}=\vv{b}$ for some $\vv{x}\in \{0,1\}^n$, and set $|S| = N$.
	
	Apply Lemma~\ref{lem:lec-in-higher-dimensions} to the points of $S$ in $\metric{\infty}$-norm. The output is some radius $r$ and point $\vv{x}^*$ such that the $\ell_\infty$-ball centered at $\vv{x}^{*}$ with radius $r$ is the largest such ball with center inside the convex hull of $S$ not containing any points of $S$. Note that $r$ is in-fact the linear discrepancy of $\mm{A}$. Since $r$ and $\vv{x}^*$ can be computed in time $O(N^d)$, $\lindisc(\mm{A})$ can be computed in time $O(2n\delta)^{d^2 + d}$. 
	\end{proof}

	\subsection{Poly-time Approximation Algorithm}
	Next, we prove Theorem~\ref{thm:approx} present a $2^n$-approximation algorithm for linear discrepancy. Recall that $\round(\vv{w})$ is the function which rounds each coordinate of $\vv{w}$ to its nearest integer (with ties broken arbitrarily). Let the operator norms of a matrix $\mm{A}$ be:
    \[\norm{\mm{A}}_{p \rightarrow q} = \max_{\vv{x}\in \RR^n\setminus\{0\}} \frac{\norm{\mm{A}\vv{x}}_{q}}{\norm{\vv{x}}_{p}}.\]
    Note that 
	\[\lindisc(\mm{A}) \leq \max_{\vv{w} \in [0,1]^n} \norm{\mm{A}(\mm{w} - \round(\mm{w}))}_{\infty} \le \frac{1}{2} \max_{\vv{z} \in [-1,1]^n}\norm{\mm{A}\vv{z}}_{\infty} = \frac{1}{2}\norm{\mm{A}}_{\infty \rightarrow \infty}.\]
	
	To bound $\lindisc(\mm{A})$ from below, we show that $\norm{\mm{A}}_{\infty \rightarrow \infty} \leq 2^{n+1} \cdot \lindisc(\mm{A})$. This completes the proof of the theorem, since $\norm{\mm{A}}_{\infty \to \infty}$ equals the largest $\ell_1$ norm of any row of $\mm{A}$, and can be computed in polynomial time.

        Let us try to interpret the statement $\norm{\mm{A}}_{\infty \rightarrow \infty} \leq 2^{n+1} \cdot \lindisc(\mm{A})$. Note that $\norm{\mm{A}\vv{z}}_{\infty}$ is equal to the Minkowski $\polytope{P}$-norm $\norm{\vv{z}}_{\polytope{P}}$ for $\polytope{P} = \{\vv{x}: \norm{\mm{A}\vv{x}}_{\infty} \leq 1\}$ i.e. $\norm{\vv{z}}_{\polytope{P}} = \inf\{t \geq 0: \vv{z} \in t\polytope{P}\}$ so
	\[\norm{\mm{A}}_{\infty \rightarrow \infty} = \max_{\vv{z} \in [-1, 1]^n} \norm{\mm{A}\vv{z}}_{\infty} = \max_{\vv{z} \in [-1, 1]^n} \norm{\vv{z}}_{\polytope{P}}.\]
	By interpreting $\vv{z}$ as the difference of two vectors $\vv{x}, \vv{x}' \in [0,1]^n$ we have that 
	\[\norm{\mm{A}}_{\infty \rightarrow \infty} = \max_{\vv{z} \in [-1,1]^n} \norm{\vv{z}}_{\polytope{P}} = \max_{\vv{x}, \vv{x}' \in [0,1]^n} \norm{\vv{x} - \vv{x}'}_{\polytope{P}}.\]
	It is an easy, and well-known fact that $\lindisc(\mm{A})$ is the smallest $t$ such that $[0,1]^n \subseteq \bigcup_{\vv{x}\in \{0,1\}^n}(\vv{x} + \polytope{P})$; see  \cite{matouvsek1999geometric}. We then just need to show that the diameter of the unit hyper-cube with respect to the Minkowski $\polytope{P}$-norm is no more than this scale-factor $t$ times $O(2^n)$. 
	We prove the following more general statement.
	\begin{lemma}
		\label{lem:approx-algorithm}
		Let $\polytope{K}$ be a convex symmetric polytope and $S \subset \RR^n$ be convex. Suppose there exist $N$ elements $x_1, ..., x_N \in S$ such that 
		\[S \subseteq \bigcup_{x_i} x_i + t\polytope{K}.\]
		Then $ \max_{x, x' \in S} \norm{x - x'}_{\polytope{K}} \leq 2tN$.
	\end{lemma}
	\begin{proof}
		Fix any two points $x$ and $x'$ in $S$. Let $\polytope{P}_i$ be the polytope $x_i + t\polytope{K}$. Since $S$ is convex, the line segment $\lambda x + (1 - \lambda)x'$ for $\lambda \in [0,1]$ is in $S$. Therefore $\lambda x + (1 - \lambda)x'$ intersects a sequence of polytopes $\polytope{P}_{k_1}, ..., \polytope{P}_{k_r}$ with centres $x_{k_1}, ..., x_{k_r}$, such that any two consequtive polytopes in the sequence intersect. Since the polytopes are convex, we can assume that they appear in the sequence at most once, so $r \le N$. By the triangle inequality we have
		\begin{align*}
			\norm{x - x'}_K 
			&= \norm{(x - x_{k_1}) + (x_{k_1} - x_{k_2}) + \cdots + (x_{k_r} - x')}_{K}\\ 
			&\leq \norm{x - x_{k_1}}_{K} + \norm{x_{k_1} - x_{k_2}}_{K} + \cdots + \norm{x_{k_r} - x'}_{K}\\
			&\leq t + 2t(N-1) + t = 2tN
		\end{align*}
		where the last inequality follows as $x \in \polytope{P}_{k_1}$, $x' \in \polytope{P}_{k_r}$, and $\norm{x_{k_i} - x_{k_{i+1}}}_K \leq 2t$.
		\begin{comment}
		Just for completeness, we show that $\norm{x_{k_i} - x_{k_{i+1}}}_K \leq 2t$. Since $P_{k_i}$ and $P_{k_{i+1}}$ are consecutive in the sequence of polytopes that $\lambda x + (1 - \lambda) x'$ intersect, $P_{k_i} \cap P_{k_i} \neq \emptyset$. Let $y$ be a point in their intersection. Then 
		\[\norm{x_{k_i} - x_{k_{i+1}}}_K \leq \norm{x_{k_i} - y}_{K} + \norm{y - x_{k_{i+1}}}_K \leq 2t.\]
		\end{comment}
	\end{proof}
	
	\begin{proof}[Proof of Theorem \ref{thm:approx}]
		In Lemma \ref{lem:approx-algorithm}, set $\polytope{K}$ to be the parallelepiped defined by $\mm{A}$, $S = [0,1]^n$, $t = \lindisc(\mm{A})$, and $\{x_1, ..., x_N\} = \{0,1\}^{n}$. 
	\end{proof} 
