% !TEX root = MAIN-lindisc.tex

\section{Background}
	\subsection{Applications}
	Linear discrepancy, and combinatorial discrepancy more
        generally, has applications in rounding LP relaxations. One
        example is the use of the Lovett-Meka algorithm in improving
        bin-packing results \cite{lovett2015constructive}.
	
	Originally designed for discrepancy minimization in poly-time, the algorithm has since been used by Rothvo{\ss} and Rothvo{\ss}-Hoberg to tighten the additive integrality gap for bin packing in one dimension \cite{hoberg2017logarithmic, rothvoss2013approximating}. Let $OPT$ be the optimum number of bins necessary for a bin-packing instance. Rothvo{\ss}' 2014 work produces a packing which uses $OPT + O(\log OPT \cdot \log\log OPT)$ bins. This is the first result in several decades which improves upon the $OPT + O(\log^2 OPT)$ bin solution of the Karmarkar-Karp algorithm. The crucial idea in the algorithm involves rounding the solution for the Gilmore-Gomory LP using the constructive partial colouring method a la Lovett-Meka.
	
	Hoberg and Rothvo{\ss}' more recent work improved upon this result. By applying a two stage packing technique in conjunction with the discrepancy results of Lovett-Meka, they obtained a poly-time algorithm which achieves an additive gap of $O(\log OPT)$, matching certain combinatorial lower bounds.
	
	Bansal and Khan also used a round-and-approximate approach to solve two dimensional bin packing with axis aligned rectangular objects into unit square bins achieving a 1.405-approximation \cite{bansal2014improved}. This beats the factor of 1.5 lower bound for a wide class of rounding algorithms.

	\subsection{Combinatorial Discrepancy}
		Let $X$ be a finite set of size $n$ and $\family{S}$ be a subset of the power set of $X$ of size $m$. Let $\chi: X \rightarrow \{-1,1\}$ be a colouring of the elements in $X$. The \gls{disc} of a set $S \in \family{S}$ with respect to $\chi$ is defined as 
		\[\disc(S, \chi) = \left|\sum_{s \in S} \chi(s)\right|.\]
		The discrepancy of set system $(X, \family{S})$ is
		\[\disc(\family{S}) = \min_{\mm{\chi}}\max_{S \in \family{S}} \disc(S, \chi).\]
		Let $\mm{B} \in \{0,1\}^{m \times n}$ be the incident matrix of $\family{S}$ where rows correspond to sets in $\family{S}$ and columns correspond to elements in $X$. Then the discrepancy of the set system is equivalent to the discrepancy of $\mm{B}$ given by 
		\begin{equation}
			\label{eq:discrepancy}
			\disc(\mm{B}) = \min_{\vv{x} \in \{-1,1\}^n}\norm{\mm{B}\vv{x}}_{\infty}.
		\end{equation}
		
		Intuitively, the definition of $\disc(\family{S})$ is an attempt to capture the ``complexity" (according to Matou\v{s}ek) inherent in $\family{S}$, but for this standard discrepancy falls short \cite{matouvsek1999geometric}. Consider a matrix $\mm{B} \in \RR^{m \times n}$ and let $\mm{A} \in \RR^{m \times 2n}$ be the concatenation of two copies of $\mm{B}$ side by side. Regardless of the discrepancy of $\mm{B}$, $\disc(\mm{A}) = 0$ since there exists $\vv{x} \in \{-1, 1\}^n$ such that $\norm{\mm{A}\vv{x}}_{\infty} = 0$, namely 
		\[\vv{x}^{\intercal} = [\underbrace{-1, ..., -1}_{n}, \underbrace{1, ..., 1}_{n}].\] 
		
		The \gls{herdisc} of $\mm{A}$, defined as the maximum discrepancy over all sub-matrices, is resilient to such alterations. Formally, if $\mm{B}$ is a sub-matrix of $\mm{A}$, then
		\begin{equation}
			\label{eq:hereditarydiscrepancy}
			\herdisc(\mm{A}) = \max_{\mm{B}}\min_{\vv{x} \in \{-1,1\}^n}\norm{\mm{B}\vv{x}}_{\infty}.
		\end{equation}
		
		Observe that the colouring $\vv{x}$ above is a vertex of the $\{-1,1\}^n$ hyper-cube. In the forgoing, it would be helpful to let these colourings be verticies of $\{0,1\}^n$ instead. Thus we perform a change of variables $x_i \mapsto \frac{1 - x_i}{2}$. Now define discrepancy as
		\begin{equation}
		\label{eq:disc}
			\disc(\mm{A}) = \min_{\vv{x} \in \{0,1\}^{n}} \left\|\mm{A}\left(\frac{1}{2}\cdot\ind{1} - \vv{x}\right)\right\|_{\infty}
		\end{equation} 
		
		There is nothing unique about the vector $\frac{1}{2}\ind{1}$. For any $\vv{w} \in [0,1]^n$ we can consider
		\[\lindisc(\mm{A}, \vv{w}) = \min_{\vv{x} \in \{0,1\}^{n}} \norm{\mm{A}\left(\vv{w} - \vv{x}\right)}_{\infty}.\]
		This is the \gls{lindisc} of $\mm{A}$ with respect to $\vv{w}$. The linear discrepancy of $\mm{A}$ is defined as
		\begin{equation}
			\label{eq:lindisc}
			\lindisc(\mm{A}) = \max_{\vv{w} \in [0,1]^n} \lindisc(\mm{A}, \vv{w}) = \max_{\vv{w} \in [0,1]^n}\min_{\vv{x} \in \{0,1\}^{n}} \norm{\mm{A}\left(\vv{w} - \vv{x}\right)}_{\infty}
		\end{equation}
		It will sometimes be useful to consider a maximizer of Equation (\ref{eq:lindisc}) i.e. $\mm{w}^* \in [0,1]^n$ such that $\lindisc(\mm{A}, \mm{w}^*) = \lindisc(\mm{A})$. Call such a  $\vv{w^*}$ a \textbf{deep hole} of $\mm{A}$. Every $\mm{A}$ has at least one deep hole, since linear discrepancy is a continuous function over the compact set $[0,1]^n$. Further, if $\vv{w}^*$ is a deep hole then so is $\ind{1} - \vv{w}^*$; simply take the colouring to be $\ind{1}-\vv{x}$.
		
		Let us observe some basic properties of linear discrepancy. From the definitions it is easy to see that $\lindisc(\mm{A})$ is invariant under row and column exchanges. Further, scaling the matrix $\mm{A}$ scales $\lindisc(\mm{A})$ proportionally. If row $i$ of $\mm{A}$ is $\mm{r}_i$, then $\lindisc(\mm{A}) \geq \max_{i \in [m]} \lindisc(\vv{r}_i)$. This extends to any subset of the rows of $\mm{A}$. Thus the sub-matrices $\mm{A}'$ of $\mm{A}$ which satisfy $\lindisc(\mm{A}')\geq \lindisc(\mm{A})$ are a subset of the columns of $\mm{A}$.
		
		Each discrepancy variant mentioned above has a corresponding geometric interpretation. Let $\polytope{P}_{A} = \{\vv{x}: \norm{\mm{A}\vv{x}}_{\infty} \leq 1\}$ be the \textbf{fundamental parallelepiped} of $\mm{A}$. Observe that $\polytope{P}_{A}$ is convex and symmetric. Imagine placing a scaled copy $t \cdot \polytope{P}_{A}$ at every corner of the unit hyper-cube, $\{0,1\}^n$. The smallest $t$ for which some polytope contains $\frac{1}{2}\ind{1}$ is exactly $\disc(\mm{A})$. Similarly, the smallest $t$ for which the union of the polytopes covers all of $\{0,1\}^n$ is $\lindisc(\mm{A})$. 
		 
		\fig{geometricview}{1}{The geometric interpretation of discrepancy (left) and linear discrepancy (right). The dark grey polygons on the left represent $\polytope{P}_{A}$ placed at each corner of the unit cube. The scaling required to cover the centre is the discrepancy. The scaling required to cover the entire cube is the linear discrepancy.}
		
		Formally, we can define these as
		\begin{align*}
			\disc(\mm{A}) &= \inf\left\{t \geq 0: \frac{1}{2}\ind{1} \in \bigcup_{\vv{x} \in \{0,1\}^n} \vv{x} + t\polytope{P}_{A}\right\} \mbox{ and }\\
			\lindisc(\mm{A}) &= \inf\left\{t \geq 0: [0,1]^n \subset \bigcup_{\vv{x} \in \{0,1\}^n} \vv{x} + t\polytope{P}_{A}\right\}.
		\end{align*}
		See Figure \ref{fig:geometricview}.
		
		The analogous interpretation of hereditary discrepancy might first appear to be the scaling which covers all points of the form $\{0, 1/2, 1\}^n$ --- these correspond to the centres of all low dimensional faces of the unit hyper-cube --- but it is not so. Consider the row matrix $\mm{A} = [1,2]$. The maximum discrepancy is achieved on the subset $\mm{A}' = [2]$ so $\herdisc(\mm{A}) = 1$. However, for any $\vv{w} \in \{0,1/2,1\}^2$, there exists some $\vv{x} \in \{0,1\}^2$ such that $(w_1 - x_1) + 2(w_2 - x_2) \leq \frac{1}{2}$. To accurately represent $\herdisc(\mm{A})$ we must take care when dealing with the deleted columns. If column $i$ is removed, then $w_i \in \{0,1\}$ and we must ensure that the chosen colouring $\vv{x}$ satisfies $x_i = w_i$. Thus the hereditary discrepancy is the scaling which covers the centre of all low dimensional faces \emph{using polytopes placed at a vertex on the boundary of that face}. 
		
		\ifforyoureyesonly
		Various resources give more thorough treatments of these topics \cite{beck1996discrepancy, chazelle2001discrepancy, matouvsek1999geometric}.
		\fi	
	\subsection{Bounds on Linear Discrepancy}
		\subsubsection{Upper Bounds}
		We can extract the following linear discrepancy result from the geometric interpretations. 
		
		\begin{theorem}
			\label{eq:lindiscubstandardherdisc}
			(Lov\'{a}sz et al. 1986, \cite{lovasz1986discrepancy}) $\lindisc(\mm{A}) \leq 2\herdisc(\mm{A})$.
		\end{theorem}
		\ifforyoureyesonly
			\begin{proof}
				Let $\polytope{P}$ be the fundamental parallelepiped of $\mm{A}$ and let $t$ be a scalar such that \[\polytope{U} = \bigcup_{\vv{x} \in \{0,1\}^n} \vv{x} + t\polytope{P}_{A}\]
				covers the centre of every face in any dimension. This is at most the scaling required to cover the centre of every face with a polytope placed at a vertex on the boundary of that face so $t \leq \herdisc(\mm{A})$. We show that
				\[\polytope{V} = \bigcup_{\vv{x} \in \{0,1\}^n} \vv{x} + 2t\polytope{P}_{A}\]
				covers every point in $[0,1]^n$. Thus $\lindisc(\mm{A}) \leq 2t \leq 2\herdisc(\mm{A})$. 
				
				It suffices to show that $\polytope{V}$ covers all the dyadic rational points in $[0,1]^n$, rationals whose denominators are powers of two, since they are dense in the hyper-cube. Let $\vv{a} = \frac{1}{2^{k}}\vv{r}$ where $0 \leq r_i \leq 2^{k}$. The proof proceeds by induction on the exponent $k$. In the base case $\vv{a} \in \{0,1/2,1\}^n$ are exactly the centre of faces in every dimension. Suppose all $\vv{a} = \frac{1}{2^{k}}\vv{r}$ are covered by $\polytope{V}$, let us show that $\vv{b} = \frac{1}{2^{k+1}}\vv{s}$ for $0 \leq s_i \leq 2^{k+1}$ is covered by $\polytope{V}$ as well. Note that $2\vv{b} \in [0,2]^n$. There exists $\vv{z} \in \{0,1\}^n$ such that $2\vv{b} - \vv{z} \in [0,1]^n$. Since the denominators of $2\vv{b} - \vv{z}$ are powers of two with exponent less than or equal to $k$, the induction hypothesis implies that there exists a $\vv{x} \in \{0,1\}^n$ such that $2\vv{b} - \vv{z} \in \vv{x} + 2t\polytope{P}_{A}$ or 
				\[\vv{b} \in \frac{\vv{z} + \vv{x}}{2} + t\polytope{P}_{A}.\]
				Note that $\frac{\vv{z} + \vv{x}}{2} \in \{0,1/2,1\}^n$. By the definition of $t$ there exists some $\vv{y} \in \{0,1\}^n$ such that $\frac{\vv{z} + \vv{x}}{2} \in \vv{y} + t\polytope{P}_{A}$. It follows from the convexity of $\polytope{P}_{A}$ that $\vv{b} \in \left(\vv{y} + t\polytope{P}_{A}\right) + t\polytope{P}_{A} = \vv{y} + 2t\polytope{P}_{A}$.
			\end{proof}
		\fi
			
		Since $\herdisc(\mm{A})$ can be well approximated by the \gls{gamma2}-norm of a matrix --- refer to the glossary for the definition of the \gls{gamma2}-norm --- and the \gls{gamma2}-norm can be calculated in time polynomial in $m$ and $n$, this provides a constructive upper bound for linear discrepancy \cite{matousek2014factorization}. 
		
		It is natural to ask if there exists a constant $c$ such that $\herdisc(\mm{A}) \leq c \cdot \lindisc(\mm{A})$, but no such constant exists for general matrices. Consider the case where $\mm{A} = [2^0, 2^1, 2^2, ..., 2^{n-1}]$. The sub-matrix $\mm{A}' = [2^{n-1}]$ achieves the largest discrepancy of $2^{n-2}$ so $\herdisc(\mm{A}) = 2^{n-2}$. However, $\lindisc(\mm{A})$ is rather modest. Note that $\mm{A}\vv{x} \in \{0, 1, ..., 2^{n}-1\}$ for $\vv{x} \in \{0,1\}^n$ and $\mm{A}\vv{w} \in [0, 2^n - 1]$ for $\vv{w} \in [0,1]^n$. Since for any $\vv{w}$ we can chose some $\vv{x}$ such that $|\mm{A}\vv{w} - \mm{A}\vv{x}| \leq \frac{1}{2}$, $\lindisc(\mm{A}) \leq \frac{1}{2}$.
		
		\subsubsection{Lower Bound}
		The discrepancy is a trivial lower bound for linear discrepancy so any lower bound on discrepancy is a lower bound for linear discrepancy. Thus the following folk-lore results appearing in Matou\v{s}ek's book are useful. 
		\begin{theorem}
			(Matou\v{s}ek 1999, \cite{matouvsek1999geometric}) Let $\mm{A}$ be the incidence matrix of a set system. Then 
			\[\disc(\mm{A}) \geq \sqrt{\frac{n}{m}\lambda_1}\]
			where $\lambda_1$ is the smallest eigen-value of $\mm{A}^{\intercal}\mm{A}$. 
		\end{theorem}
		\ifforyoureyesonly
		\begin{proof}
			For lower bounds it can be useful to consider the different discrepancies in-terms of different norms. Notably,
			\[\disc(\mm{A}) = \min_{\vv{z} \in \{-1,1\}^n} \norm{\mm{A}\vv{z}}_{\infty} \geq \min_{\vv{z} \in \{-1,1\}^n}\frac{\left\|\mm{A}\vv{z}\right\|_2}{\sqrt{m}} = \min_{\vv{z} \in \{-1,1\}^n}\sqrt{\frac{\vv{z}^{\intercal}\left(\mm{A}^{\intercal}\mm{A}\right)\vv{z}}{m}}.\]
			We can bound this further by extending the domain of $\vv{z}$. In particular
			\[\min_{\vv{z} \in \{-1,1\}^n}\sqrt{\frac{\vv{z}^{\intercal}\left(\mm{A}^{\intercal}\mm{A}\right)\vv{z}}{m}} \geq \min_{\norm{\vv{z}}_2 = \sqrt{n}}\sqrt{\frac{\vv{z}^{\intercal}\left(\mm{A}^{\intercal}\mm{A}\right)\vv{z}}{m}} = \min_{\norm{\vv{u}}_{2} = 1} \sqrt{\frac{n}{m} \cdot \vv{u}^{\intercal}\left(\mm{A}^{\intercal}\mm{A}\right)\vv{u}} = \sqrt{\frac{n}{m}\lambda_1}.\]
		\end{proof}
		\fi
		
		Unfortunately it is hard in general to distinguish between matrices with drastically different discrepancies. In particular, a breakthrough result due to Spencer showed that, for incidence matrices $\mm{A} \in \{0,1\}^{m \times n}$, $\disc(\mm{A}) \leq O(\sqrt{n})$ when $m \in \poly(n)$ \cite{spencer1985six} yet it is hard to distinguish between matrices with discrepancy zero and those with discrepancy $\Omega(\sqrt{n})$ \cite{charikar2011tight}.
		
		Other lower bounds for linear discrepancy depend on linear algebraic properties of the matrix.
		\begin{theorem}
			\label{thm:lindisclbdet}
			(Lov\'{a}sz et al. 1986, \cite{lovasz1986discrepancy}) If $\mm{A} \in \RR^{n \times n}$ then
			\[\lindisc(\mm{A}) \geq \frac{|\det(\mm{A})|^{\frac{1}{n}}}{2}.\]
		\end{theorem}
		\ifforyoureyesonly
		\begin{proof}
			Similar to the hereditary discrepancy upper bound, use the geometric interpretation of linear discrepancy. Let $t = \lindisc(\mm{A})$ and $\polytope{P}_{A}$ be the fundamental parallelepiped of $\mm{A}$. We know that $\cup_{\vv{x} \in \{0,1\}^n} \vv{x} + t\polytope{P}_{A}$ covers the unit hyper-cube so $\cup_{\vv{x} \in \ZZ^n} \vv{x} + t\polytope{P}_{A}$ covers all of $\RR^m$. It follows that $\vol(t\polytope{P}_{A}) = t^n\vol(\polytope{P}) \geq \vol([0,1]^n) = 1$. Let $f$ be the linear map which takes $\vv{y} \mapsto \mm{A}\vv{y}$. Assume this map is invertible; otherwise $\det(\mm{A}) = 0$ and the theorem is trivial. Note that $f$ scales the volume of a parallelepiped by $|\det(\mm{A})|$. Since $\polytope{P}_{A} = \{\vv{y} \in \RR^n: \|\mm{A}\vv{y}\|_\infty \leq 1\}=f^{-1}([-1,1]^n)$, we have $\vol(\polytope{P}_{A}) = 2^{n}|\det(\mm{A})|^{-1}$. By substituting $\vol(\polytope{P}_{A})$ into the above inequality and rearranging, we obtain $t \geq \frac{|\det(\mm{A})|^{1/n}}{2}$ as required.
		\end{proof}
		\fi
	
	\subsection{Totally Unimodular Matrices}
	\begin{comment}
		\subsection{Characterizations} 
		We can decide if $A$ is total unimodular in polynomial time since there are only polynomially many determinants to check. However an equivalent characterization of totally unimodular matrices might be more useful: let $A$ be a $\{-1,0,1\}$-matrix. $A$ is totally unimodular if and only if for every (square) \emph{Eularian} submatrix $A_{i,j}$ of $A$, the sum of all entries in $A_{i,j}$ is congruent to zero modulo four. Note: an \emph{Eularian} matrix is one with row and column sums all even \cite{camion1965characterization}.    
		\begin{theorem}
		Let $A$ be a matrix with entries in $\{-1,0,1\}$. The following are equivalent:
		\begin{enumerate}
		\item $A$ is TUM; every square submatrix --- the matrix obtained from $A$ by removing any collection of rows and columns --- has determinant $\{-1, 0, 1\}$.
		\item For each integral vector $\mathbf{b}$ the polyhedron $\{\mathbf{x}: \mathbf{x} \geq 0, A \mathbf{x} \leq \mathbf{b}\}$ is integral.
		\item For all integral vectors $\mathbf{a}, \mathbf{a}', \mathbf{b}, \mathbf{b}'$ the polyhedron $\{\mathbf{x}: \mathbf{a} \leq \mathbf{x} \leq \mathbf{a}', \mathbf{b} \leq A \mathbf{x} \leq \mathbf{b}'\}$ is integral.
		\item Each collection of columns of $A$ can be split into two parts so that the sum of the columns in one part minus the sum of the columns in the other part is a vector with entries in $\{-1, 0, 1\}$. 
		\item Each nonsingular submatrix of $A$ has a row with an odd number of nonzero components.
		\item The sum of the entries in any square submatrix with even row and column sums (Euclidean sub-matrices) is divisible by four.
		\item No square submatrix of $A$ has determinant $\{-2,2\}$.
		\end{enumerate}
		\end{theorem}
	\end{comment}
	
	A \gls{totallyunimodular} (\acrshort{tum}) matrix is an integer valued matrix where every sub-determinant is $-1$, $0$, or $1$. A closely related type of matrix we will also consider is the \gls{stronglyunimodular} (\acrshort{sum}) matrix. These are \acrshort{tum} matrices which remain \acrshort{tum} if any single entry is set to zero.
	
	There has been extensive work to bound the discrepancies of \acrshort{tum} matrices.
	\begin{comment}
	If you actually take a look at the paper then you might notice that they say the hereditary discrepancy is at most one. This scaling by a half is intentional since it is attached to the definition of hereditary discrepancy. We need to substitute in $2\herdisc(\mm{A}) = \herdisc'(\mm{A})$ where $\herdisc(\mm{A})$ is the hereditary discrepancy defined in-terms of the $\{0,1\}^n$ hyper-cube and $\herdisc'(\mm{A})$ is the hereditary discrepancy defined in-terms of the $\{-1,1\}^n$ hyper-cube. Ghouila-Houri were using the former definition.  
	\end{comment}
	\begin{theorem}
		\label{thm:ghouilahouriherdisctum}
		(Ghouila-Houri 1962, \cite{ghouila1962caracterisation}). A matrix $\mm{A}$ with entries in $\{-1,0,1\}$ is \acrshort{tum} if and only if $\herdisc(\mm{A}) \leq \frac{1}{2}$.
	\end{theorem}
	
	To prove this theorem it suffices to show that the discrepancy of a \acrshort{tum} matrix $\mm{A}$ is at most a half, since all sub-matrices of a \acrshort{tum} matrix are also \acrshort{tum}. By Theorems \ref{eq:lindiscubstandardherdisc} and \ref{thm:ghouilahouriherdisctum}, $\lindisc(\mm{A}) \leq 1$ for \acrshort{tum} matrix $\mm{A}$. Peng and Yan improved the hereditary discrepancy upper bound for \gls{stronglyunimodular} matrices $\mm{B}$, by showing that $\lindisc(\mm{B}) \leq (1 - 3^{-(n+1)/2})\herdisc(\mm{B})$. Independent results quickly followed which improved the upper bound for \acrshort{tum} matrices \cite{doerr2004linear, bohman2004linear}. 
	\begin{theorem}
		\label{thm:lindiscubimprovedherdisc}
		(Doerr 2004, \cite{doerr2004linear}) Let $\mm{A} \in \RR^{m \times n}$ be a totally unimodular matrix. Then
		\[\lindisc(\mm{A}) \leq 1 - \frac{1}{n+1}.\]
		If $m \geq 2$, we also have $\lindisc(\mm{A}) \leq 1 - \frac{1}{m}$. 
	\end{theorem}
	\ifforyoureyesonly
	We will recount Doerr's result, but first we require the following theorem.
	\begin{theorem}
		\label{thm:hoffmankruskaltumintpolyhedron}
		(Hoffman and Kruskal 1956, \cite{hoffman2010integral}). Let $\mm{A} \in \RR^{m \times n}$ be a totally unimodular matrix and let $\vv{b}, \vv{b}' \in \ZZ^m$ and $\mm{c}, \mm{c}' \in \ZZ^n$. Then $\{\vv{x} \in \RR^n: \vv{b} \leq \mm{A}\vv{x} \leq \vv{b}', \vv{c} \leq \vv{x} \leq \vv{c}'\}$ is an integral polyhedron. 
	\end{theorem}
	
	The proof of Doerr's result is as follows.
	\begin{proof}
		Hoffman, Kruskal immediately gives $\lindisc(A) < 1$ since for any $\vv{p} \in [0,1]^n$, 
		\[\polytope{P} = \{\vv{x} \in [0,1]^n: \floor{\mm{A}\vv{p}} \leq \mm{A}\vv{x} \leq \ceil{\mm{A}\vv{p}}\}\]
		is a non-empty integer polyhedron ($\vv{p} \in \polytope{P}$).
		
		Let $\vv{b} = \mm{A}\vv{p}$, the \emph{rows} of $\mm{A}$ be $\vv{r}_1, ..., \vv{r}_m$, and $k = \min(m, n+1)$. We want to find $\vv{z} \in \{0,1\}^n$ such that
		\[|b_i - \vv{a}_i \cdot \vv{z}| \leq 1 - \frac{1}{k}\]
		for every $i \in [m]$. Let $I = \left\{i \in [m]: |b_i - \round(b_i)| \leq \frac{1}{k}\right\}$. These are the \emph{critical} rows since the error can be greater than $1 - \frac{1}{k}$ if we do not round carefully. Let $I_B \subset I$ be a basis for the critical rows. Observe that $|I_B| \leq \min(m,n)$ since the rank of the row space is equal to the rank of $\mm{A}$. Divide the rows of $\mm{A}$ into two groups: the first group consists of indices $i \in ([m]-I) \cup I_B$. We let
		\[\polytope{P} = \{\vv{x} \in [0,1]^n: \floor{\vv{b}} = \floor{\mm{A}\vv{p}} \leq \mm{A}\vv{x} \leq \ceil{\mm{A}\vv{p}} = \ceil{\vv{b}}\}\]
		as before. Let $I^{-} = \{i \in I_B: b_i > \round(b_i)\}$ and $I^{+} = \{i \in I_B: b_i < \round(b_i)\}$. Let $f: \polytope{P} \rightarrow \RR$ where
		\begin{align}
			\label{form:DoerrObjFun}
			f(\vv{x}) = \sum_{i \in I^{-}}(\vv{a}_i \cdot \vv{x} - \floor{b_i}) + \sum_{i \in I^{+}}(\ceil{b_i} - \vv{a}_i \cdot \vv{x}) 
		\end{align}
		be the objective function we wish to minimize. Observe that $f$ is non-negative on $\polytope{P}$ by construction. Further, $\vv{p}$ is a feasible solution and 
		\[f(\vv{p}) < \frac{|I^{-} \cup I^{+}|}{k} \leq 1.\]
		By Hoffman and Kruskal, there exists an integral optimum solution $\vv{z}$ to this LP. Since $f(\vv{z}) \leq f(\vv{p}) < 1$, every term in Equation (\ref{form:DoerrObjFun}) equals zero so $f(\vv{z}) = 0$. Thus for all $i \in I_B$, $|\vv{a}_i \cdot (\vv{z} - \vv{p})| < \frac{1}{k} \leq 1 - \frac{1}{k}$ (as long as $k \geq 2$). For $i \in [m] - I$, such that $b_i \in \left[\frac{1}{k}, 1 - \frac{1}{k}\right]$, we can choose $\vv{a}_i \cdot \vv{z} \in \{\floor{b_i}, \ceil{b_i}\}$ since $\vv{z} \in \polytope{P}$. Thus $|b_i - \vv{a}_i\cdot \vv{z}| \leq \left|1 - \frac{1}{k}\right|$ in this case as well.
		
		In the second cases we need to consider $j \in I - I_B$. These are the rows with index in $I$ which are linear combinations of the rows with index in $I_B$ i.e. $\vv{a}_j = \sum_{i \in I_B} c_i \vv{a}_i$. Let $\mm{A}_B$ be the matrix consisting of rows $i \in I_B$ and $\mm{A}_{B,i}$ be $\mm{A}_B$ with the $i$\textsuperscript{th} row replaced with $\vv{a}_j$. By Cramer's rule, we have that  \[c_i = \frac{\det(\mm{A}_{B,i})}{\det(\mm{A}_B)}\] 
		Since $\mm{A}$ is \acrshort{tum} and $I_B$ are a set of linearly independent rows, $\det(\mm{A}_B) \in \{-1, 1\}$ and $\det(\mm{A}_{B,i}) \in \{-1, 0, 1\}$. If $I - I_B \neq \emptyset$, then the rank of $\mm{A}$ is at most $\min\{m-1,n\}$, which implies that $\rank(\mm{A}) \leq k-1$. Thus, $|I_B| \leq k-1$, and we have
		\[|\vv{a}_j\cdot(\vv{p} - \vv{z})| \leq \sum_{i \in I_B} |c_i \vv{a}_i\cdot(\vv{p} - \vv{z})| < \frac{1}{k} |I_B| \leq 1 - \frac{1}{k}.\]
        %\sn{I added some more explanation above because I was initially a little lost.}
		We see that for this $\vv{z} \in \polytope{P}$, $|b_i - \vv{a}_i\cdot\vv{z}| \leq 1 - \frac{1}{k}$ for every entry of $\norm{\mm{A}(\vv{p}-\vv{z})}$ as required. 
	\end{proof}
	\fi 
	
	Further, Doerr characterizes all \acrshort{tum} with linear discrepancy equal to $1 - \frac{1}{n+1}$ as follows.
	\begin{theorem}
		(Doerr 2004, \cite{doerr2004linear}) Let $\mm{A}$ be as above. $\lindisc(\mm{A}) = 1-\frac{1}{n+1}$ if and only if there is a set of $n + 1$ rows of $\mm{A}$ where each subset of $n$ rows is linearly independent --- in the trivial case $\mm{A}$ contains only one non-zero entry. Further, if $\lindisc(\mm{A}, \vv{p}) = 1 - \frac{1}{n+1}$ for $\vv{p} \in [0,1]^n$, then $p_i \in \left\{\frac{1}{n+1}, ..., \frac{n}{n+1}\right\}$ for all $i \in [n]$.
	\end{theorem}
	
	An example is the following $(n + 1) \times n$ matrix.
	\[\mm{A}_1 = \begin{bmatrix}
	1 & 0 & \cdots & 0 & 0\\
	0 & 1 & \cdots & 0 & 0\\
	\vdots & \vdots & \ddots & \vdots & \vdots\\
	0 & 0 & \cdots & 1 & 0\\
	0 & 0 & \cdots & 0 & 1\\
	1 & 1 & \cdots & 1 & 1
	\end{bmatrix}.\]
	The upper bound follows from Theorem (\ref{thm:lindiscubimprovedherdisc}) as $\mm{A}_1$ is \acrshort{tum}. The lower bound follows by considering
	\[\vv{w}^{\intercal} = \left[\frac{1}{n+1}, ..., \frac{1}{n+1}\right].\] 