% !TEX root = MAIN-lindisc.tex

\appendix
\section{Appendix}
\subsection{Bit Complexity of Linear Discrepancy}
\label{app:lemma-certificateinpi2}
\begin{proof}[Proof of Lemma \ref{lem:certificateinpi2}.]
	Let  $\mm{r}_i$ for $i \in [m]$ be the rows of $\mm{A}$, $\lindisc(\mm{A}) = \lambda_A$, and $\vv{w}^*$ be a deep-hole of $\mm{A}$. For every $\vv{x} \in \{0,1\}^{n}$ there exists an $i \in [m]$ and $\sigma \in \{-1, 1\}$ such that $\sigma\vv{r}_i(\vv{w}^* - \vv{x}) \geq \lambda_A$. Let $\vv{b}_{x} = \sigma\mathbf{r}_i$ and consider the following linear program over the variables $\vv{w} \in \RR^n$ and $\lambda \in \RR$:
	\begin{align*}
	\mbox{Maximize: } &\lambda\\
	\mbox{Subject to: } &\mathbf{b_x}(\mathbf{w}-\mathbf{x}) \geq \lambda &\mbox{for all } \mathbf{x} \in \{0,1\}^n\\
	&\mathbf{0} \leq \mathbf{w} \leq \mathbf{1}
	\end{align*}
	
	Let $\lambda^*$ be the optimum value of this
	linear program. First note that $\lambda_A
	\leq \lambda^*$ since $(\vv{w}^*,\lambda)$
	satisfies the constraints. Next we show that
	$\lambda_A \geq \lambda^*$. Suppose, towards contradiction, that $\lambda_A < \lambda^*$. Then there exists $\vv{w}' \in [0,1]^n$ such that 
	\[\|\mm{A}(\vv{w}' - \vv{x})\|_\infty \ge \vv{b}_x(\vv{w}' - \vv{x}) \ge \lambda^*>\lambda_A\] 
	for every $\vv{x} \in \{0,1\}^{n}$. Since $\lambda_A = \lindisc(\mm{A})$, we cannot have $\lindisc(\mm{A}, \mathbf{w}') > \lambda_A$. Thus $\lambda^* = \lindisc(\mm{A})$. Since this LP has $n$ variables, the number of bits required to express the linear discrepancy and some deep-hole $\vv{w}^*$ of $\mm{A}$ are polynomial in $n$ and the bit complexity of the largest entry of $\mm{A}$ \cite{schrijver1998theory}. 
\end{proof} 
	
\subsection{Largest Empty Ball Problem}
\label{app:largest-empty-ball}
Let $V$ be a set of $n$ points in the plane and let $\ch(V)$ denote the convex hull of $V$. The largest empty circle problem, denoted LEC, takes $V$ and outputs both a radius $r$ and point $\vv{x}^* \in \ch(V)$ such that the circle centered at $\vv{x}^*$ with radius $r$ is the largest empty circle not containing any point of $V$. We generalize this problem to other norms and to higher dimensions as follows: $V$ is a set of $n$ points in $\RR^d$, and the goal is to compute a point $\vv{x}^*$ in $\ch(V)$ such that $\vv{x}^* + rB$ does not contain any point of $V$, where $B$ is the unit ball of either the $\ell_2^d$ or the $\ell_\infty^d$ norm. In the following we present an algorithm which solves this largest empty ball (LEB) problem.

\begin{lemma}{\textup{(LEC in Higher Dimensions.)}}
	\label{lem:lec-in-higher-dimensions}
	Let $V$ be a set of $n$ points in $\RR^{d}$ for some fixed constant $d$. The LEB of $V$, in both $\metric{2}$- and $\metric{\infty}$-norms, can be computed in time $O(n^{d})$.
\end{lemma}
\begin{proof}
	We use the following terminology. Define a face $F$ of the
        Voronoi diagram $\mathrm{vd}(V)$ of $V$ to be a subset of
        $\RR^d$ such that, for some $S\subseteq V$, and every $\vv{x}
        \in F$, $S$ are the points in $V$ closest to $\vv{x}$. In
        particular, this means that any $\vv{x}\in F$ is equidistant
        from all points in $S$. 
	
	The algorithm of Toussaint \cite{toussaint1983computing}
        computes the LEB of $n$ points $V$ in the plane with respect to the $\metric{2}$-norm as follows, 
	\begin{enumerate}
		\item Compute $\mathrm{vd}(V)$. Note that
                  $\mathrm{vd}(V)$ is the union of Voronoi faces of
                  dimension $k$, the set of which we denote $\mathrm{vd}_{k}(V)$, over all $k = 0, ..., d-1$. 
		\item Compute the convex hull of $V$, denoted $\ch(V)$. Let $h$ be the number of facets of $\ch(V)$. 
		\item Preprocess the points of $\ch(V)$ so that queries of the form ``Is a point $x$ in $\ch(V)$?'' can be answered in time $O(\log h)$. For every $v \in \mathrm{vd}_{0}(V)$, determine if $v \in \ch(V)$. Let $C_1 = \{v \in \mathrm{vd}_{0}(V): v \in \ch(V)\}$.
		\item Determine the intersection points of faces in $\mathrm{vd}_{k}(V)$ with faces of $\ch(V)$ of co-dimension $k$, for pairs of such faces that intersect at a unique point. Let $C_2$ be the set of all such intersection points.
		\item For all points $v \in C_1 \cup C_2$, find the largest empty circle centered at $v$. Output a $v$ which maximizes this radius.
	\end{enumerate}
	We find the analogue of each step for points in $\RR^{d}$ with
        respect to the $\metric{2}$-norm, and then adapt the algorithm
        to the $\metric{\infty}$-norm.
	
	In the following let $N = n^{\ceil{d/2}}$. The complexity, i.e. total number of faces of every dimension, of the $\metric{2}$-Voronoi diagram in $\RR^{d}$ for fixed $d$ is $O(N)$ and can be computed in time $O(N + n \log n)$ by a classic result of Chazelle \cite{chazelle1993optimal}. The complexity of $\ch(V)$ is $O(N)$ and can also be computed in time $O(N + n\log n)$.
	
	To determine the set $C_1$ of Voronoi intersection points inside the convex hull, we let $\mathcal{H}$ be the set of bounding hyperplanes of $\ch(V)$. Assume, without loss of generality, that $\ch(V)$ contains the origin, and, for each $H \in \mathcal{H}$, let $H^-$ be the half-space with $H$ as its boundary containing the origin. Then $\ch(V) = \bigcap_{H \in \mathcal{H}} H^-$. We simply test, for each Voronoi intersection point $\vv{v}$, whether $\vv{v} \in H^-$ for each $H\in \mathcal{H}$, in total time $O(N)$. Since there are at most $O(N)$ Voronoi intersection points, we can find $C_1$ in time $O(N^2)$. 
	
	To determine the set $C_2$ of all unique intersection points
        of $k$-faces of $\mathrm{vd}_k(V)$ and faces of $\ch(V)$ of
        co-dimension $k$ will require solving several linear
        systems. Note that the points in each face $F$ in
        $\mathrm{vd}_{k}(V)$ satisfy $d-k$ equality constraints
        $\ip{\vv{a}_1}{\vv{x}} = b_1, \ip{\vv{a}_k}{\vv{x}} = b_k$ for
        linearly independent vectors
        $\vv{a}_1, ... \vv{a}_k \in \RR^{d}$. Similarly, the points in
        each face of co-dimension $k$ of $\ch(V)$ satisfy $k$ linearly
        independent equality constraints. Since there are at most
        $O(2^dN) = O(N)$ faces of $\ch(V)$, there are at most that
        many faces of $\ch(V)$ of co-dimension $k$. We can then go
        over all Voronoi faces $F$ of dimension $k$, and all faces $G$
        of $\ch(V)$ of co-dimension $k$, and solve the corresponding
        system of $(d-k) + k = d$ linear equations. If the system has
        a unique solution, we check if that solution is in $F\cap G$,
        and, if so, we add it to $C_2$. Thus, for constant $d$, the
        size of $C_2$ and the time to compute it are bounded bounded
        above by $O(N \cdot 2^d N) = O(N^2)$.
	
	In total there are at most $O(N + N^2)$ points in $C_1 \cup C_2$ which can be computed in time $O(N^2)$. Thus solving the largest empty ball problem in dimension $d$ for constant $d$ takes time $O(n^{d})$. 
	
	Next we consider the largest empty ball problem in
        $\metric{\infty}$-norm. The convex hull remains the same, so
        we just have to consider the Voronoi diagram with respect to
        the $\metric{\infty}$-norm. Again, constructing the Voronoi
        diagram can be done in expected time
        $O(n^{\ceil{d/2}}\log^{d-1}n)$ using the randomized algorithm
        of Boissonnat et al.
        \cite{DBLP:journals/dcg/BoissonnatSTY98}. Next we consider the
        number of intersections between the Voronoi diagram and the
        convex hull. First note that Voronoi diagrams with respect to
        the $\metric{\infty}$-norm need not consist of only
        hyperplanes and their intersections. Indeed, in $\RR^{d}$, for
        two points with the same $y$-coordinate, there exists regions
        with affine dimension two which are equidistant to both
        points. To remedy this, we assume that no two points in $V$
        have the same $i$-th coordinate, for any $i \in [d]$. This is
        without loss of generality, by perturbing the points in $V$
        slightly. It remains to consider the complexity of each
        bisector in $\metric{\infty}$-norm. By Claim
        \ref{claim:bound-facet-linf-bisector}, in constant dimension
        $d$, each such bisector can have at most $O(d^2)$
        facets. Therefore, the complexity of any face of the Voronoi
        diagram, being the intersection of at most $d$ bisectors, is
        bounded by a function of $d$. Thus the bounds of the
        $\metric{2}$-norm algorithm still hold, up to constant factors
        that depend on $d$. 
\end{proof}

\begin{claim}{\textup{(Bound on Number of Facets of $\metric{\infty}$ Bisectors.)}}
	\label{claim:bound-facet-linf-bisector}
	Let $\vv{u}, \vv{v} \in \RR^{d}$ be such that assume that $u_i \neq v_i$ for all $i \in [d]$. Then the bisector $\{\vv{x}: \|\vv{x} - \vv{u}\|_\infty = \|\vv{x} - \vv{v}\|_\infty\}$ has at most $O(d^{2})$ facets.
\end{claim}
\begin{proof}
  Let $\vv{x}$ be a point in the bisector at $\metric{\infty}$ distance $r$ from $\vv{u}$ and $\vv{v}$. Pick coordinates $i$ and $j$ and signs $\sigma$ and $\tau$ in $\{-1, +1\}$ such that
  \begin{equation}
    \label{eq:bisector-facet}
    \sigma_i (x_i - u_i) = \tau_j ({x}_j - v_j) = r.
  \end{equation}
  Moreover, let us make this choice so that either $i \neq j$ or
  $\sigma_i \neq \tau_i$. This is always possible, since, otherwise,
  the assumption on $\vv{u}$ and $\vv{v}$ is violated. Then,
  \eqref{eq:bisector-facet} defines a hyperplane in $\RR^d$, namely
  $H_{i, j, \sigma, \tau} =\{\vv{x}: \sigma_i x_i - \tau_j x_j =
  \sigma_i u_i - \tau_j v_j\}$. Note that there are at most
  ${2d\choose 2} \in O(d^2)$ such hyperplanes, and each $\vv{x}$ in
  the bisector lies in at at least one of them. Moreover, a point $\vv{x}$
  in $H_{i,j,\sigma, \tau}$ lies in the bisector if and only if it
  satisfies the inequalities 
  \begin{align*}
    |x_k - u_k| &\le \sigma_i (x_i - u_i) \ \ \forall k \in [d],\\
    |x_k - v_k| &\le \tau_j ({x}_j - v_j) \ \ \forall k \in [d].\\
  \end{align*}
  Thus, the bisector is the union of $(d-1)$-dimensional convex
  polyhedra, one per each of the $O(d^2)$ hyperplanes $H_{i,j,\sigma,
    \tau}$. 
\end{proof}