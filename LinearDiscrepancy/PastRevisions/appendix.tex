% !TEX root = MAIN-lindisc.tex

\appendix
	\section{Interesting Cases}
		\subsection{Disjoint Columns} 
		Consider matrix $\mm{A} \in \RR^{m \times n}$ with at most one-nonzero entry per column. Let $\mathbf{r}_i$ be the rows of $\mm{A}$. Then $\lindisc(\mm{A}) = \max_{i} \lindisc(\vv{r}_i)$ as the subset of non-zero entries can be calculated separately. 
	
		\subsection{Linear Discrepancy of Sub-Matrix} 
		Let $\vv{r}_i$ be row $i$ of $\mm{A}$ and let $\mm{A}_{i,j}$ be the sub-matrix of $\mm{A}$ consisting of rows $i$ and $j$. We show an example where, for a fixed $\vv{w}$, $\lindisc(\mm{A}_{i,j}, \vv{w})$ is small for all pairs of rows, but $\lindisc(\mm{A}, \mm{w})$ is large. Let
		\[\mm{A} = 
		\begin{bmatrix}
		ka & a & 3a & a & 2a \\
		3ka & 3a &3a & a & 2a \\
		ka & 3a & a & a & 2a
		\end{bmatrix}
		\]
		for any positive $a$ and $k \gg a$. Choose $\vv{w}^{\intercal} = [1/k, 0, 0, 0, 0]$. Observe that $\mm{A}\vv{w} = [a, 3a, a]$. Then 
		\begin{align*}
			\lindisc(\mm{A}_{1,2}, \vv{w}) = 0 &\mbox{ by taking } \vv{x}_{1,2} = [0,1,0,0,0]\\
			\lindisc(\mm{A}_{1,3}, \vv{w}) = 0 &\mbox{ by taking } \vv{x}_{1,3} = [0,0,0,1,0]\\
			\lindisc(\mm{A}_{2,3}, \vv{w}) = 0 &\mbox{ by taking } \vv{x}_{2,3} = [0,0,1,0,0].
		\end{align*}		
		However $\lindisc(\mm{A},\vv{w}) = a$ since the best colouring is $\vv{x} = [0,0,0,0,1]$. 
	
		\subsection{Two Power of Two Rows} \label{ssec:twopower}
		Let $a_i = 2^i$ and $\vv{a}_n$ be the row vector $[a_0, ..., a_{n-1}]$. Given a permutation $\pi$ of $\{0,1,...,n-1\}$, let $\pi(\vv{a}_n)$ be the row vector $[a_{\pi(0)},...,a_{\pi(n-1)}]$. Consider matrix $\mm{A}_{\pi}$ which consists of rows $\vv{a}_n$ and $\pi(\vv{a}_n)$ for some permutation $\pi$. We will calculate $\lindisc(\mm{A}_{\pi})$ for various $\pi$. 
		
		As a simple example, if $e$ is the identity permutation then
		\[\mm{A}_e = \begin{bmatrix}
		2^{0} & 2^{1} & \cdots & 2^{n-2} & 2^{n-1} \\
		2^{0} & 2^{1} & \cdots & 2^{n-2} & 2^{n-1}
		\end{bmatrix}.\]
		Then $\lindisc(\mm{A}_e) = \lindisc(\vv{a}_n) = 1/2$. 
		
		Assume that $n$ is even and let $c$ be a cyclic shift of $\{0, ..., n-1\}$. Then $\mm{A}_c$ is\footnote{The meticulous reader might notice that this matrix does not exactly correspond with our definitions above; the columns are in a different order. However this should not be cause for concern as linear discrepancy is invariant under permutation of columns. Instead, this arrangement will expedite our explanation later on.}
		\[\mm{A}_c = \begin{bmatrix}
		\horzbar & \vv{a}_n & \horzbar\\
		\horzbar & c(\vv{a}_n) & \horzbar
		\end{bmatrix} =  
		\begin{bmatrix}
		2^{n-1} & 2^{n-2} & \cdots & 2^1 & 2^0 \\
		2^{n-2} & 2^{n-3} & \cdots & 2^0 & 2^{n-1}
		\end{bmatrix}
		\]
		
		We will show that $\lindisc(\mm{A}_c) \geq \frac{2^{n} - 1}{6}$ by finding a suitable $\vv{w} \in [0,1]^n$. Let 
		\[\vv{w}^{\intercal} = \left[1 - \frac{1}{2^{n-1}}, 0, ..., 0, \frac{1}{2}\right] \mbox{ and } \vv{x}^{\intercal} = [0,1,...,0,1].\]
		We claim that $\vv{x}$ is the best colouring for $\vv{w}$. Certainly,
		\[\norm{\mm{A}_c(\vv{w} - \vv{x})}_{\infty} = \left\|\frac{1}{2}
		\begin{bmatrix}
		2^{n}-1\\
		2^{n}-1
		\end{bmatrix} - \frac{1}{3}
		\begin{bmatrix}
		2^{n} - 1\\
		2 \cdot (2^{n} - 1)
		\end{bmatrix}\right\|_{\infty} = \frac{2^{n} - 1}{6}.\]
	
		For $\vv{y} \in \{0,1\}^{n}$, let $(\vv{y})_2$ be the value of $\vv{y}$ interpreted as a binary string, so $(1001)_2 = 9$. Since the colourings are symmetric about the centre \footnote{That is to say $\norm{\mm{A}_c(\vv{w} - \vv{y})}_{\infty} = \norm{\mm{A}_c(\vv{w} - \bar{\vv{y}})}_{\infty}$ where $\bar{y}_i = 1 - y_i$.}, we can assume that $y_1 = 0$. Consider any $\vv{y}$ such that $(\vv{y})_2 < (\vv{x})_2$. Observe that 
		\[\norm{\mm{A}_c(\vv{w} - \vv{y})}_{\infty} \geq |\vv{a}_n \cdot (\vv{w} - \vv{y})| = \left|\frac{2^{n} - 1}{2} - (\vv{y})_2\right| > \left|\frac{2^{n} - 1}{2} - (\vv{x})_2\right| = \norm{\mm{A}_c (\vv{w} - \vv{x})}_{\infty}.\]
		Similarly, for any $\vv{y}$ such that $(\vv{y})_2 > (\vv{x})_2$ either the last bit of $\vv{y}$ is $1$ or $0$. If the last bit is $1$ then $c(\vv{a}_n) \cdot \vv{y} \geq c(\vv{a}_n) \cdot \vv{x} \geq (2^n-1)/2$. Otherwise $c(\vv{a}_n) \cdot \vv{y} \leq 2^{n-2} - 1$. In the former case,
		\[\norm{\mm{A}_c(\vv{w} - \vv{y})}_{\infty} \geq |c(\vv{a}_n) \cdot (\vv{w} - \vv{y})| \geq \left|\frac{2^{n} - 1}{2} - c(\vv{a}_n) \cdot \vv{x}\right| = \norm{\mm{A}_c(\vv{w} - \vv{x})}_{\infty}.\]
		In the latter case
		\[|c(\vv{a}_n) \cdot (\vv{w} - \vv{y})| \geq \left|\frac{2^{n} - 1}{2} - \frac{(2^{n-1} - 2)}{2}\right| \geq \frac{2^{n-1}}{2} \geq \norm{\mm{A}_c(\vv{w} - \vv{x})}_{\infty}.\]		
		
		Let $r$ be the permutation which reverses the sequence $\{0, ..., n-1\}$. Then 
		\[\mm{A}_r  = 
			\begin{bmatrix}
				2^0 & 2^{1} & \cdots & 2^{n-2} & 2^{n-1} \\
				2^{n-1} & 2^{n-2} & \cdots & 2^1 & 2^{0}
			\end{bmatrix}.\]
		Lov\'{a}sz et al. showed that $\lindisc(\mm{A}_r) \geq 2^{n/3 - 1}$ \cite{lovasz1986discrepancy}. 
		
		This lead us to wonder: which permutation $\pi$ produces the largest $\lindisc(\mm{A}_{\pi})$? %\xy{We still don't know yet.}
\ifforyoureyesonly
	\section{Group Colouring}
		The following is a cursory studies of group colouring motivated by Haviv and Regev's $\Pi_2$-hardness reduction for $\class{CRP}$ \cite{haviv2006hardness}. Perhaps we can use this problem directly to show the hardness of linear discrepancy.
	
		Let $G = (V,E)$ and $A$ be an Abelian group. $G$ is \emph{$A$-colourable} if for every edge-colouring $\phi: E \rightarrow A$ and orientation of $G$, there exists a vertex-colouring $\chi: V \rightarrow A$ such that for every directed edge $uv$, $\phi(uv) \neq \chi(u) - \chi(v)$. The group colouring problem asks if $G$ is $A$-colourable.  Let the minimum $|A|$ for which $G$ is $A$-colourable be the group colouring number of $G$, denoted $\chi_g(G)$. Here we consider $\chi_g$ for some common graphs. 
		 
		Remark that the colourability of $A$ is independent of the orientation choice. Suppose for some fixed orientation $\theta$, $G$ is $A$-colourable. We will show that $G$ is $A$-colourable for any orientation $\theta'$ which differs from $\theta$ on a single edge $uv$. W.l.o.g $uv$ is orientated $u \rightarrow v$ in $\theta$ and $v \rightarrow u$ in $\theta'$. For any colouring $\phi'$ of $E$ with orientation $\theta'$ we will find an appropriate colouring $\chi'$ of $V$. Consider the colouring $\phi$ with orientation $\theta$ which is identical to $\phi'$ except $\phi(uv) = -\phi'(vu)$. Since the claim holds for $\theta$ there exists a colouring $\chi$ of $V$ such that $\chi(u) - \chi(v) \neq \phi(uv)$. Then $\chi(v) - \chi(u) \neq \phi'(vu)$ so $\chi$ is also a proper colouring for $\phi'$. By successively flipping edges it is possible to obtain any orientation from $\theta$. Thus we can always fix an arbitrary orientation at the outset. 
			
		\subsection{Trees}
		We claim that $\chi_g(T) = 2$ for a tree $T$. This is quite easy, but useful, to show as follows. Let $A = \ZZ_2$. Choose a root node $r$. Observe that there are currently two possible colors for every node. W.l.o.g let $\chi(r) = 0$. For every child $c$ of $r$ the choice of possible colors decreases by at most one due to the constraint imposed by the edge $cr$. Now $\chi(c)$ is fixed. Repeat the argument down the tree until we reach a leaf $l$. The choice of $\chi(l)$ is fixed so colour $l$ according. 
		
		Observe an important property of the above argument. As long as the number of possible colour choices for every vertex is at least its degree plus one then the graph is $A$-colourable.
		
		\begin{corollary}
			$\chi_g(K_n) = n$.
		\end{corollary} 
		
		\subsection{Cycles}
		$|A| = 3$ is a tight constraint for cycles regardless of the parity of $|V|$. This is the first somewhat surprising property of group colouring compared to standard graph colouring.
		
		As with the argument above for trees, we know that $|A| \leq 3$. To show that $|A| \geq 3$, we must show that even cycles require at least 3 colors. Suppose that $A = \ZZ_2$ was sufficient and consider the edge colouring of $C_4$ as shown in Figure \ref{fig:groupcolorcycles}. W.l.o.g suppose $\chi(v_0) = 0$. Then $\chi(v_1) = \chi(v_2) = \chi(v_3) = 0$. However, this violate the edge coloured $0$. 
		
		\begin{figure}
			\centering
			\begin{minipage}{0.45\textwidth}
				\centering
				\includegraphics[width=0.7\textwidth]{figures/groupcolorcycles}
				\caption{$C_4$ cannot be colored with $\ZZ_2$.}
				\label{fig:groupcolorcycles}
			\end{minipage}\hfill
			\begin{minipage}{0.45\textwidth}
				\centering
				\includegraphics[width=0.7\textwidth]{figures/friendship4}
				\caption{The graph $F_4$.}
				\label{fig:friendship4}
			\end{minipage}
		\end{figure}		
			
		A friendship graph $F_n$ is a graph consisting of $n$ triangles sharing a common point. See Figure \ref{fig:friendship4}. We know that $C_3$ is $\ZZ_3$ colourable so by $\ZZ_3$ colouring every triangle separately then permuting the colors so that all triangles have a colour in common, we see that $\chi_g(F_n) = 3$. 

		A wheel $W_n$ is the cycle $C_n$ with verticies adjacent to a new vertex $v$. $W_n$ is $3$-colourable when $n$ is even and $4$-colourable when $n$ is odd. From the argument above for cycles, $\chi_g(W_n) = 4$.
		
		\subsection{Peterson Graph}
		As an illustrative example we will show that the Peterson graph is $\ZZ_{3}$-colourable. The techniques used here will be applicable elsewhere. 
		
		\begin{claim}
			For the Peterson graph $P$, $\chi_g(P) = 3$. 
		\end{claim}
		\begin{proof}	
			\fig{labelledpeterson}{0.75}{Labeling of the Peterson graph that we will use throughout the proof.}		
			
			Fix an arbitrary orientation. Label the Peterson graph as shown in Figure \ref{fig:labelledpeterson}. Initially, when the graph is uncolored, none of the edge constraints are violated and every vertex has color choices $\{0,1,2\}$. W.l.o.g. suppose $v_0$ is colored $0$. Since $v_4$, $v_5$, and $v_1$ are adjacent to $v_0$, fixing the color of $v_0$ restricts each of these vertices to two color choices. Fix the color of $v_7$ so that both the color choices of $v_5$ are still valid. Do the same for $v_6$ with respect to $v_1$. Since $v_9$ is adjacent to two fixed vertices, $v_6$ and $v_7$, it has at least one color choice available. Fix $v_9$. Now $v_4$ is adjacent to two fixed vertices, $v_0$ and $v_9$, so it has at least one color choice available. Fix $v_4$. All uncolored vertices currently have two choices remaining. Observe that $v_5 \rightarrow v_8 \rightarrow v_3 \rightarrow v_2 \rightarrow v_1$ is a Hamilton path on the uncolored vertices. Starting from $v_5$ color all the vertices on the path. Since there are two choices at every node, at least one choice will be viable. See Figure \ref{fig:groupcolorpeterson} for a visual of the coloring process.
			
			\fig{groupcolorpeterson}{0.75}{Vertex colouring progress. Read the diagram left to right and top to bottom. The number in the parenthesis indicates the number of valid colours remaining for the vertex. $F$ indicates a fixed vertex. $f$ indicates a vertex whose choice of colour is fixed. Labels coloured red indicates change.}
		\end{proof}
		
		\subsection{Hyper-Cubes}
		Let $Q_n$ denoted the hyper-cube in dimension $n$. Again, using the same argument as we had for trees, we know that for $Q_n$ to be $A$-colorable it suffices for $|A| \geq n+1$. Further, since $Q_n$ contain cycles, for $Q_n$ to be $A$-colourable it is necessary for $|A| \geq 3$. \xy{A tighter bound will be added once I figure out what it is suppose to be.}
		
		\subsection{Summary of Results}
		Table \ref{tab:groupcolor} summarizes our results. Incidentally, \cite{lai2002group} already showed that an analogue of Brooks' theorem holds for group colouring. That is, $\chi_g(G) \leq \Delta(G) + 1$. 
		\begin{table}[ht]
			\centering
			\begin{tabular}{|c|c|c|c|c|c|c|c|}
				\hline
				\textbf{Graph} $G$ & Trees & $C_n$ & $F_n$ & $W_n$ & $P$ & $Q_{n}$ & $K_n$ \\ \hline
				$\chi_g(G)$ & 2 & 3 & 3 & 4 & 3 & $\Omega(\log n)$ & $n$ \\ \hline
			\end{tabular}
			\caption{A summary of group colouring results.}
			\label{tab:groupcolor}
		\end{table}
		
		These are only some trivial results that I consider; extensive surveys can be found elsewhere \cite{lai2011group}.
\fi