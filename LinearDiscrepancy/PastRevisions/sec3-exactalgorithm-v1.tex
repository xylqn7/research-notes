% !TEX root = MAIN-lindisc.tex

\section{Computing Linear Discrepancy}

	\subsection{Geometric Interpretation}
	Each discrepancy variant mentioned above has a corresponding geometric interpretation. Let $\polytope{P}_{A} = \{\vv{x}: \norm{\mm{A}\vv{x}}_{\infty} \leq 1\}$ be the \emph{fundamental parallelepiped} of $\mm{A}$. Observe that $\polytope{P}_{A}$ is convex and symmetric. Imagine placing a scaled copy $t \cdot \polytope{P}_{A}$ at every corner of the unit hyper-cube $\{0,1\}^n$. The smallest $t$ for which some polytope contains $\frac{1}{2}\ind{1}$ is exactly $\disc(\mm{A})$. Similarly, the smallest $t$ for which the union of all polytopes covers the unit-hypercube $\{0,1\}^n$ is $\lindisc(\mm{A})$. 
 
	\fig{geometricview}{1}{The geometric interpretation of discrepancy (left) and linear discrepancy (right). The dark gray polygons on the left represent $\polytope{P}_{A}$ placed at each corner of the unit cube. The scaling required to cover the center is the discrepancy. The scaling required to cover the entire cube is the linear discrepancy.}
	
	Formally, we can define these as
	\begin{align*}
		\disc(\mm{A}) &= \inf\left\{t \geq 0: \frac{1}{2}\ind{1} \in \bigcup_{\vv{x} \in \{0,1\}^n} \vv{x} + t\polytope{P}_{A}\right\} \mbox{ and }\\
		\lindisc(\mm{A}) &= \inf\left\{t \geq 0: [0,1]^n \subset \bigcup_{\vv{x} \in \{0,1\}^n} \vv{x} + t\polytope{P}_{A}\right\}.
	\end{align*}
	See Figure \ref{fig:geometricview}. Hereditary discrepancy can be defined analogously\footnote{It might first appear as though the hereditary discrepancy is the scaling which covers all points of the form $\{0, 1/2, 1\}^n$ --- these correspond to the centers of all low dimensional faces of the unit hyper-cube --- but it is not so. Consider the row matrix $\mm{A} = [1,2]$. The maximum discrepancy is achieved on the subset $\mm{A}' = [2]$ so $\herdisc(\mm{A}) = 1$. However, for any $\vv{w} \in \{0,1/2,1\}^2$, there exists some $\vv{x} \in \{0,1\}^2$ such that $(w_1 - x_1) + 2(w_2 - x_2) \leq \frac{1}{2}$. To accurately represent $\herdisc(\mm{A})$ we must take care when dealing with the deleted columns. If column $i$ is removed, then $w_i \in \{0,1\}$ and the chosen colouring $\vv{x}$ must satisfy $x_i = w_i$. Thus the hereditary discrepancy is the scaling which covers the center of all low dimensional faces \emph{using polytopes placed at a vertex on the boundary of that face}.} 
	
	Various resources give more thorough treatments of these topics \cite{beck1996discrepancy, chazelle2001discrepancy, matouvsek1999geometric}.


	\subsection{Linear Discrepancy of a Row Matrix} 	
	\label{sssection:onerow}
	We begin by developing some intuition for the linear discrepancy of a one-row matrix, $\mm{A} = [a_1, ..., a_n]$. Make the simplifying assumption that the entries of $\mm{A}$ are non-negative and sorted in decreasing order. Define the \emph{subset sums} of $\mm{A}$ to be the set $\family{S}(\mm{A}) = \{s_1, ..., s_{2^n}\}$ where each $s_i = \mm{A}\vv{x}$ for exactly one $\vv{x} \in \{0,1\}^n$. Enumerate the element of $\family{S}(\mm{A})$ in non-decreasing order, i.e. $s_i \leq s_{i+1}$. If $\ell_{A} = 2\cdot\lindisc(\mm{A})$, then $\ell_A$ is the width of the largest gap between consecutive entries in $\family{S}(\mm{A})$.
	
	Suppose $\mm{A}_i = [a_1, ..., a_i]$. Consider what happens for the first couple of values of $i$. $\family{S}(\mm{A}_1) = [0, a_1]$ and $\lindisc(\mm{A}_1) = \frac{a_1}{2}$. $\family{S}(\mm{A}_2)$ contains the contents of $\family{S}(\mm{A}_1)$ and the contents of $\family{S}(\mm{A}_1)$ shifted to the right by $a_2$. Since $a_1 \geq a_2$, $\family{S}(\mm{A}_2) = [0, a_2, a_1, a_1 + a_2]$ where the largest gap is of size $\max(a_2, a_1 - a_2)$. See Figure \ref{fig:onerowalgexp}. In general, the entries of $\family{S}(\mm{A}_i)$ consists of two copies of $\family{S}(\mm{A}_{i-1})$ with one shifted to the right by $a_i$. The gaps in $\family{S}(\mm{A}_i)$ are gaps previously in $\family{S}(\mm{A}_{i-1})$ or between an element of $\family{S}(\mm{A}_{i-1})$ and one in $\{a_i + s: s \in \family{S}(\mm{A}_{i-1})\}$. 
	
	\fig{onerowalgexp}{1}{Obtaining $\family{S}(\mm{A}_2)$ from $\family{S}(\mm{A}_1)$ when $a_1 \geq a_2$.}
	
	A similar structure occurs for general matrices with real valued entries with two special caveats: (1) the previous interval is shifted left or right depending on the sign of the current entry (negative and positive respectively) and (2) the smallest entry of $\family{S}(\mm{A})$ is not zero but the sum of the negative entries in $\mm{A}$.
	
	\begin{comment}
		\begin{lemma}
		\label{lem:onerowsortinghelps}
		Suppose $\mm{A}_{k-1} = [a_1, ..., a_{k-1}]$ with entries in $\RR$ and $|a_i| \geq |a_{i+1}|$. Let the largest gap in $\family{S}(\mm{A}_{k-1})$ be of size $\ell_{k-1}$. Then, for $\mm{A}_{k} = [a_1, ..., a_{k-1}, a_{k}]$ where $a_k$ is non-negative and $a_{k} \leq |a_{i}|$ for all $i \in [k-1]$, the largest gap in $\family{S}(\mm{A}_k)$ is of size $\max(a_k, \ell_{k-1} - a_k)$.
		\end{lemma}
		\begin{proof}
		The entries of $\family{S}(\mm{A}_{k})$ are exactly those in $\family{S}(\mm{A}_{k-1})$ along with those in $\{a_k + s: s \in \family{S}(\mm{A}_{k-1})\}$. Let $\ell = \max(a_k, \ell_{k-1} - a_k)$.
		
		We first show that $2\cdot\lindisc(\mm{A}_{k}) \leq \ell$ by showing that gaps between consecutive entries in $\family{S}(\mm{A}_{k})$ have size at most $\ell$. Each consecutive pair $(s_{j}, s_{j+1})$ in $\family{S}(\mm{A}_{k-1})$ of size greater than $\ell$ corresponds to two pairs $(s_{j}, s_{j} + a_k)$ and $(s_{j} + a_k, s_{j+1})$ in $\family{S}(\mm{A}_{k})$. Since $s_{j+1} - s_{j} \leq \ell_{k-1}$, the size of each gap is at most $\ell$.
		
		
		Next we will show that $2\cdot\lindisc(\mm{A}_{k}) \geq \ell$ by producing a pair of consecutive entries in $\family{S}(\mm{A}_{k})$ which achieves gap $\ell$. Suppose $\ell = a_{k}$. Recall that $s_0$ is the smallest subset sum of all entries in $\mm{A}_{k}$. The gap between $s_0$ and $s_1$ must be at least the magnitude of some entry in $\mm{A}_k$. Since $|a_{i}| \geq a_{k}$ for every $i \in [k-1]$, $(s_0, s_0 + a_{k})$ is a consecutive pair in $\family{S}(\mm{A}_k)$.  
		
		Suppose instead that $\ell = \ell_{k-1} - a_k > a_k$. Let $(s_{j^*}, s_{j^*+1})$ be a consecutive pair in $\family{S}(\mm{A}_{k-1})$ that achieves gap $\ell_{k-1}$ and suppose for a contradiction that $s_{j^*} + a_k$ and $s_{j^*+1}$ do not appear consecutively in $\family{S}(\mm{A}_k)$. Then there must be some $s \in \family{S}(\mm{A}_k)$ such that $s_{j^*} + a_k < s < s_{j^*+1}$. $s$ cannot be an element of $\family{S}(\mm{A}_{k-1})$ since $s_{j^*}$ and $s_{j^*+1}$ are consecutive in $\family{S}(\mm{A}_{k-1})$, so $s - a_k$ must be an element of $\family{S}(\mm{A}_{k-1})$. However since $s > s_{j^*} + a_k$, $s - a_k > s_{j^*}$. This is a contradiction since $s_{j^*}$ and $s_{j^*+1}$ are consecutive entries in $\family{S}(\mm{A}_{k-1})$. See Figure \ref{fig:polytimealgrowmatrix}. Thus $(s_{j^*} + a_k, s_{j^*+1})$ must be a consecutive pair in $\family{S}(\mm{A}_k)$.
		\end{proof}
		
		\fig{polytimealgrowmatrix}{0.6}{Suppose $a_k < \ell_{k-1} - a_k$ and there exists $s \in \family{S}(\mm{A}_{k})$ such that $s_{j^*} + a_k < s < s_{j^*+1}$.}
		
		Next we remove the non-negativity constraint.
		\begin{lemma}
		\label{lem:onerowaddnegatives}
		Let $\mm{A}_{k-1}$ and $\ell_{k-1}$ be as the above. Then, for $\mm{A}_{k} = [a_1, ..., a_{k-1}, a_{k}]$ where $a_k$ is negative and $|a_{k}| \leq |a_{i}|$ for all $i \in [k-1]$, the largest gap in $\family{S}(\mm{A}_k)$ is of size $\max(|a_k|, \ell_{k-1} - |a_k|)$. 
		\end{lemma}
		\begin{proof}
		Again let $\ell = \max(|a_k|, \ell_{k-1} - |a_k|)$. The argument is similar to the above. It is not hard to see that $2\cdot\lindisc(\mm{A}_k) \leq \ell$. Thus we will only show that $2\cdot\lindisc(\mm{A}_k) \geq \ell$. If $\ell = |a_k|$, then the consecutive pair $(s_0, s_0 - a_{k})$ in $\family{S}(\mm{A}_k)$ achieves a gap of size $\ell$.
		
		Next suppose that $\ell = \ell_{k-1} - |a_k| > |a_k|$. Again there exists a pair of consecutive entries $(s_{j^*}, s_{j^*+1})$ in $\family{S}(\mm{A}_{k-1})$ which achieves gap $\ell_{k-1}$. Suppose for a contradiction that $s_j$ and $s_{j^*+1} - |a_k|$ do not appear consecutively in $\family{S}(\mm{A}_k)$. Then there must be some $s \in \family{S}(\mm{A}_k)$ such that $s_{j^*} < s < s_{j^*+1} - |a_k|$. $s$ cannot be an element of $\family{S}(\mm{A}_{k-1})$ since $s_{j^*}$ and $s_{j^*+1}$ are consecutive in $\family{S}(\mm{A}_{k-1})$, so $s + |a_k|$ must be an element of $\family{S}(\mm{A}_{k-1})$. However since $s < s_{j^*+1} - |a_k|$, $s + |a_k| < s_{j^* + 1}$. This is a contradiction since $s_{j^*}$ and $s_{j^*+1}$ are consecutive entries in $\family{S}(\mm{A}_{k-1})$.
		\end{proof}
		
		\begin{corollary}
		\label{cor:onerowtakemagnitudes}
		Let $\mm{A} = [a_1, ..., a_n]$ and $\mm{A}' = [|a_1|, ..., |a_n|]$. Then $\lindisc(\mm{A}) = \lindisc(\mm{A}')$.
		\end{corollary}
		
		Lemma \ref{lem:onerowsortinghelps} and Corollary \ref{cor:onerowtakemagnitudes} suggest an algorithm: replace the entries of $\mm{A}$ by their magnitudes. Sort $\mm{A}$. Consider each entry in turn and update the largest gap accordingly. See Algorithm \ref{alg:lindisc}.
	\end{comment}
	
	\begin{lemma}
		\label{lem:onerowsortinghelps}
		Suppose $\mm{A}_{k-1} = [a_1, ..., a_{k-1}]$ with entries in $\RR$ and $|a_i| \geq |a_{i+1}|$. Let the largest gap in $\family{S}(\mm{A}_{k-1})$ be of size $\ell_{k-1}$. Then, for $\mm{A}_{k} = [a_1, ..., a_{k-1}, a_{k}]$ where $|a_{k}| \leq |a_{i}|$ for all $i \in [k-1]$, the largest gap in $\family{S}(\mm{A}_k)$ is of size $\max(|a_k|, \ell_{k-1} - |a_k|)$.
	\end{lemma}
	\begin{proof}
		Again, it is important to remember that the entries of $\family{S}(\mm{A}_{k})$ are exactly those in $\family{S}(\mm{A}_{k-1})$ along with those in $\{a_k + s: s \in \family{S}(\mm{A}_{k-1})\}$. Let $\ell = \max(|a_k|, \ell_{k-1} - |a_k|)$.
		
		We first show that $2\cdot\lindisc(\mm{A}_{k}) \leq \ell$ by showing that gaps between consecutive entries in $\family{S}(\mm{A}_{k})$ have size at most $\ell$. Each consecutive pair $(s_{j}, s_{j+1})$ in $\family{S}(\mm{A}_{k-1})$ of size greater than $\ell$ are no longer consecutive in $\family{S}(\mm{A}_{k})$ since $s_{j} \leq s_{j} + a_k \leq s_{j+1}$ if $a_k > 0$ and $s_{j} \leq s_{j+1} + a_k \leq s_{j+1}$ if $a_k < 0$. See Figure \ref{fig:polytimealgrowmatrixlemma1}. Since $s_{j+1} - s_{j} \leq \ell_{k-1}$, the size of each gap in $\family{S}(\mm{A}_k)$ is at most $\ell$.

		\fig{polytimealgrowmatrixlemma1}{1}{All consecutive pairs in $\family{S}(\mm{A}_{k-1})$ of size greater than $|a_k|$ will be divided into two or more consecutive pairs in $\family{S}(\mm{A}_k)$. The red interval indicates what happens when $a_k > 0$. The blue interval indicates what happens when $a_k < 0$.} 
		
		Next we will show that $2\cdot\lindisc(\mm{A}_{k}) \geq \ell$ by producing a pair of consecutive entries in $\family{S}(\mm{A}_{k})$ which achieves gap $\ell$. Suppose $\ell = |a_{k}|$. Recall that $s_0$ is the smallest subset sum of all entries in $\mm{A}_{k}$. The gap between $s_0$ and $s_1$ must be at least the magnitude of some entry in $\mm{A}_k$. Since $|a_{i}| \geq |a_{k}|$ for every $i \in [k-1]$, $(s_0, s_0 + |a_{k}|)$ is a consecutive pair in $\family{S}(\mm{A}_k)$.  
		
		When $\ell = \ell_{k-1} - |a_k| > |a_k|$, we split our analysis into two cases: (1) $a_k > 0$ and (2) $a_k < 0$. 
		
		In the former case, let $(s_{j^*}, s_{j^*+1})$ be a consecutive pair in $\family{S}(\mm{A}_{k-1})$ that achieves gap $\ell_{k-1}$ and suppose for a contradiction that $s_{j^*} + a_k$ and $s_{j^*+1}$ do not appear consecutively in $\family{S}(\mm{A}_k)$. Then there must be some $s \in \family{S}(\mm{A}_k)$ such that $s_{j^*} + a_k < s < s_{j^*+1}$. Note that $s$ cannot be an element of $\family{S}(\mm{A}_{k-1})$ since $s_{j^*}$ and $s_{j^*+1}$ are consecutive in $\family{S}(\mm{A}_{k-1})$, so $s - a_k$ must be an element of $\family{S}(\mm{A}_{k-1})$. However since $s > s_{j^*} + a_k$, we have $s - a_k > s_{j^*}$. This is a contradiction since $s_{j^*}$ and $s_{j^*+1}$ are consecutive entries in $\family{S}(\mm{A}_{k-1})$. See Figure \ref{fig:polytimealgrowmatrix}. Thus $(s_{j^*} + a_k, s_{j^*+1})$ must be a consecutive pair in $\family{S}(\mm{A}_k)$.
		
		\fig{polytimealgrowmatrix}{0.6}{Suppose $a_k < \ell_{k-1} - a_k$ and there exists $s \in \family{S}(\mm{A}_{k})$ such that $s_{j^*} + a_k < s < s_{j^*+1}$.}
		
		The later case of $a_k < 0$, is similar. Again there exists a pair of consecutive entries $(s_{j^*}, s_{j^*+1})$ in $\family{S}(\mm{A}_{k-1})$ which achieves gap $\ell_{k-1}$. Suppose for a contradiction that $s_j$ and $s_{j^*+1} - |a_k|$ do not appear consecutively in $\family{S}(\mm{A}_k)$. Then there must be some $s \in \family{S}(\mm{A}_k)$ such that $s_{j^*} < s < s_{j^*+1} - |a_k|$. Again, $s$ cannot be an element of $\family{S}(\mm{A}_{k-1})$ since $s_{j^*}$ and $s_{j^*+1}$ are consecutive in $\family{S}(\mm{A}_{k-1})$, so $s + |a_k|$ must be an element of $\family{S}(\mm{A}_{k-1})$. However since $s < s_{j^*+1} - |a_k|$, we have $s + |a_k| < s_{j^* + 1}$. This is a contradiction since $s_{j^*}$ and $s_{j^*+1}$ are consecutive entries in $\family{S}(\mm{A}_{k-1})$.
	\end{proof}

	\begin{corollary}
		\label{cor:onerowtakemagnitudes}
		Let $\mm{A} = [a_1, ..., a_n]$ and $\mm{A}' = [|a_1|, ..., |a_n|]$. Then $\lindisc(\mm{A}) = \lindisc(\mm{A}')$.
	\end{corollary}
	
	Lemma \ref{lem:onerowsortinghelps} and Corollary \ref{cor:onerowtakemagnitudes} suggest an algorithm: replace the entries of $\mm{A}$ by their magnitudes. Sort $\mm{A}$. Consider each entry in turn and update the largest gap accordingly. See Algorithm \ref{alg:lindisc}.
	
	\begin{theorem}
		\label{thm:onerowlindisc}
		Algorithm \ref{alg:lindisc} on a row matrix $\mm{A}$ outputs $\lindisc(\mm{A})$. 
	\end{theorem}
	\begin{proof}
		By Corollary $\ref{cor:onerowtakemagnitudes}$ it is sufficient to consider row matrices with non-negative entries. Suppose that $\mm{A} = [a_1, ..., a_n]$ is such a matrix with entries sorted in decreasing order. Algorithm \ref{alg:lindisc} correctly outputs the linear discrepancy for matrices with a single entry. Let $\mm{A}_i = [a_1, ..., a_i]$. Given the largest gap in $\family{S}(\mm{A}_i)$, Lemma \ref{lem:onerowsortinghelps} tells us the largest gap in $\family{S}(\mm{A}_{i+1})$. Since $\lindisc(\mm{A})$ is half the size of the largest gap in $\family{S}(\mm{A})$, the algorithm correctly returns $\lindisc(\mm{A})$ as required.
	\end{proof} 
	
	\begin{algorithm}
		\DontPrintSemicolon % Some LaTeX compilers require you to use \dontprintsemicolon instead 
		\KwIn{Matrix $\mm{A} \in \RR^{m \times n}$.}
		\KwOut{$\lindisc(\mm{A})$.}
		\For{$i$ from $1$ to $n$}{
			$\mm{A}[i] \leftarrow |a_i|$\;
		}
		sort $\mm{A}$ in decreasing order\;
		$\ell \leftarrow a_1$\;
		\For{$i$ from $2$ to $n$}{
			$\ell \leftarrow \max(a_i, \ell - a_i)$\;
		}
		\Return{$\frac{\ell}{2}$}\;
		\caption{Linear discrepancy of row matrix.}
		\label{alg:lindisc}
	\end{algorithm}
	
	Thus, for any row matrix $\mm{A}$ with $n$ elements, we can find $\lindisc(\mm{A})$ in time $O(n \log n)$.  
	
	\begin{corollary}
		If $\mm{A}$ is an arithmetic progression of the form $[a_0, a_0 + k, \cdots, a_{0} + (n-1)k]$, then 
		\[\lindisc(A) = \frac{\max(a_0, k)}{2}.\]
		
		Similarly, if $\mm{A}$ is a geometric progression of the form $[a_0r^{0}, \cdots, a_{0}r^{n-1}]$, then 
		\[\lindisc(A) = \begin{cases}
		\frac{a_0}{2} &\mbox{if } r \leq 1,\\
		a_0\left(r^{n-1} - \frac{\left(r^{(n-1)}-1\right)}{(r-1)}\right) &\mbox{otherwise}.
		\end{cases}\]
	\end{corollary}
		
	\subsection{Poly-time Approximation Algorithm}
	Next, we present a $2^n$-approximation algorithm for linear discrepancy. Note that 
	\[\lindisc(\mm{A}) \leq \max_{\vv{w} \in [0,1]^n} \norm{\mm{A}(\mm{w} - \round(\mm{w}))}_{\infty} = \frac{1}{2} \max_{\vv{z} \in [-1,1]^n}\norm{\mm{A}\vv{z}}_{\infty} = \frac{1}{2}\norm{\mm{A}}_{\infty \rightarrow \infty}\]
	where $\norm{\mm{A}}_{p \rightarrow q}$ is the operator norms defined as
	\[\norm{\mm{A}}_{p \rightarrow q} = \max_{\vv{x}\in \RR^n} \frac{\norm{\mm{A}\vv{x}}_{q}}{\norm{\vv{x}}_{p}}.\]
	
	To bound $\lindisc(\mm{A})$ from below, we show that $\norm{\mm{A}}_{\infty \rightarrow \infty} \leq O(2^n) \cdot \lindisc(\mm{A})$. Let us try to interpret this statement. Note that $\norm{\mm{A}\vv{z}}_{\infty}$ is equal to the Minkowski $\polytope{P}$-norm $\norm{\vv{z}}_{\polytope{P}}$ for $\polytope{P} = \{\vv{x}: \norm{\mm{A}\vv{x}}_{\infty} \leq 1\}$ i.e. $\norm{\vv{z}}_{\polytope{P}} = \inf\{t \geq 0: \vv{z} \in t\polytope{P}\}$ so
	\[\norm{\mm{A}}_{\infty \rightarrow \infty} = \max_{\vv{z} \in [-1, 1]^n} \norm{\mm{A}\vv{z}}_{\infty} = \max_{\vv{z} \in [-1, 1]^n} \norm{\vv{z}}_{\polytope{P}}.\]
	By interpreting $\vv{z}$ as the difference of two vectors $\vv{x}, \vv{x}' \in [0,1]^n$ we have that 
	\[\norm{\mm{A}}_{\infty \rightarrow \infty} = \max_{\vv{z} \in [-1,1]^n} \norm{\vv{z}}_{\polytope{P}} = \max_{\vv{x}, \vv{x}' \in [0,1]^n} \norm{\vv{x} - \vv{x}'}_{\polytope{P}} = \diam_{\polytope{P}}\left([0,1]^n\right).\]
	Recall that $\lindisc(\mm{A})$ is the scaling of $\polytope{P}$ that covers $[0,1]^n$ when $2^n$ copies of $\polytope{P}$ are placed at the verticies $\vv{x} \in \{0,1\}^n$. Here we just need to show that the diameter of the unit hyper-cube with Minkowski $\polytope{P}$-norm is no more than this scale-factor times $O(2^n)$. 
	
	We generalize this as the following.
	\begin{claim}
		Let $\polytope{K}$ be a convex symmetric polytope and $S \subset \RR^n$ be convex. Suppose there exist $N$ elements $x_1, ..., x_N \in S$ such that 
		\[S \subset \bigcup_{x_i} x_i + t\polytope{K}.\]
		Then $\diam_K(S) = \max_{x, x' \in S} \norm{x - x'}_{K} \leq O(N) \cdot t$.
	\end{claim}
	\begin{proof}
		Fix any two points $x$ and $x'$ in $S$. Let $\polytope{P}_i$ be the polytope $x_i + t\polytope{K}$. Since $S$ is convex, the line segment $\lambda x + (1 - \lambda)x'$ for $\lambda \in [0,1]$ is in $S$. Therefore $\lambda x + (1 - \lambda)x'$ intersects a sequence of polytopes $\polytope{P}_{k_1}, ..., \polytope{P}_{k_r}$ with centres $x_{k_1}, ..., x_{k_r}$. Since the polytopes are convex, they can appear in the sequence at most once. By the triangle inequality we have
		\begin{align*}
			\norm{x - x'}_K 
			&= \norm{(x - x_{k_1}) + (x_{k_1} - x_{k_2}) + \cdots + (x_{k_r} - x')}_{K}\\ 
			&\leq \norm{x - x_{k_1}}_{K} + \norm{x_{k_1} - x_{k_2}}_{K} + \cdots + \norm{x_{k_r} - x'}_{K}\\
			&\leq t + 2t(N-1) + t = 2tN
		\end{align*}
		where the last inequality follows as $x \in \polytope{P}_{k_1}$, $x' \in \polytope{P}_{k_r}$, and $\norm{x_{k_i} - x_{k_{i+1}}}_K \leq 2t$.
		\begin{comment}
		Just for completeness, we show that $\norm{x_{k_i} - x_{k_{i+1}}}_K \leq 2t$. Since $P_{k_i}$ and $P_{k_{i+1}}$ are consecutive in the sequence of polytopes that $\lambda x + (1 - \lambda) x'$ intersect, $P_{k_i} \cap P_{k_i} \neq \emptyset$. Let $y$ be a point in their intersection. Then 
		\[\norm{x_{k_i} - x_{k_{i+1}}}_K \leq \norm{x_{k_i} - y}_{K} + \norm{y - x_{k_{i+1}}}_K \leq 2t.\]
		\end{comment}
	\end{proof}
	
	Thus the lower bound follows by setting $\polytope{K}$ to be the parallelepiped defined by $\mm{A}$, $S = [0,1]^n$, $t = \lindisc(\mm{A})$, and $\{x_1, ..., x_N\} = \{0,1\}^{n}$.  