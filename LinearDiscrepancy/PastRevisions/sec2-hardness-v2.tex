% !TEX root = MAIN-lindisc.tex

\section{Hardness Result}	
	We show that discrepancy ($\class{DS}$), hereditary discrepancy ($\class{HDS}$), and linear discrepancy ($\class{LDS}$) are $\class{NP}$-Hard by reducing from monotone not-all-equal 3-$\class{SAT}$ ($\class{MNAE3SAT}$) \cite{gold1978complexity} to each. We first perform the reduction for $\class{DS}$ and $\class{HDS}$. They will serve as models for our $\class{LDS}$ reduction. All decision problems for discrepancy and its variants are similar to the description below.

	\problem{Monotone Not-All-Equal 3-$\class{SAT}$}{MNAE3SAT}{Let $U$ be a collection of variables $\{u_1, ..., u_n\}$ and $\mathcal{C}$ be a 3-$\mathsf{CNF}$ with clauses $\{C_1, ..., C_m\}$ such that $C_i = t_{i,1} \lor t_{i,2} \lor t_{i,3}$ for positive literals $t_{i,j}$.}{Does there exist a truth assignment $\tau: U \rightarrow \{\mathsf{T}, \mathsf{F}\}$ such that $\mathcal{C}$ is satisfied and each clause has at least one true and one false literal?}{}
	
	\problem{Discrepancy}{DS}{Let $\mm{A} \in \RR^{m \times n}$ be a matrix and $t \geq 0$ a real value.}{Is $\disc(\mm{A}) \leq t$?}{}
		
	\subsection{Discrepancy and Hereditary Discrepancy}
		
		Let 3-$\mathsf{CNF}$ $\mathcal{C}$ be a $\class{MNAE3SAT}$ instance as described above. The corresponding $\class{DS}$ instance will be the incidence matrix $\mm{A} \in \{0,1\}^{m \times n}$ of $\mathcal{C}$. Column $\vv{a}_i$ of $\mm{A}$ corresponds to variable $u_i$ and row $\vv{r}_j$ of $\mm{A}$ corresponds to clause $C_j$. Finally let the target of $\class{DS}$ be $t = \frac{1}{2}$. 
		
		To see that this reduction is complete, suppose that $\mathcal{C}$ is a $\NO$-instance of $\class{MNAE3SAT}$ i.e. for every truth assignments $\tau$, there exists a clause $C_i$ whose literals get the same truth assignment. Each $\vv{x} \in \{0, 1\}^n$ corresponds to a truth assignment. If $x_i = 1$ (resp. $x_i = 0$) then $u_i$ is true (resp. $u_i$ is false). Let $C_j$ be the clause whose literals have the same truth value. Then 
		\[\disc(\mm{A}) \geq \left|\vv{r}_j \left(\frac{1}{2} \cdot \ind{1} - \vv{x}\right)\right| = \frac{3}{2} > \frac{1}{2}\] so $\mm{A}$ is a $\NO$-instance of $\class{DS}$. 
		
		To see that this reduction is sound, suppose instead that $\mathcal{C}$ is a $\YES$-instance of $\class{MNAE3SAT}$. Let $\tau^*$ be a satisfying truth assignment of $\mathcal{C}$. Let $\vv{x}^*$ be the indicator vector of the true variables in $\tau^*$. Then 
		\[\disc(\mm{A}) \leq \left\|\mm{A}\left(\frac{1}{2} \cdot \ind{1} - \vv{x}^{*}\right)\right\|_{\infty} = \frac{1}{2}\] 
		since every clause has exactly two elements with the same truth value. Thus $\mm{A}$ is a $\YES$-instance of $\class{DS}$. 

		Hereditary discrepancy can be shown hard using the same reduction with $t = 1$. A $\NO$-instance of $\class{MNAE3SAT}$ translates into a $\NO$-instance of $\class{HDS}$ since $\herdisc(\mm{A}) \geq  \disc(\mm{A}) = \frac{3}{2}$. A $\YES$-instance of $\class{MNAE3SAT}$ translates into a $\YES$-instance of $\class{HDS}$ as it did for $\class{DS}$. 
		
		Let $\tau^*$ be a satisfying assignment of $\mathcal{C}$ and $\vv{x}^*$ be the indicator vector of the true variables. Consider any subset of the variables $U' \subseteq U$. We will show that $\mm{A}_{U'}$, the matrix $\mm{A}$ restricted to the variables in $U'$, satisfies $\disc(\mm{A}_{U'}) \leq 1$. Consider a row of $\mm{A}_{U'}$. Either the row has fewer than three or exactly three non-zero entries. In the former case any assignment to the variables achieves discrepancy $\leq 1$ for that row. In the latter case all variables in the associated clause are intact. The rows of $A_{U'}$ which have three non-zero entries correspond to a subset of the clauses in $\mathcal{C}$. Since $\tau^*$ is a satisfying assignment of $\mathcal{C}$, it must also be a satisfying assignment of this subset of clauses. Let $\vv{x}^*_{U'}$ be the vector $\vv{x}^*$ restricted to the variables in $U'$ and $\frac{1}{2}\cdot\ind{1}_{U'}$ be the vector $\frac{1}{2}\cdot\ind{1}$ restricted to the variables in $U'$. Then 
		\[\herdisc(\mm{A}) \leq \max_{U' \subset U}\left\|\mm{A}_{U'}\left(\frac{1}{2}\cdot\ind{1}_{U'} - \vv{x}_{U'}\right)\right\|_{\infty} \leq 1\] 
		since $U'$ was an arbitrary subset of $U$.
		
	\subsection{Linear Discrepancy}
		Before we show that linear discrepancy is hard, we will show that the value of $\lindisc(\mm{A})$ can be expressed using a polynomial number of bits in the bit complexity of a matrix for rational matrices.
		\begin{lemma}
			\label{lem:certificateinpi2}
			Let $\mm{A} \in \QQ^{m \times n}$ with $\gamma = \max_{i,j} |A_{i,j}|$, the bit complexity of $\lindisc(\mm{A})$ is polynomial in $n$ and $\log \gamma$. 
		\end{lemma}
		\begin{proof}
			Let  $\mm{r}_i$ for $i \in [m]$ be the rows of $\mm{A}$, $\lindisc(\mm{A}) = \lambda_A$, and $\vv{w}^*$ be a deep-hole of $\mm{A}$. For every $\vv{x} \in \{0,1\}^{n}$ there exists an $i \in [m]$ and $\sigma \in \{-1, 1\}$ such that $\sigma\vv{r}_i(\vv{w}^* - \vv{x}) \geq \lambda_A$. Let $\vv{b}_{x} = \sigma\mathbf{r}_i$ and consider the following linear program over the variables $\vv{w} \in \RR^n$ and $\lambda \in \RR$:
			\begin{align*}
				\mbox{Maximize: } &\lambda\\
				\mbox{Subject to: } &\mathbf{b_x}(\mathbf{w}-\mathbf{x}) \geq \lambda &\mbox{for all } \mathbf{x} \in \{0,1\}^n\\
				&\mathbf{0} \leq \mathbf{w} \leq \mathbf{1}
			\end{align*}
			
			Let $\lambda^*$ be the optimum value of this linear program. First note that $\lambda_A \leq \lambda^*$ since $(\vv{w}^*,\lambda)$ satisfies the constraints. Next we show that $\lambda_A \geq \lambda^*$. Suppose for a contradiction that $\lambda_A < \lambda^*$. Then there exists $\vv{w}' \in [0,1]^n$ such that 
            \[\|\mm{A}(\vv{w}' - \vv{x})\|_\infty \ge \vv{b}_x(\vv{w}' - \vv{x}) \ge \lambda^*>\lambda_A\] 
            for every $\vv{x} \in \{0,1\}^{n}$. Since $\lambda_A = \lindisc(\mm{A})$, we cannot have $\lindisc(\mm{A}, \mathbf{w}') > \lambda_A$. Thus $\lambda^* = \lindisc(\mm{A})$. Since this LP has $n$ variables, the number of bits required to express the linear discrepancy and some deep-hole $\vv{w}^*$ of $\mm{A}$ are polynomial in $n$ and the bit complexity of the largest entry of $\mm{A}$ \cite{schrijver1998theory}. 
		\end{proof} 
		Note that a polynomial sized deep-hole $\vv{w}^*$ is a $\Pi_2$ certificate for linear discrepancy so this decision problem is at worst $\Pi_2$-Complete. 
	
		\begin{proof}[Proof of Theorem \ref{thm:hardness}]
			Consider the incidence matrix used in the standard discrepancy and hereditary discrepancy reductions. Let the target $t$ be $\frac{3}{2} - \epsilon$ for $\epsilon > 0$ to be determined later. This reduction is complete since $\lindisc(\mm{A}) \geq \disc(\mm{A}) = \frac{3}{2}$.
			
			To show this reduction is sound, let $\mathcal{C}$ be a $\YES$-instance of $\class{MNAE3SAT}$ and $\tau$ be a satisfying assignment. Suppose $\vv{w}^* \in [0,1]^n$ is a deep-hole of $\mm{A}$. If $w_i^* = \frac{1}{2}$ for all $i \in [n]$ then $\lindisc(\mm{A}) = \disc(\mm{A})$ and we are done. Let $\epsilon$ be the smallest non-zero gap between $w_i^*$ and $1/2$ i.e. for all $w_i^* \neq \frac{1}{2}$,
			\[\left|w_i^* - \frac{1}{2}\right| \leq \frac{1}{2} - \epsilon.\] 
			We will show that $\lindisc(\mm{A}, \vv{w}^*) \leq \frac{3}{2} - \epsilon$ by constructing a colouring $\vv{x}^*$. Let 
			\[x_i^* = 
			\begin{cases}
			\round(w_i^*) &\mbox{if } w_i^* \neq \frac{1}{2}\\
			\tau(u_i) &\mbox{otherwise}
			\end{cases}\]
			where $\round(w_i^*)$ is $w_i^*$ rounded to the closest integer and $u_i$ is the variable corresponding to column $i$. Let $\vv{r}$ be a row of matrix $\mm{A}$ with non-zero entries in columns $i$, $j$, and $k$. We bound the discrepancy of row $\vv{r}$ based on the number of rounded variables $R_v$ among $\{x_i, x_j, x_k\}$.  
			\begin{enumerate}
				\item[$R_v = 0$:] Since none of the variables are rounded, $w_i^* = w_j^* = w_k^* = \frac{1}{2}$ and 
				\[\left|\vv{r}\left(\vv{x}^*-\vv{w}^*\right)\right| = \left|\left(x_i^* - \frac{1}{2}\right) + \left(x_j^* - \frac{1}{2}\right) + \left(x_k^* - \frac{1}{2}\right)\right| = \frac{1}{2}\]
				since $\tau$ is a satisfying assignment.
				\item[$R_v = 1$:] W.l.o.g assume that that $x_i^*$ is the rounded value and $w_j^* = w_k^* = \frac{1}{2}$. Then  
				\[\left|\vv{r}\left(\vv{x}^* - \vv{w}^*\right)\right| = \left|\left(x_i^* - w_i^*\right) + \left(x_j^* - \frac{1}{2}\right) + \left(x_k^* - \frac{1}{2}\right)\right| \leq \left(\frac{1}{2} - \epsilon\right) + 1 = \frac{3}{2} - \epsilon.\]
				\item[$R_v = 2$:] W.l.o.g assume that $x_i^*$ and $x_j^*$ are the rounded values and $w_k^* = \frac{1}{2}$. Then  
				\[\left|\vv{r}\left(\vv{x}^* - \vv{w}^*\right)\right| = \left|\left(x_i^* - w_i^*\right) + \left(x_j^* - w_j^*\right) + \left(x_k^* - \frac{1}{2}\right)\right| \leq 2 \cdot \left(\frac{1}{2} - \epsilon\right) + \frac{1}{2} = \frac{3}{2} - 2\epsilon.\]
				\item[$R_v = 3$:] All three values are rounded so  
				\[\left|\vv{r}\left(\vv{x}^* - \vv{w}^*\right)\right| = \left|\left(x_i^* - w_i^*\right) + \left(x_j^* - w_j^*\right) + \left(x_k^* - w_k^*\right)\right| \leq 3\cdot\left(\frac{1}{2} - \epsilon\right) = \frac{3}{2} - 3\epsilon.\]
			\end{enumerate}
			Since $\vv{r}$ was an arbitrary row of $\mm{A}$, $\lindisc(\mm{A}) = \lindisc(\mm{A}, \vv{w}^*) \leq \frac{3}{2} - \epsilon$ as required. By Lemma \ref{lem:certificateinpi2} there exists a rational deep-hole $\vv{w}^*$ with entries whose bit complexity is polynomial in the bit complexity of $\mm{A}$. Thus $\epsilon$ can be expressed using a polynomial number of bits in the bit-complexity of $\mm{A}$.
		\end{proof}
		
		%\subsubsection{Hardness of Approximation}