% !TEX root = MAIN-lindisc.tex

\section{Introduction}
	First a few words regarding notation. Bold-face capital letters such as $\mm{A}$ denote matrices; $A_{i,j}$ is the entry in its $i$\textsuperscript{th} row and $j$\textsuperscript{th} column. Bold-face lowercase letters such as $\vv{x}$ denote vectors; $x_i$ is its $i$\textsuperscript{th} entry. We denote the all ones vector by $\ind{1}$. Whenever we write $i \in [n]$, we mean $i \in \{1,2, ..., n\}$. For simplicity of exposition, assume that $\mm{A} \in \RR^{m \times n}$ in the forgoing sections. We will explicitly indicate other uses of $\mm{A}$.
	
	Let $X$ be a finite set of size $n$ and $\family{S}$ be a subset of the power set of $X$ of size $m$.\sn{I'd focus more on linear discrepancy than other discrepancy measures, and start with motivation about rounding.} Let $\chi: X \rightarrow \{-1,1\}$ be a colouring of the elements in $X$. The \gls{disc} of a set $S \in \family{S}$ with respect to $\chi$ is defined as 
	\[\disc(S, \chi) = \left|\sum_{s \in S} \chi(s)\right|.\]
	The discrepancy of set system $(X, \family{S})$ is
	\[\disc(\family{S}) = \min_{\mm{\chi}}\max_{S \in \family{S}} \disc(S, \chi).\]
	Let $\mm{A} \in \{0,1\}^{m \times n}$ be the incident matrix of $\family{S}$ where rows correspond to sets in $\family{S}$ and columns correspond to elements in $X$. Then the discrepancy of the set system is equivalent to the discrepancy of $\mm{A}$ given by 
	\begin{equation}
		\label{eq:discrepancy}
		\disc(\mm{A}) = \min_{\vv{x} \in \{-1,1\}^n}\norm{\mm{A}\vv{x}}_{\infty}.
	\end{equation}
	Sometimes $\disc(\mm{A})$ can be small ``by accident", thus it is useful to define a discrepancy variant\footnote{Consider any matrix $\mm{B} \in \RR^{m \times n}$ and let $\mm{A} \in \RR^{m \times 2n}$ be the concatenation of two copies of $\mm{B}$ side by side. Regardless of the discrepancy of $\mm{B}$, $\disc(\mm{A}) = 0$ since there exists $\vv{x} \in \{-1, 1\}^n$ such that $\norm{\mm{A}\vv{x}}_{\infty} = 0$, namely 
		\[\vv{x}^{\intercal} = [\underbrace{-1, ..., -1}_{n}, \underbrace{1, ..., 1}_{n}].\]}. 
		
	The \gls{herdisc} of $\mm{A}$ is the maximum discrepancy over all sub-matrices, i.e. if $\mm{B}$ is a sub-matrix of $\mm{A}$, then
	\begin{equation}
		\label{eq:hereditarydiscrepancy}
		\herdisc(\mm{A}) = \max_{\mm{B}\in \RR^{k \times \ell}}\min_{\vv{x} \in \{-1,1\}^{\ell}}\norm{\mm{B}\vv{x}}_{\infty}.
	\end{equation}
	
	Observe that every colouring $\vv{x}$ above is a vertex of the $\{-1,1\}^n$-hypercube. In the forgoing, it would be helpful to let $\vv{x}$ be a vertex of the $\{0,1\}^n$-hypercube instead. Thus we perform a change of variables $x_i \mapsto \frac{1 - x_i}{2}$. The discrepancy definition becomes
	\begin{equation}
	\label{eq:disc}
		\disc(\mm{A}) = \min_{\vv{x} \in \{0,1\}^{n}} \left\|\mm{A}\left(\frac{1}{2}\cdot\ind{1} - \vv{x}\right)\right\|_{\infty}
	\end{equation} 
	
	There is nothing special about the vector $\frac{1}{2}\ind{1}$. For any $\vv{w} \in [0,1]^n$ we can consider
	\[\lindisc(\mm{A}, \vv{w}) = \min_{\vv{x} \in \{0,1\}^{n}} \norm{\mm{A}\left(\vv{w} - \vv{x}\right)}_{\infty}.\]
	This is the \gls{lindisc} of $\mm{A}$ with respect to $\vv{w}$. The linear discrepancy of $\mm{A}$ is defined as
	\begin{equation}
		\label{eq:lindisc}
		\lindisc(\mm{A}) = \max_{\vv{w} \in [0,1]^n} \lindisc(\mm{A}, \vv{w}) = \max_{\vv{w} \in [0,1]^n}\min_{\vv{x} \in \{0,1\}^{n}} \norm{\mm{A}\left(\vv{w} - \vv{x}\right)}_{\infty}
	\end{equation}
	
	Linear discrepancy can be bounded above by hereditary discrepancy by the following theorem. 
	\begin{theorem}
		\label{eq:lindiscubstandardherdisc}
		(Lov\'{a}sz et al. 1986, \cite{lovasz1986discrepancy}) $\lindisc(\mm{A}) \leq 2\herdisc(\mm{A})$.
	\end{theorem}
	\ifforyoureyesonly
	\begin{proof}
		Let $\polytope{P}$ be the fundamental parallelepiped of $\mm{A}$ defined as $\polytope{P} = \{\vv{y} \RR^{n}: \norm{\mm{A}\vv{y}}_{\infty} \leq 1\}$ and let $t$ be a scalar such that \[\polytope{U} = \bigcup_{\vv{x} \in \{0,1\}^n} \vv{x} + t\polytope{P}_{A}\]
		covers the center of every face in any dimension. This is at most the scaling required to cover the center of every face with a polytope placed at a vertex on the boundary of that face so $t \leq \herdisc(\mm{A})$. We show that
		\[\polytope{V} = \bigcup_{\vv{x} \in \{0,1\}^n} \vv{x} + 2t\polytope{P}_{A}\]
		covers every point in $[0,1]^n$. Thus $\lindisc(\mm{A}) \leq 2t \leq 2\herdisc(\mm{A})$. 
		
		It suffices to show that $\polytope{V}$ covers all the dyadic rational points in $[0,1]^n$, rationals whose denominators are powers of two, since they are dense in the hyper-cube. Let $\vv{a} = \frac{1}{2^{k}}\vv{r}$ where $0 \leq r_i \leq 2^{k}$. The proof proceeds by induction on the exponent $k$. In the base case $\vv{a} \in \{0,1/2,1\}^n$ are exactly the center of faces in every dimension. Suppose all $\vv{a} = \frac{1}{2^{k}}\vv{r}$ are covered by $\polytope{V}$, let us show that $\vv{b} = \frac{1}{2^{k+1}}\vv{s}$ for $0 \leq s_i \leq 2^{k+1}$ is covered by $\polytope{V}$ as well. Note that $2\vv{b} \in [0,2]^n$. There exists $\vv{z} \in \{0,1\}^n$ such that $2\vv{b} - \vv{z} \in [0,1]^n$. Since the denominators of $2\vv{b} - \vv{z}$ are powers of two with exponent less than or equal to $k$, the induction hypothesis implies that there exists a $\vv{x} \in \{0,1\}^n$ such that $2\vv{b} - \vv{z} \in \vv{x} + 2t\polytope{P}_{A}$ or 
		\[\vv{b} \in \frac{\vv{z} + \vv{x}}{2} + t\polytope{P}_{A}.\]
		Note that $\frac{\vv{z} + \vv{x}}{2} \in \{0,1/2,1\}^n$. By the definition of $t$ there exists some $\vv{y} \in \{0,1\}^n$ such that $\frac{\vv{z} + \vv{x}}{2} \in \vv{y} + t\polytope{P}_{A}$. It follows from the convexity of $\polytope{P}_{A}$ that $\vv{b} \in \left(\vv{y} + t\polytope{P}_{A}\right) + t\polytope{P}_{A} = \vv{y} + 2t\polytope{P}_{A}$.
	\end{proof}
	\fi
	
	Since $\herdisc(\mm{A})$ can be well approximated by the \gls{gamma2}-norm of $\mm{A}$, and the \gls{gamma2}-norm of $\mm{A}$ can be calculated in time polynomial in $m$ and $n$, there exists a constructive upper bound for linear discrepancy \cite{DBLP:journals/corr/MatousekNT14}. It is natural to ask if there exists a constant $c$ such that $\herdisc(\mm{A}) \leq c \cdot \lindisc(\mm{A})$, but no such constant exists\footnote{Consider the case where $\mm{A} = [2^0, 2^1, 2^2, ..., 2^{n-1}]$. The sub-matrix $\mm{A}' = [2^{n-1}]$ achieves the largest discrepancy of $2^{n-2}$ so $\herdisc(\mm{A}) = 2^{n-2}$. However, $\lindisc(\mm{A})$ is rather modest. Note that $\mm{A}\vv{x} \in \{0, 1, ..., 2^{n}-1\}$ for $\vv{x} \in \{0,1\}^n$ and $\mm{A}\vv{w} \in [0, 2^n - 1]$ for $\vv{w} \in [0,1]^n$. Since for any $\vv{w}$ we can chose some $\vv{x}$ such that $|\mm{A}\vv{w} - \mm{A}\vv{x}| \leq \frac{1}{2}$, $\lindisc(\mm{A}) \leq \frac{1}{2}$.}.
	
	It will be useful to consider a maximizer of Equation (\ref{eq:lindisc}) i.e. $\mm{w}^* \in [0,1]^n$ such that $\lindisc(\mm{A}, \mm{w}^*) = \lindisc(\mm{A})$. Call $\vv{w^*}$ a \emph{deep-hole} of $\mm{A}$. Every $\mm{A}$ has at least one deep-hole, since linear discrepancy is a continuous function over the compact set $[0,1]^n$. Further, if $\vv{w}^*$ is a deep-hole then so is $\ind{1} - \vv{w}^*$, simply take the colouring to be $\ind{1}-\vv{x}$.
	
	Let us observe some basic properties of linear discrepancy. From the definitions it is easy to see that $\lindisc(\mm{A})$ is invariant under row and column exchanges. Further, scaling the matrix $\mm{A}$ scales $\lindisc(\mm{A})$ proportionally. If row $i$ of $\mm{A}$ is $\mm{r}_i$, then $\lindisc(\mm{A}) \geq \max_{i \in [m]} \lindisc(\vv{r}_i)$. This extends to any subset of the rows of $\mm{A}$. Thus the sub-matrices $\mm{A}'$ of $\mm{A}$ which satisfy $\lindisc(\mm{A}')\geq \lindisc(\mm{A})$ are a subset of the columns of $\mm{A}$.
	
	\subsection{Geometric Interpretation}
	Each discrepancy variant mentioned above has a corresponding geometric interpretation. Let $\polytope{P}_{A} = \{\vv{x}: \norm{\mm{A}\vv{x}}_{\infty} \leq 1\}$ be the \emph{fundamental parallelepiped} of $\mm{A}$. Observe that $\polytope{P}_{A}$ is convex and symmetric. Imagine placing a scaled copy $t \cdot \polytope{P}_{A}$ at every corner of the unit hyper-cube $\{0,1\}^n$. The smallest $t$ for which some polytope contains $\frac{1}{2}\ind{1}$ is exactly $\disc(\mm{A})$. Similarly, the smallest $t$ for which the union of all polytopes covers the unit-hypercube $\{0,1\}^n$ is $\lindisc(\mm{A})$. 
 
	\fig{geometricview}{1}{The geometric interpretation of discrepancy (left) and linear discrepancy (right). The dark gray polygons on the left represent $\polytope{P}_{A}$ placed at each corner of the unit cube. The scaling required to cover the center is the discrepancy. The scaling required to cover the entire cube is the linear discrepancy.}
	
	Formally, we can define these as
	\begin{align*}
		\disc(\mm{A}) &= \inf\left\{t \geq 0: \frac{1}{2}\ind{1} \in \bigcup_{\vv{x} \in \{0,1\}^n} \vv{x} + t\polytope{P}_{A}\right\} \mbox{ and }\\
		\lindisc(\mm{A}) &= \inf\left\{t \geq 0: [0,1]^n \subset \bigcup_{\vv{x} \in \{0,1\}^n} \vv{x} + t\polytope{P}_{A}\right\}.
	\end{align*}
	See Figure \ref{fig:geometricview}. Hereditary discrepancy can be defined analogously\footnote{It might first appear as though the hereditary discrepancy is the scaling which covers all points of the form $\{0, 1/2, 1\}^n$ --- these correspond to the centers of all low dimensional faces of the unit hyper-cube --- but it is not so. Consider the row matrix $\mm{A} = [1,2]$. The maximum discrepancy is achieved on the subset $\mm{A}' = [2]$ so $\herdisc(\mm{A}) = 1$. However, for any $\vv{w} \in \{0,1/2,1\}^2$, there exists some $\vv{x} \in \{0,1\}^2$ such that $(w_1 - x_1) + 2(w_2 - x_2) \leq \frac{1}{2}$. To accurately represent $\herdisc(\mm{A})$ we must take care when dealing with the deleted columns. If column $i$ is removed, then $w_i \in \{0,1\}$ and the chosen colouring $\vv{x}$ must satisfy $x_i = w_i$. Thus the hereditary discrepancy is the scaling which covers the center of all low dimensional faces \emph{using polytopes placed at a vertex on the boundary of that face}.} 
	
	Various resources give more thorough treatments of these topics \cite{beck1996discrepancy, chazelle2001discrepancy, matouvsek1999geometric}.
	
	\subsection{Applications}
	Linear discrepancy, and combinatorial discrepancy more generally, has applications in rounding LP relaxations. Originally designed for discrepancy minimization in poly-time, the Lovett-Meka algorithm \cite{lovett2015constructive} has since been used by Rothvo{\ss} and Rothvo{\ss}-Hoberg to tighten the additive integrality gap for bin packing in one dimension \cite{hoberg2017logarithmic, rothvoss2013approximating}.\sn{I think
          we need less details about these applications, but we need
          more about prior results on hardness and (approximation)
          algorithms for discrepancy measures.} 
	
	Let $OPT$ be the optimum number of bins necessary for a bin-packing instance. Rothvo{\ss}' 2014 work produces a packing which uses $OPT + O(\log OPT \cdot \log\log OPT)$ bins. This is the first result in several decades which improves upon the $OPT + O(\log^2 OPT)$ bin solution of the Karmarkar-Karp algorithm. The crucial idea is to round the solution for the Gilmore-Gomory LP using the constructive partial colouring method.
	
	Hoberg and Rothvo{\ss}' more recent work improved upon this result. By applying a two stage packing technique in conjunction with the discrepancy results of Lovett-Meka, they obtained a poly-time algorithm which achieves an additive gap of $O(\log OPT)$, matching certain combinatorial lower bounds.
	
	Bansal and Khan also used a round-and-approximate approach to solve two dimensional bin packing with axis-aligned rectangular objects into unit square bins achieving a 1.405-approximation \cite{bansal2014improved}. This beats the factor 1.5 lower bound for a wide class of rounding algorithms.
	
	\subsection{Hardness Results}
	There are no know hardness results explicitly for linear discrepancy. However there does exist several results showing the hardness and hardness of approximation for discrepancy and hereditary discrepancy \cite{charikar2011tight, guruswami2004inapproximability}. 
	
	In the study of lattices, the covering radius problem appears to be quite similar to linear discrepancy. Let $\mm{B} \in \ZZ^{n \times n}$ be a basis consisting of $n$ linearly independent columns $\vv{b}_i$. The lattice of $\mm{B}$ is $\lattice_{\mm{B}} = \{\mm{B}\vv{x}:\vv{x} \in \ZZ^n\}$. Notice that a given lattice has many basis representations. The \gls{coveringradius} of $\lattice_{\mm{B}}$ in $\metric{p}$-norm is defined as  
	\begin{equation}
	\label{eq:coveringradius}
	\rho(\lattice_{\mm{B}}) = \max_{\vv{w} \in \RR^n}\min_{\vv{x} \in \ZZ^{n}}\norm{\mm{B} \cdot (\vv{w}-\vv{x})}_p
	= \max_{\vv{w} \in [0,1]^n}\min_{\vv{x} \in \ZZ^{n}}\norm{\mm{B} \cdot (\vv{w}-\vv{x})}_p
	\end{equation}
	and is independent of the basis. In the forgoing we will consider $\rho$ in $\metric{\infty}$-norm. Haviv and Regev showed that the covering radius problem ($\class{CRP}$) is $\Pi_2$-hard\cite{haviv2006hardness}.
	
	In this work we show the following hardness results for linear discrepancy. 
	\begin{theorem}
		\label{thm:hardness}
		Linear discrepancy is $\class{NP}$-hard and is contained in the class $\Pi_2$.
	\end{theorem}
	
	\subsection{Algorithmic Results}
	Suppose $\mm{A} \in \RR^{d \times n}$ for constant $d$ with fundamental parallelepiped $\polytope{P}_{A}$. By the geometric interpretation above, $\lindisc(\mm{A})$ is the smallest scaling of $\polytope{P}_{A} + \vv{x}$ for $\vv{x} \in \{0,1\}^{n}$ such their union covers the unit hypercube $\{0,1\}^{n}$. Apply $\mm{A}$ to $[0,1]^{n}$ to obtained the zonotope $\polytope{Z}_A \in \RR^{d}$. Apply $\mm{A}$ to each fundamental parallelepiped $\polytope{P}_A$ to obtain the $\metric{\infty}$ unit-ball centered at $\mm{A}\vv{x}$ for each $\vv{x} \in \{0,1\}^n$. Then $\lindisc(\mm{A})$ can be interpreted as the radius of the largest empty square with center in $\polytope{Z}_A$.
	
	If we had to find the largest such circle, instead of square, then this is identical to the largest empty circle (LEC) problem in computational geometry \cite{toussaint1983computing}. In the plane, it is possible to compute the largest empty circle contained in the convex hull of $n$ points in time $O(n\log n)$ by constructing the Voronoi diagram associated with these points. This corresponds to finding the solution to a variant of linear discrepancy 
	\[\lindisc_{2}(\mm{A}) = \max_{\vv{w} \in [0,1]^{n}}\min_{\vv{x} \in \{0,1\}^n} \norm{\mm{A}(\vv{w} - \vv{x})}_{2}\]
	in time $O(n2^{n})$ where $\mm{A} \in \RR^{2 \times n}$. Generally there exists algorithms, both randomized and deterministic, which compute the Voronoi diagram in $\metric{2}$ of $n$ points in $\RR^d$ with $O(n^{\ceil{d/2}})$ expected \cite{clarkson1989applications, seidel1991small} and $O(n\log n + n^{\ceil{d/2}})$ deterministic \cite{chazelle1993optimal} time\footnote{It is well known that computing the Voronoi diagram of $n$ points in $\RR^{d}$ for $\metric{2}$-norm can be interpreted as computing the convex hull of $n$ points in $\RR^{d+1}$ \cite{brown1979voronoi}. Thus all references here compute the convex hull of $n$ points in $\RR^{d}$.}. This can be transformed into algorithms which solve $\lindisc_{2}(\mm{A})$ for $\mm{A} \in \RR^{d \times n}$ in $O(2^{\ceil{nd/2}})$ expected and $O(n2^{n} + 2^{\ceil{nd/2}})$ deterministic time.
	
	To find the largest empty square, we can construct the Voronoi diagram in the $\metric{\infty}$-norm. For $n$ points in $\RR^{d}$, such randomized algorithms have expected running time $O(n^{\ceil{d/2}}\log^{d-1}n)$ \cite{DBLP:journals/dcg/BoissonnatSTY98}.\footnote{This is a side note: I actually do not feel comfortable saying that if there exists an algorithm for constructing a Voronoi diagram then there is also a polytime algorithm to solve the largest empty square problem, but that is what many Computational Geometers seem comfortable with (see \emph{Computing the Largest Empty Rectangle} of Chazelle, Drysdale, and Lee --- who cite an incorrect proof of Shamos ---- as well as the paper of Toussaint referenced above).}
	
	In this work we present algorithms for computing linear discrepancy exactly.
	\begin{theorem}
		\label{thm:exact-one-row}
		For matrix $\mm{A} \in \RR^{1 \times n}$, $\lindisc(\mm{A})$ can be computed in time $O(n\log n)$. 
	\end{theorem}
		
	\begin{theorem}
		\label{thm:approx-one-row}
		For matrix $\mm{A} \in \RR^{1 \times n}$ and any $\vv{w} \in [0,1]^n$, we can find an $\vv{x} \in \{0,1\}^{n}$ such that $\norm{\mm{A}(\vv{w} - \vv{x})}_{\infty} \leq \lindisc(\mm{A})$ in time $O(n\log n)$.
	\end{theorem}

	\begin{theorem}
		\label{thm:exact-const-row}
		For matrix $\mm{A} \in \RR^{d \times n}$ where $d$ is some fixed constant and $\max_{i,j}|A_{i,j}| \leq \delta$, $\lindisc(\mm{A})$ can be computed in time $O\left(d(n\delta)^{d^2}\right)$.  
	\end{theorem}

	We further present a linear discrepancy approximation algorithms.
	\begin{theorem}
		\label{thm:approx}
		For matrix $\mm{A} \in \RR^{d \times n}$, 
		\[\norm{\mm{A}}_{\infty \rightarrow \infty} \leq O(2^{n})\lindisc(\mm{A}) \leq O(2^{n}) \norm{\mm{A}}_{\infty \rightarrow \infty}\]
	\end{theorem}
	where $\norm{\mm{A}}_{p \rightarrow q}$ is the operator norms defined as
	\[\norm{\mm{A}}_{p \rightarrow q} = \max_{\vv{x}\in \RR^n} \frac{\norm{\mm{A}\vv{x}}_{q}}{\norm{\vv{x}}_{p}}.\]
	
	\subsection{Paper Structure}
	In section two we prove the hardness result for linear discrepancy by first reproving hardness results for discrepancy and hereditary discrepancy. In section three  we presents algorithms for Theorems \ref{thm:exact-const-row}, \ref{thm:approx-one-row}, \ref{thm:exact-const-row}, and \ref{thm:approx} in this order. The techniques used to prove Theorem \ref{thm:exact-one-row} and \ref{thm:approx-one-row} are independent of the techniques used to prove Theorem \ref{thm:exact-const-row} which are independent of the techniques used to prove Theorem \ref{thm:approx} so the subsection can be read in any order. In section four we cover many possible directions in which to continue this research.
	