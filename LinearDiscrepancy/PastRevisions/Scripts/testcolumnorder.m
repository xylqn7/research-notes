%% ===> [SETUP] Input Matrix A <===
clear;
gray = [0.5 0.5 0.5];
% A = [ 1,2,4,8,16,32;
%       32,16,8,4,2,1 ];
% A = [ 4,3,2,1,1,1;
%       1,1,1,1,2,3 ];
% A = [3, 0, 6, 1, 1, 3;
%      4, 4, 0, 1, -1, -4];
A = [9 1 6 6;
     1 9 6 -6];
A = [6 6 9 1;
     6 -6 1 9];

Apos = A .* (A>0);
Aneg = A .* (A<0);
hSumPos = sum(Apos, 2);
hSumNeg = sum(Aneg, 2);
xMaxSum = hSumPos(1);
yMaxSum = hSumPos(2);
xMinSum = hSumNeg(1);
yMinSum = hSumNeg(2);
nA = size(A, 2);
nP = nA - 2;

% A = permutecolumns(A);
% p = randperm(nA);
% A = A(:, p);

clf;
[oldL, oldC, r] = visualizelattice(A(:, 1:2));
disp(A(:, 1:2));
fprintf("lindisc_2(A) = %.4f\n---\n", r);

for iA = 3:nA
    %---> Generate Data <---
    Anew = A(:, 1:iA);
    [newL, newC, r] = visualizelattice(Anew);
    
    %---> Display Input and Lindisc_2 <---
    disp(Anew');
    fprintf("lindisc_2(A) = %.4f\n---\n", r);
    
    % ---> Plot Voronoi Diagram & Max Circum-centers <---
    subplot(ceil(nP/2), 2, iA-2);
    
    voronoi(oldL(:,1), oldL(:,2), 'c-'); hold on;
    oldHullPts  = convhull(oldL);
    oldHull     = oldL(oldHullPts, :);
    plot(oldHull(:, 1), oldHull(:,2), 'Color', gray); hold on;
    plot(oldC(:,1), oldC(:,2), 'm.', 'MarkerSize', 20); hold on;
    
    shiftL = oldL + A(:, iA)';
    shiftC = oldC + A(:, iA)';
    voronoi(shiftL(:,1), shiftL(:,2), 'c-'); hold on;
    shiftHull = oldHull + A(:, iA)';
    plot(shiftHull(:, 1), shiftHull(:,2), 'Color', gray); hold on;
    plot(shiftC(:,1), shiftC(:,2), 'm.', 'MarkerSize', 20); hold on;
    
    voronoi(newL(:,1), newL(:,2), 'b-'); hold on;
    newHullPts  = convhull(newL);
    newHull     = newL(newHullPts,:);
    plot(newHull(:,1), newHull(:,2), 'k-'); hold on;
    plot(newC(:,1), newC(:,2), 'r.', 'MarkerSize', 20);
    axis([xMinSum xMaxSum yMinSum yMaxSum]);
    
    %---> Update Data <---
    oldL = newL;
    oldC = newC;
end

function newA = permutecolumns(A)
    nCol = size(A, 2);
    newA = A;
    detA = 0;
    
    % ---> Find Initial Pair of Columns <---
    index1 = 1;
    index2 = 2;
    for iCol = 1:nCol
        for jCol = (iCol+1):nCol
            hTwoCol = [A(:,iCol), A(:,jCol)];
            hDet    = abs(det(hTwoCol));
            if hDet > detA
                index1  = iCol;
                index2  = jCol;
                detA    = hDet; 
            end
        end
    end
    newA(:, [1 index1]) = newA(:, [index1 1]);
    newA(:, [2 index2]) = newA(:, [index2 2]);
    
    % ---> Find Best Vector to Add <---
    for iCol = 3:(nCol-1)
        [index, detPart] = findnextcolumn((iCol-1), newA);
        detA = detA + detPart;
        newA(:, [iCol index]) = newA(:, [index iCol]);
    end
end

function [outIndex, outDet] = findnextcolumn(nColPart, A)
    nColA = size(A, 2);
    nColRem = nColA - nColPart;
    
    if nColRem > 0
        outIndex    = -1;
        outDet      = -1; 
        for iColRem = (nColPart+1):nColA
            hDetSum = 0;
            hCurrentCol = A(:, iColRem);
            for jColPart = 1:nColPart
                hTwoCol = [hCurrentCol, A(:, jColPart)];
                hDet    = abs(det(hTwoCol));
                hDetSum = hDetSum + hDet;
            end
            if hDetSum > outDet
                outIndex = iColRem;
                outDet = hDetSum;
            end
        end
    end
end