A = [   1 -0.5    0 -0.5;
     -0.5    1 -0.5 -0.5;
        0 -0.5    1 -0.5;
     -0.5 -0.5 -0.5    1];
 b = ones(4,1) * 0.5;
 b(4) = 0;
 disp(b);
 
 disp(A\b);