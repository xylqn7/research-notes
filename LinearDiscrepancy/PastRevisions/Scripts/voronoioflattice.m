%% ===> [INPUT] Two Row Vectors <===
% Coding convension for ease of workspace access:
% [Prefix Chart]
% a     - input
% g     - global variables
% h     - helper variables
% i     - indices
% mask  - logical vectors used as masks
% x, y  - column vectors used for graphing
clear;
% A = [ 1,2,4,8,16,32;
%       32,16,8,4,2,1 ];
aMatrix       = [ 1,2,4,8,16;
                16,8,4,2,1 ];
nA      = size(aMatrix, 2);


%% ===> [SETUP] Vertices of Zonotope <===
% ---> General Global Variables <---
gError  = 0.001;    % Used to compare doubles to zero

% ---> All 0-1 Combinations of Columns of V <---
nPowerA     = 2^nA;                         % # vertices of zonotope
hSubsetAll  = dec2bin(0:nPowerA-1) - '0';   % A row for binary digits of each number
hSubsetAll  = hSubsetAll';                  % A col is '''
gLattice    = aMatrix * hSubsetAll;         % All lattice coordinates
xLattice    = gLattice(1,:)';               % Column of all x-coordinates
yLattice    = gLattice(2,:)';               % Column of all y-coordinates

% ---> Convex Hull & Coordinates of Voronoi Intersections <---
gTriangle   = delaunayTriangulation(xLattice,yLattice);
gHull       = convexHull(gTriangle);
gZonotope   = polyshape(gLattice(:,gHull)');
[gVoronoi,gVoronoiCell] = voronoin(gLattice');
nVoronoi    = size(gVoronoi,1);
nCell       = size(gVoronoiCell,1);

%% ===> [CALC] Find Maximum Circm-radius <===
% ---> Find Intersections outside Zonotope <---
% 'gVoronoi' has all Voronoi intersections along with (inf, inf)
% in row 1. Zero out this row so we can do arithmetics properly.
% Check to see which points are outside the convex hull using 
% 'maskOutsideHull'. 'gCenterExt' has non-zero rows when they
% are outside the hull. 'gCenterAll' contains all centers inside
% the hull.
gCenterAll      = gVoronoi;
gCenterAll(1,:) = [0,0];
maskOutsideHull = ~isinterior(gZonotope, gCenterAll);
gCenterExt      = gCenterAll .* maskOutsideHull;
gCenterAll      = removezerorows(gCenterAll - gCenterExt);

% ---> Setup Edges Incident External Circum-centers <---
% This was pretty complicated since the values returned by the 
% built-in Voronoi function does not output the Voronoi edges.
% Instead we have 'gVoronoiCell' which are the Voronoi cells of
% the lattice points given by a vector of Voronoi centers on 
% its cell boundary. 
%
% 'gEdge' is a vector with a Cell corresponding to each Voronoi
% center. We add all adjacent centers to its entry in 'gEdge'.
%
% Iterate through Voronoi cells which DO NOT contain (inf, inf) 
% on its boundary. Check if the center on the bd is outside the hull 
% (non-zero entries in 'gCenterExt'). If it is then we add the 
% two adjacent centers to its entry in 'gEdge' if they are not 
% already there.
gEdge = cell(nVoronoi, 1);
for iVoronoi = 1:nCell
    hEdge = gVoronoiCell{iVoronoi};
    if ~ismember(hEdge,1)
        nEdge = length(hEdge);
        for jEdge = 1:nEdge
            hVertexAdj = hEdge(jEdge);
            if abs(gCenterExt(hVertexAdj,1)) >= gError
                iLeft = calculateindexmod(jEdge+1, nEdge);
                iRight = calculateindexmod(jEdge-1, nEdge);
                hLeft = hEdge(iLeft);
                hRight = hEdge(iRight);
                if ~any(ismember(gEdge{hVertexAdj}, hLeft))
                    gEdge{hVertexAdj} = [gEdge{hVertexAdj}, hLeft];
                end
                if ~any(ismember(gEdge{hVertexAdj}, hRight))
                    gEdge{hVertexAdj} = [gEdge{hVertexAdj}, hRight];
                end
            end
        end
    end
end

% ---> Calculate Intersection of Edges with Boundary <---
% This is another step that required more work due to my unfamilarity
% with the software. We try to find the intersect between the convex
% hull of the zonotope with the Voronoi diagram. Iterate through the
% set of edges stored in 'gEdge'. If they intersect the convex hull
% add this point of intersection to 'gBoundaryInt'.
gBoundaryInt    = zeros(nVoronoi, 2);
hCount          = 1;
for iVoronoi = 2:nVoronoi
    if abs(gCenterExt(iVoronoi,1)) >= gError
        hEndPtThis  = gVoronoi(iVoronoi, :);
        hVertexAdj  = gEdge{iVoronoi};
        nEdges      = length(hVertexAdj);
        for jEdge = 1:nEdges
            hEndPtOther  = gVoronoi(hVertexAdj(jEdge), :);
            hEdge        = [hEndPtThis; hEndPtOther];
            [hSegmentIn, hSegmentOut] = intersect(gZonotope, hEdge);
            if ~isempty(hSegmentOut) && ~isempty(hSegmentIn)
                gBoundaryInt(hCount,:) = intersect(hSegmentIn, hSegmentOut);
                hCount = hCount + 1;
            end
        end
    end
end
gBoundaryInt     = removezerorows(gBoundaryInt);
gCenterAll       = [gCenterAll; gBoundaryInt];

% ---> Identify Circum-centers with Largest Radius <---
[gNeighborNear, gRadiusAll]  = nearestNeighbor(gTriangle, gCenterAll);
gRadiusMax  = max(gRadiusAll);
maskRadius  = gRadiusAll >= (gRadiusMax-gError);
gCenterMax  = gCenterAll .* maskRadius;
gCenterMax  = removezerorows(gCenterMax);
xCenterMax = gCenterMax(:, 1);
yCenterMax = gCenterMax(:, 2);

%% ===> [PLOT] Voronoi Diagram & Max Circum-centers <===
clf;
plot(xLattice(gHull), yLattice(gHull), 'k-'); hold on;
voronoi(xLattice, yLattice, 'b-'); hold on;
plot(xCenterMax, yCenterMax, 'r.', 'MarkerSize', 20);

%% ===> [HELPER FUNCTIONS] <===
% ---> [FUNCTION] removezerorows <---
% INPUT: Z \in \RR^{m x 2}
% OUTPUT: M subset of the rows of Z after removing all zero rows
function M = removezerorows(Z)
    xz = nonzeros(Z(:,1));
    yz = nonzeros(Z(:,2));
    M = [xz, yz];
end

% ---> [FUNCTION] calculateindexmod <---
% INPUT:
%           index   - in [0, m+1] 
%           m       - modulos
% OUTPUT:   i       - the proper index in [1, m] 
function i = calculateindexmod(index, m)
    if index == 0
        i = m;
    elseif index > m
        i = 1;
    else
        i = index;
    end
end