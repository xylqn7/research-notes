% !TEX root = MAIN-lindisc.tex

\section{Calculating Linear Discrepancy}
    In the following we consider restrictions and variants of linear discrepancy. The first subsection considers matrices with a single row. The second subsection considers matrices $\mm{A} \in \ZZ^{d \times n}$ with constant $d$ and entry of largest magnitude $\delta$, we compute $\lindisc(\mm{A})$ in time $O\left(d(2n\delta)^{d^2}\right)$. The third subsection presents a poly-time $2^n$ approximation to $\lindisc(\mm{A})$ for $\mm{A} \in \RR^{m \times n}$.
	\subsection{Linear Discrepancy of a Row Matrix} 	
	\label{sssection:onerow}
	We begin by developing some intuition for the linear discrepancy of a one-row matrix, $\mm{A} = [a_1, ..., a_n]$. Make the simplifying assumption that the entries of $\mm{A}$ are non-negative and sorted in decreasing order. Define the \emph{subset sums} of $\mm{A}$ to be the multi-set $\family{S}(\mm{A}) = \{s_1, ..., s_{2^n}\}$ where each $s_i = \mm{A}\vv{x}$ for exactly one $\vv{x} \in \{0,1\}^n$. Enumerate the element of $\family{S}(\mm{A})$ in non-decreasing order, i.e. $s_i \leq s_{i+1}$. If $\ell_{A} = 2\cdot\lindisc(\mm{A})$, then $\ell_A$ is the width of the largest gap between consecutive entries in $\family{S}(\mm{A})$.
	
	Suppose $\mm{A}_i = [a_1, ..., a_i]$. Consider what happens for the first couple of values of $i$. $\family{S}(\mm{A}_1) = [0, a_1]$ and $\lindisc(\mm{A}_1) = \frac{a_1}{2}$. $\family{S}(\mm{A}_2)$ contains the contents of $\family{S}(\mm{A}_1)$ and the contents of $\family{S}(\mm{A}_1)$ shifted to the right by $a_2$. Since $a_1 \geq a_2$, $\family{S}(\mm{A}_2) = [0, a_2, a_1, a_1 + a_2]$ where the largest gap is of size $\max(a_2, a_1 - a_2)$. See Figure \ref{fig:onerowalgexp}. In general, the entries of $\family{S}(\mm{A}_i)$ consists of two copies of $\family{S}(\mm{A}_{i-1})$ with one shifted to the right by $a_i$. The gaps in $\family{S}(\mm{A}_i)$ are gaps previously in $\family{S}(\mm{A}_{i-1})$ or between an element of $\family{S}(\mm{A}_{i-1})$ and one in $\{a_i + s: s \in \family{S}(\mm{A}_{i-1})\}$. 
	
	\fig{onerowalgexp}{1}{Obtaining $\family{S}(\mm{A}_2)$ from $\family{S}(\mm{A}_1)$ when $a_1 \geq a_2$.}
	
	A similar structure occurs for general matrices with real valued entries with two special caveats: (1) the previous interval is shifted left or right depending on the sign of the current entry (negative and positive respectively) and (2) the smallest entry of $\family{S}(\mm{A})$ is not zero but the sum of the negative entries in $\mm{A}$.
	
	\begin{lemma}
		\label{lem:onerowsortinghelps}
		Suppose $\mm{A}_{k-1} = [a_1, ..., a_{k-1}]$ with entries in $\RR$ and $|a_i| \geq |a_{i+1}|$. Let the largest gap in $\family{S}(\mm{A}_{k-1})$ be of size $\ell_{k-1}$. Then, for $\mm{A}_{k} = [a_1, ..., a_{k-1}, a_{k}]$ where $|a_{k}| \leq |a_{i}|$ for all $i \in [k-1]$, the largest gap in $\family{S}(\mm{A}_k)$ is of size $\max(|a_k|, \ell_{k-1} - |a_k|)$.
	\end{lemma}
	\begin{proof}
		Again, it is important to remember that the entries of $\family{S}(\mm{A}_{k})$ are exactly those in $\family{S}(\mm{A}_{k-1})$ along with those in $\{a_k + s: s \in \family{S}(\mm{A}_{k-1})\}$. Let $\ell = \max(|a_k|, \ell_{k-1} - |a_k|)$.
		
		We first show that $2\cdot\lindisc(\mm{A}_{k}) \leq \ell$ by showing that gaps between consecutive entries in $\family{S}(\mm{A}_{k})$ have size at most $\ell$. Each consecutive pair $(s_{j}, s_{j+1})$ in $\family{S}(\mm{A}_{k-1})$ of size greater than $\ell$ are no longer consecutive in $\family{S}(\mm{A}_{k})$ since $s_{j} \leq s_{j} + a_k \leq s_{j+1}$ if $a_k > 0$ and $s_{j} \leq s_{j+1} + a_k \leq s_{j+1}$ if $a_k < 0$. See Figure \ref{fig:polytimealgrowmatrixlemma1}. Since $s_{j+1} - s_{j} \leq \ell_{k-1}$, the size of each gap in $\family{S}(\mm{A}_k)$ is at most $\ell$.

		\fig{polytimealgrowmatrixlemma1}{1}{All consecutive pairs in $\family{S}(\mm{A}_{k-1})$ of size greater than $|a_k|$ will be divided into two or more consecutive pairs in $\family{S}(\mm{A}_k)$. The red interval indicates what happens when $a_k > 0$. The blue interval indicates what happens when $a_k < 0$.} 
		
		Next we will show that $2\cdot\lindisc(\mm{A}_{k}) \geq \ell$ by producing a pair of consecutive entries in $\family{S}(\mm{A}_{k})$ which achieves gap $\ell$. Suppose $\ell = |a_{k}|$. Recall that $s_0$ is the smallest subset sum of all entries in $\mm{A}_{k}$. The gap between $s_0$ and $s_1$ must be at least the magnitude of some entry in $\mm{A}_k$. Since $|a_{i}| \geq |a_{k}|$ for every $i \in [k-1]$, $(s_0, s_0 + |a_{k}|)$ is a consecutive pair in $\family{S}(\mm{A}_k)$.  
		
		When $\ell = \ell_{k-1} - |a_k| > |a_k|$, we split our analysis into two cases: (1) $a_k > 0$ and (2) $a_k < 0$. 
		
		In the former case, let $(s_{j^*}, s_{j^*+1})$ be a consecutive pair in $\family{S}(\mm{A}_{k-1})$ that achieves gap $\ell_{k-1}$ and suppose for a contradiction that $s_{j^*} + a_k$ and $s_{j^*+1}$ do not appear consecutively in $\family{S}(\mm{A}_k)$. Then there must be some $s \in \family{S}(\mm{A}_k)$ such that $s_{j^*} + a_k < s < s_{j^*+1}$. Note that $s$ cannot be an element of $\family{S}(\mm{A}_{k-1})$ since $s_{j^*}$ and $s_{j^*+1}$ are consecutive in $\family{S}(\mm{A}_{k-1})$, so $s - a_k$ must be an element of $\family{S}(\mm{A}_{k-1})$. However since $s > s_{j^*} + a_k$, we have $s - a_k > s_{j^*}$. This is a contradiction since $s_{j^*}$ and $s_{j^*+1}$ are consecutive entries in $\family{S}(\mm{A}_{k-1})$. See Figure \ref{fig:polytimealgrowmatrix}. Thus $(s_{j^*} + a_k, s_{j^*+1})$ must be a consecutive pair in $\family{S}(\mm{A}_k)$.
		
		\fig{polytimealgrowmatrix}{0.6}{Suppose $a_k < \ell_{k-1} - a_k$ and there exists $s \in \family{S}(\mm{A}_{k})$ such that $s_{j^*} + a_k < s < s_{j^*+1}$.}
		
		The later case of $a_k < 0$, is similar. Again there exists a pair of consecutive entries $(s_{j^*}, s_{j^*+1})$ in $\family{S}(\mm{A}_{k-1})$ which achieves gap $\ell_{k-1}$. Suppose for a contradiction that $s_j$ and $s_{j^*+1} - |a_k|$ do not appear consecutively in $\family{S}(\mm{A}_k)$. Then there must be some $s \in \family{S}(\mm{A}_k)$ such that $s_{j^*} < s < s_{j^*+1} - |a_k|$. Again, $s$ cannot be an element of $\family{S}(\mm{A}_{k-1})$ since $s_{j^*}$ and $s_{j^*+1}$ are consecutive in $\family{S}(\mm{A}_{k-1})$, so $s + |a_k|$ must be an element of $\family{S}(\mm{A}_{k-1})$. However since $s < s_{j^*+1} - |a_k|$, we have $s + |a_k| < s_{j^* + 1}$. This is a contradiction since $s_{j^*}$ and $s_{j^*+1}$ are consecutive entries in $\family{S}(\mm{A}_{k-1})$.
	\end{proof}

	\begin{corollary}
		\label{cor:onerowtakemagnitudes}
		Let $\mm{A} = [a_1, ..., a_n]$ and $\mm{A}' = [|a_1|, ..., |a_n|]$. Then $\lindisc(\mm{A}) = \lindisc(\mm{A}')$.
	\end{corollary}
	
	Lemma \ref{lem:onerowsortinghelps} and Corollary \ref{cor:onerowtakemagnitudes} suggest an algorithm: replace the entries of $\mm{A}$ by their magnitudes. Sort $\mm{A}$. Consider each entry in turn and update the largest gap accordingly. See Algorithm \ref{alg:lindisc}.
	
	\begin{proof}[Proof of Theorem \ref{thm:exact-one-row}]
		By Corollary $\ref{cor:onerowtakemagnitudes}$ it is sufficient to consider row matrices with non-negative entries. Suppose that $\mm{A} = [a_1, ..., a_n]$ is such a matrix with entries sorted in decreasing order. Algorithm \ref{alg:lindisc} correctly outputs the linear discrepancy for matrices with a single entry. Let $\mm{A}_i = [a_1, ..., a_i]$. Lemma \ref{lem:onerowsortinghelps} gives us a recursive method for computing largest gap in $\family{S}(\mm{A}_{i+1})$ from the largest gap in $\family{S}(\mm{A}_i)$. Since $\lindisc(\mm{A})$ is half the size of the largest gap in $\family{S}(\mm{A})$, Algorithm \ref{lem:onerowsortinghelps} output $\lindisc(\mm{A})$ as required.
	\end{proof} 
	
	\begin{algorithm}
		\DontPrintSemicolon % Some LaTeX compilers require you to use \dontprintsemicolon instead 
		\KwIn{Matrix $\mm{A} \in \RR^{m \times n}$.}
		\KwOut{$\lindisc(\mm{A})$.}
		\For{$i$ from $1$ to $n$}{
			$\mm{A}[i] \leftarrow |a_i|$\;
		}
		sort $\mm{A}$ in decreasing order\;
		$\ell \leftarrow a_1$\;
		\For{$i$ from $2$ to $n$}{
			$\ell \leftarrow \max(a_i, \ell - a_i)$\;
		}
		\Return{$\frac{\ell}{2}$}\;
		\caption{Linear discrepancy of row matrix.}
		\label{alg:lindisc}
	\end{algorithm}
	
	Thus, for any row matrix $\mm{A}$ with $n$ elements, we can find $\lindisc(\mm{A})$ in time $O(n \log n)$.  
	
	\begin{corollary}
		If $\mm{A}$ is an arithmetic progression of the form $[a_0, a_0 + k, \cdots, a_{0} + (n-1)k]$, then 
		\[\lindisc(A) = \frac{\max(a_0, k)}{2}.\]
		
		Similarly, if $\mm{A}$ is a geometric progression of the form $[a_0r^{0}, \cdots, a_{0}r^{n-1}]$, then 
		\[\lindisc(A) = \begin{cases}
		\frac{a_0}{2} &\mbox{if } r \leq 1,\\
		a_0\left(r^{n-1} - \frac{\left(r^{(n-1)}-1\right)}{(r-1)}\right) &\mbox{otherwise}.
		\end{cases}\]
	\end{corollary}
	
	\subsubsection{One Row Linear Discrepancy Rounding}	
	Let $\lindisc(\mm{A}) = \ell$. By the definition of linear discrepancy, for every $\vv{w} \in [0,1]^{n}$ there exists an $\vv{x} \in \{0,1\}^{n}$ such that $\norm{\mm{A}(\vv{w} - \vv{x})}_{\infty} \leq \ell$. In-fact, if $\vv{w}$ is not a deep-hole, there exists an $\vv{x}$ which satisfies $\norm{\mm{A}(\vv{w} - \vv{x})}_{\infty} < \ell$. However it is not obvious that finding such an $\vv{x}$ can be done efficiently i.e. in polynomial time with respect to the bit complexity of $\mm{A}$ and $n$. By reducing from the subset-sum problem, we observe that it is difficult to compute $\lindisc(\mm{A}, \vv{w})$ let alone find an $\mm{x}$ which minimizes $\norm{\mm{A}(\vv{w} - \vv{x})}_{\infty} \leq \ell$.
	
	\begin{proof}[Proof of Theorem \ref{thm:approx-one-row}]
		To begin, let $\mm{A} = [a_1, ..., a_n]$ for positive $a_i$ in non-increasing order. We will consider $\mm{A} \in \RR^{n}$ at the end. Let $w = \mm{A}\vv{w}$. As before, let $\mm{S} = [s_{0}, ..., s_{2^{n}-1}]$ be the subset-sums of $\mm{A}$ where each $s_i = \mm{A}\vv{x}$ for an $\vv{x} \in \{0,1\}^n$ and $s_i \leq s_{i+1}$ for all $i$. Recall that $2\cdot\lindisc(\mm{A})$ is the largest gap between any two consecutive entries in $\mm{S}$. Our algorithm will find a pair of lattice points containing $w$. If we can show that the size of this interval is no more than the gap between some two consecutive entries in $\mm{S}$, then closest lattice point to $w$ among these two will be within $\lindisc(\mm{A})$ of $w$.
		
		Just as in Algorithm \ref{alg:lindisc}, we refine the interval between two lattice points containing $w$ by incrementally adding the entries of $\mm{A}$ in decreasing order. Initially our interval is $g_0 = [0, \sum_{i = 1}^{n} a_i]$. We maintain the invariants: (1) $w \in g_i$ for all $i$, and (2) the end-points of $g_i$ are lattice points. 
		
		Suppose $w \in g_i = [u, v]$ and we are considering $a_i$. If $u + a_{i} > w$ then set $v \leftarrow \min(v, u + a_i)$. Otherwise let $u \leftarrow u + a_i$. Algorithm \ref{alg:lindiscvariant} computes this interval and the associated vectors $\vv{u}$ and $\vv{v}$ representing its endpoints.
		
		Consider the values of $u$ and $v$ at the end of the algorithm. We claim that the final interval $[u,v]$ is at most the width of some gap between two consecutive terms in $\mm{S}$, the array of all subset sums of $\mm{A}$. Notice $u = a_1u_1 + \cdots + a_nu_n$ where $\vv{u} = [u_1, ..., u_n]$ is an endpoint of the interval once Algorithm \ref{alg:lindiscvariant} complete.  
		
		We partition $\vv{u}$ into maximal blocks where all entries in the same block have the same value i.e. $[u_1, u_2, ..., u_{\ell_1}], ..., [u_{\ell_r + 1}, u_{\ell_r+2}, ..., u_{n}]$ such that $u_{\ell_i + 1} = u_{\ell_i + 2} = \cdots = u_{\ell_{i+1}}$ for $i = 0, 1, ..., r-1$ where $\ell_0 = 0$.
		
		We claim that Algorithm \ref{alg:lindiscvariant} outputs an interval containing $w$ whose width is at most the distance between some two consecutive entries in $\mm{S}$, the subset sums of $\mm{A}$. The proof is by induction on $r$, the number of blocks. In the base case, $r = 1$ and there is only one block. Thus $u = 0$ or $u = \sum a_i$. In the case where $u = 0$, we must have $a_i > w$ for all $i \in [n]$. Thus $w \in [0, a_n]$ with consecutive elements $0$ and $a_n$ of $\mm{S}$. In the latter case we can output $\vv{w}$ since it is already a lattice point.
		
		Suppose the claim holds for all matrices where the algorithm outputs a vector $\vv{u}$ with $k$ blocks. Show that the algorithm works for a matrix $\mm{A}$ whose output $\vv{u}$ has $k+1$ blocks. Let $\vv{u}' = [u_1, ..., u_{\ell_{k+1}}]$ and $\vv{v}' = [v_1, ..., v_{\ell_{k+1}}]$ be the final vectors after running the algorithm on $\mm{A}' = [a_1, ..., a_{\ell{k+1}}]$. Further let $u' = \sum_{i = 1}^{\ell_{k+1}} a_iu_i$ and $v' = \sum_{i = 1}^{\ell_{k+1}} a_iv_i$. By the induction hypothesis, the width of $[u', v']$ is at most the distance between some two consecutive elements in the list of subset sums of $\mm{A}'$. The last block of $\vv{u}$ is $[u_{\ell_{k+1} + 1}, ..., u_{n}]$. The entries of this block are either all zeros or all ones. Consider each case in-turn. 
		
		First suppose $u_{\ell_{k+1}+1} = \cdots = u_{n} = 0$. Since none of the $a_i$ for $i = \ell_{k+1} + 1, ..., n$ were added to $u$, it must be the case that $u' + a_i > w$ for all such $i$. Thus the interval $[u, v] = [u', \min\left(v', u' + a_n\right)]$ has width at most $a_n$. Since $0$ and $a_n$ are consecutive in $\mm{S}$, as $|a_n|$ is the entry with the smallest magnitude in $\mm{A}$, the output interval satisfies our requirements.  
		
		Next suppose $u_{\ell_{k+1}+1} = \cdots = u_{n} = 1$. It must be the case that $u = u' + a_{\ell_{k+1} + 1} + \cdots + a_n \leq w$. We show that $u$ and $v'$ are consecutive in $\mm{S}$. Observe that $a_{\ell_{k+1}}$ is in the $k$\textsuperscript{th} block and so $u_{\ell_{k+1}} = 0$. Let $[u'', v'']$ be our interval after processing the $k-1$\textsuperscript{st} block i.e. $u'' = \sum_{i = 1}^{\ell_k}a_iu_i$ and $v'' = \sum_{i = 1}^{\ell_k} a_iv_i$. Notice that since none of the entries in the $k$\textsuperscript{th} block were added to $u''$, we must have $u'' + a_{i} > w$ for all $i = \ell_{k} + 1, ..., \ell_{k+1}$. In such cases, we always update $v'' \leftarrow \min(v'', u'' + a_{i})$ after each such $i$, thus the interval $[u', v']$ has width at most $a_{\ell_{k+1}}$. Thus it suffices to show that $a_{\ell_{k+1} + 1} + \cdots + a_n$ and $a_{\ell_{k+1}}$ are consecutive in $\mm{S}$. This is true since $a_i > a_{\ell_{k+1}}$ for all $i < \ell_{k+1}$. 
		
		Now consider the case where $\mm{A} \in \RR^{n}$. Without loss of generality we can assume that none of the entries are zero. Let $A_{-} = \{a_i \in \mm{A}: a_i < 0\}$ and $\mm{A}_{+} = \{a_i \in \mm{A}: a_i > 0\}$. It suffices to set $u_0 = \sum_{a \in \mm{A}_{-}}a$ and $v_0 \sum_{a \in \mm{A}_{+}} a$ and let $\vv{u}$ and $\vv{v}$ be the indicator vectors of $\mm{A}_{-}$ and $\mm{A}_{+}$ respectively. The remainder of the algorithm is identical except that the matrix should be sorted in decreasing order of \emph{magnitude} and every time an element $a_i \in \mm{A}_{-}$ is added to $u$, it's entry in $\vv{u}$ should be set to zero. 
	\end{proof} 
	
	\begin{algorithm}
		\DontPrintSemicolon % Some LaTeX compilers require you to use \dontprintsemicolon instead 
		\KwIn{A vector $\vv{w} \in [0,1]^n$ and a row matrix $\mm{A} = [a_1, ..., a_n]$ of positive integers sorted in increasing order.}
		\KwOut{A vector $\vv{x} \in \{0,1\}^{n}$ such that $\norm{\mm{A}(\vv{w} - \vv{x})}_{\infty} \leq \lindisc(\mm{A})$.}
		$\mm{A} \leftarrow \op{sort-decreasing}(\mm{A})$\;
		$\vv{u} \leftarrow \op{zeros}(n)$\;
		$\vv{v} \leftarrow \op{ones}(n)$\;
		$w \leftarrow \mm{A}\vv{w}$, $u \leftarrow \mm{A}\vv{u}$, $v \leftarrow \mm{A}\vv{v}$\;
		\Return $\vv{v}$ if $w == v$\;
		\For{$k = 1..n$}{
			\If{$u + a_k > w$}{
				$v \leftarrow \min\left(t, u + a_k\right)$\;
				\If{$v == u + a_k$}{
					$\vv{v} \leftarrow \op{copy}(\vv{u})$\;
					$\vv{v}[k] \leftarrow 1$\;
				}
			} \Else{
				$u \leftarrow u + a_{k}$\;
				$\vv{u}[k] \leftarrow 1$\;
			}
		}
		\Return{$\vv{u}$ if $u$ is closer to $w$ else $\vv{v}$}\;
		\caption{Finding a close lattice point to $\mm{A}\vv{w}$.}
		\label{alg:lindiscvariant}
	\end{algorithm}

	\subsection{Constant Rows with Bounded Matrix Entries}
	Let $\mm{A} \in \ZZ^{d \times n}$ with $\max_{i,j} |A_{i,j}| \leq \delta$. Let $Z = \mm{A}[0,1]^{d}$ be the zonotope of $\mm{A}$ and let $T = [-n\delta, n\delta]^{d} \cap \ZZ^{d}$ be the set of all integer lattice points of $Z$. The following algorithm computes $\lindisc(\mm{A})$ in polynomial time with respect to $n$ for fixed $d$ and $\delta$. 

	\begin{proof}[Proof of Theorem \ref{thm:exact-const-row}]
		
	For every one of the $(2n\delta + 1)^{d}$ integral points $\vv{b} \in T$, compute the feasibility of $\mm{A}\vv{x} = \vv{b}$ using dynamic programming. This procedure generalizes dynamic programming algorithms for knapsack and subset sum and will be outlined in the following. Let $\vv{a}_1, ..., \vv{a}_n$ be the columns of $\mm{A}$. Construct a matrix $\mm{M}$ with dimensions $[-n\delta, n\delta]^{d} \times n$. Cell $(\vv{v}, i)$ of $\mm{M}$ contains the indicator $[\mm{M}(\vv{v}-\vv{a}_i, i-1) \lor \mm{M}(\vv{v},i-1)]$; this corresponds to a linear combination of the first $i-1$ columns of $\mm{A}$ which adds up to $\vv{v}-\vv{a}_i$ or a linear combination of the first $i-1$ columns which adds up to $\vv{v}$. The first column of $\mm{M}$ is the indicator vector for $\vv{a}_1$. Computing the entries of $\mm{M}$ takes time $O(2n\delta)^{d+1}$. $\mm{M}(\vv{b},n)$ indicates the feasibility of $\mm{A}\vv{x} = \vv{b}$. Computing this for all $\vv{b}$ takes time $O(2n\delta)^{2d}$. Let $S \subseteq T$ be the set of lattice points in $Z$ and set $|S| = N$.
	
	Apply Lemma \ref{lem:lec-in-higher-dimensions} to the points of $S$ in $\metric{\infty}$-norm. The output is some radius $r$ and point $\vv{x}^*$ such that the circle centered at $\vv{x}^{*}$ with radius $r$ is the largest circle with center inside the convex hull of $S$ not containing any points of $S$. Note that $r$ is in-fact the linear discrepancy of $\mm{A}$. Since $r$ and $\vv{x}^*$ can be computed in time $O(N^d)$, $\lindisc(\mm{A})$ can be computed in time $O(2n\delta)^{2d^2}$. 
	\end{proof}
	
	\begin{comment}
		\textbf{Compute the Voronoi Diagram of $S$.} This can be done in $O\left(N\log N + N^{\floor{d/2}}\right)$ time for $\metric{2}$-norm \cite{de1997computational}. 
	
	We require the following computational geometry terminology. Let $I$ be a set of points. Let $\ch(I)$ denote the convex hull of $I$. For each $\vv{v}_i \in I$, let $V_i$ be the Voronoi cell of $\vv{v}_i$ i.e. $V_i = \{\vv{x} \in \RR^{d}: \norm{\vv{x} - \vv{v}_i}_2 \leq \norm{\vv{x} - \vv{v}_j}\mbox{ for all }j \in I\}$. For any $J \subset I$, let $N(J) = \cap_{j \in J} V_j$ denote the Voronoi intersection of $J$. When the set of Voronoi cells do not intersect, $N(J) = \emptyset$. The Voronoi diagram of $I$, denoted $V(I)$, is the set of all non-empty $N(J)$ for every subset $J$ of $I$\footnote{Around here there is subset sum statement that needs to be referenced or proved.}. When $\dim(N(J)) \geq 1$ (resp. $N(J) \neq \emptyset$ and $\dim(N(J)) = 0$), $N(J)$ is the \emph{Voronoi faces} of $J$. Intuitively, $N(J)$ denotes the portion of $V(I)$ consisting of all points in $\RR^d$ which are equidistant to every point of $J$.
	
	Let $\mathcal{I}$ be the subsets of $S$ which define Voronoi intersection points i.e. $I \in \mathcal{I}$ are sets of vertices such that $N(I) \neq \emptyset$ and $\dim(N(I)) = 0$. We will use $\vv{n}_I$ to denote the unique point in $N(I)$. 
	
	Note that the combinatorial complexity of a Voronoi diagram in $\RR^{d}$ is $\Theta\left(N^{\ceil{d/2}}\right)$ so $|\mathcal{I}| \in O\left(N^{\ceil{d/2}}\right)$ \cite{DBLP:journals/dcg/BoissonnatSTY98}. 
	
	\textbf{Compute deep hole candidates.}
	We generate candidate deep-holes corresponding to $\vv{n}_I$ with $I \in \mathcal{I}$ and with intersections of the Voronoi diagram with the zonotope. The precise nature of the candidate deep-hole depends on the location of $\vv{n}_I$ with respect to the convex hull, $\ch(I)$, and zonotope $Z$. For $\vv{n}_i \in \ch(I)$, let
	\begin{enumerate}
	\item $\vv{n}_I \in \ch(I)$. Let $\vv{h}_{I} = \vv{n}_I$ be the candidate deep-hole.
	\item $\vv{n}_I \notin \ch(I)$, but $\vv{n}_I \in Z$. $\vv{n}_{I}$ does generate a candidate deep-hole. 
	\end{enumerate}
	Let this set $\vv{h}_{I}$ be $H_1$. Let $H_2$ be the set  
	\[\{\vv{c}: \vv{c} \mbox{ is the unique intersection point of a Voronoi face and a face of the zonotope}\}.\]
	Let $C = H_1 \cup H_2$ be this collection of candidate deep-holes. \footnote{Originally we considered the third group of candidate deep-holes for $\vv{n}_{I} \notin Z$. We will find a point in $Z$ which will serve as the candidate deep-hole for $\vv{n}_{I}$. Each $J \subset I$ of size $k \leq d$ has an associated edge $N(J)$ of the Voronoi diagram with affine dimension $d-k+1$ and $\vv{n}_{I}$ on its boundary (it is possible that $N(J)$ is unbounded). For all edges $N(J)$ which intersect a face of $Z$ with co-dimension $d-k+1$, let $\vv{h}_J$ be their intersection. Let $\norm{\vv{h}_J - \vv{v}_j}_{2} = r_J$ for all points $\vv{v}_j$ of $J$. Let the candidate deep-hole for $\vv{n}_{I}$ be the $\vv{h}_{J}$ associated with the largest $r_{J}$. But honestly this is more trouble than it is worth.} Observe that $|C| \leq N$. We claim that $r^* = \max_{\vv{h}_{I} \in C} r_{I} = \lindisc(\mm{A})$ with deep-hole $\vv{h}^* = \argmax_{\vv{h}_{I} \in C} r_{I}$. The algorithm runs in time 
	\[O\left((2n\delta)^{2d} + 2d(2n\delta)^{2d}\log(2n\delta) + 2d(2n\delta)^{d^2} + (2n\delta)^{2d}\right) \in O\left(d(2n\delta)^{d^2}\right).\]
	
	\begin{lemma}{\textup(Some Candidate Deep-hole is a Deep-hole.)}
	\label{lem:some-candidate-deephole-is-deep}
	Let $\mm{A} \in \ZZ^{d \times n}$. If $\lindisc(\mm{A}) = \ell$, then there exists a point $\vv{h}'$ among the candidate deep-holes generated by the algorithm such that $\lindisc(\mm{A}, \vv{h}') \geq \ell$.
	\end{lemma}
	\begin{proof}
	Consider a deep-hole $\vv{h}$ of $\mm{A}$ i.e. $\lindisc_{2}(\mm{A}, \vv{h}) = \ell$. Let $P = \{\vv{p}_1,..., \vv{p}_k\}$ be the set of points on the boundary of the radius $\ell$ ball centered at $\vv{h}$. If $\vv{h}$ does not already coincide with a candidate deep-hole, then we show that it is possible to perturb $\vv{h}$ and find another deep-hole which does. Consider the two settings: (1) $\vv{h}$ is in the interior of $Z$ and (2) $\vv{h}$ is on the boundary of $Z$. For vectors $\vv{v}, \vv{x} \in \RR^{d}$ and function $f: \RR^{d} \rightarrow \RR$, let $D_{\vv{v}}(f)(\vv{x}) = \ip{\vv{v}}{(\nabla f)(\vv{x})}$ be the directional derivative of $f$ along $\vv{v}$. 
	
	In the first case we will show that some deep-hole coincides with a Voronoi intersection point in the interior of the convex hull of $P$, denoted $\ch(P)$, i.e. candidate deep-hole set $H_1$ in the algorithm. Suppose $\vv{h}$ does not coincide with a Voronoi intersection point in the interior of the convex hull of $P$, then there exists a hyper-plane $F$ which ``separates'' $\vv{h}$ from the points of $P$ i.e. all points of $P$ are either on $F$ or on one side of $F$ and $\vv{h}$ is either on $F$ or on the other side of $F$. Let $\vv{v}$ be the normal of $F$. Then $\ip{\vv{v}}{\vv{p}_i - \vv{h}} \leq 0$ for all $\vv{p}_i \in P$. Define the distance function $f_{\vv{p}_i}(\vv{x}) = \norm{\vv{p}_i - \vv{x}}_2$ for each $\vv{p}_i \in P$. Observe that 
	\[(\nabla f_{\vv{p}_i})(\vv{x}) = \frac{\vv{p}_i - \vv{x}}{f_{\vv{p}_i}(\vv{x})}\]
	where the denominator is a positive constant if $\vv{x} \neq \vv{p}_i$. Together, we have 
	\[\left(D_{-\vv{v}}f_{\vv{p}_i}\right)(\vv{h}) = \ip{-\vv{v}}{\nabla f_{\vv{p}_i}(\vv{h})} = \frac{-1}{f_{\vv{p}_i}(\vv{\vv{h}})}\ip{\vv{v}}{\vv{p}_i - \vv{h}} \geq 0.\]
	Thus by taking $\vv{h}' = \vv{h} - \epsilon \vv{v}$ for $\epsilon > 0$ we will find another deep-hole of $\vv{A}$ either on the boundary of the zonotope or at a Voronoi intersection point in the interior of $\ch(P)$.
	
	In the second case we will show that a deep-hole coincides a unique intersection of a Voronoi face and a face of $Z$ i.e. candidate deep-hole set $H_2$ in the algorithm. Suppose $\vv{h}$ is on the boundary but not such a point. Further, let $\vv{h}$ be in the relative interior of some $(d-k)$-dimensional face $F$ of $Z$ and $V$ be the Voronoi face of $P$. Suppose $\vv{h}$ is not the unique intersection of $F$ and $V$ i.e. $\dim(F \cap V) \geq 1$. Let $\vv{v} \in F \cap V$. We will show that $\sign(D_{\vv{v}}f_{\vv{p}_i}(\vv{h})) = \sign(D_{\vv{v}}f_{\vv{p}_j}(\vv{h}))$ for all $\vv{p}_i, \vv{p}_j \in P$ --- in the case when $D_{\vv{v}}f_{\vv{p}}(\vv{h}) = 0$ we say that $\sign(D_{\vv{v}}f_{\vv{p}}(\vv{h}))$ is both positive and negative and matches $\sign(D_{\vv{v}}f_{\vv{p}_i}(\vv{h}))$ for all $i$. Suppose there exists $i,j$ such that  $D_{\vv{v}}f_{\vv{p}_i}(\vv{h}) < 0$ and $D_{\vv{v}}f_{\vv{p}_j}(\vv{h}) > 0$. Then for $\epsilon > 0$ sufficiently small,
	\[f_{\vv{p}_i}(\vv{h} + \epsilon\vv{v}) < f_{\vv{p}_i}(\vv{h}) \mbox{ and }f_{\vv{p}_j}(\vv{h} + \epsilon\vv{v}) > f_{\vv{p}_j}(\vv{h}).\]
	Since $\vv{h}, \vv{v} \in F \cap V$, we have $\vv{h} + \epsilon\vv{v} \in F \cap V$ by convexity. Thus $\vv{h} + \epsilon\vv{v}$ is on the Voronoi face between all points in $P$. In particular $f_{\vv{p}_i}(\vv{h} + \epsilon\vv{v}) = f_{\vv{p}_j}(\vv{h} + \epsilon\vv{v})$ which is impossible. Thus by taking $\vv{h}' = \vv{h} + \epsilon\vv{v}$ or $\vv{h}' = \vv{h} - \epsilon\vv{v}$ we get another deep-hole.
	\end{proof}	
	\end{comment}
	
	\subsubsection{Largest Enclosing Circle Problem Extension}
	Let $V$ be a set of $n$ points in the plane and let $\ch(V)$ denote the convex hull of $V$. The largest empty circle problem, denoted LEC, takes $V$ and outputs both a radius $r$ and point $x^* \in \ch(V)$ such that the circle centered at $x^*$ with radius $r$ is the largest empty circle not containing any point of $V$. In the following we presents an algorithm which solves the LEC problem with points in $\RR^{d}$ distances induced by both $\metric{2}$- and $\metric{\infty}$-norms.
	
	\begin{lemma}{\textup{(LEC in Higher Dimensions.)}}
		\label{lem:lec-in-higher-dimensions}
		Let $V$ be a set of $n$ points in $\RR^{d}$ for some fixed constant $d$. The LEC of $V$, in both $\metric{2}$- and $\metric{\infty}$-norms, can be computed in time $O(n^{d})$.
	\end{lemma}
	\begin{proof}
		The algorithm of Toussaint \cite{toussaint1983computing} computes the LEC of $n$ points $V$ in the plane with respect to the $\metric{2}$-norm as follows, 
		\begin{enumerate}
			\item Compute the Voronoi diagram of $V$, denoted $\mathcal{V}$. Note that $\mathcal{V}$ is the union of Voronoi faces of dimension $k$, denoted $\mathrm{vd}_{k}(V)$ for all $k = 0, ..., d-1$. 
			\item Compute the convex hull of $V$, denoted $\ch(V)$. Let $h$ be the number of facets of $\ch(V)$. 
			\item Preprocess the points of $\ch(V)$ so that queries of the form ``Is a point $x$ in $\ch(V)$?'' can be answered in time $O(\log h)$. For every $v \in \mathrm{vd}_{0}(V)$, determine if $v \in \ch(V)$. Let $C_1 = \{v \in \mathrm{vd}_{0}(V): v \in \ch(V)\}$.
			\item Determine the intersection points of $\mathrm{vd}_{k}(V)$ with faces of $\ch(V)$ of co-dimension $k$. Let $C_2$ be the set of all such intersection points.
			\item For all points $v \in C_1 \cup C_2$, find the largest empty circle centered at $v$. Output a $v$ which maximizes this radius.
		\end{enumerate}
		We find the analogue of each step for points in $\RR^{d}$ with respect to $\metric{2}$-norms and then generalize to $\metric{\infty}$-norms.
		
		In the following let $N = n^{\ceil{d/2}}$. The complexity, i.e. total number of faces of every dimension, of the $\metric{2}$-Voronoi diagram in $\RR^{d}$ for fixed $d$ is $O(N)$ and can be computed in time $O(N + n \log n)$ by a classic result of Chazelle \cite{chazelle1993optimal}. The complexity of $\ch(V)$ is $O(2^d N)$ and can also be computed in time $O(N + n\log n)$.
		
		To determine the set $C_1$ of Voronoi intersection points inside the convex hull, we consider the dual problem. Without loss of generality, assume that the origin is strictly inside $\ch(V)$. For a half-plane $H$ not containing the origin, let $H^-$ be the half-space with $H$ as its boundary containing the origin. Let $\ch(V)$ be the intersection of half-spaces $H^- \in \mathcal{H}$. The dual of each hyperplane $H$ is a point denoted $D_0(H)$. Similarly, the dual of each point $\vv{v} \in \RR^{d}$ is a hyperplane, denoted $D_0(\vv{v})$. If $\vv{v} \in H^{-}$, then $D_0(H) \in D_0(\vv{v})^{-}$. A point inside $\ch(V)$ must be in $H^{-}$ for every $H \in \mathcal{H}$, thus $D_0(\vv{v})^{-}$ must contain every $D_0(H)$. For each Voronoi intersection point $\vv{v}$ answering ``Is $\vv{v} \in \ch(V)$?'' can be done in time $O(N)$. Since there are at most $O(N)$ Voronoi intersection points, we can find $C_1$ in time $O(N^2)$. 
		
		To determine the set $C_2$ of all intersection points of $k$-faces of $\mathrm{vd}_k(V)$ and faces of $\ch(V)$ of co-dimension $k$ will require solving several linear systems. Note that each face $F$ in $\mathrm{vd}_{k}(V)$ is the set of vectors $\vv{x}$ which satisfy exactly $d-k$ equality constraints $\ip{\vv{a}_1}{\vv{x}} = b_1, \ip{\vv{a}_k}{\vv{x}} = b_k$ for vectors $\vv{a}_1, ... \vv{a}_k \in \RR^{d}$. Since there are at most $O(2^dN)$ faces of $\ch(V)$, there are at most that many faces of $\ch(V)$ of co-dimension $k$. By solving a system of linear equations consisting of $d-k$ equalities defining a Voronoi face $F$ and $k$ constraints defining a $\ch(V)$ face $G$ for every pair of $F$ and $G$, we find all points in $C_2$. Thus both the time to compute $C_2$ and the size of $C_2$ is bounded above by $O(N \cdot 2^d N) = O(N^2)$.
		
		In total there are at most $O(N + N^2)$ points in $C_1 \cup C_2$ which were computed in time $O(2N^2)$. Thus the solving the largest empty circle problem in dimension $d$ for some constant $d$ takes time $O(n^{d})$. 
		
		Next we consider the largest empty circle problem in $\metric{\infty}$-norm. The convex hull remains the same, so we just have to consider the Voronoi diagram with respect to the $\metric{\infty}$-norm. Again, constructing the Voronoi diagram can be done in expected time $O(n^{\ceil{d/2}}\log^{d-1}n)$ using a randomized algorithm of Boissonnat et al.  \cite{DBLP:journals/dcg/BoissonnatSTY98}. Next we consider the number of intersections between the Voronoi diagram and the convex hull. First note that for Voronoi diagrams with respect to the $\metric{\infty}$-norm need not consist of only hyperplanes and their intersections. Indeed, in  $\RR^{d}$, for two points with the same $y$-coordinate, there exists regions with affine dimension $2$ which are equidistant to both points. To remedy this, we assume that the points $V$ are in general position. It remains to consider the complexity of each bisector in $\metric{\infty}$-norm. By Claim \ref{claim:bound-facet-linf-bisector}, in constant dimension $d$, each such bisector can have at most $2^{d}-1$ facets. Thus the bounds of the $\metric{2}$-norm algorithm still holds when replacing the complexity of the Voronoi diagram with $O(2^{d}n^{\ceil{d/2}})$.
	\end{proof}

	\begin{claim}{\textup{(Bound on Number of Facets of $\metric{\infty}$ Bisectors.)}}
		\label{claim:bound-facet-linf-bisector}
		Let $\vv{u}, \vv{v} \in \RR^{d}$ in general position, then the bisector, with respect to $\metric{\infty}$-norm, has at most $2^{d}-1$ facets.
	\end{claim}
	\begin{proof}
		Each hyperplane specifies a subset of coordinates which are tight for $\vv{u}$ and $\vv{v}$. Since there are $2^{d}-1$ subsets (not including the empty set), there can be at most this many hyperplanes.
	\end{proof}
		
	\subsection{Poly-time Approximation Algorithm}
	Next, we prove Theorem \ref{thm:approx} present a $2^n$-approximation algorithm for linear discrepancy. Note that 
	\[\lindisc(\mm{A}) \leq \max_{\vv{w} \in [0,1]^n} \norm{\mm{A}(\mm{w} - \round(\mm{w}))}_{\infty} = \frac{1}{2} \max_{\vv{z} \in [-1,1]^n}\norm{\mm{A}\vv{z}}_{\infty} = \frac{1}{2}\norm{\mm{A}}_{\infty \rightarrow \infty}.\]
	
	To bound $\lindisc(\mm{A})$ from below, we show that $\norm{\mm{A}}_{\infty \rightarrow \infty} \leq O(2^n) \cdot \lindisc(\mm{A})$. Let us try to interpret this statement. Note that $\norm{\mm{A}\vv{z}}_{\infty}$ is equal to the Minkowski $\polytope{P}$-norm $\norm{\vv{z}}_{\polytope{P}}$ for $\polytope{P} = \{\vv{x}: \norm{\mm{A}\vv{x}}_{\infty} \leq 1\}$ i.e. $\norm{\vv{z}}_{\polytope{P}} = \inf\{t \geq 0: \vv{z} \in t\polytope{P}\}$ so
	\[\norm{\mm{A}}_{\infty \rightarrow \infty} = \max_{\vv{z} \in [-1, 1]^n} \norm{\mm{A}\vv{z}}_{\infty} = \max_{\vv{z} \in [-1, 1]^n} \norm{\vv{z}}_{\polytope{P}}.\]
	By interpreting $\vv{z}$ as the difference of two vectors $\vv{x}, \vv{x}' \in [0,1]^n$ we have that 
	\[\norm{\mm{A}}_{\infty \rightarrow \infty} = \max_{\vv{z} \in [-1,1]^n} \norm{\vv{z}}_{\polytope{P}} = \max_{\vv{x}, \vv{x}' \in [0,1]^n} \norm{\vv{x} - \vv{x}'}_{\polytope{P}} = \diam_{\polytope{P}}\left([0,1]^n\right).\]
	Recall that $\lindisc(\mm{A})$ is the scaling of $\polytope{P}$ that covers $[0,1]^n$ when $2^n$ copies of $\polytope{P}$ are placed at the verticies $\vv{x} \in \{0,1\}^n$. Here we just need to show that the diameter of the unit hyper-cube with Minkowski $\polytope{P}$-norm is no more than this scale-factor times $O(2^n)$. 
	
	We generalize this as the following.
	\begin{lemma}
		\label{lem:approx-algorithm}
		Let $\polytope{K}$ be a convex symmetric polytope and $S \subset \RR^n$ be convex. Suppose there exist $N$ elements $x_1, ..., x_N \in S$ such that 
		\[S \subset \bigcup_{x_i} x_i + t\polytope{K}.\]
		Then $\diam_K(S) = \max_{x, x' \in S} \norm{x - x'}_{K} \leq O(N) \cdot t$.
	\end{lemma}
	\begin{proof}
		Fix any two points $x$ and $x'$ in $S$. Let $\polytope{P}_i$ be the polytope $x_i + t\polytope{K}$. Since $S$ is convex, the line segment $\lambda x + (1 - \lambda)x'$ for $\lambda \in [0,1]$ is in $S$. Therefore $\lambda x + (1 - \lambda)x'$ intersects a sequence of polytopes $\polytope{P}_{k_1}, ..., \polytope{P}_{k_r}$ with centres $x_{k_1}, ..., x_{k_r}$. Since the polytopes are convex, they can appear in the sequence at most once. By the triangle inequality we have
		\begin{align*}
			\norm{x - x'}_K 
			&= \norm{(x - x_{k_1}) + (x_{k_1} - x_{k_2}) + \cdots + (x_{k_r} - x')}_{K}\\ 
			&\leq \norm{x - x_{k_1}}_{K} + \norm{x_{k_1} - x_{k_2}}_{K} + \cdots + \norm{x_{k_r} - x'}_{K}\\
			&\leq t + 2t(N-1) + t = 2tN
		\end{align*}
		where the last inequality follows as $x \in \polytope{P}_{k_1}$, $x' \in \polytope{P}_{k_r}$, and $\norm{x_{k_i} - x_{k_{i+1}}}_K \leq 2t$.
		\begin{comment}
		Just for completeness, we show that $\norm{x_{k_i} - x_{k_{i+1}}}_K \leq 2t$. Since $P_{k_i}$ and $P_{k_{i+1}}$ are consecutive in the sequence of polytopes that $\lambda x + (1 - \lambda) x'$ intersect, $P_{k_i} \cap P_{k_i} \neq \emptyset$. Let $y$ be a point in their intersection. Then 
		\[\norm{x_{k_i} - x_{k_{i+1}}}_K \leq \norm{x_{k_i} - y}_{K} + \norm{y - x_{k_{i+1}}}_K \leq 2t.\]
		\end{comment}
	\end{proof}
	
	\begin{proof}[Proof of Theorem \ref{thm:approx}]
		In Lemma \ref{lem:approx-algorithm}, set $\polytope{K}$ to be the parallelepiped defined by $\mm{A}$, $S = [0,1]^n$, $t = \lindisc(\mm{A})$, and $\{x_1, ..., x_N\} = \{0,1\}^{n}$. 
	\end{proof} 
